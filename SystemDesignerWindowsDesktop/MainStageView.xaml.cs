﻿using System.Windows.Controls;
using System.Windows.Input;

namespace SystemDesignerWindowsDesktop {
    /// <summary>
    /// Description for MainStageView.
    /// </summary>
    public partial class MainStageView : UserControl {
        /// <summary>
        /// Initializes a new instance of the MainStageView class.
        /// </summary>
        public MainStageView() {
            InitializeComponent();
        }
    }
}