﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using SystemDesigner.Draw;
using DisplayCore.Annotations;

namespace SystemDesignerWindowsDesktop.Model
{

    /// <summary>
    /// Describes parameters for testing a neural network
    /// </summary>
    [XmlType("NeuralTestPack")]
    public class NeuralTestPack : INotifyPropertyChanged
    {
        private int _pictureResolution = 16;
        private string _name;
        private double _errorToStopAt = 0.01;
        private int _epochToStopAt = 100;

        [XmlArray("HiddenLayer")]
        [XmlArrayItem("Layer", Type = typeof(IntProxy))]
        public ObservableCollection<IntProxy> HiddenLayer { get; set; }

        

        [XmlIgnore]
        public int AmountInputs => PictureResolution * PictureResolution;

        [XmlAttribute("PictureResolution")]
        public int PictureResolution
        {
            get => _pictureResolution;
            set
            {
                _pictureResolution = value; 
                OnPropertyChanged();
                OnPropertyChanged(nameof(AmountInputs));
            }
        }

        [XmlAttribute("SelectedNetwork")]
        public string SelectedNetwork { get; set; }

        [XmlArray("TestShapes")]
        [XmlArrayItem("Drawing", Type = typeof(Drawing))]
        public ObservableCollection<Drawing> TestShapes { get; set; }

        [XmlArray("TrainingShapes")]
        [XmlArrayItem("Drawing", Type = typeof(Drawing))]
        public ObservableCollection<Drawing> TrainingShapes { get; set; }

        [XmlAttribute("LearningRate")]
        public double LearningRate { get; set; } = 0.03;

        [XmlIgnore]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value; 
                OnPropertyChanged();
            }
        }

        [XmlAttribute("ErrorToStopAt")]
        public double ErrorToStopAt
        {
            get { return _errorToStopAt; }
            set
            {
                _errorToStopAt = value; 
                OnPropertyChanged();
            }
        }

        [XmlAttribute("EpochToStopAt")]
        public int EpochToStopAt
        {
            get { return _epochToStopAt; }
            set
            {
                _epochToStopAt = value; 
                OnPropertyChanged();
            }
        }

        #region TestResults

        [XmlIgnore]
        public TimeSpan ElapsedTimeConverting { get; set; }
        [XmlIgnore]
        public TimeSpan ElapsedTimeTraining { get; set; }
        [XmlIgnore]
        public int EpochStoppedAt { get; set; }
        [XmlIgnore]
        public double FinalError { get; set; }
        [XmlIgnore]
        public string HiddenLayerString => string.Join(",", HiddenLayer);
        [XmlIgnore]
        public int CorrectTests { get; set; }

        #endregion



        public override string ToString()
        {
            return
                $"{Name}: Learning rate: {LearningRate} Max. epoch: {EpochToStopAt} Min.Error: {ErrorToStopAt} Hidden layer: {HiddenLayerString} Picture resolution: {PictureResolution}";
        }

        public NeuralTestPack()
        {
            HiddenLayer = new ObservableCollection<IntProxy>();
            TestShapes = new ObservableCollection<Drawing>();
            TrainingShapes = new ObservableCollection<Drawing>();
            SelectedNetwork = "System Designer";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
