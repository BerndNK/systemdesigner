﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using DisplayCore.Annotations;

namespace SystemDesignerWindowsDesktop.Model
{
    /// <summary>
    /// Proxy for int, to bind the value in XAML.
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    [XmlType("Int")]
    public sealed class IntProxy : INotifyPropertyChanged
    {
        private int _value;

        [XmlAttribute("Value")]
        public int Value
        {
            get => _value;
            set
            {
                _value = value; 
                OnPropertyChanged();
            }
        }

        public IntProxy(){}

        public IntProxy(int value)
        {
            Value = value;
        }

        public static implicit operator int(IntProxy value) => value.Value;

        public static implicit operator IntProxy(int value) => new IntProxy(value);

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString() => Value.ToString();
    }
}