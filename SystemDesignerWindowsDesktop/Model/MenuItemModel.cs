﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using DisplayCore.Annotations;

namespace SystemDesignerWindowsDesktop.Model
{
    /// <summary>
    /// Model for the menu items on the application.
    /// </summary>
    public class MenuItemModel : INotifyPropertyChanged
    {
        private bool _enabled = true;
        private BindingList<MenuItemModel> _items;
        private ICommand _command;
        private string _shortcut;
        private string _iconPath;
        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                _name = value; 
                OnPropertyChanged();
            }
        } // name of this item

        public string IconPath
        {
            get => _iconPath;
            set { _iconPath = value;
                OnPropertyChanged();
            }
        } // path to icon that shall be displayed for this item

        public string Shortcut
        {
            get => _shortcut;
            set { _shortcut = value;
                OnPropertyChanged();
            }
        } // the shortcut string that shall be displayed

        public ICommand Command
        {
            get => _command;
            set { _command = value;
                OnPropertyChanged();
            }
        } // the command that shall be executed when the user clicks on this item

        public BindingList<MenuItemModel> Items
        {
            get => _items;
            set { _items = value;
                OnPropertyChanged();
            }
        } // children objects of this item

        public bool Enabled
        {
            get => _enabled;
            set { _enabled = value;
                OnPropertyChanged();
            }
        } // whether the item may be clicked on or not

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuItemModel"/> class.
        /// </summary>
        public MenuItemModel()
        {
            Items = new BindingList<MenuItemModel>();
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
