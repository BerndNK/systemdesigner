﻿using System;
using SystemDesigner.Stage;

namespace SystemDesignerWindowsDesktop.Model {
    public class DataService : IDataService {
        public void GetMainStageHandler(Action<SvgStageHandler, Exception> callback) {
            var item = new SvgStageHandler();
            callback(item, null);
        }
    }
}