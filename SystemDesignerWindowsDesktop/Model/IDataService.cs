﻿using System;
using SystemDesigner.Stage;

namespace SystemDesignerWindowsDesktop.Model {
    public interface IDataService {
        /// <summary>
        /// Gets the main stage handler.
        /// </summary>
        /// <param name="callback">The callback.</param>
        void GetMainStageHandler(Action<SvgStageHandler, Exception> callback);
    }
}
