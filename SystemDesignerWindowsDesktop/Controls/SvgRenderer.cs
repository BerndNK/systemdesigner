﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SystemDesigner.Stage;
using SystemDesignerWindowsDesktop.Extensions;
using SystemDesignerWindowsDesktop.ViewModel;
using DisplayCore.Render;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms;
using Point = DisplayCore.Render.Geometry.Point;
using Rectangle = DisplayCore.Render.Geometry.Rectangle;

namespace SystemDesignerWindowsDesktop.Controls
{
    /// <summary>
    /// Provides a IRenderer implementation to render Svg items
    /// </summary>
    /// <seealso cref="System.Windows.Controls.Control" />
    /// <seealso cref="DisplayCore.Render.IRenderer" />
    [TemplatePart(Name = "PART_Canvas", Type = typeof(Canvas))]
    public class SvgRenderer : Control, IRenderer
    {
        #region DependencyProperties

        public static readonly DependencyProperty SvgStageProperty = DependencyProperty.Register(
            "SvgStage", typeof(IStage<ISvgItem>), typeof(SvgRenderer), new PropertyMetadata(default(IStage<ISvgItem>), PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var svgRenderer = dependencyObject as SvgRenderer;
            if (svgRenderer == null) return;
            switch (dependencyPropertyChangedEventArgs.Property.Name)
            {
                case nameof(SvgStage):
                    svgRenderer.OnSvgStageChanged(dependencyPropertyChangedEventArgs);
                    break;
                default:
                    Debug.Print("PropertyChangedCallback was triggered for unknown dependencyProperty: " + dependencyPropertyChangedEventArgs.Property);
                    return;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the SVG stage.
        /// </summary>
        /// <value>
        /// The SVG stage.
        /// </value>
        public IStage<ISvgItem> SvgStage {
            get { return (IStage<ISvgItem>)GetValue(SvgStageProperty); }
            set { SetValue(SvgStageProperty, value); }
        }

        #endregion

        #region Fields

        /// <summary>
        /// The canvas used to render all items
        /// </summary>
        private Canvas _canvas;

        /// <summary>
        /// The renderer handler
        /// </summary>
        private readonly RendererHandler _rendererHandler;

        /// <summary>
        /// Index used to track whether the order of rendering has changed. (So an item moved in front or back another items, or an item has been deleted)
        /// </summary>
        private int _currentExpectedChildIndex;

        /// <summary>
        /// Gets or sets the canvas children map. This is used to just apply property updates, instead of removing/adding new display objects each Render call
        /// </summary>
        /// <value>
        /// The canvas children map.
        /// </value>
        private Dictionary<int, UIElement> _canvasChildrenMap;

        /// <summary>
        /// The display objects that were removed in order to fix the rendering order.
        /// </summary>
        private List<int> _removedChildren;

        /// <summary>
        /// The expected rendering order of the various item id's. (This represents in which order display objects was last rendered)
        /// </summary>
        private List<int> _expectedRenderingOrder;

        /// <summary>
        /// Whether the rendering order has changed.
        /// </summary>
        private bool _renderingOrderHasChanged;
        #endregion

        /// <summary>
        /// Initializes the <see cref="SvgRenderer"/> class.
        /// </summary>
        static SvgRenderer()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SvgRenderer), new FrameworkPropertyMetadata(typeof(SvgRenderer)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SvgRenderer"/> class.
        /// </summary>
        public SvgRenderer()
        {
            _rendererHandler = new RendererHandler { Renderer = this };

            _canvasChildrenMap = new Dictionary<int, UIElement>();
            _removedChildren = new List<int>();
            _expectedRenderingOrder = new List<int>();
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= OnLoaded;

            // _rendererHandler.Render(SvgStage);
        }


        /// <summary>
        /// Called when [SVG stage changed].
        /// </summary>
        /// <param name="dependencyPropertyChangedEventArgs"></param>
        private void OnSvgStageChanged(DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            // remove event handler from old stage
            var oldStage = (dependencyPropertyChangedEventArgs.OldValue as SvgStage);
            if (oldStage?.Root != null) oldStage.Root.ChildrenChanged -= OnStageHasChanged;

            var newRoot = SvgStage?.Root;
            if (newRoot != null) newRoot.ChildrenChanged += OnStageHasChanged;

            // _rendererHandler.Render(SvgStage);
        }

        /// <summary>
        /// Called when [stage has changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnStageHasChanged(object sender, EventArgs eventArgs)
        {
            //_rendererHandler.Render(SvgStage);
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes call <see cref="M:System.Windows.FrameworkElement.ApplyTemplate" />.
        /// </summary>
        public override void OnApplyTemplate()
        {
            _canvas = GetTemplateChild("PART_Canvas") as Canvas;

            CompositionTarget.Rendering += CompositionTargetOnRendering;

            base.OnApplyTemplate();
        }

        private void CompositionTargetOnRendering(object sender, EventArgs eventArgs)
        {
            _rendererHandler?.Render(SvgStage);
        }

        #region VariousRenderer

        private void Render<T>(SvgItem target, Rectangle renderBoundingBox, double rotation, Point rotationPoint) where T : Shape, new()
        {

            UIElement existingElement;
            T item;

            // check if the rectangle is already in the canvas
            if (_canvasChildrenMap.TryGetValue(target.Id, out existingElement))
            {
                // if so, cast it
                item = existingElement as T;
                if (item == null) throw new Exception("UIElement in Canvas map was not expected type. This should NEVER happen.");
            }
            else
            {
                // if not, create new rectangle and add it with the id
                item = new T();
                _canvasChildrenMap.Add(target.Id, item);

                _canvas.Children.Add(item);
            }

            // width and height of the bounding box matches the size of the drawn rectangle
            if(existingElement == null || Math.Abs(item.Width - renderBoundingBox.Width) > 0.0001)
                item.Width = renderBoundingBox.Width;

            if(existingElement == null || Math.Abs(item.Height - renderBoundingBox.Height) > 0.0001)
                item.Height = renderBoundingBox.Height;
            
            item.Fill = new SolidColorBrush(target.FillColor.ToWindowsColor());
            item.StrokeThickness = target.LineThickness;
            item.Stroke = new SolidColorBrush(target.LineColor.ToWindowsColor());
            item.Opacity = target.Alpha;
            item.Visibility = Visibility.Visible;
            Canvas.SetLeft(item, renderBoundingBox.X);
            Canvas.SetTop(item, renderBoundingBox.Y);
            
            
            var rotateTransform = new RotateTransform(rotation / Math.PI * 180, rotationPoint.X, rotationPoint.Y);
            if (!item.RenderTransform.Value.ValueEquals(rotateTransform.Value))
                item.RenderTransform = rotateTransform;
        }

        /// <summary>
        /// In line implementation for rendering a SvgLine. Used in Render
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void RenderLine(SvgLine svgRect, Rectangle renderBoundingBox, double rotation, Point rotationPoint)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// In-line implementation for rendering a SvgPolyline. Used in Render
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void RenderPolyline(SvgPolyline target, Rectangle renderBoundingBox, double rotation, Point rotationPoint)
        {
            UIElement existingElement;
            Polyline polyline;


            // check if the rectangle is already in the canvas
            if (_canvasChildrenMap.TryGetValue(target.Id, out existingElement))
            {
                // if so, cast it
                polyline = existingElement as Polyline;
                if (polyline == null) throw new Exception("UIElement in Canvas map was not expected type. This should NEVER happen.");
            }
            else
            {
                // if not, create new rectangle and add it with the id
                polyline = new Polyline();
                _canvasChildrenMap.Add(target.Id, polyline);

                _canvas.Children.Add(polyline);
            }
            
            // set the points if they have changed
            if (target.HasChanged)
            {
                polyline.Points = new PointCollection(from point in target
                                                      select new System.Windows.Point(point.X, point.Y));
                polyline.Points.Add(polyline.Points.First());
                // set the flag, that we rendered this state of the polyline
                target.HasChanged = false;
            }

            polyline.StrokeThickness = target.StrokeThickness;
            polyline.Stroke = new SolidColorBrush(target.FillColor.ToWindowsColor());

            polyline.Opacity = target.Alpha;
            polyline.Visibility = Visibility.Visible;
            Canvas.SetLeft(polyline, renderBoundingBox.X);
            Canvas.SetTop(polyline, renderBoundingBox.Y);
            // use scale transform to stretch the polyline
            var transformGroup = new TransformGroup();
            var scaleTransform = new ScaleTransform(renderBoundingBox.Width / target.LineDimensionsWidth, renderBoundingBox.Height / target.LineDimensionsHeight);
            var roateTransform = new RotateTransform(rotation / Math.PI * 180, rotationPoint.X, rotationPoint.Y);

            transformGroup.Children.Add(scaleTransform);
            transformGroup.Children.Add(roateTransform);

            polyline.RenderTransform = transformGroup;
        }

        private void RenderImage(SvgImage target, Rectangle renderBoundingBox, double rotation, Point rotationPoint)
        {
            UIElement existingElement;
            Image image;


            // check if the rectangle is already in the canvas
            if (_canvasChildrenMap.TryGetValue(target.Id, out existingElement))
            {
                // if so, cast it
                image = existingElement as Image;
                if (image == null) throw new Exception("UIElement in Canvas map was not expected type. This should NEVER happen.");
            }
            else
            {
                // if not, create new rectangle and add it with the id
                image = new Image();
                _canvasChildrenMap.Add(target.Id, image);

                _canvas.Children.Add(image);
            }


            // width and height of the bounding box matches the size of the drawn rectangle
            image.Width = renderBoundingBox.Width;
            image.Height = renderBoundingBox.Height;
            // set the image source
            var bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri("Assets/" + target.ImageSource, UriKind.Relative);
            bitmap.EndInit();
            image.Source = bitmap;

            image.Opacity = target.Alpha;
            image.Visibility = Visibility.Visible;
            Canvas.SetLeft(image, renderBoundingBox.X);
            Canvas.SetTop(image, renderBoundingBox.Y);
            image.RenderTransform = new RotateTransform(rotation / Math.PI * 180, rotationPoint.X, rotationPoint.Y);
        }


        #endregion

        #region ISvgRenderer Implementation

        /// <summary>
        /// Notifies the Renderer that a batch call of the method Render is imminent and therefore is told to prepare to render process
        /// </summary>
        public void RenderBegin()
        {
            // reset the expected rendering index, removed children and flags
            _currentExpectedChildIndex = 0;
            _renderingOrderHasChanged = false;
            _removedChildren.Clear();
        }

        /// <summary>
        /// Notifies the Renderer that the render procedure has finished and therefore is told to clean up after the rendering
        /// </summary>
        public void RenderEnd()
        {
            // remove all cached items, that did not get rendered this cycle (which means they have been deleted from the rendering tree)
            if (_renderingOrderHasChanged)
            {
                // get all ids that have been cached
                var allCachedIds = _canvasChildrenMap.Keys.ToArray();
                // now only select the id's which have not been rendered this cycle
                var idsThatHaveNotBeenRendered = allCachedIds.Where(x => !_expectedRenderingOrder.Contains(x));

                // expectedRenderingOrder contains every ID that has been rendered
                foreach (var id in idsThatHaveNotBeenRendered)
                {
                    // only remove the Id's from the dictionary. The canvas doesn't contain them anymore, since they have already been removed in the Render method.
                    if (_canvasChildrenMap.ContainsKey(id)) _canvasChildrenMap.Remove(id);
                }
            }
        }

        /// <summary>
        /// Render function that renders the target
        /// </summary>
        /// <param name="target">SvgItem to draw</param>
        /// <param name="renderBoundingBox">Bounding box for the target</param>
        /// <param name="rotation">Rotation of the target</param>
        /// <param name="rotationPoint">The rotation point.</param>
        /// <exception cref="ArgumentNullException">Thrown when target is null</exception>
        /// <exception cref="ArgumentException">Thrown when target has unknown type, or renderBoundingBox is not valid</exception>
        public void Render(IRenderable target, Rectangle renderBoundingBox, double rotation, Point rotationPoint, Rectangle rotatedBoundingBox, double alpha)
        {
            // include guards
            if (target == null) throw new ArgumentNullException(nameof(target));
            if (_canvas == null) return; // template has not been applied yet
            if (renderBoundingBox.Width < 0 || renderBoundingBox.Height < 0) throw new ArgumentException(@"RenderBoundingBox is invalid", nameof(renderBoundingBox));

            UIElement existingElement;

            if (_currentExpectedChildIndex >= _expectedRenderingOrder.Count)
                _renderingOrderHasChanged = true;


            //_canvasChildrenMap.TryGetValue(target.Id, out existingElement);
            
            // check if the expected next id matches
            if (!_renderingOrderHasChanged && _expectedRenderingOrder[_currentExpectedChildIndex] != target.Id)
            {
                // if not, this means that the rendering order has changed. Remove all items from this point on from the rendering tree.
                for (var i = _currentExpectedChildIndex; i < _expectedRenderingOrder.Count; i++)
                {
                    if (_canvasChildrenMap.TryGetValue(_expectedRenderingOrder[i], out existingElement))
                    {
                        _canvas.Children.Remove(existingElement); // remove the child from the rendering tree (of the canvas)
                        _removedChildren.Add(_expectedRenderingOrder[i]); // add the removed id (so we can restore it later)
                    }

                }

                _expectedRenderingOrder.RemoveRange(_currentExpectedChildIndex, _expectedRenderingOrder.Count - _currentExpectedChildIndex);
                _renderingOrderHasChanged = true;
            }



            // check if a element already is rendered for the id
            _canvasChildrenMap.TryGetValue(target.Id, out existingElement);

            // when the rendering order has changed, add the id to the expected list (so we correct the list)
            if (_renderingOrderHasChanged)
            {
                _expectedRenderingOrder.Add(target.Id);
                if (_removedChildren.Contains(target.Id) && existingElement != null)
                    _canvas.Children.Add(existingElement);
            }
            else ++_currentExpectedChildIndex; // otherwise increment the expected index for next call

            // don't render if outside of the part the user can actually see. However DO render when the rendering order has changed. (This is to ensure, that invisible objects, though invisible still get added to the canvas at the right position)
            if (!_renderingOrderHasChanged && (rotatedBoundingBox.X > _canvas.ActualWidth || rotatedBoundingBox.Y > _canvas.ActualHeight))
            {
                if (existingElement != null) existingElement.Visibility = Visibility.Hidden;
                return;
            }
            if (!_renderingOrderHasChanged && (rotatedBoundingBox.X + rotatedBoundingBox.Width < 0 || rotatedBoundingBox.Y + rotatedBoundingBox.Height < 0))
            {
                if (existingElement != null) existingElement.Visibility = Visibility.Hidden;
                return;
            }


            // depending on the SvgItem type, call a different Render method 
            if (target.GetType() == typeof(SvgRect))
                Render<System.Windows.Shapes.Rectangle>(target as SvgRect, renderBoundingBox, rotation, rotationPoint);
            else if (target.GetType() == typeof(SvgEllipse))
                Render<Ellipse>(target as SvgEllipse, renderBoundingBox, rotation, rotationPoint);
            else if (target.GetType() == typeof(SvgLine))
                RenderLine(target as SvgLine, renderBoundingBox, rotation, rotationPoint);
            else if (target.GetType() == typeof(SvgPolyline))
                RenderPolyline(target as SvgPolyline, renderBoundingBox, rotation, rotationPoint);
            else if (target.GetType() == typeof(SvgImage))
                RenderImage(target as SvgImage, renderBoundingBox, rotation, rotationPoint);
            else throw new ArgumentException("SvgItem has unknown type");

        }

        #endregion
    }
}
