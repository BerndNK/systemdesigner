﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace SystemDesignerWindowsDesktop.Controls
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:ASSM.shared.controls.ExpandableTextBlock"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:ASSM.shared.controls.ExpandableTextBlock;assembly=ASSM.shared.controls.ExpandableTextBlock"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:ExpandableTextBlock/>
    ///
    /// </summary>
    [TemplatePart(Name = "PART_TextBox", Type = typeof(TextBox))]
    [TemplatePart(Name = "PART_Button", Type = typeof(Button))]
    [TemplatePart(Name = "PART_DockPanel", Type = typeof(DockPanel))]
    public class ExpandableTextBlock : Control, INotifyPropertyChanged
    {

        #region DependencyProperties
        public static readonly DependencyProperty SourceTextProperty = DependencyProperty.Register(
            "SourceText", typeof(string), typeof(ExpandableTextBlock), new PropertyMetadata(default(string), SourceTextChanged));

        private static void SourceTextChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var tb = dependencyObject as ExpandableTextBlock;
            if (tb?._textBox == null) return;

            tb._button.Visibility = tb._textBox.DesiredSize.Height >= tb.CompactHeight ? Visibility.Visible : Visibility.Hidden;
        }

        public string SourceText {
            get { return (string)GetValue(SourceTextProperty); }
            set { SetValue(SourceTextProperty, value); }
        }

        public static readonly DependencyProperty CompactHeightProperty = DependencyProperty.Register(
            "CompactHeight", typeof(double), typeof(ExpandableTextBlock), new PropertyMetadata(60.0));

        public double CompactHeight {
            get { return (double)GetValue(CompactHeightProperty); }
            set { SetValue(CompactHeightProperty, value); }
        }

        public static readonly DependencyProperty ExpandHeightProperty = DependencyProperty.Register(
            "ExpandHeight", typeof(double), typeof(ExpandableTextBlock), new PropertyMetadata(double.MaxValue));

        public double ExpandHeight {
            get { return (double)GetValue(ExpandHeightProperty); }
            set { SetValue(ExpandHeightProperty, value); }
        }

        public static readonly DependencyProperty ButtonVisibilityProperty = DependencyProperty.Register(
            "ButtonVisibility", typeof(Visibility), typeof(ExpandableTextBlock), new PropertyMetadata(default(Visibility)));

        public Visibility ButtonVisibility {
            get { return (Visibility)GetValue(ButtonVisibilityProperty); }
            set { SetValue(ButtonVisibilityProperty, value); }
        }


        #endregion

        #region Member
        private TextBox _textBox;
        private Button _button;
        private DockPanel _dockPanel;

        #endregion
        static ExpandableTextBlock()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ExpandableTextBlock), new FrameworkPropertyMetadata(typeof(ExpandableTextBlock)));
        }

        public ExpandableTextBlock()
        {
            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (_textBox == null) return;
            _button.Visibility = _textBox.DesiredSize.Height >= CompactHeight ? Visibility.Visible : Visibility.Hidden;

            // _textBlock.LineStackingStrategy = 
        }

        public override void OnApplyTemplate()
        {
            _textBox = GetTemplateChild("PART_TextBox") as TextBox;
            _button = GetTemplateChild("PART_Button") as Button;
            _dockPanel = GetTemplateChild("PART_DockPanel") as DockPanel;

            if (_button != null)
            {
                _button.Click += ButtonOnClick;

                if (_dockPanel != null)
                {
                    _dockPanel.MaxHeight = CompactHeight;
                    if (_textBox != null)
                    {
                        _button.Visibility = _textBox.DesiredSize.Height >= CompactHeight ? Visibility.Visible : Visibility.Hidden;
                    }
                }
            }



            base.OnApplyTemplate();
        }

        private void ButtonOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            _dockPanel.MaxHeight = Math.Abs(_dockPanel.MaxHeight - CompactHeight) < 1 ? ExpandHeight : CompactHeight;
            _button.Content = Math.Abs(_dockPanel.MaxHeight - ExpandHeight) < 1 ? "-" : "+";
        }

        #region INotifyPropertyChangedImplementation
        private void CallPropertyChanged(string on)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(@on));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
