﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SystemDesignerWindowsDesktop.Controls.Converter {
    public class RadianToDegreeConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            return (double) value/Math.PI*180;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            return (double)value/180 * Math.PI;
        }
    }
}
