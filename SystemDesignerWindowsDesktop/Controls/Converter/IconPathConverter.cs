﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace SystemDesignerWindowsDesktop.Controls.Converter
{
    /// <summary>
    /// Takes a simple file name as appends the Assets / prefix
    /// </summary>
    /// <seealso cref="System.Windows.Data.IValueConverter" />
    public class IconPathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "Assets/" + value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
