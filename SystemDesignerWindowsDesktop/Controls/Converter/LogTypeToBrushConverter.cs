﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using SystemDesigner.Log;

namespace SystemDesignerWindowsDesktop.Controls.Converter
{
    public class LogTypeToBrushConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = new Color();
            if (value is LogType)
            {
                switch ((LogType)value)
                {
                    case LogType.Error:
                        color = Color.FromRgb(255, 0, 0);
                        break;
                    case LogType.Info:
                        color = Color.FromRgb(0, 96, 255);
                        break;
                    case LogType.All:
                    default:
                        throw new ArgumentOutOfRangeException(nameof(value), value, null);
                }
            }
            return new SolidColorBrush(color);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null; // not supported
        }
    }
}
