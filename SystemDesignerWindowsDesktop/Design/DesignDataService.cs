﻿using System;
using System.Linq;
using SystemDesigner.Stage;
using SystemDesignerWindowsDesktop.Model;

namespace SystemDesignerWindowsDesktop.Design {
    public class DesignDataService : IDataService {
        public void GetMainStageHandler(Action<SvgStageHandler, Exception> callback) {
            var item = new SvgStageHandler();
            item.StageList.Add(new SvgStage());
            item.ActiveStage = item.StageList.First();
            item.AddTestShapes();
            callback(item, null);
        }
    }
}