﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SystemDesigner.Helpers;
using SystemDesigner.Input.Shortcuts;
using SystemDesigner.Log;
using SystemDesigner.Stage;
using SystemDesigner.Stage.Tools;
using SystemDesignerResources.Text;
using SystemDesignerWindowsDesktop.Model;
using SystemDesignerWindowsDesktop.Utils;
using Dragablz;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms;
using InputType = SystemDesigner.Input.InputType;
using Point = DisplayCore.Render.Geometry.Point;

namespace SystemDesignerWindowsDesktop.ViewModel {
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainStageViewModel : ViewModelBase {
        /// <summary>
        /// Gets the stage handler.
        /// </summary>
        /// <value>
        /// The stage handler.
        /// </value>
        public SvgStageHandler StageHandler { get; private set; }

        public RelayCommand AddTestCommand { get; }
        public RelayCommand RotateCommand { get; }

        public RelayCommand<IStageTool<SvgStage>>  SelectToolCommand { get; }

        public RelayCommand<MouseEventArgs> StageMouseMoveCommand { get; }
        public RelayCommand<MouseButtonEventArgs> StageMouseDownCommand { get; }
        public RelayCommand<MouseButtonEventArgs> StageMouseUpCommand { get; }

        /// <summary>
        /// Gets the current active MainStageViewModel.
        /// </summary>
        public static MainStageViewModel Current { get; private set; }


        /// <summary>1
        /// Gets or sets the application menu.
        /// </summary>
        public ObservableCollection<MenuItemModel> AppMenu { get; set; }

        /// <summary>
        /// Gets the default application menu base (without tool specific options).
        /// </summary>
        public List<MenuItemModel> DefaultAppMenu { get; private set; }

        public Func<object> AddNewStageCommand {
            get
            {
                return () => new SvgStage();
            }
        }

        private MouseButtonState _leftMouseButtonStateLastMoveEvent;
        
        /// <summary>
        /// Gets or sets the mouse position in x which will be passed to stage
        /// </summary>
        public double MouseX {
            get { return _mouseX; }
            set { Set(ref _mouseX, value); }
        }

        private double _mouseX;

        /// <summary>
        /// Gets or sets the mouse position in y which will be passed to stage
        /// </summary>
        public double MouseY {
            get { return _mouseY; }
            set { Set(ref _mouseY, value); }
        }

        /// <summary>
        /// Gets or sets the width of the stage.
        /// </summary>
        /// <value>
        /// The width of the stage.
        /// </value>
        public double StageWidth
        {
            get { return _stageWidth; }
            set
            {
                Set(ref _stageWidth, value);
                ArrangeActiveTool();
            }
        }

        /// <summary>
        /// Gets or sets the height of the stage.
        /// </summary>
        /// <value>
        /// The height of the stage.
        /// </value>
        public double StageHeight
        {
            get { return _stageHeight; }
            set
            {
                Set(ref _stageHeight, value);
                ArrangeActiveTool();
            }
        }

        public IInterTabClient InterTabClient { get; private set; }

        public Visibility NeuralTesterVisibility
            => StageHandler.ActiveStage.ActiveTool is NeuralNetworkTestTool
                    ? Visibility.Visible
                    : Visibility.Collapsed;

        public Visibility ToolControlVisibility
            => NeuralTesterVisibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;



        private double _mouseY;
        private double _stageWidth;
        private double _stageHeight;
        private MenuItemModel _redoItem; // the undo menu item entry
        private MenuItemModel _undoItem; // the undo menu item entry

        /// <summary>
        /// Initializes a new instance of the MainStageViewModel class.
        /// </summary>
        public MainStageViewModel(IDataService dataService)
        {
            Current = this;

            // init interTabClient for Dragablz
            InterTabClient = new InterTabClient();
            // setup commands
            AddTestCommand = new RelayCommand(AddTest);
            RotateCommand = new RelayCommand(Rotate);
            SelectToolCommand = new RelayCommand<IStageTool<SvgStage>>(OnSelectTool);
            StageMouseMoveCommand = new RelayCommand<MouseEventArgs>(OnStageMouseMove);
            StageMouseDownCommand = new RelayCommand<MouseButtonEventArgs>(OnStageMouseDown);
            StageMouseUpCommand = new RelayCommand<MouseButtonEventArgs>(OnStageMouseUp);

            // setup menu
            DefaultAppMenu = GetDefaultAppMenu();
            AppMenu = new ObservableCollection<MenuItemModel>();
            UpdateMenuItems();


            dataService.GetMainStageHandler((handler, exception) =>
            {
                if (exception != null)
                {

                }
                StageHandler = handler;
                RestoreLastSession();
            });

        }

        private void RestoreLastSession()
        {
            var filePaths = Settings.OpenDocumentsLastSession.Split('|');
            foreach (var filePath in filePaths)
            {
                OpenDoc(filePath);
            }
        }

        private List<MenuItemModel> GetDefaultAppMenu()
        {
            var toReturn = new List<MenuItemModel>();
            // File menu
            toReturn.Add(new MenuItemModel()
            {
                Name =  TextResources.File,
                Items =
                {
                    new MenuItemModel()
                    {
                        Name = TextResources.NewDoc,
                        Shortcut = ShortcutManager.GetInstance().GetCombinationFor("NewDocument")?.ToString(),
                        Command = new RelayCommand(NewDocument)
                    },
                    new MenuItemModel()
                    {
                        Name = TextResources.Open,
                        Shortcut = ShortcutManager.GetInstance().GetCombinationFor("Open")?.ToString()
                    },
                    new MenuItemModel()
                    {
                        Name = TextResources.Save,
                        Shortcut = ShortcutManager.GetInstance().GetCombinationFor("Save")?.ToString()
                    },
                    new MenuItemModel()
                    {
                        Name = TextResources.SaveAs,
                        Shortcut = ShortcutManager.GetInstance().GetCombinationFor("SaveAs")?.ToString()
                    }
                }
            });

            // Edit menu
            _undoItem = new MenuItemModel
            {
                Name = TextResources.Undo,
                Shortcut = ShortcutManager.GetInstance().GetCombinationFor("Undo")?.ToString(),
                Command = new RelayCommand(Undo)
                        
            };
            _redoItem = new MenuItemModel
            {
                Name = TextResources.Redo,
                Shortcut = ShortcutManager.GetInstance().GetCombinationFor("Redo")?.ToString(),
                Command = new RelayCommand(Redo)
            };
            toReturn.Add(new MenuItemModel()
            {
                Name = TextResources.Edit,
                Items =
                {
                    _undoItem,
                    _redoItem
                }
            });


            // Options menu
            toReturn.Add(new MenuItemModel()
            {
                Name = TextResources.Options,
                Items =
                {
                    new MenuItemModel()
                    {
                        Name = TextResources.RestoreDefaultShortcuts,
                        Shortcut = ShortcutManager.GetInstance().GetCombinationFor("RestoreDefaultShortcuts")?.ToString(),
                        Command = new RelayCommand(() => ShortcutManager.GetInstance().RestoreDefaults())
                    }
                }
            });

            SetEnabledMenuItems();
            return toReturn;
        }

        /// <summary>
        /// Performs the undo operation on the current active stage.
        /// </summary>
        public void Undo()
        {
            if (StageHandler.ActiveStage == null) return;
            if (!StageHandler.ActiveStage.CanUndo) return;

            StageHandler.ActiveStage.Undo();
            SetEnabledMenuItems();
        }

        /// <summary>
        /// Performs the redo operation on the current active stage.
        /// </summary>
        public void Redo()
        {
            if (StageHandler.ActiveStage == null) return;
            if (!StageHandler.ActiveStage.CanRedo) return;

            StageHandler.ActiveStage.Redo();
            SetEnabledMenuItems();
        }

        /// <summary>
        /// Refreshes the enabled states of all relevant menu items.
        /// </summary>
        private void SetEnabledMenuItems()
        {
            _undoItem.Enabled = StageHandler?.ActiveStage != null && StageHandler.ActiveStage.CanUndo;
            _redoItem.Enabled = StageHandler?.ActiveStage != null && StageHandler.ActiveStage.CanRedo;
        }

        /// <summary>
        /// Adds a new document to the stage list.
        /// </summary>
        public void NewDocument()
        {
            StageHandler.StageList.Add(new SvgStage());
        }

        private void Rotate() {
            var selectTool = StageHandler.ActiveStage.ActiveTool as SelectTool;
            if (selectTool != null) {
                if (selectTool.SelectedItem != null) selectTool.SelectedItem.Rotation += 0.05;
                else StageHandler.ActiveStage.Root.Rotation += 0.05;
            }
            else StageHandler.ActiveStage.Root.Rotation += 0.05;
        }


        private void ArrangeActiveTool()
        {
            StageHandler?.ActiveStage?.ActiveTool?.UiContainer?.Arrange(StageWidth, StageHeight);
        }


        private void OnSelectTool(IStageTool<SvgStage> tool) {
            StageHandler.ActiveStage.SetActiveTool(tool);
            ArrangeActiveTool();
            UpdateMenuItems();

            RaisePropertyChanged(nameof(NeuralTesterVisibility));
            RaisePropertyChanged(nameof(ToolControlVisibility));
        }

        private void UpdateMenuItems()
        {
            AppMenu.Clear();
            foreach (var menuItem in DefaultAppMenu)
            {
                AppMenu.Add(menuItem);
            }

            var activeTool = StageHandler?.ActiveStage?.ActiveTool;
            var toolFunctions = activeTool?.ToolFunctions;
            if (toolFunctions == null) return;

            var toolMenu = new MenuItemModel() {Name = activeTool.Name};
            foreach (var toolFunction in toolFunctions)
            {
                toolMenu.Items.Add(new MenuItemModel()
                {
                    Name = toolFunction.Name
                });
            }
            
            AppMenu.Add(toolMenu);
        }

        private void OnStageMouseMove(MouseEventArgs obj) {
            if (obj.LeftButton == MouseButtonState.Released && _leftMouseButtonStateLastMoveEvent == MouseButtonState.Pressed)
                StageHandler.ActiveStage?.OnInputUp(new Point(MouseX, MouseY), InputType.Mouse, -1);

            _leftMouseButtonStateLastMoveEvent = obj.LeftButton;

            StageHandler.ActiveStage?.OnInputMove(new Point(MouseX, MouseY), InputType.Mouse, -1);

        }

        private void OnStageMouseUp(MouseButtonEventArgs obj) {
            _leftMouseButtonStateLastMoveEvent = MouseButtonState.Released;
            StageHandler.ActiveStage?.OnInputUp(new Point(MouseX, MouseY), InputType.Mouse, -1);
        }

        private void OnStageMouseDown(MouseButtonEventArgs obj) {
            StageHandler.ActiveStage?.OnInputDown(new Point(MouseX, MouseY), InputType.Mouse, -1);
        }

        private void AddTest() {
            StageHandler.AddTestShapes();
        }

        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
        /// <summary>
        /// Opens a dialog to open a stored stage.
        /// </summary>
        public void Open()
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() != true) return;

            OpenDoc(openFileDialog.FileName);
        }

        private void OpenDoc(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath) || !File.Exists(filePath)) return;
            // read the text
            var text = File.ReadAllText(filePath);

            // parse it
            var stage = StageHandler.AddNewStageFromXml(text);

            // set the source and name of the stage
            stage.Source = filePath;
            stage.Name = filePath.Split('\\').Last().Split('.').First();
        }

        /// <summary>
        /// Saves the current active stage under the path it was opened. Opens a dialog if it's a new stage.
        /// </summary>
        public void Save()
        {
            // abort if there is no active stage
            if (StageHandler?.ActiveStage == null) return;
            if (string.IsNullOrWhiteSpace(StageHandler.ActiveStage?.Source))
            {
                SaveAs();
                return;
            }

            File.WriteAllText(StageHandler.ActiveStage.Source, StageHandler.GetActiveStageAsXml());
        }

        /// <summary>
        /// Opens a dialog to save the currently opened stage under a new file path.
        /// </summary>
        public void SaveAs()
        {
            var saveFileDialog = new SaveFileDialog()
            {
                DefaultExt = "sysD"
            };
            if (saveFileDialog.ShowDialog() != true) return;

            File.WriteAllText(saveFileDialog.FileName, StageHandler.GetActiveStageAsXml());
            StageHandler.ActiveStage.Name = saveFileDialog.FileName.Split('\\').Last().Split('.').First();
        }

        /// <summary>
        /// Closes the currently active stage.
        /// </summary>
        public void CloseDocument()
        {
            if (StageHandler?.ActiveStage == null) return;
            // get the index of the current active stage
            var selectedIndex = StageHandler.StageList.IndexOf(StageHandler.ActiveStage);
            // remove it
            StageHandler.StageList.Remove(StageHandler.ActiveStage);
            // then select the stage which was next to the previously selected stage
            if (selectedIndex < StageHandler.StageList.Count)
                StageHandler.ActiveStage = StageHandler.StageList[selectedIndex];
            else // in the special case, that the index of the removed stage was the last item (or was the only stage in the list), select the last element (or default which is null in case the list is empty)
                StageHandler.ActiveStage = StageHandler.StageList.LastOrDefault();
        }
    }
}