﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using SystemDesigner.Helpers;
using SystemDesigner.Input.Shortcuts;
using SystemDesigner.Log;
using SystemDesigner.Stage.Tools;
using SystemDesignerWindowsDesktop.Model;
using SystemDesignerWindowsDesktop.Utils;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MahApps.Metro;

namespace SystemDesignerWindowsDesktop.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class AppWindowViewModel : ViewModelBase
    {

        /// <summary>
        /// Gets the application icon path.
        /// </summary>
        /// <value>
        /// The application icon path.
        /// </value>
        public string ApplicationIconPath => string.Equals(ThemeManager.DetectAppStyle().Item1.Name, "baseLight", StringComparison.CurrentCultureIgnoreCase) ? ".\\Assets\\Icon24.png" : ".\\Assets\\BlackIcon24.png";

        /// <summary>
        /// Gets or sets the title foreground brush
        /// </summary>
        public Brush TitleForegroundBrush {
            get { return _titleForegroundBrush; }
            set { Set(ref _titleForegroundBrush, value); }
        }
        private Brush _titleForegroundBrush;


        /// <summary>
        /// Gets the lost focus command.
        /// </summary>
        /// <value>
        /// The lost focus command.
        /// </value>
        public RelayCommand<EventArgs> DeactivatedCommand { get; }

        /// <summary>
        /// Gets the got focus command.
        /// </summary>
        /// <value>
        /// The got focus command.
        /// </value>
        public RelayCommand<EventArgs> ActivatedCommand { get; }


        private List<KeyCombination> _pressedCombinations = new List<KeyCombination>();

        /// <summary>
        /// Initializes a new instance of the AppWindowViewModel class.
        /// </summary>
        public AppWindowViewModel(IDataService dataService)
        {
            TitleForegroundBrush = Application.Current.FindResource("LabelTextBrush") as Brush;

            // ReSharper disable once ExplicitCallerInfoArgument
            ThemeManager.IsThemeChanged += (sender, args) => RaisePropertyChanged(nameof(ApplicationIconPath));

            DeactivatedCommand = new RelayCommand<EventArgs>(OnDeactivated);
            ActivatedCommand = new RelayCommand<EventArgs>(OnActivated);

            SetupShortcuts();
            if(!File.Exists(FreeHandTool.NeuralNetworkPath)) throw new Exception("System Designer may not be started without a valid neuralNetwork file at the location: "+FreeHandTool.NeuralNetworkPath);
            FreeHandTool.NeuralNetworkXml = File.ReadAllText(FreeHandTool.NeuralNetworkPath);

            Application.Current.MainWindow.KeyUp += MainWindowOnKeyUp;
            Application.Current.MainWindow.KeyDown += MainWindowOnKeyDown;

            Application.Current.MainWindow.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // store each open stage to restore on the next launch
            if (MainStageViewModel.Current?.StageHandler?.StageList == null) return;

            Settings.OpenDocumentsLastSession =
               string.Join("|", MainStageViewModel.Current.StageHandler.StageList
                .Where(x => !string.IsNullOrWhiteSpace(x.Source))
                .Select(x => x.Source)
                .ToList());
        }

        private void SetupShortcuts()
        {
            // open log shortcut
#if DEBUG
            Shortcut.AppShortcuts.Add(new Shortcut("OpenLog",
                new List<KeyCombination> { new KeyCombination("D", KeyboardModifier.Alt | KeyboardModifier.Ctrl) })
            { OnPressedAction = () => LogDisplayWindow.GetInstance().Show() });
#endif


            Shortcut.AppShortcuts.Add(new Shortcut("CloseDocument",
                    new List<KeyCombination> { new KeyCombination("W", KeyboardModifier.Ctrl) })
            { OnPressedAction = () => MainStageViewModel.Current?.CloseDocument() });

            Shortcut.AppShortcuts.Add(new Shortcut("RestoreDefaultShortcuts")
            { OnPressedAction = () => ShortcutManager.GetInstance().RestoreDefaults() });

            
            #region File menu

            Shortcut.AppShortcuts.Add(new Shortcut("NewDocument",
                    new List<KeyCombination> { new KeyCombination("N", KeyboardModifier.Ctrl) })
                { OnPressedAction = () => MainStageViewModel.Current?.NewDocument() });

            Shortcut.AppShortcuts.Add(new Shortcut("Open",
                    new List<KeyCombination> { new KeyCombination("O", KeyboardModifier.Ctrl) })
                { OnPressedAction = () => MainStageViewModel.Current?.Open() });

            Shortcut.AppShortcuts.Add(new Shortcut("Save",
                    new List<KeyCombination> { new KeyCombination("S", KeyboardModifier.Ctrl) })
                { OnPressedAction = () => MainStageViewModel.Current?.Save() });

            Shortcut.AppShortcuts.Add(new Shortcut("SaveAs",
                    new List<KeyCombination> { new KeyCombination("S", KeyboardModifier.Ctrl | KeyboardModifier.Shift) })
                { OnPressedAction = () => MainStageViewModel.Current?.SaveAs() });

            #endregion

            #region Edit menu

            Shortcut.AppShortcuts.Add(new Shortcut("Undo",
                    new List<KeyCombination> { new KeyCombination("Z", KeyboardModifier.Ctrl) })
                { OnPressedAction = () => MainStageViewModel.Current?.Undo() });


            Shortcut.AppShortcuts.Add(new Shortcut("Redo",
                    new List<KeyCombination> { new KeyCombination("Y", KeyboardModifier.Ctrl) })
                { OnPressedAction = () => MainStageViewModel.Current?.Redo() });

            #endregion

            ShortcutManager.GetInstance().LoadShortcuts();
        }

        private KeyCombination GetKeyCombination(KeyEventArgs keyEventArgs)
        {
            // translate pressed keys into KeyCombination model
            var modifier = KeyboardModifier.None;

            modifier |= Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)
                ? KeyboardModifier.Ctrl
                : KeyboardModifier.None;

            modifier |= Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)
                ? KeyboardModifier.Shift
                : KeyboardModifier.None;

            modifier |= Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt)
                ? KeyboardModifier.Alt
                : KeyboardModifier.None;

            return new KeyCombination(keyEventArgs.Key.ToString(), modifier);
        }

        private void MainWindowOnKeyDown(object o, KeyEventArgs keyEventArgs)
        {
            var combination = GetKeyCombination(keyEventArgs);
            // store the pressed combination in an array (to prevent multiple triggers when the user holds the keys down, as windows would then trigger this event multiple times)
            if (_pressedCombinations.Contains(combination)) return;
            _pressedCombinations.Add(combination);

            // send the combination to the shortcut manager
            ShortcutManager.GetInstance().EnterCombination(combination);
        }

        private void MainWindowOnKeyUp(object o, KeyEventArgs keyEventArgs)
        {
            var combination = GetKeyCombination(keyEventArgs);

            // remove the combination from the pressedCombination array. We ignore modifiers here, since these won't count anyway when the KEY has been released
            var existing = _pressedCombinations.Where(x => x.Key == combination.Key).ToList();
            if (existing.Count == 0) return;
            foreach (var keyCombination in existing)
            {
                _pressedCombinations.Remove(keyCombination);
            }

            // tell the shortcut manager that the combination has been released
            ShortcutManager.GetInstance().ReleaseCombination(combination);
        }

        private void OnActivated(EventArgs o)
        {
            TitleForegroundBrush = Application.Current.FindResource("LabelTextBrush") as Brush;
        }

        private void OnDeactivated(EventArgs o)
        {
            TitleForegroundBrush = Application.Current.FindResource("GrayBrush5") as Brush;
        }
    }
}