﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Xml.Linq;
using System.Xml.Serialization;
using SystemDesigner.Log;
using SystemDesigner.Neural;
using SystemDesigner.Stage;
using SystemDesigner.Stage.Tools;
using SystemDesigner.Stage.Tools.Helper;
using SystemDesigner.Utils;
using SystemDesignerWindowsDesktop.Model;
using DisplayCore.Annotations;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Win32;
using Brush = System.Windows.Media.Brush;
using Color = System.Drawing.Color;
using Drawing = SystemDesigner.Draw.Drawing;
using PixelFormat = System.Drawing.Imaging.PixelFormat;
using Point = DisplayCore.Render.Geometry.Point;

namespace SystemDesignerWindowsDesktop.ViewModel
{
    public class NeuralNetworkTesterViewModel : ViewModelBase
    {
        private ObservableCollection<Drawing> _loadedShapes;
        private Drawing _selectedDrawing;
        private int _epoch = 0;
        private string _detectedShape;
        private FreeHandTool _freeHandTool;
        private List<string> _possibleShapes;
        private INeuralNetworkManager _networkManager;
        private CancellationTokenSource _cancellationTokenSource;

        private string _trainText;
        private Task _trainTask;
        private ObservableCollection<INeuralNetworkManager> _availableNetworks;
        private INeuralNetworkManager _selectedNetwork;
        private double _learningRate = 3;
        private NeuralTestPack _currentTestPack;
        private int _packsTested;
        private int _timeout = 10;
        private double _finalError;
        private string _testDirectory = "TestPacks";
        private bool _uiAvailable = true;
        private string _testState;
        private bool _testRunning;
        private string _currentTest;

        public ObservableCollection<ConvertedBitmapWithResult> BitmapsWithResults { get; set; }
        public ObservableCollection<ConvertedBitmapWithResult> TestBitmapsWithResults { get; set; }

        public ObservableCollection<NeuralTestPack> NeuralTestPacks { get; set; }

        public ICommand LoadShapesCommand { get; }
        public ICommand LoadTestShapesCommand { get; }
        public ICommand SelectShapeCommand { get; }
        public ICommand TrainCommand { get; }
        public ICommand TestCommand { get; }
        public ICommand ConvertCommand { get; }

        public Drawing SelectedDrawing {
            get { return _selectedDrawing; }
            set { Set(ref _selectedDrawing, value); }
        }

        public string DetectedShape {
            get { return _detectedShape; }
            set { Set(ref _detectedShape, value); }
        }

        public FreeHandTool FreeHandTool {
            get { return _freeHandTool; }
            set { Set(ref _freeHandTool, value); }
        }

        public string TrainText {
            get { return _trainText; }
            set { Set(ref _trainText, value); }
        }

        public ObservableCollection<INeuralNetworkManager> AvailableNetworks {
            get { return _availableNetworks; }
            private set { Set(ref _availableNetworks, value); }
        }

        public INeuralNetworkManager SelectedNetwork {
            get => AvailableNetworks.First(x => x.Name == CurrentTestPack.SelectedNetwork);
            set => CurrentTestPack.SelectedNetwork = value.Name;
        }

        public double LearningRate {
            get { return _learningRate; }
            set { Set(ref _learningRate, value); }
        }

        public NeuralTestPack CurrentTestPack {
            get { return _currentTestPack; }
            set {
                _currentTestPack = value;
                Set(ref _currentTestPack, value);
                PropertyChangedHandler?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedNetwork)));
            }
        }

        public bool UiAvailable {
            get { return _uiAvailable; }
            set { Set(ref _uiAvailable, value); }
        }

        public int PacksTested {
            get { return _packsTested; }
            set { Set(ref _packsTested, value); }
        }

        public string TestState {
            get { return _testState; }
            set => Set(ref _testState, value);
        }

        public ICommand SaveAsPackCommand { get; }

        public ICommand RunPackCommand { get; }
        public ICommand LoadPackCommand { get; }
        public ICommand GeneratePackCommand { get; }
        public ICommand SaveNetworkCommand { get; }

        public ICommand RemoveHiddenLayer { get; }

        public ICommand AddHiddenLayer { get; }

        public int Timeout {
            get => _timeout;
            set => Set(ref _timeout, value);
        }

        public string TestDirectory {
            get => _testDirectory;
            set => Set(ref _testDirectory, value);
        }

        public bool TestRunning {
            get => _testRunning;
            set => Set(ref _testRunning, value);
        }

        public string CurrentTest {
            get => _currentTest;
            set => Set(ref _currentTest, value);
        }



        public NeuralNetworkTesterViewModel()
        {
            LoadShapesCommand = new RelayCommand(LoadShapes);
            LoadTestShapesCommand = new RelayCommand(LoadTestShapes);
            TrainCommand = new RelayCommand(Train);
            TestCommand = new RelayCommand(() => Test());
            SaveAsPackCommand = new RelayCommand(SaveAsPack);
            RunPackCommand = new RelayCommand(RunPacks);
            LoadPackCommand = new RelayCommand(LoadTestPacks);
            GeneratePackCommand = new RelayCommand(GenerateTestScenarios);
            SaveNetworkCommand = new RelayCommand(SaveNetwork);
            CurrentTestPack = new NeuralTestPack();
            RemoveHiddenLayer = new RelayCommand(() => CurrentTestPack.HiddenLayer.Remove(CurrentTestPack.HiddenLayer.Last()));
            AddHiddenLayer = new RelayCommand(() => CurrentTestPack.HiddenLayer.Add(5));
            TrainText = "Train";
            _cancellationTokenSource = new CancellationTokenSource();

            AvailableNetworks = new ObservableCollection<INeuralNetworkManager> { new NeuralNetworkManager(), new EncogManager() };
            SelectedNetwork = AvailableNetworks.First();

            ConvertCommand = new RelayCommand(ConvertDrawings);
            SelectShapeCommand = new RelayCommand<Drawing>(SelectShape);
            BitmapsWithResults = new ObservableCollection<ConvertedBitmapWithResult>();
            TestBitmapsWithResults = new ObservableCollection<ConvertedBitmapWithResult>();
            NeuralTestPacks = new ObservableCollection<NeuralTestPack>();
            NeuralTestPacks.Add(new NeuralTestPack());
            LoadTestPacks();
            FreeHandTool = new FreeHandTool();
            FreeHandTool.DrawHelper.DrawingCompleted += DrawHelperOnDrawingCompleted;
            if (NeuralNetworkTestTool.Current != null) NeuralNetworkTestTool.Current.FreeHandTool = FreeHandTool;

            var manager = new NeuralNetworkManager();
            var xml = manager.ToXml();
            manager.FromXml(xml);

        }

        private void SaveNetwork()
        {

            var saveFileDialog = new SaveFileDialog()
            {
                DefaultExt = "neuralNetwork"
            };
            if (saveFileDialog.ShowDialog() != true) return;

            File.WriteAllText(saveFileDialog.FileName, ((NeuralNetworkManager)SelectedNetwork).ToXml());
        }

        private async void RunPacks()
        {
            if (TestRunning) return;
            TestRunning = true;
            UiAvailable = false;
            PacksTested = 0;

            // stopwatch used to track time spent
            var clock = new Stopwatch();
            var log = new StringBuilder();
            var logTimeString = DateTime.Now.ToString("yyyy-MM-dd_hh-mm");
            var csv = GetResultCsvDictionary();
            var trainingShapesLastRun = new List<Drawing>();
            var testShapesLastRun = new List<Drawing>();
            var resolutionLastRun = -1;

            TestState = "Started";

            log.AppendLine($"Starting test session. Packs: {NeuralTestPacks.Count}");
            foreach (var neuralTestPack in NeuralTestPacks)
            {
                log.AppendLine($"Testing pack {PacksTested + 1} \"{neuralTestPack.Name}\" with neural network \"{SelectedNetwork.Name}\"");
                CurrentTestPack = neuralTestPack;
                if (CurrentTestPack.TrainingShapes.Count == 0 || CurrentTestPack.TestShapes.Count == 0) continue;
                CurrentTest = CurrentTestPack.ToString();


                csv["File"].Add(neuralTestPack.Name);
                csv["Used network"].Add(neuralTestPack.SelectedNetwork);
                csv["Picture resolution"].Add(neuralTestPack.PictureResolution.ToString());
                csv["Target error"].Add(neuralTestPack.ErrorToStopAt.ToString(CultureInfo.InvariantCulture));
                csv["Max. epoch"].Add(neuralTestPack.EpochToStopAt.ToString());
                csv["Test shapes count"].Add(neuralTestPack.TestShapes.Count.ToString());
                csv["Training shapes count"].Add(neuralTestPack.TrainingShapes.Count.ToString());
                csv["Hidden layer amount"].Add(neuralTestPack.HiddenLayer.Count.ToString());
                csv["Hidden layer sizes"].Add(string.Join("-", neuralTestPack.HiddenLayer));
                csv["Learning rate"].Add(neuralTestPack.LearningRate.ToString(CultureInfo.InvariantCulture));

                var doConvert = trainingShapesLastRun.Count != CurrentTestPack.TrainingShapes.Count ||
                                testShapesLastRun.Count != CurrentTestPack.TestShapes.Count;

                if (!doConvert) doConvert = resolutionLastRun != CurrentTestPack.PictureResolution;

                if (!doConvert) doConvert = trainingShapesLastRun.Any(s => !CurrentTestPack.TrainingShapes.Select(x => x.FileName).Contains(s.FileName));
                if (!doConvert) doConvert = testShapesLastRun.Any(s => !CurrentTestPack.TestShapes.Select(x => x.FileName).Contains(s.FileName));

                if (doConvert)
                {
                    TestState = "Converting";
                    clock.Start();
                    await Task.Run(() => ConvertDrawings());
                    clock.Stop();

                    trainingShapesLastRun.AddRange(CurrentTestPack.TrainingShapes);
                    testShapesLastRun.AddRange(CurrentTestPack.TestShapes);

                    CurrentTestPack.ElapsedTimeConverting = clock.Elapsed;
                    clock.Restart();

                    resolutionLastRun = CurrentTestPack.PictureResolution;
                }
                else
                {
                    CurrentTestPack.TrainingShapes = new ObservableCollection<Drawing>(trainingShapesLastRun);
                    CurrentTestPack.TestShapes = new ObservableCollection<Drawing>(testShapesLastRun);
                }

                TestState = "Training";
                clock.Start();

                var task = Task.Run(() =>
                {
                    try
                    {

                        Train();
                        CurrentTestPack.CorrectTests = Test();
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                });

                if (await Task.WhenAny(task, Task.Delay(Timeout * 1000 * 60)) != task)
                {
                    // timeout
                    _cancellationTokenSource.Cancel();
                    // wait for the Train task to run out
                    await Task.Run(() =>
                    {
                        while (TrainText != "Train") ;
                        CurrentTestPack.CorrectTests = 0;
                    });
                }
                clock.Stop();

                CurrentTestPack.ElapsedTimeTraining = clock.Elapsed;
                CurrentTestPack.EpochStoppedAt = _epoch;
                CurrentTestPack.FinalError = _finalError;


                TestState = "Test";

                log.AppendLine(
                    $"Converting took {CurrentTestPack.ElapsedTimeConverting:g} Training took {CurrentTestPack.ElapsedTimeTraining:g} with a final error of {CurrentTestPack.FinalError} at epoch {CurrentTestPack.EpochStoppedAt} and guessed {CurrentTestPack.CorrectTests}/{CurrentTestPack.TestShapes.Count} tests corrected.");

                csv["Elapsed time converting"].Add(neuralTestPack.ElapsedTimeConverting.ToString("g"));
                csv["Elapsed time training"].Add(neuralTestPack.ElapsedTimeTraining.ToString("g"));
                csv["Final error"].Add(neuralTestPack.FinalError.ToString(CultureInfo.InvariantCulture));
                csv["Final epoch"].Add(neuralTestPack.EpochStoppedAt.ToString());
                csv["Correct test shapes guessed"].Add(neuralTestPack.CorrectTests.ToString());


                PacksTested++;
            }


            TestState = "Writing log";
            if (!Directory.Exists($".\\{TestDirectory}\\result"))
                Directory.CreateDirectory($".\\{TestDirectory}\\result");
            File.WriteAllText($".\\{TestDirectory}\\result\\log" + logTimeString, log.ToString());

            // write csv
            var csvBuilder = new StringBuilder();
            // write header
            foreach (var csvKey in csv.Keys)
            {
                csvBuilder.Append($"{csvKey}, ");
            }
            csvBuilder.AppendLine();

            // write values
            for (var i = 0; i < NeuralTestPacks.Count; i++)
            {
                foreach (var csvKey in csv.Keys)
                {
                    csvBuilder.Append($"{csv[csvKey][i]}, ");
                }
                csvBuilder.AppendLine();
            }


            File.WriteAllText($".\\{TestDirectory}\\result\\results{logTimeString}.csv", csvBuilder.ToString());

            TestRunning = false;
            UiAvailable = true;
            CurrentTest = null;
        }

        private void GenerateTestScenarios()
        {
            var count = 0;
            var folderCount = 0;
            var resolutions = new[] { 16, 32, 64 };
            var rates = new[] { 1.0, 0.5, 0.25, 0.1, 0.05, 0.025, 0.01, 0.005, 0.0025, 0.001 };
            var hiddenLayer = new[]
            {
                new List<IntProxy> {10, 5},
                new List<IntProxy> {10},
                new List<IntProxy> {10, 5, 5},
                new List<IntProxy> {10, 5, 2},
                new List<IntProxy> {30, 20},
                new List<IntProxy> {30, 20,10},
                new List<IntProxy> {30, 20,5},
                new List<IntProxy> {10, 9,8,7,6,5,4,3,2,1},
                new List<IntProxy> {8, 4},
                new List<IntProxy> {8, 2},
                new List<IntProxy> {12, 10},
                new List<IntProxy> {12, 8},
                new List<IntProxy> {12, 6},
                new List<IntProxy> {12, 4},
            };

            var networks = new[] { "System Designer", "Encog" };

            foreach (var network in networks)
            {
                foreach (var resolution in resolutions)
                {
                    foreach (var rate in rates)
                    {
                        foreach (var intProxies in hiddenLayer)
                        {
                            count++;
                            if (count % 1000 == 0)
                            {
                                folderCount++;
                                TestDirectory = "Generated" + folderCount;
                            }
                            var pack = new NeuralTestPack();
                            pack.EpochToStopAt = 100;
                            pack.ErrorToStopAt = 0.005;
                            pack.PictureResolution = resolution;
                            pack.LearningRate = rate;
                            pack.SelectedNetwork = network;
                            pack.HiddenLayer = new ObservableCollection<IntProxy>(intProxies);
                            pack.TestShapes = CurrentTestPack.TestShapes;
                            pack.TrainingShapes = CurrentTestPack.TrainingShapes;
                            CurrentTestPack = pack;
                            SaveAsPack();
                        }
                    }
                }
            }

            DebugLogger.GetInstance().LogInfo(count + " test packs.");
        }

        private static Dictionary<string, List<string>> GetResultCsvDictionary()
        {
            var csv = new Dictionary<string, List<string>>();
            csv.Add("File", new List<string>());
            csv.Add("Used network", new List<string>());
            csv.Add("Picture resolution", new List<string>());
            csv.Add("Target error", new List<string>());
            csv.Add("Max. epoch", new List<string>());
            csv.Add("Test shapes count", new List<string>());
            csv.Add("Training shapes count", new List<string>());
            csv.Add("Hidden layer amount", new List<string>());
            csv.Add("Hidden layer sizes", new List<string>());
            csv.Add("Elapsed time converting", new List<string>());
            csv.Add("Elapsed time training", new List<string>());
            csv.Add("Final error", new List<string>());
            csv.Add("Final epoch", new List<string>());
            csv.Add("Correct test shapes guessed", new List<string>());
            csv.Add("Learning rate", new List<string>());

            return csv;
        }

        /// <summary>
        /// Saves the current configuration as a test pack.
        /// </summary>
        private void SaveAsPack()
        {
            // variables used to create the final path
            var name = $"{TestDirectory}Pack";
            var path = Directory.GetCurrentDirectory() + $"\\{TestDirectory}\\";
            var count = 0;
            const string fileExtension = ".xml";

            // create the directory if it doesn't exist
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // check if the file already exists, if so, increase the count at the end of the fileName e.g. Circle0.xml -> Circle1.xml and so on
            while (File.Exists(path + name + count + fileExtension)) count++;

            var xmlSerializer = new XmlSerializer(typeof(NeuralTestPack));
            using (var stringWriter = new StringWriter())
            {
                foreach (var testShape in CurrentTestPack.TestShapes)
                {
                    testShape.Points.Clear();
                }
                foreach (var testShape in CurrentTestPack.TrainingShapes)
                {
                    testShape.Points.Clear();
                }
                xmlSerializer.Serialize(stringWriter, CurrentTestPack);
                var xml = stringWriter.ToString();
                File.WriteAllText(path + name + count + fileExtension, xml);
            }
        }

        /// <summary>
        /// Loads all test packs from the TestPacks directory.
        /// </summary>
        /// <exception cref="System.Exception">Failed to parse XML.</exception>
        private void LoadTestPacks()
        {
            NeuralTestPacks.Clear();

            if (!Directory.Exists($"./{TestDirectory}")) Directory.CreateDirectory($"./{TestDirectory}");
            foreach (var file in Directory.EnumerateFiles($"./{TestDirectory}"))
            {

                var xml = File.ReadAllText(file);
                // deserialize
                // parse the XML
                try
                {
                    var doc = XDocument.Parse(xml);
                    if (doc.Root == null) throw new Exception("Failed to parse xml.");

                    // create the serializer
                    var xmlSerializer = new XmlSerializer(typeof(NeuralTestPack));
                    using (var reader = doc.Root.CreateReader())
                    {
                        // deserialize and add it to the stage
                        var pack = (NeuralTestPack)xmlSerializer.Deserialize(reader);
                        pack.Name = file.Split('\\').Last().Split('.').First();
                        NeuralTestPacks.Add(pack);
                    }
                }
                catch (Exception e)
                {
                    DebugLogger.GetInstance().LogError(e.ToString());
                    return;
                }
            }
        }

        private int Test()
        {
            if (_networkManager == null) return -1;
            var correctShapes = 0;
            for (var i = 0; i < BitmapsWithResults.Count; i++)
            {
                var drawing = CurrentTestPack.TrainingShapes[i];
                byte[] inBytes;
                ConvertDrawing(drawing, out inBytes);
                drawing.InBytes = inBytes;
                var inputArray = GetInputArray(drawing);
                var calculatedOutput = _networkManager.Compute(inputArray).ToList();

                var detectedShape = _possibleShapes[calculatedOutput.IndexOf(calculatedOutput.Max())];
                if (detectedShape == drawing.ShapeName) BitmapsWithResults[i].Brush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0, 255, 0));
                else BitmapsWithResults[i].Brush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 0));
            }

            for (var i = 0; i < TestBitmapsWithResults.Count; i++)
            {
                var drawing = CurrentTestPack.TestShapes[i];
                byte[] inBytes;
                ConvertDrawing(drawing, out inBytes);
                drawing.InBytes = inBytes;
                var inputArray = GetInputArray(drawing);
                var calculatedOutput = _networkManager.Compute(inputArray).ToList();

                var detectedShape = _possibleShapes[calculatedOutput.IndexOf(calculatedOutput.Max())];
                if (detectedShape == drawing.ShapeName)
                {
                    TestBitmapsWithResults[i].Brush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(0, 255, 0));
                    correctShapes++;
                }
                else TestBitmapsWithResults[i].Brush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 0, 0));
            }
            return correctShapes;
        }

        private void DrawHelperOnDrawingCompleted(object sender, Drawing drawing)
        {
            if (_possibleShapes == null || _networkManager == null || drawing.Points.Count == 0) return;
            byte[] inBytes;
            ConvertDrawing(drawing, out inBytes);
            drawing.InBytes = inBytes;
            var inputArray = GetInputArray(drawing);
            try
            {
                var calculatedOutput = _networkManager.Compute(inputArray).ToList();
                DetectedShape = _possibleShapes[calculatedOutput.IndexOf(calculatedOutput.Max())];
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private async void Train()
        {
            if (TrainText != "Train")
            {
                _cancellationTokenSource.Cancel();
                return;
            }

            TrainText = "Stop";
            var inputs = GetInputs();
            var desiredOutputs = GetOutputs();

            _networkManager = SelectedNetwork;
            _networkManager.Init(CurrentTestPack.AmountInputs, CurrentTestPack.HiddenLayer.Select(x => x.Value).ToList(), desiredOutputs[0].Length);
            _networkManager.SetTrainingData(inputs, desiredOutputs);
            var ct = _cancellationTokenSource.Token;

            await Task.Run(() =>
              {
                  _epoch = 0;
                  do
                  {
                      TestState = $"Epoch #{_epoch} Error: {_networkManager.Error:0.0000000}";
                      DebugLogger.GetInstance().LogInfo(TestState);

                      _networkManager.Iteration(CurrentTestPack.LearningRate);

                      var dispatcherOperation = Application.Current.Dispatcher.BeginInvoke((Action)(() => Test()), DispatcherPriority.ApplicationIdle);
                      while (dispatcherOperation.Status != DispatcherOperationStatus.Completed)
                      {

                      }


                      ++_epoch;
                  } while (_epoch < CurrentTestPack.EpochToStopAt && _networkManager.Error > CurrentTestPack.ErrorToStopAt && !ct.IsCancellationRequested);
                  DebugLogger.GetInstance().LogInfo($"FINISHED Epoch #{_epoch} Error: {_networkManager.DebugInformation}");
              }, ct);

            TrainText = "Train";
            _finalError = _networkManager.Error;
            _cancellationTokenSource = new CancellationTokenSource();
        }

        private void NetworkManagerOnOnTrain(object sender, EventArgs eventArgs)
        {
            if (_epoch % 20 == 0)
                Application.Current.Dispatcher.Invoke(Test);
            _epoch++;
        }

        private double[][] GetOutputs()
        {
            var outputs = new double[CurrentTestPack.TrainingShapes.Count][];
            var crnt = 0;
            foreach (var loadedShape in CurrentTestPack.TrainingShapes)
            {
                outputs[crnt] = GetOutputArray(loadedShape);
                crnt += 1;
            }
            return outputs;
        }

        private double[] GetOutputArray(Drawing loadedShape)
        {
            // array of all 0.0 doubles except at the position where the shape matches the one of the drawing. E.g. 0, 1, 0
            return (from shape in _possibleShapes
                    select shape == loadedShape.ShapeName ? 1.0 : 0.0).ToArray();
        }

        private double[][] GetInputs()
        {
            var inputs = new double[CurrentTestPack.TrainingShapes.Count][];
            var crnt = 0;
            foreach (var loadedShape in CurrentTestPack.TrainingShapes)
            {
                inputs[crnt] = GetInputArray(loadedShape);
                crnt += 1;
            }
            return inputs;
        }

        private static double[] GetInputArray(Drawing loadedShape)
        {
            return loadedShape.InBytes.Select(x => Convert.ToDouble(x) / 255).ToArray(); // a value between 0 and 1, with 1 being 255 so full white
        }

        private void ConvertDrawings()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                BitmapsWithResults.Clear();
                TestBitmapsWithResults.Clear();
            });
            foreach (var drawingFile in CurrentTestPack.TrainingShapes)
            {

                var content = File.ReadAllText(drawingFile.FileName);
                var drawing = DrawHelper.Deserialize(content);

                // add all loaded points to the drawingFile array
                drawingFile.Points.Clear();
                foreach (var point in drawing.Points)
                {
                    drawingFile.Points.Add(point);
                }
                drawingFile.ShapeName = new Regex("([A-Za-z])+").Match(drawingFile.FileName.Split('\\').Last().Split('.').First()).Value;

                byte[] inBytes;

                var convertedBitmapWithResult = new ConvertedBitmapWithResult { BitmapImage = ConvertDrawing(drawing, out inBytes), Brush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 255, 255)) };
                Application.Current.Dispatcher.Invoke(() => { BitmapsWithResults.Add(convertedBitmapWithResult); });

                drawingFile.InBytes = inBytes;
            }

            foreach (var drawingFile in CurrentTestPack.TestShapes)
            {

                var content = File.ReadAllText(drawingFile.FileName);
                var drawing = DrawHelper.Deserialize(content);

                // add all loaded points to the drawingFile array
                drawingFile.Points.Clear();
                foreach (var point in drawing.Points)
                {
                    drawingFile.Points.Add(point);
                }
                drawingFile.ShapeName = new Regex("([A-Za-z])+").Match(drawingFile.FileName.Split('\\').Last().Split('.').First()).Value;


                byte[] inBytes;
                var convertedBitmapWithResult = new ConvertedBitmapWithResult { BitmapImage = ConvertDrawing(drawing, out inBytes), Brush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 255, 255)) };
                Application.Current.Dispatcher.Invoke(() => { TestBitmapsWithResults.Add(convertedBitmapWithResult); });

                drawingFile.InBytes = inBytes;
            }
            // list containing all possible shapes
            _possibleShapes = CurrentTestPack.TrainingShapes.Select(x => x.ShapeName).Distinct().ToList();
        }

        private BitmapImage ConvertDrawing(Drawing drawing, out byte[] drawingInBytes)
        {
            var pictureResolution = (int)Math.Sqrt(CurrentTestPack.AmountInputs);

            var b = new Bitmap(pictureResolution, pictureResolution, PixelFormat.Format8bppIndexed);
            var ncp = b.Palette;
            for (var i = 0; i < 256; i++)
                ncp.Entries[i] = Color.FromArgb(255, i, i, i);
            b.Palette = ncp;

            var boundsRect = new Rectangle(0, 0, pictureResolution, pictureResolution);
            var bmpData = b.LockBits(boundsRect, ImageLockMode.WriteOnly, b.PixelFormat);

            var ptr = bmpData.Scan0;

            var bytes = pictureResolution * pictureResolution;
            var rgbValues = new byte[bytes];


            Action<int, int, byte> applyPixel = (xPos, yPos, strength) =>
            {
                var index = yPos * pictureResolution + xPos;
                if (rgbValues[index] < strength)
                    rgbValues[index] = strength;
            };


            var minX = drawing.Points.Min(x => x.X);
            var minY = drawing.Points.Min(x => x.Y);
            var maxX = drawing.Points.Max(x => x.X);
            var maxY = drawing.Points.Max(x => x.Y);
            var lastPoint = drawing.Points.Last();
            foreach (var drawingPoint in drawing.Points)
            {

                var diffX = lastPoint.X - drawingPoint.X;
                var diffY = lastPoint.Y - drawingPoint.Y;
                var angle = Math.Atan2(diffY, diffX) + Math.PI;
                var dist = Math.Sqrt(diffX * diffX + diffY * diffY);
                var crntDist = 0.0;
                while (crntDist < dist)
                {
                    crntDist += 1.0;
                    if (crntDist > dist) crntDist = dist;
                    var crntX = lastPoint.X + Math.Cos(angle) * crntDist;
                    var crntY = lastPoint.Y + Math.Sin(angle) * crntDist;

                    // minus one, because it starts at 0, so the max. amount we're allowed to get is the max -1. e.g. for 28 pixels, 27
                    // remove additional 2 and add 1 in the end, so the x and y value never reach the exact minimum and maximum. That's because we add pixel to the surroundings anyway
                    var x = (int)Math.Round((crntX - minX) / (maxX - minX) * (pictureResolution - 3)) + 1;
                    var y = (int)Math.Round((crntY - minY) / (maxY - minY) * (pictureResolution - 3)) + 1;
                    if (x >= pictureResolution)
                        x = pictureResolution - 1;
                    if (y >= pictureResolution)
                        y = pictureResolution - 1;
                    if (x < 0)
                        x = 0;
                    if (y < 0)
                        y = 0;

                    applyPixel(x, y, 255);
                    if (x > 0) applyPixel(x - 1, y, 255);
                    if (y > 0) applyPixel(x, y - 1, 255 / 2);
                    if (x > 0 && y > 0) applyPixel(x - 1, y - 1, 255 / 2);
                    if (x < pictureResolution - 1) applyPixel(x + 1, y, 255 / 2);
                    if (y < pictureResolution - 1) applyPixel(x, y + 1, 255 / 2);
                    if (x < pictureResolution - 1 && y < pictureResolution - 1) applyPixel(x + 1, y + 1, 255 / 2);
                }


                lastPoint = drawingPoint;
            }

            drawingInBytes = rgbValues;

            Marshal.Copy(rgbValues, 0, ptr, bytes);
            b.UnlockBits(bmpData);
            return BitmapToImageSource(b);
        }

        BitmapImage BitmapToImageSource(Image bitmap)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Bmp);
                memory.Position = 0;
                var bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        private void SelectShape(Drawing obj)
        {
            SelectedDrawing = obj;
        }

        private void LoadShapes()
        {
            CurrentTestPack.TrainingShapes.Clear();
            var openFileDialog1 = new OpenFileDialog
            {
                Title = @"Select the input pictures",
                Filter = @"XML files (*.xml)|*.xml|All files (*.*)|*.*",
                Multiselect = true
            };

            // when user pressed "OK" save the file paths
            if (openFileDialog1.ShowDialog() != true) return;

            foreach (var crntFileName in openFileDialog1.FileNames)
            {
                var content = File.ReadAllText(crntFileName);
                var drawing = DrawHelper.Deserialize(content);
                drawing.FileName = crntFileName;
                // adjust points
                if (drawing.Points.Count == 0) continue;
                var minX = drawing.Points.Min(x => x.X);
                var minY = drawing.Points.Min(x => x.Y);
                var maxX = drawing.Points.Max(x => x.X);
                var maxY = drawing.Points.Max(x => x.Y);
                foreach (var drawingPoint in drawing.Points)
                {
                    drawingPoint.X -= minX;
                    drawingPoint.Y -= minY;
                    drawingPoint.X = drawingPoint.X / (maxX - minX) * 300;
                    drawingPoint.Y = drawingPoint.Y / (maxY - minY) * 300;
                }
                var fileName = crntFileName.Split('\\').Last();

                drawing.ShapeName = new Regex("([A-Za-z])+").Match(fileName).Value;
                CurrentTestPack.TrainingShapes.Add(drawing);
            }
            CurrentTestPack.TrainingShapes.Shuffle();
        }

        private void LoadTestShapes()
        {
            CurrentTestPack.TestShapes.Clear();
            var openFileDialog1 = new OpenFileDialog
            {
                Title = @"Select the input pictures",
                Filter = @"XML files (*.xml)|*.xml|All files (*.*)|*.*",
                Multiselect = true
            };

            // when user pressed "OK" save the file paths
            if (openFileDialog1.ShowDialog() != true) return;

            foreach (var crntFileName in openFileDialog1.FileNames)
            {
                var content = File.ReadAllText(crntFileName);
                var drawing = DrawHelper.Deserialize(content);
                drawing.FileName = crntFileName;
                // adjust points
                if (drawing.Points.Count == 0) continue;
                var minX = drawing.Points.Min(x => x.X);
                var minY = drawing.Points.Min(x => x.Y);
                var maxX = drawing.Points.Max(x => x.X);
                var maxY = drawing.Points.Max(x => x.Y);
                foreach (var drawingPoint in drawing.Points)
                {
                    drawingPoint.X -= minX;
                    drawingPoint.Y -= minY;
                    drawingPoint.X = drawingPoint.X / (maxX - minX) * 300;
                    drawingPoint.Y = drawingPoint.Y / (maxY - minY) * 300;
                }
                var fileName = crntFileName.Split('\\').Last();

                drawing.ShapeName = new Regex("([A-Za-z])+").Match(fileName).Value;
                CurrentTestPack.TestShapes.Add(drawing);
            }
            CurrentTestPack.TestShapes.Shuffle();
        }
    }

    public class ConvertedBitmapWithResult : INotifyPropertyChanged
    {
        private Brush _brush;
        private BitmapImage _bitmapImage;

        public BitmapImage BitmapImage {
            get { return _bitmapImage; }
            set {
                _bitmapImage = value;
                OnPropertyChanged();
            }
        }

        public Brush Brush {
            get { return _brush; }
            set {
                _brush = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
