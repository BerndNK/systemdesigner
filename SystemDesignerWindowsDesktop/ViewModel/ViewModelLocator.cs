﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using SystemDesignerWindowsDesktop.Model;

namespace SystemDesignerWindowsDesktop.ViewModel {
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class ViewModelLocator {
        static ViewModelLocator() {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            if (ViewModelBase.IsInDesignModeStatic) {
                SimpleIoc.Default.Register<IDataService, Design.DesignDataService>();
            } else {
                SimpleIoc.Default.Register<IDataService, DataService>();
            }

            SimpleIoc.Default.Register<AppWindowViewModel>();
            SimpleIoc.Default.Register<MainStageViewModel>();
            SimpleIoc.Default.Register<NeuralNetworkTesterViewModel>();
        }

        /// <summary>
        /// Gets the application window view model.
        /// </summary>
        /// <value>
        /// The application window view model.
        /// </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public AppWindowViewModel AppWindowViewModel => ServiceLocator.Current.GetInstance<AppWindowViewModel>();

        /// <summary>
        /// Gets the main stage view model.
        /// </summary>
        /// <value>
        /// The main stage view model.
        /// </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public MainStageViewModel MainStageViewModel => new MainStageViewModel(ServiceLocator.Current.GetInstance<IDataService>());

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance",
            "CA1822:MarkMembersAsStatic",
            Justification = "This non-static member is needed for data binding purposes.")]
        public NeuralNetworkTesterViewModel NeuralNetworkTesterViewModel
            => ServiceLocator.Current.GetInstance<NeuralNetworkTesterViewModel>();

        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup() {
        }
    }
}