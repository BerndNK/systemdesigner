﻿using System;
using System.Collections.Generic;
using SystemDesigner.Neural;
using Encog.Engine.Network.Activation;
using Encog.ML.Data.Basic;
using Encog.Neural.Networks;
using Encog.Neural.Networks.Layers;
using Encog.Neural.Networks.Training.Propagation;
using Encog.Neural.Networks.Training.Propagation.Back;

namespace SystemDesignerWindowsDesktop.ViewModel {
    public class EncogManager : INeuralNetworkManager
    {
        public double Error => _train.Error;
        public string DebugInformation => "";
        public string Name => "Encog";

        private BasicNetwork _network;
        private BasicMLDataSet _trainingSet;
        private Propagation _train;

        public void Init(int inputSize, List<int> hiddenLayer, int outputLayerSize)
        {
            _network = new BasicNetwork();
            _network.AddLayer(new BasicLayer(null, true, inputSize));
            foreach (var i in hiddenLayer)
            {
                _network.AddLayer(new BasicLayer(new ActivationSigmoid(), true, i));
            }
            _network.AddLayer(new BasicLayer(new ActivationSigmoid(), false, outputLayerSize));
            _network.Structure.FinalizeStructure();
            _network.Reset();
        }

        public void SetTrainingData(double[][] input, double[][] desiredOutput)
        {
            _trainingSet = new BasicMLDataSet(input, desiredOutput);
            _train = new Backpropagation(_network, _trainingSet);
            //_train = new ResilientPropagation(_network, _trainingSet);
        }



        public double[] Compute(double[] inputArray)
        {
            var output = new double[_network.OutputCount];
            _network.Compute(inputArray, output);
            return output;
        }

        public void Iteration(double learningRate)
        {
#if DEBUG
            if (_network == null || _trainingSet == null || _train == null) throw new InvalidOperationException();
#endif
            _train.Iteration();
        }
    }
}
