﻿using System.Windows.Media;

namespace SystemDesignerWindowsDesktop.ViewModel
{
    /// <summary>
    /// Extends the Matrix class with a value based equal check.
    /// </summary>
    public static class MatrixExtension
    {
        public static bool ValueEquals(this Matrix source, Matrix target)
        {
            if (source.Determinant != target.Determinant) return false;
            if (source.M11 != target.M11) return false;
            if (source.M12 != target.M12) return false;
            if (source.M21 != target.M21) return false;
            if (source.M22 != target.M22) return false;
            if (source.OffsetX != target.OffsetX) return false;
            if (source.OffsetY != target.OffsetY) return false;

            return true;
        }
    }
}
