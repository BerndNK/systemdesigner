﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using SystemDesigner.Log;

namespace SystemDesignerWindowsDesktop.Utils
{
    /// <summary>
    /// Interaction logic for LogDisplayWindow.xaml
    /// </summary>
    public partial class LogDisplayWindow :  INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        private static LogDisplayWindow _instance;

        public List<LogMessage> LogAll => LogManager.GetMessages(DebugLogger.GetInstance(), LogType.All).ToList();
        public List<LogMessage> LogInfo => LogManager.GetMessages(DebugLogger.GetInstance(), LogType.Info).ToList();
        public List<LogMessage> LogError => LogManager.GetMessages(DebugLogger.GetInstance(), LogType.Error).ToList();
        

        public static LogDisplayWindow GetInstance() => _instance ?? (_instance = new LogDisplayWindow());

        private LogDisplayWindow()
        {
            DebugLogger.GetInstance().MessageLogged += OnMessageLogged;
            

            DataContext = this;
            InitializeComponent();
        }

        private void OnMessageLogged(object sender, MessageLoggedEventArgs messageLoggedEventArgs)
        {
            OnPropertyChanged(nameof(LogAll));
            OnPropertyChanged(nameof(LogInfo));
            OnPropertyChanged(nameof(LogError));
        }


        #region UI Events
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _instance = null;
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        #endregion
    }
}
