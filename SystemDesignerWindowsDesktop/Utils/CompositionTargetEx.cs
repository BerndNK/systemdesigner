﻿using System;
using System.Windows.Media;

namespace SystemDesignerWindowsDesktop.Utils {
    /// <summary>
    /// Wraps the Rendering event from WPF to add a check whether the RenderingTime has changed. This is due the fact, that the Rendering event may be fired multiple times. Checking a difference in the RenderingTime will ensure that for each frame this new FrameUpdating event is only called once.
    /// </summary>
    /// <see cref="https://evanl.wordpress.com/2009/12/06/efficient-optimal-per-frame-eventing-in-wpf/"/>
    public static class CompositionTargetEx {

        private static TimeSpan _last = TimeSpan.Zero;

        private static event EventHandler<RenderingEventArgs> _frameUpdating;

        public static event EventHandler<RenderingEventArgs> FrameUpdating {
            add {
                if (_frameUpdating == null) CompositionTarget.Rendering += CompositionTarget_Rendering;
                _frameUpdating += value;
            }
            remove {
                _frameUpdating -= value;
                if (_frameUpdating == null) CompositionTarget.Rendering -= CompositionTarget_Rendering;
            }
        }

        private static void CompositionTarget_Rendering(object sender, EventArgs e) {
            var args = (RenderingEventArgs)e;
            if (args.RenderingTime == _last) return;
            _last = args.RenderingTime;
            _frameUpdating?.Invoke(sender, args);
        }
    }
}
