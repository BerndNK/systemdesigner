﻿using System;
using System.Windows.Media;

namespace SystemDesignerWindowsDesktop.Extensions {
    public static class ColorConvertExtension {
        public static Color ToWindowsColor(this UInt32 color) => Color.FromArgb((byte)(color >> 24 & 0xff),(byte) (color >> 16 & 0xff), (byte) (color >> 8 & 0xff), (byte) (color & 0xff));
    }
}
