﻿using System;
using System.Windows;
using SystemDesigner.Helpers;
using SystemDesigner.Input.Shortcuts;
using SystemDesigner.Log;
using SystemDesigner.Stage.Tools.Ui;
using SystemDesigner.Utils;
using GalaSoft.MvvmLight.Threading;
using MahApps.Metro;

namespace SystemDesignerWindowsDesktop {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {
        static App() {
            DispatcherHelper.Initialize();
        }

        protected override void OnStartup(StartupEventArgs e) {
            ThemeManager.AddAccent("DeepBlue", new Uri("pack://application:,,,/System Designer;component/Themes/DeepBlue.xaml"));
            
            var theme = ThemeManager.DetectAppStyle(Application.Current);
            ThemeManager.ChangeAppStyle(Application.Current, ThemeManager.GetAccent("DeepBlue"), theme.Item1);

            // so long as there is not actual debug window, we pass the DebugLoggs to the console
            DebugLogger.GetInstance().MessageLogged += (sender, args) => Console.WriteLine(args.Message);

            DeviceInfo.DeviceDpi = UiVariables.DefaultDpi; // wpf has scaling implemented, so we set the default dpi for windows

            base.OnStartup(e);
        }
    }
}
