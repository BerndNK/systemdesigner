﻿using System.Windows;
using System.Windows.Controls;

namespace SystemDesignerWindowsDesktop {
    /// <summary>
    /// Description for NeuralNetworkTester.
    /// </summary>
    public partial class NeuralNetworkTester : UserControl {
        /// <summary>
        /// Initializes a new instance of the NeuralNetworkTester class.
        /// </summary>
        public NeuralNetworkTester() {
            InitializeComponent();
        }
    }
}