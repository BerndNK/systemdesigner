﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using DisplayCore.Annotations;

namespace DisplayCore.Render.Geometry
{
    /// <summary>
    /// Describes a two dimensional point with a X and a Y value
    /// </summary>
    public class Point<T> : INotifyPropertyChanged where T : struct
    {
        private T _y;
        private T _x;

        /// <summary>
        /// Gets or sets the x.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        [XmlElement("X")]
        public T X
        {
            get { return _x; }
            set
            {
                _x = value; 
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        [XmlElement("Y")]
        public T Y
        {
            get { return _y; }
            set
            {
                _y = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Point"/> struct.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public Point(T x = default(T), T y = default(T))
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"{X},{Y}";
        }

        /// <summary>
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    /// <summary>
    /// Standard Point class which is a Point of double
    /// </summary>
    [XmlType(TypeName = "Point")]
    public class Point : Point<double>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Point"/> class.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public Point(double x = 0, double y = 0) : base(x, y)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Point"/> class.
        /// </summary>
        public Point() : base()
        {
            // used so Point may be serialized
        }

        /// <summary>
        /// Rotates the point around the given point by the given radians.
        /// </summary>
        /// <param name="pivotPoint">The other point which will be the pivot of rotation.</param>
        /// <param name="radians">The radians to rotate.</param>
        public Point RotateAroundPoint(Point pivotPoint, double radians)
        {
            // get distance and angle to the other point
            var distX = X - pivotPoint.X;
            var distY = Y - pivotPoint.Y;
            var dist = Math.Sqrt(distX * distX + distY * distY);
            var rot = Math.Atan2(distY, distX);

            // add the radians
            rot += radians;

            // set the new position
            return new Point(pivotPoint.X + Math.Cos(rot) * dist, pivotPoint.Y + Math.Sin(rot) * dist);
        }
    }
}