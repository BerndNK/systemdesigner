﻿using System;

namespace DisplayCore.Render.Geometry
{
    public class Line
    {
        public Point LineOrigin { get; set; }
        public Point LineEnd { get; set; }

        public Point Delta
        {
            get
            {
                var angle = Angle;
                var distance = Length;
                return new Point(Math.Cos(angle)*distance, Math.Sin(angle)*distance);
            }
        }

        public Line(Point lineOrigin, Point lineEnd)
        {
            LineOrigin = lineOrigin;
            LineEnd = lineEnd;
        }

        public Line()
        {
            
        }

        public Line WithSmallerLength(double lengthInPercent)
        {
            var angle = Angle;
            var distance = Length * lengthInPercent;
            return new Line(LineOrigin, new Point(LineOrigin.X + Math.Cos(angle)*distance, LineOrigin.Y + Math.Sin(angle)*distance));
        }

        public double Angle
        {
            get
            {
                var distX = LineEnd.X - LineOrigin.X;
                var distY = LineEnd.Y - LineOrigin.Y;
                
                return Math.Atan2(distY, distX);
            }
        }

        public double Length
        {
            get
            {
                
                var distX = LineEnd.X - LineOrigin.X;
                var distY = LineEnd.Y - LineOrigin.Y;
                
                return Math.Sqrt(distX * distX + distY * distY);
            }
        }

        public bool IntersectsWith(Line translatedLine)
        {
            // https://stackoverflow.com/questions/4977491/determining-if-two-line-segments-intersect/4977569#4977569
            var x00 = LineOrigin.X;
            var x10 = translatedLine.LineOrigin.X;
            var x01 = Delta.X;
            var x11 = translatedLine.Delta.X;

            var y00 = LineOrigin.Y;
            var y10 = translatedLine.LineOrigin.Y;
            var y01 = Delta.Y;
            var y11 = translatedLine.Delta.Y;

            var d = x11 * y01 - x01 * y11;

            var s = (1 / d) * ((x00 - x10) * y01 - (y00 - y10) * x01);
            var t = (1 / d) * -(-(x00 - x10) * y11 + (y00 - y10) * x11);

            return s > 0 && s < 1 && t > 0 && t < 1;
        }
    }
}
