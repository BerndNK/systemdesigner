﻿using System;
using System.Collections.Generic;

namespace DisplayCore.Render.Geometry {
    /// <summary>
    /// Describes a two dimensional area with a X, Y, width and height.
    /// </summary>
    public class Rectangle<T> where T:struct{
        /// <summary>
        /// Gets or sets the x.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        public T X { get; set; }
        /// <summary>
        /// Gets or sets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        public T Y { get; set; }
        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public T Width { get; set; }
        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public T Height { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Rectangle"/> struct.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="w">The w.</param>
        /// <param name="h">The h.</param>
        public Rectangle(T x, T y, T w, T h) {
            X = x;
            Y = y;
            Width = w;
            Height = h;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString() {
            return $"X: {X} Y: {Y} W: {Width} H: {Height}";
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj) {
            if (obj is Rectangle<T>) {
                return Equals((Rectangle<T>)obj);
            }
            return false;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode() {
            unchecked {
                var hashCode = X.GetHashCode();
                hashCode = (hashCode*397) ^ Y.GetHashCode() ;
                hashCode = (hashCode*397) ^ Width.GetHashCode();
                hashCode = (hashCode*397) ^ Height.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// Equals the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public bool Equals(Rectangle<T> obj) {
            return GetHashCode() == obj.GetHashCode();
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(Rectangle<T> a, Rectangle<T> b)
        {
            return a?.GetHashCode() == b?.GetHashCode();
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(Rectangle<T> a, Rectangle<T> b)
        {
            return !(a == b);
        }
    }

    /// <summary>
    /// Standard Rectangle class which is a Rectangle of double
    /// </summary>
    public class Rectangle : Rectangle<double> {
        /// <summary>
        /// Initializes a new instance of the <see cref="Rectangle"/> class.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="w">The w.</param>
        /// <param name="h">The h.</param>
        public Rectangle(double x = 0, double y = 0, double w = 0, double h = 0) : base(x,y,w,h) {
            
        }

        public bool Intersects(Rectangle withRectangle)
        {
            return !(withRectangle.X + withRectangle.Width < X
                     || withRectangle.X > X + Width
                     || withRectangle.Y + withRectangle.Height < Y
                     || withRectangle.Y > Y + Height);
        }

        public Line TopEdge => new Line(new Point(X, Y), new Point(X + Width, Y));
        public Line RightEdge => new Line(new Point(X+Width,Y), new Point(X + Width, Y+Height));
        public Line BottomEdge => new Line(new Point(X,Y+Height), new Point(X + Width, Y+Height));
        public Line LeftEdge => new Line(new Point(X,Y), new Point(X, Y+Height));

        public IEnumerable<Line> GetBoundaryLines()
        {
            yield return TopEdge;
            yield return RightEdge;
            yield return BottomEdge;
            yield return LeftEdge;
        }

        /// <summary>
        /// Returns a new Rectangle which surrounds this Rectangle after a rotation around a point.
        /// </summary>
        /// <param name="pivotPoint">The pivot point of the rotation.</param>
        /// <param name="rotation">The angle to rotate in radians.</param>
        /// <returns>New Rectangle surrounding the original rectangle after rotation. Note if the angle = 0, it's equivalent to the original.</returns>
        public Rectangle RotateAroundPoint(Point pivotPoint, double rotation) {
            // get the upper left and lower right points describing the bounding box
            var upperLeft = new Point(X, Y);
            var lowerRight = new Point(X + Width, Y + Height);
            var upperRight = new Point(X + Width, Y);
            var lowerLeft = new Point(X, Y + Height);
            // rotate them both around the rotationPoint
            upperLeft = upperLeft.RotateAroundPoint(pivotPoint, rotation);
            lowerRight = lowerRight.RotateAroundPoint(pivotPoint, rotation);
            upperRight = upperRight.RotateAroundPoint(pivotPoint, rotation);
            lowerLeft = lowerLeft.RotateAroundPoint(pivotPoint, rotation);

            var lowestX = Math.Min(Math.Min(upperLeft.X, lowerRight.X), Math.Min(upperRight.X, lowerLeft.X));
            var lowestY = Math.Min(Math.Min(upperLeft.Y, lowerRight.Y), Math.Min(upperRight.Y, lowerLeft.Y));

            var highestX = Math.Max(Math.Max(upperLeft.X, lowerRight.X), Math.Max(upperRight.X, lowerLeft.X));
            var highestY = Math.Max(Math.Max(upperLeft.Y, lowerRight.Y), Math.Max(upperRight.Y, lowerLeft.Y));

            // convert the points back to X,Y,Width,Height and return it as a new rectangle
            return new Rectangle(lowestX, lowestY, highestX-lowestX, highestY-lowestY);
        }
    }
}