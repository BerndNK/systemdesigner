﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace DisplayCore.Render.Geometry
{

    /// <summary>
    /// Provides extensions methods for the classes in the Geometry package.
    /// </summary>
    public static class GeometryExtensions
    {

        /// <summary>
        /// Gets the combined bounding box of all given rectangles.
        /// </summary>
        /// <param name="list">List of rectangles.</param>
        /// <returns>New rectangle which spans all of the given rectangles.</returns>
        public static Rectangle GetCombinedBoundingBox(this IEnumerable<Rectangle> list)
        {
            // track the boundaries of all rectangles.
            var minX = double.MaxValue;
            var maxX = double.MinValue;
            var minY = double.MaxValue;
            var maxY = double.MinValue;

            foreach (var crnt in list)
            {
                // get the most left x position and most up y position
                if (crnt.X < minX) minX = crnt.X;
                if (crnt.Y < minY) minY = crnt.Y;
                // get the most right boundary (x + width)
                if (crnt.X + crnt.Width > maxX) maxX = crnt.X + crnt.Width;
                // analog with y and height
                if (crnt.Y + crnt.Height > maxY) maxY = crnt.Y + crnt.Height;
            }

            // transform these boundary values to x,y,width and height
            return new Rectangle(minX, minY, maxX - minX, maxY - minY);
        }

        /// <summary>
        /// Replaces the ref Rectangle if its values are different from [this] rectangle.
        /// </summary>
        /// <typeparam name="T">Type of the containing instance for the rectangle that shall be replaced</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="target">The target which contains the rectangle to be replaced.</param>
        /// <param name="outExpr">Expression pointing to the member to replace.</param>
        public static void ReplaceIfDifferent<T>(this Rectangle source, T target, Expression<Func<T, Rectangle>> outExpr)
        {
            var expr = (MemberExpression)outExpr.Body;
            var prop = (PropertyInfo)expr.Member;
            if (source.GetHashCode() != prop.GetValue(target)?.GetHashCode())
            {
                prop.SetValue(target, source, null);
            }
        }


        /// <summary>
        /// Replaces the ref Point if its values are different from [this] Point.
        /// </summary>
        /// <typeparam name="T">Type of the containing instance for the rectangle that shall be replaced</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="target">The target which contains the rectangle to be replaced.</param>
        /// <param name="outExpr">Expression pointing to the member to replace.</param>
        public static void ReplaceIfDifferent<T>(this Point source, T target, Expression<Func<T, Point>> outExpr)
        {
            var expr = (MemberExpression)outExpr.Body;
            var prop = (PropertyInfo)expr.Member;
            var targetPoint = prop.GetValue(target) as Point;
            if (targetPoint == null || Math.Abs(source.X - targetPoint.X) > 0.0000001 || Math.Abs(source.Y - targetPoint.Y) > 0.0000001)
            {
                prop.SetValue(target, source, null);
            }
        }
    }
}
