﻿using System;
using DisplayCore.Render.Geometry;

namespace DisplayCore.Render {
    /// <summary>
    /// Handles the render cycle of a IRenderable and a IRenderer
    /// </summary>
    public class RendererHandler {
        #region Events
        /// <summary>
        /// Describes a event handler for a render request event
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="renderBoundingBox">The render bounding box.</param>
        /// <param name="rotation">The rotation.</param>
        /// <param name="rotationPoint">The rotation pivot. (Around which point the target shall be rotated)</param>
        /// <param name="rotatedBoundingBox">The rotated bounding box. (Bounding box surround the boundingBox after the rotation)</param>
        /// <param name="alpha"></param>
        public delegate void RenderEventHandler(IRenderable target, Rectangle renderBoundingBox, double rotation, Point rotationPoint, Rectangle rotatedBoundingBox, double alpha); // event handler
        /// <summary>
        /// Occurs when a IRenderable wants to get rendered. This event is listened to by this RendererHandler
        /// </summary>
        public static event RenderEventHandler RenderRequest; // public Render Event. If a display object wants to get rendered, this event is called
        #endregion


        #region Fields
        private bool _addedListener; // whether the listener is currently attached or not
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the renderer.
        /// </summary>
        /// <value>
        /// The renderer.
        /// </value>
        public IRenderer Renderer { get; set; }
        #endregion

        /// <summary>
        /// Handles a Render procedure for the target item
        /// </summary>
        /// <param name="root">Root of the display tree which shall be rendered</param>
        /// <exception cref="InvalidOperationException">Thrown when method is called when there is no Renderer set</exception>
        public void Render(IRenderable root) {
            if (root == null) throw new ArgumentNullException(nameof(root));
            if (Renderer == null) throw new InvalidOperationException("Cannot Render when the Renderer is not set");
            StartRenderListener();

            Renderer.RenderBegin();
            root.InvokeRender();
            Renderer.RenderEnd();

            StopRenderListener();
        }

        /// <summary>
        /// Adds a Event Listener to the SvgItem.Render Event
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when method is called when Listener is already started</exception>
        private void StartRenderListener() {
            if (_addedListener) throw new InvalidOperationException("Listener is already started");
            _addedListener = true;
            RenderRequest += Renderer.Render;
        }

        /// <summary>
        /// Removes the Event Listener from the SvgItem.Render Event
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when method is called when Listener is not started</exception>
        private void StopRenderListener() {
            if (!_addedListener) throw new InvalidOperationException("Listener is not started");
            _addedListener = false;
            RenderRequest -= Renderer.Render;
        }

        /// <summary>
        /// Calls the RenderRequest event
        /// </summary>
        /// <param name="renderable"><c>Renderable</c> target that wants to be rendered</param>
        /// <param name="boundingBox">Bounding box of the <c>Renderable</c></param>
        /// <param name="rotation">Rotation to render the <c>Renderable</c> in</param>
        /// <param name="rotationPivot">The rotation pivot.</param>
        /// <param name="rotatedBoundingBox">The rotated bounding box. (Bounding box surround the boundingBox after the rotation)</param>
        /// <param name="alpha"></param>
        public static void InvokeRender(IRenderable renderable, Rectangle boundingBox, double rotation, Point rotationPivot, Rectangle rotatedBoundingBox, double alpha) {
            RenderRequest?.Invoke(renderable, boundingBox, rotation, rotationPivot, rotatedBoundingBox, alpha);
        }
    }
}