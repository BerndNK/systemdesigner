﻿using System.Collections;
using System.Collections.Generic;

namespace DisplayCore.Render.Utils {
    /// <summary>
    /// Provides a elegant way to have multiple Renderables stored
    /// </summary>
    public class RenderableBatch : ICollection<IRenderable> {
        #region Fields
        private readonly List<IRenderable> _renderables;
        #endregion
        
        #region Properties
        #region ICollection<IRenderable> Implementation
        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count => _renderables.Count;
        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly => true;
        #endregion
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderableBatch"/> class.
        /// </summary>
        public RenderableBatch() {
            _renderables = new List<IRenderable>();
            // TODO write unit tests for this class
        }

        /// <summary>
        /// Renders the specified renderer handler.
        /// </summary>
        /// <param name="rendererHandler">The renderer handler.</param>
        public void Render(RendererHandler rendererHandler) {
            foreach (var renderable in _renderables) {
                rendererHandler.Render(renderable);
            }
        }

        #region ICollection<IRenderable> Implementation

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<IRenderable> GetEnumerator() => _renderables.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        public void Add(IRenderable item) {
            _renderables.Add(item);
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear() {
            _renderables.Clear();
        }

        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(IRenderable item) => _renderables.Contains(item);

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        public void CopyTo(IRenderable[] array, int arrayIndex) {
            _renderables.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove(IRenderable item) => _renderables.Remove(item);

        #endregion
    }
}
