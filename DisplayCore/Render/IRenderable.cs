﻿namespace DisplayCore.Render {
    /// <summary>
    /// Describes a object which can be rendered by a IRenderer
    /// </summary>
    public interface IRenderable {
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        int Id { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is visible.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is visible; otherwise, <c>false</c>.
        /// </value>
        bool IsVisible { get; set; }

        /// <summary>
        /// Tells the item to render. A Container will give this call to its children, while a standard DisplayItem will call RendererHandler's Render Event
        /// </summary>
        /// <param name="parentPivotX">Pivot X of this parent (including parent parents. Therefore its not equal parent.PivotX)</param>
        /// <param name="parentPivotY">Pivot Y of this parent (including parent parents. Therefore its not equal parent.PivotY)</param>
        /// <param name="parentScaleX">Scale X of this parent (including parent parents. Therefore its not equal parent.ScaleX)</param>
        /// <param name="parentScaleY">Scale Y of this parent (including parent parents. Therefore its not equal parent.ScaleY)</param>
        /// <param name="parentRotation">Rotation of this parent (including parent parents. Therefore its not equal parent.Rotation)</param>
        /// <param name="alpha"></param>
        void InvokeRender(double parentPivotX = 0, double parentPivotY = 0, double parentScaleX = 1, double parentScaleY = 1, double parentRotation = 0, double alpha = 1);
    }
}
