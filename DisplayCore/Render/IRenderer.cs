﻿using DisplayCore.Display;
using DisplayCore.Render.Geometry;

namespace DisplayCore.Render {
    /// <summary>
    /// Describes a object which can render a IRenderable
    /// </summary>
    public interface IRenderer {

        /// <summary>
        /// Notifies the Renderer that a batch call of the method Render is imminent and therefore is told to prepare to render process
        /// </summary>
        void RenderBegin();

        /// <summary>
        /// Notifies the Renderer that the render procedure has finished and therefore is told to clean up after the rendering
        /// </summary>
        void RenderEnd();

        /// <summary>
        /// Render function that renders the target
        /// </summary>
        /// <param name="target">SvgItem to draw</param>
        /// <param name="renderBoundingBox">Bounding box for the target</param>
        /// <param name="rotation">Rotation of the target</param>
        /// <param name="rotationPoint">The rotation point.</param>
        /// <param name="rotatedBoundingBox">The rotated bounding box. (Bounding box surround the boundingBox after the rotation)</param>
        /// <param name="alpha"></param>
        void Render(IRenderable target, Rectangle renderBoundingBox, double rotation, Point rotationPoint, Rectangle rotatedBoundingBox, double alpha);

    }
}