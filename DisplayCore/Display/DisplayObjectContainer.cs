﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using DisplayCore.Render.Geometry;

namespace DisplayCore.Display
{
    /// <summary>
    /// Implementation of IDisplayObjectContainer
    /// </summary>
    /// <typeparam name="T">The IDisplayObject this container is for</typeparam>
    /// <seealso cref="DisplayCore.Display.DisplayObject" />
    /// <seealso cref="System.Collections.Generic.ICollection{T}" />
    public abstract class DisplayObjectContainer<T> : DisplayObject, IDisplayObjectContainer, ICollection<T> where T : IDisplayObject
    {
        #region Fields
        /// <summary>
        /// The children of this container
        /// </summary>
        protected readonly List<T> _childs;
        private bool _childBoundaryChanged = true;

        #endregion

        #region Properties
        /// <summary>
        /// Gets the number children.
        /// </summary>
        /// <value>
        /// The number children.
        /// </value>
        public int NumChildren => _childs.Count;

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public override double Width {
            get {
                CalculateBaseSize();
                return base.Width;
            }
            set {
                CalculateBaseSize();
                base.Width = value;
                CalculateBaseSize(true);
                ChildrenChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public override double Height {
            get {
                CalculateBaseSize();
                return base.Height;
            }
            set {
                CalculateBaseSize();
                base.Height = value;
                CalculateBaseSize(true);
                ChildrenChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets or sets the scale x.
        /// </summary>
        /// <value>
        /// The scale x.
        /// </value>
        public override double ScaleX {
            get { return base.ScaleX; }
            set {
                base.ScaleX = value;
                CalculateBaseSize(true);
                ChildrenChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets or sets the scale y.
        /// </summary>
        /// <value>
        /// The scale y.
        /// </value>
        public override double ScaleY {
            get { return base.ScaleY; }
            set {
                base.ScaleY = value;
                CalculateBaseSize(true);
                ChildrenChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Occurs when one or more [children changed].
        /// </summary>
        public event EventHandler ChildrenChanged;

        #region ICollection<T>

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public int Count => _childs.Count;
        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" /> is read-only.
        /// </summary>
        public bool IsReadOnly => false;

        #endregion

        protected override Rectangle LocalRotatedBoundingBox {
            get {
                CalculateBaseSize();
                return LocalRotatedBoundingBoxCache;
            }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayObjectContainer{T}"/> class.
        /// </summary>
        protected DisplayObjectContainer()
        {
            _childs = new List<T>();
            PropertyChanged += (sender, args) =>
            {
                // for certain changed properties, ChildrenChanged has been to raised
                switch (args.PropertyName)
                {
                    case nameof(X):
                    case nameof(Y):
                    case nameof(Rotation):
                        _childBoundaryChanged = true;
                        ChildrenChanged?.Invoke(this, EventArgs.Empty);
                        break;
                }
            };
        }

        /// <summary>
        /// Hits the test point.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns></returns>
        public override bool HitTestPoint(double x, double y)
        {
            var translatedPoint = TranslatePoint(new Point(x, y));

            return _childs.Any(child => child.HitTestPoint((double)translatedPoint.X, (double)translatedPoint.Y));
        }

        /// <summary>
        /// Hits the test rect.
        /// </summary>
        /// <param name="rect">The rect.</param>
        /// <returns></returns>
        public override bool HitTestRect(Rectangle rect)
        {
            var translatedRect = TranslateRectangle(rect);

            return _childs.Any(x => x.HitTestRect(translatedRect));
        }

        /// <summary>
        /// Tells this container that one of it's children has had a transformation change
        /// </summary>
        public void RegisterChildBoundaryChange()
        {
            _childBoundaryChanged = true; // set a flag that a change has been made
        }

        /// <summary>
        /// Checks if the rectangle is intersecting with one of the containers children. Returning all children which were hit. Implying that the point is within the same parent.
        /// </summary>
        /// <param name="rect">The rectangle to check with values in this item's local coordinate system</param>
        /// <returns>
        /// All children which were hit, empty list for no hit
        /// </returns>
        public IList<T> HitTestRectAll(Rectangle rect)
        {
            var translatedRect = TranslateRectangle(rect);
            return _childs.Where(x => x.HitTestRect(translatedRect)).ToList();
        }

        /// <summary>
        /// Checks if the point is intersecting with one of the containers children. Returning all children which were hit. Implying that the point is within the same parent.
        /// </summary>
        /// <param name="x">X Position of the point in this item's local coordinate system</param>
        /// <param name="y">Y Position of the point in this item's local coordinate system</param>
        /// <returns>
        /// All children which were hit, empty list for no hit
        /// </returns>
        public IList<T> HitTestPointAll(double x, double y)
        {
            var translatedPoint = TranslatePoint(new Point(x, y));
            return _childs.Where(child => child.HitTestPoint((double)translatedPoint.X, (double)translatedPoint.Y)).ToList();
        }



        /// <summary>
        /// Translates a rectangle into this display objects grid.
        /// </summary>
        /// <param name="rect">The rect.</param>
        /// <returns></returns>
        private Rectangle TranslateRectangle(Rectangle rect)
        {
            var translatedRect = new Rectangle(rect.X, rect.Y, rect.Width, rect.Height);
            translatedRect.X -= X;
            translatedRect.Y -= Y;
            translatedRect.X /= ScaleX;
            translatedRect.Y /= ScaleY;
            translatedRect.Width /= ScaleX;
            translatedRect.Height /= ScaleY;

            return translatedRect;
        }

        /// <summary>
        /// Translates a point into this display objects grid.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns></returns>
        private Point TranslatePoint(Point point)
        {
            var translatedPoint = new Point((point.X - X) / ScaleX, (point.Y - Y) / ScaleY); // given point inside this

            if (Math.Abs(Rotation) < 0.01) return translatedPoint;

            // calculate new resulting x, y positions
            // get distance to pivot of the display object
            var distance = Math.Sqrt(Math.Pow(translatedPoint.X, 2) + Math.Pow(translatedPoint.Y, 2));

            // get current angle between upper left position of display object and the Pivot of the parent
            var crntRot = Math.Atan2(translatedPoint.Y, translatedPoint.X);

            // add parent rotation to current rotation and calculate the corresponding x/y values for that angle
            var rotX = (Math.Cos(crntRot - Rotation) * distance);
            var rotY = (Math.Sin(crntRot - Rotation) * distance);

            // add parent Pivot for final x/y position on the screen
            translatedPoint.X = PivotX + rotX;
            translatedPoint.Y = PivotY + rotY;

            return translatedPoint;
        }


        /// <summary>
        /// Checks if the rectangle is intersecting with one of the containers children. Returning the first children which is hit. Implying that the point is within the same parent.
        /// </summary>
        /// <param name="rect">The rectangle to check with values in this item's local coordinate system</param>
        /// <returns>
        /// First children which was hit, empty list for no hit
        /// </returns>
        public T HitTestRectFirst(Rectangle rect)
        {
            var translatedRect = TranslateRectangle(rect);
            // use index to go from the most top child to the bottom, because we want the most forefront item to be hit first
            for (var i = _childs.Count - 1; i >= 0; i--)
            {
                var crnt = _childs[i];
                if (crnt.HitTestRect(translatedRect)) return crnt;
            }

            return default(T);
        }

        /// <summary>
        /// Checks if the point is intersecting with one of the containers children. Returning the first children which is hit. Implying that the point is within the same parent.
        /// </summary>
        /// <param name="x">X Position of the point in this item's local coordinate system</param>
        /// <param name="y">Y Position of the point in this item's local coordinate system</param>
        /// <returns>
        /// First children which was hit, empty list for no hit
        /// </returns>
        public T HitTestPointFirst(double x, double y)
        {
            var translatedPoint = TranslatePoint(new Point(x, y));
            // use index to go from the most top child to the bottom, because we want the most forefront item to be hit first
            for (var i = _childs.Count - 1; i >= 0; i--)
            {
                var crnt = _childs[i];
                if (crnt.HitTestPoint((double)translatedPoint.X, (double)translatedPoint.Y)) return crnt;
            }

            return default(T);
        }


        /// <summary>
        /// Iterates through the child's and calculate the base width/height, pivotX/Y and middleX/Y values
        /// </summary>
        /// <param name="force">Force the execution. (Ignore if child's have changed or not)</param>
        protected void CalculateBaseSize(bool force = false)
        {
            if (!_childBoundaryChanged && !force) return;
            _childBoundaryChanged = false;

            // check boundaries of each child if the succeed the minX/Y maxX/Y
            var combined = _childs
                .Select(x => new Rectangle(x.LeftBoundary, x.TopBoundary, x.BottomBoundary - x.TopBoundary,
                    x.RightBoundary - x.LeftBoundary)).GetCombinedBoundingBox();

            var minX = combined.X;
            var minY = combined.Y;
            var maxX = combined.X + combined.Width;
            var maxY = combined.Y + combined.Height;

            BaseWidth = combined.Width;
            BaseHeight = combined.Height;

            if (BaseWidth < 0) BaseWidth = 0;
            if (BaseHeight < 0) BaseHeight = 0;
            CachedWidth = BaseWidth * ScaleX;
            CachedHeight = BaseHeight * ScaleY;
            if (Math.Abs(BaseWidth) < 0.01) _pivotX = 0;
            else _pivotX /= BaseWidth;
            if (Math.Abs(BaseHeight) < 0.01) _pivotY = 0;
            else _pivotY /= BaseHeight;

            LocalRotatedBoundingBoxCache = new Rectangle(X - PivotX + minX, Y - PivotY + minY, (maxX - minX) * ScaleX, (maxY - minY) * ScaleY);
        }

        /// <summary>
        /// Adds multiple DisplayObjects to this container
        /// </summary>
        /// <param name="childs">The child's to add</param>
        /// <exception cref="ArgumentNullException">Thrown when child's is null</exception>
        /// <exception cref="ArgumentException">Thrown when at least one child has it's parent already set or one of the child is it's own container</exception>
        public void AddMultiple(IEnumerable<T> childs)
        {
            if (childs == null) throw new ArgumentNullException(nameof(childs));

            // promote child's to list to avoid multiple enumerations
            var asList = new List<T>(childs);
            if (asList.Any(child => child.Parent != null))
            {
                throw new ArgumentException("At least one child has it's parent already set", nameof(childs));
            }
            foreach (var item in asList)
            {
                Add(item);
            }
        }

        /// <summary>
        /// Adds multiple DisplayObjects to this container
        /// </summary>
        /// <param name="childs">The child's to add</param>
        /// <exception cref="ArgumentNullException">Thrown when child's is null</exception>
        /// <exception cref="ArgumentException">Thrown when at least one child has it's parent already set or one of the child is it's own container</exception>
        public void AddMultiple(params T[] childs)
        {
            AddMultiple(childs.AsEnumerable());
        }

        /// <summary>
        /// Adds a child at a specific display index
        /// </summary>
        /// <param name="child">The child to add</param>
        /// <param name="index">Where the child shall be pushed at</param>
        /// <exception cref="ArgumentNullException">Thrown when child is null</exception>
        /// <exception cref="ArgumentException">Thrown when child already has a parent or a child is it's own container</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when index is out of the bounds of this containers child's</exception>
        public void AddAt(T child, int index)
        {
            if (child == null) throw new ArgumentNullException(nameof(child));
            if (child.Parent != null) throw new ArgumentException("Child already has a parent", nameof(child));
            if (index < 0 || index > _childs.Count) throw new ArgumentOutOfRangeException(nameof(index), index, "Index must be within bounds of child's");

            // remove all child's after the index
            // store each in a temp list
            var removed = new List<T>();
            for (var i = index; i < _childs.Count; i++)
            {
                removed.Add(_childs[i]);
            }
            // remove from child list
            _childs.RemoveRange(index, _childs.Count - index);

            // add new child
            Add(child);

            // add previously removed child's
            foreach (var item in removed)
            {
                _childs.Add(item);
            }
        }

        /// <summary>
        /// Removes a child at a specific index
        /// </summary>
        /// <param name="index">Index of the child which shall be removed</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when given index is outside of the bounds of the child's of this container</exception>
        public void RemoveAt(int index)
        {
            if (index < 0 || index >= _childs.Count) throw new ArgumentOutOfRangeException(nameof(index), index, "Given index was outside of bounds of the child's");

            Remove(_childs[index]);
        }

        /// <summary>
        /// Gets the child at the given index
        /// </summary>
        /// <param name="index">Index of the child which shall be returned</param>
        /// <returns>Child which is at the given index</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when index is outside of the bounds of the children of this container</exception>
        public T GetChildAt(int index)
        {
            if (index < 0 || index >= _childs.Count) throw new ArgumentOutOfRangeException(nameof(index), index, "Index must be within bounds of the child's of this container");

            return _childs[index];
        }

        /// <summary>
        /// Gets the index for a child of this container
        /// </summary>
        /// <param name="child">The child of which the index is wanted</param>
        /// <returns>Index of the child or -1 if the item is not a child of this container</returns>
        /// <exception cref="ArgumentNullException">Thrown when child is null</exception>
        public int GetChildIndex(T child)
        {
            if (child == null) throw new ArgumentNullException(nameof(child));
            if (!Contains(child)) return -1;

            return _childs.IndexOf(child);
        }

        /// <summary>
        /// Changes the index of a child
        /// </summary>
        /// <param name="child">The child to change the index from</param>
        /// <param name="newIndex">New index of the child</param>
        /// <exception cref="ArgumentNullException">Thrown when child is null</exception>
        /// <exception cref="ArgumentException">Thrown when child is not actually a child of this container</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when index is outside of bounds of children</exception>
        public void SetChildIndex(T child, int newIndex)
        {
            if (child == null) throw new ArgumentNullException(nameof(child));
            if (child.Parent != this || !_childs.Contains(child)) throw new ArgumentException("Child is not a child of this container", nameof(child));
            if (newIndex < 0 || newIndex >= NumChildren) throw new ArgumentOutOfRangeException(nameof(newIndex), newIndex, "Index must be withing bounds of children of this container");

            SwapChildrenAt(_childs.IndexOf(child), newIndex);
        }

        /// <summary>
        /// Swaps the index of two children
        /// </summary>
        /// <param name="child1">First swap partner</param>
        /// <param name="child2">Second swap partner</param>
        /// <exception cref="ArgumentNullException">Thrown when child1 or child2 is null</exception>
        /// <exception cref="ArgumentException">Thrown when child1 or child2 are not children of this container</exception>
        public void SwapChildren(T child1, T child2)
        {
            if (child1 == null) throw new ArgumentNullException(nameof(child1));
            if (child2 == null) throw new ArgumentNullException(nameof(child2));
            if (child1.Parent != this || !_childs.Contains(child1)) throw new ArgumentException("Child is not a child of this container", nameof(child1));
            if (child2.Parent != this || !_childs.Contains(child2)) throw new ArgumentException("Child is not a child of this container", nameof(child2));

            SwapChildrenAt(_childs.IndexOf(child1), _childs.IndexOf(child2));
        }

        /// <summary>
        /// Swaps the index of two children
        /// </summary>
        /// <param name="index1">Index of the first swap partner</param>
        /// <param name="index2">Index of the second swap partner</param>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when index1 or index2 is outside of the bounds of the children of this container</exception>
        public void SwapChildrenAt(int index1, int index2)
        {
            if (index1 < 0 || index1 >= NumChildren) throw new ArgumentOutOfRangeException(nameof(index1), index1, "Index was outside of the bounds of the children of this container");
            if (index2 < 0 || index2 >= NumChildren) throw new ArgumentOutOfRangeException(nameof(index2), index2, "Index was outside of the bounds of the children of this container");

            // swap them in the list
            var tmp = _childs[index1];
            _childs[index1] = _childs[index2];
            _childs[index2] = tmp;
        }


        #region ICollection<T> Implementation
        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<T> GetEnumerator()
        {
            return _childs.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException">
        /// Child already has a set parent
        /// or
        /// Child to add may not be the container itself
        /// </exception>
        public void Add(T item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            if (item.Parent != null) throw new ArgumentException(@"Child already has a set parent", nameof(item));
            if (Equals(item, this)) throw new ArgumentException(@"Child to add may not be the container itself", nameof(item));

            _childs.Add(item);

            item.Parent = this;
            item.PropertyChanged += ChildOnPropertyChanged;

            var asContainer = item as IDisplayObjectContainer;
            if (asContainer != null)
                asContainer.ChildrenChanged += (sender, args) =>
                {
                    ChildrenChanged?.Invoke(this, EventArgs.Empty);
                    _childBoundaryChanged = true;
                };

            _childBoundaryChanged = true;
            ChildrenChanged?.Invoke(this, EventArgs.Empty);
        }


        private void ChildOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == nameof(RenderBoundingBox)) return; // ignore changes of the render bounding box
            if (propertyChangedEventArgs.PropertyName == nameof(RenderRotationPoint)) return; // ignore changes of the render bounding box
            if (propertyChangedEventArgs.PropertyName == nameof(RenderRotation)) return; // ignore changes of the render bounding box
            RegisterChildBoundaryChange();
            ChildrenChanged?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        public void Clear()
        {
            foreach (var svgItem in _childs)
            {
                svgItem.Parent = null;
            }
            _childs.Clear();
        }

        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="ArgumentNullException"></exception>
        public bool Contains(T item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));

            return _childs.Contains(item);
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:System.Collections.Generic.ICollection`1" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            _childs.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1" />.</param>
        /// <returns>
        /// true if <paramref name="item" /> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise, false. This method also returns false if <paramref name="item" /> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException">Child is not a child of this container</exception>
        public bool Remove(T item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            if (item.Parent != this || !_childs.Contains(item)) throw new ArgumentException("Child is not a child of this container", nameof(item));

            if (!_childs.Remove(item)) return false;
            item.Parent = null;
            item.PropertyChanged -= ChildOnPropertyChanged;

            return true;
        }
        #endregion

        #region IRenderable Implementation

        /// <summary>
        /// Invokes the render.
        /// </summary>
        /// <param name="parentPivotX">The parent pivot x.</param>
        /// <param name="parentPivotY">The parent pivot y.</param>
        /// <param name="parentScaleX">The parent scale x.</param>
        /// <param name="parentScaleY">The parent scale y.</param>
        /// <param name="parentRotation">The parent rotation.</param>
        /// <param name="parentAlpha"></param>
        public override void InvokeRender(double parentPivotX = 0D, double parentPivotY = 0D, double parentScaleX = 1D,
            double parentScaleY = 1D, double parentRotation = 0D, double parentAlpha = 1)
        {
            if (!IsVisible || parentAlpha < double.Epsilon || Alpha < double.Epsilon) return;

            for (var index = 0; index < _childs.Count; index++)
            {
                var child = _childs[index];
                child.InvokeRender(X * parentScaleX + parentPivotX, Y * parentScaleY + parentPivotY,
                    parentScaleX * ScaleX, parentScaleY * ScaleY, parentRotation + Rotation, parentAlpha * Alpha);
            }

            GetRenderBoundingBox(parentPivotX, parentPivotY, parentScaleX, parentScaleY, parentRotation).ReplaceIfDifferent(this, x => x.RenderBoundingBox);
            (Math.Abs(RenderRotation) < 0.01 ? RenderBoundingBox : RenderBoundingBox.RotateAroundPoint(new Point(RenderRotationPoint.X + RenderBoundingBox.X, RenderRotationPoint.Y + RenderBoundingBox.Y), RenderRotation)).ReplaceIfDifferent(this, x => x.RotatedBoundingBox);

            if (Math.Abs(RenderRotation - (Rotation + parentRotation)) > 0.0000001)
                RenderRotation = Rotation + parentRotation;


            new Point(PivotX * parentScaleX, PivotY * parentScaleY).ReplaceIfDifferent(this, x => x.RenderRotationPoint);
        }

        #endregion
    }
}