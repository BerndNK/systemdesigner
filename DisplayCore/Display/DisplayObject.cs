﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using DisplayCore.Annotations;
using DisplayCore.Render;
using DisplayCore.Render.Geometry;

namespace DisplayCore.Display
{
    /// <summary>
    /// A displayable object
    /// </summary>
    /// <seealso cref="DisplayCore.Display.IDisplayObject" />
    public abstract class DisplayObject : IDisplayObject
    {
        /// <summary>
        /// The identifier counter
        /// </summary>
        private static int _idCounter;

        #region Fields

        /// <summary>
        /// The scale x
        /// </summary>
        protected double BaseScaleX = 1;
        /// <summary>
        /// The scale y
        /// </summary>
        protected double BaseScaleY = 1;

        /// <summary>
        /// The base width
        /// </summary>
        protected double BaseWidth; // width of the item when scale = 1
        /// <summary>
        /// The base height
        /// </summary>
        protected double BaseHeight; // height of the item when scale = 1

        /// <summary>
        /// The width
        /// </summary>
        protected double CachedWidth; // the displayed width
        /// <summary>
        /// The height
        /// </summary>
        protected double CachedHeight; // the displayed height

        /// <summary>
        /// The pivot x
        /// </summary>
        protected double _pivotX;
        /// <summary>
        /// The pivot y
        /// </summary>
        protected double _pivotY;

        /// <summary>
        /// Flag indicating whether the local boundaries have changed (and needs to be recalculated) or not.
        /// </summary>
        protected bool HasBoundariesChanged = true;

        private double _rotation;
        private double _alpha = 1.0;
        private Rectangle _renderBoundingBox = new Rectangle(0, 0, 0, 0);
        private double _renderRotation;
        private Point _renderRotationPoint;
        private int _id;
        private Rectangle _rotatedBoundingBox;
        protected Rectangle LocalRotatedBoundingBoxCache;
        private double _x;
        private double _y;
        #endregion

        #region Properties


        /// <summary>
        /// Gets or sets the x.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        public double X {
            get { return _x; }
            set {
                _x = value;
                HasBoundariesChanged = true;
                OnPropertyChanged();
            }
        } // X position of item

        /// <summary>
        /// Gets or sets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        public double Y {
            get { return _y; }
            set {
                _y = value;
                HasBoundariesChanged = true;

                OnPropertyChanged();
            }
        } // Y position of item

        /// <summary>
        /// Gets or sets the pivot x.
        /// </summary>
        /// <value>
        /// The pivot x.
        /// </value>
        public double PivotX {
            get { return _pivotX; }
            set {
                _pivotX = value;
                HasBoundariesChanged = true;

                OnPropertyChanged();
            }
        } // Pivot of the item (counting from the upper left corner up)

        /// <summary>
        /// Gets or sets the pivot y.
        /// </summary>
        /// <value>
        /// The pivot y.
        /// </value>
        public double PivotY {
            get { return _pivotY; }
            set {
                _pivotY = value;
                HasBoundariesChanged = true;

                OnPropertyChanged();
            }
        } // Pivot of the item (counting from the upper left corner up)

        /// <summary>
        /// Gets or sets the alpha.
        /// </summary>
        /// <value>
        /// The alpha.
        /// </value>
        public double Alpha {
            get { return _alpha; }
            set {
                _alpha = value;
                OnPropertyChanged();
            }
        } // transparency

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id {
            get { return _id; }
            protected set { _id = value; }
        }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        public double Rotation {
            get { return _rotation; }
            set {
                _rotation = value;
                HasBoundariesChanged = true;

                OnPropertyChanged();
            }
        } // Rotation in Radians

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public virtual double Width {
            get {
                return CachedWidth;
            }
            set {
                // if the current values are about 0, multiplications math below will not work
                if (Math.Abs(BaseWidth) < 0.1) BaseWidth = value;
                if (Math.Abs(CachedWidth) < 0.1) CachedWidth = value;

                BaseScaleX = value / BaseWidth;
                PivotX = PivotX / CachedWidth * value;
                CachedWidth = value;
                HasBoundariesChanged = true;

                OnPropertyChanged();
                OnPropertyChanged(nameof(ScaleX));
                OnPropertyChangedBoundariesX();
            }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public virtual double Height {
            get { return CachedHeight; }
            set {
                // if the current values are about 0, multiplications math below will not work
                if (Math.Abs(BaseHeight) < 0.1) BaseHeight = value;
                if (Math.Abs(CachedHeight) < 0.1) CachedHeight = value;

                BaseScaleY = value / BaseHeight;
                PivotY = PivotY / CachedHeight * value;
                CachedHeight = value;
                HasBoundariesChanged = true;

                OnPropertyChanged();
                OnPropertyChanged(nameof(ScaleY));
                OnPropertyChangedBoundariesY();
            }
        }

        /// <summary>
        /// Gets the amount of parents.
        /// </summary>
        /// <value>
        /// The amount of parents.
        /// </value>
        public int AmountOfParents {
            get {
                var crnt = Parent;
                var amount = 0;
                while (crnt != null)
                {
                    crnt = crnt.Parent;
                    amount++;
                }
                return amount;
            }
        } // the amount of parents this child has. So Parent + Parent.Parent + Parent.Parent.Parent etc.

        #region Boundaries

        /// <summary>
        /// Gets the middle x.
        /// </summary>
        /// <value>
        /// The middle x.
        /// </value>
        public virtual double MiddleX => LocalRotatedBoundingBox.X + LocalRotatedBoundingBox.Width / 2;

        /// <summary>
        /// Gets the middle y.
        /// </summary>
        /// <value>
        /// The middle y.
        /// </value>
        public virtual double MiddleY => LocalRotatedBoundingBox.Y + LocalRotatedBoundingBox.Height / 2;
        /// <summary>
        /// Gets the left boundary.
        /// </summary>
        /// <value>
        /// The left boundary.
        /// </value>
        public virtual double LeftBoundary => LocalRotatedBoundingBox.X;
        /// <summary>
        /// Gets the right boundary.
        /// </summary>
        /// <value>
        /// The right boundary.
        /// </value>
        public virtual double RightBoundary => LocalRotatedBoundingBox.X + LocalRotatedBoundingBox.Width;
        /// <summary>
        /// Gets the top boundary.
        /// </summary>
        /// <value>
        /// The top boundary.
        /// </value>
        public virtual double TopBoundary => LocalRotatedBoundingBox.Y;
        /// <summary>
        /// Gets the bottom boundary.
        /// </summary>
        /// <value>
        /// The bottom boundary.
        /// </value>
        public virtual double BottomBoundary => LocalRotatedBoundingBox.Y + LocalRotatedBoundingBox.Height;
        #endregion


        #region RenderBoundaries
        /// <summary>
        /// Gets the render bounding box. Which describes the area in which this object has been rendered last time
        /// </summary>
        /// <value>
        /// The render bounding box.
        /// </value>
        public Rectangle RenderBoundingBox {
            get { return _renderBoundingBox; }
            protected set {
                _renderBoundingBox = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the rotated bounding box, which describes a box containing the DisplayObject in it's rotated state. If rotation = 0, it's equivalent to the <c>RenderBoundingBox</c>
        /// </summary>
        /// <value>
        /// The bounding box surrounding the rotated RenderBoundingBox.
        /// </value>
        public Rectangle RotatedBoundingBox {
            get { return _rotatedBoundingBox; }
            protected set {
                _rotatedBoundingBox = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the render rotation.
        /// </summary>
        /// <value>
        /// The render rotation.
        /// </value>
        public double RenderRotation {
            get { return _renderRotation; }
            protected set {
                _renderRotation = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the render rotation point.
        /// </summary>
        /// <value>
        /// The render rotation point.
        /// </value>
        public Point RenderRotationPoint {
            get { return _renderRotationPoint; }
            protected set {
                _renderRotationPoint = value;
                OnPropertyChanged();
            }
        }

        #endregion

        /// <summary>
        /// Gets or sets a value indicating whether this instance is visible.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is visible; otherwise, <c>false</c>.
        /// </value>
        public bool IsVisible { get; set; } = true;

        /// <summary>
        /// Gets or sets the scale x.
        /// </summary>
        /// <value>
        /// The scale x.
        /// </value>
        public virtual double ScaleX {
            get {
                return BaseScaleX;
            }
            set
            {
                if (value < 0.001)
                    value = 0.001;

                PivotX = (PivotX / Width) * BaseWidth * value;
                CachedWidth = BaseWidth * value;
                BaseScaleX = value;
                HasBoundariesChanged = true;

                OnPropertyChanged();
                OnPropertyChanged(nameof(Width));
                OnPropertyChangedBoundariesX();
            }
        } // scale for this item x wise

        /// <summary>
        /// Gets or sets the scale y.
        /// </summary>
        /// <value>
        /// The scale y.
        /// </value>
        public virtual double ScaleY {
            get {
                return BaseScaleY;
            }
            set {
                if (value < 0.001)
                    value = 0.001;

                PivotY = PivotY / Height * BaseHeight * value;
                CachedHeight = BaseHeight * value;
                BaseScaleY = value;
                HasBoundariesChanged = true;

                OnPropertyChanged();
                OnPropertyChanged(nameof(Height));
                OnPropertyChangedBoundariesY();
            }
        } // scale for this item y wise

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        public virtual IDisplayObjectContainer Parent { get; set; }

        /// <summary>
        /// Rectangle describing the bounds of this DisplayObject in the local grid, after it has been rotated.
        /// </summary>
        protected virtual Rectangle LocalRotatedBoundingBox {
            get {
                // if the localRotatedBoundingBox is null or the boundaries have changed, recalculate it. 
                if (LocalRotatedBoundingBoxCache == null || HasBoundariesChanged)
                {
                    LocalRotatedBoundingBoxCache = new Rectangle(X - PivotX, Y - PivotY, Width, Height).RotateAroundPoint(new Point(X, Y), Rotation);
                    HasBoundariesChanged = false;
                }
                return LocalRotatedBoundingBoxCache;
            }
        }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayObject"/> class.
        /// </summary>
        protected DisplayObject()
        {
            Id = GetNewId();
        }

        /// <summary>
        /// Checks if the point is intersecting with the object's boundaries. Implying that the point is within the same parent
        /// </summary>
        /// <param name="rect">The rectangle to check with values in this item's local coordinate system</param>
        /// <returns>
        /// True for intersect, False for no intersection
        /// </returns>
        public virtual bool HitTestRect(Rectangle rect)
        {
            // standard simple boundary check
            return !(rect.X + rect.Width < X - PivotX
                     || rect.X > X + Width - PivotX
                     || rect.Y + rect.Height < Y - PivotY
                     || rect.Y > Y + Height - PivotY);
        }

        /// <summary>
        /// Checks if the point is within the object's boundaries. Implying that the point is within the same parent
        /// </summary>
        /// <param name="x">X Position of the point in this item's local coordinate system</param>
        /// <param name="y">Y Position of the point in this item's local coordinate system</param>
        /// <returns>
        /// True for yes, False for no
        /// </returns>
        public virtual bool HitTestPoint(double x, double y)
        {
            var translatedPoint = TranslatePoint(new Point(x, y));
            // standard simple boundary check
            var hitTestPoint = !(translatedPoint.X < X - PivotX ||
                                 translatedPoint.X > X + Width - PivotX ||
                                 translatedPoint.Y < Y - PivotY ||
                                 translatedPoint.Y > Y + Height - PivotY);
            return hitTestPoint;

        }

        /// <summary>
        /// See https://stackoverflow.com/questions/4977491/determining-if-two-line-segments-intersect/4977569#4977569
        /// </summary>
        public bool HitTestLine(Line line)
        {
            var translatedLine = new Line(TranslatePoint(line.LineOrigin), TranslatePoint(line.LineEnd));
            var translatedRect = new Rectangle(X, Y, Width, Height);
            return translatedRect.GetBoundaryLines().Any(l => l.IntersectsWith(translatedLine));
        }


        /// <summary>
        /// Translates a rectangle into this display objects grid.
        /// </summary>
        /// <param name="rect">The rect.</param>
        /// <returns></returns>
        private Rectangle TranslateRectangle(Rectangle rect)
        {
            var translatedRect = new Rectangle(rect.X, rect.Y, rect.Width, rect.Height);
            /*translatedRect.X -= X;
            translatedRect.Y -= Y;
            translatedRect.X /= ScaleX;
            translatedRect.Y /= ScaleY;
            translatedRect.Width /= ScaleX;
            translatedRect.Height /= ScaleY;*/

            return translatedRect;
        }

        /// <summary>
        /// Translates a point into this display objects grid.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns></returns>
        private Point TranslatePoint(Point point)
        {
            return point;
            //var translatedPoint = new Point((point.X - X) / ScaleX, (point.Y - Y) / ScaleY); // given point inside this

            //if (Math.Abs(Rotation) < 0.01) return translatedPoint;

            //// calculate new resulting x, y positions
            //// get distance to pivot of the display object
            //var distance = Math.Sqrt(Math.Pow(translatedPoint.X, 2) + Math.Pow(translatedPoint.Y, 2));

            //// get current angle between upper left position of display object and the Pivot of the parent
            //var crntRot = Math.Atan2(translatedPoint.Y, translatedPoint.X);

            //// add parent rotation to current rotation and calculate the corresponding x/y values for that angle
            //var rotX = (Math.Cos(Rotation + crntRot) * distance);
            //var rotY = (Math.Sin(Rotation + crntRot) * distance);

            //// add parent Pivot for final x/y position on the screen
            //translatedPoint.X = PivotX + rotX;
            //translatedPoint.Y = PivotY + rotY;

            //return translatedPoint;
        }

        #region IPropertyChangedImplementation

        /// <summary>
        /// Helper method to invoke all properyChanged events for the boundary properties which are X relevant
        /// </summary>
        protected void OnPropertyChangedBoundariesX()
        {
            OnPropertyChanged(nameof(MiddleX));
            OnPropertyChanged(nameof(LeftBoundary));
            OnPropertyChanged(nameof(RightBoundary));
        }

        /// <summary>
        /// Helper method to invoke all properyChanged events for the boundary properties which are Y relevant
        /// </summary>
        protected void OnPropertyChangedBoundariesY()
        {
            OnPropertyChanged(nameof(MiddleY));
            OnPropertyChanged(nameof(TopBoundary));
            OnPropertyChanged(nameof(BottomBoundary));
        }

        #endregion

        #region IRenderable Implementation

        /// <summary>
        /// Tells the item to render. A Container will give this call to its children, while a standard DisplayItem will call RendererHandler's Render Event
        /// </summary>
        /// <param name="parentPivotX">Pivot X of this parent (including parent parents. Therefore its not equal parent.PivotX)</param>
        /// <param name="parentPivotY">Pivot Y of this parent (including parent parents. Therefore its not equal parent.PivotY)</param>
        /// <param name="parentScaleX">Scale X of this parent (including parent parents. Therefore its not equal parent.ScaleX)</param>
        /// <param name="parentScaleY">Scale Y of this parent (including parent parents. Therefore its not equal parent.ScaleY)</param>
        /// <param name="parentRotation">Rotation of this parent (including parent parents. Therefore its not equal parent.Rotation)</param>
        /// <param name="parentAlpha"></param>
        public virtual void InvokeRender(double parentPivotX = 0D, double parentPivotY = 0D, double parentScaleX = 1D,
            double parentScaleY = 1D, double parentRotation = 0D, double parentAlpha = 1D)
        {
            if (!IsVisible) return;
            RenderRotationPoint = new Point(PivotX * parentScaleX, PivotY * parentScaleY);
            RenderRotation = Rotation + parentRotation;

            GetRenderBoundingBox(parentPivotX, parentPivotY, parentScaleX, parentScaleY, parentRotation).ReplaceIfDifferent(this, x => x.RenderBoundingBox);
            //RenderBoundingBox = GetRenderBoundingBox(parentPivotX, parentPivotY, parentScaleX, parentScaleY, parentRotation);
            // calculate the rotatedBoundingBox (RotateAroundPoint takes an absolute rotationPoint, therefore we need to convert the relative one) If the rotation is about 0 however, the rotatedBoundingBox is the same as the unrotated one
            (Math.Abs(RenderRotation) < 0.01 ? RenderBoundingBox : RenderBoundingBox.RotateAroundPoint(new Point(RenderRotationPoint.X + RenderBoundingBox.X, RenderRotationPoint.Y + RenderBoundingBox.Y), RenderRotation)).ReplaceIfDifferent(this, x => x.RotatedBoundingBox);


            RendererHandler.InvokeRender(this, RenderBoundingBox, Rotation + parentRotation, RenderRotationPoint, RotatedBoundingBox, parentAlpha * Alpha);
        }
        #endregion

        /// <summary>
        /// Gets the render bounding box.
        /// </summary>
        /// <param name="parentPivotX">The parent pivot x.</param>
        /// <param name="parentPivotY">The parent pivot y.</param>
        /// <param name="parentScaleX">The parent scale x.</param>
        /// <param name="parentScaleY">The parent scale y.</param>
        /// <param name="parentRotation">The parent rotation.</param>
        /// <returns>Bounding box on the screen, wrapping this display object around it. Without rotation.</returns>
        protected Rectangle GetRenderBoundingBox(double parentPivotX, double parentPivotY, double parentScaleX, double parentScaleY, double parentRotation)
        {
            // get the actual X/Y/Width/Height values on the screen
            double renderX;
            double renderY;

            // Apply parent scale if it's not == 1
            var renderW = Math.Abs(parentScaleX - 1) < 0.00001 ? Width : Math.Ceiling(Width * parentScaleX);
            var renderH = Math.Abs(parentPivotY - 1) < 0.00001 ? Height : Math.Ceiling(Height * parentScaleY);
            var rotationPoint = new Point(0, 0);

            // if this rotation's and parent rotations = 0
            if (Math.Abs(Rotation) < 0.01 && Math.Abs(parentRotation) < 0.01)
            {
                // the render X and Y, are the parentPivot + this origin (X/Y - PivotX/Y) multiplied by the parent scale
                renderX = parentPivotX + Math.Ceiling((X - PivotX) * parentScaleX);
                renderY = parentPivotY + Math.Ceiling((Y - PivotY) * parentScaleY);

            }
            else
            {
                // calculate new resulting x, y positions
                // get distance to pivot of the display object
                var distance = Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));

                // get current angle between upper left position of display object and the Pivot of the parent
                var crntRot = Math.Atan2(Y, X);

                // add parent rotation to current rotation and calculate the corresponding x/y values for that angle
                var rotX = (Math.Cos(parentRotation + crntRot) * distance);
                var rotY = (Math.Sin(parentRotation + crntRot) * distance);

                // add parent Pivot for final x/y position on the screen
                renderX = parentPivotX + ((rotX - PivotX) * parentScaleX);
                renderY = parentPivotY + (rotY - PivotY) * parentScaleY;

                // set pivot X/Y
                rotationPoint.X = PivotX * parentScaleX;
                rotationPoint.Y = PivotY * parentScaleY;
            }

            // then invoke the render call
            return new Rectangle(renderX, renderY, renderW, renderH);
        }



        /// <summary>
        /// Gets a new identifier.
        /// </summary>
        public static int GetNewId()
        {
            return ++_idCounter;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}