﻿using System.ComponentModel;
using DisplayCore.Render;
using DisplayCore.Render.Geometry;

namespace DisplayCore.Display {
    /// <summary>
    /// Describes a displayable object (like a picture)
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public interface IDisplayObject : INotifyPropertyChanged, IRenderable {
        #region Properties

        /// <summary>
        /// Gets or sets the x.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        double X { get; set; }
        /// <summary>
        /// Gets or sets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        double Y { get; set; }
        /// <summary>
        /// Gets or sets the pivot x.
        /// </summary>
        /// <value>
        /// The pivot x.
        /// </value>
        double PivotX { get; set; }
        /// <summary>
        /// Gets or sets the pivot y.
        /// </summary>
        /// <value>
        /// The pivot y.
        /// </value>
        double PivotY { get; set; }

        /// <summary>
        /// Gets or sets the alpha.
        /// </summary>
        /// <value>
        /// The alpha.
        /// </value>
        double Alpha { get; set; }
        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        double Rotation { get; set; }
        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        double Width { get; set; }
        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        double Height { get; set; }

        /// <summary>
        /// Gets the amount of parents.
        /// </summary>
        /// <value>
        /// The amount of parents.
        /// </value>
        int AmountOfParents { get; }
        

        #region Boundaries
        /// <summary>
        /// Gets the middle x.
        /// </summary>
        /// <value>
        /// The middle x.
        /// </value>
        double MiddleX { get; }
        /// <summary>
        /// Gets the middle y.
        /// </summary>
        /// <value>
        /// The middle y.
        /// </value>
        double MiddleY { get; }
        /// <summary>
        /// Gets the left boundary.
        /// </summary>
        /// <value>
        /// The left boundary.
        /// </value>
        double LeftBoundary { get; }
        /// <summary>
        /// Gets the right boundary.
        /// </summary>
        /// <value>
        /// The right boundary.
        /// </value>
        double RightBoundary { get; }
        /// <summary>
        /// Gets the top boundary.
        /// </summary>
        /// <value>
        /// The top boundary.
        /// </value>
        double TopBoundary { get; }
        /// <summary>
        /// Gets the bottom boundary.
        /// </summary>
        /// <value>
        /// The bottom boundary.
        /// </value>
        double BottomBoundary { get; }
        #endregion


        /// <summary>
        /// Gets the render bounding box, which describes the area in which this object has been rendered last time
        /// </summary>
        /// <value>
        /// The render bounding box.
        /// </value>
        Rectangle RenderBoundingBox { get; }

        /// <summary>
        /// Gets the rotated bounding box, which describes a box containing the DisplayObject in it's rotated state. If rotation = 0, it's equivalent to the <c>RenderBoundingBox</c>
        /// </summary>
        /// <value>
        /// The bounding box surrounding the rotated RenderBoundingBox.
        /// </value>
        Rectangle RotatedBoundingBox { get; }

        /// <summary>
        /// Gets or sets the render rotation.
        /// </summary>
        /// <value>
        /// The render rotation.
        /// </value>
        double RenderRotation { get; }

        /// <summary>
        /// Gets or sets the render rotation point.
        /// </summary>
        /// <value>
        /// The render rotation point.
        /// </value>
        Point RenderRotationPoint { get; }

        /// <summary>
        /// Gets or sets the scale x.
        /// </summary>
        /// <value>
        /// The scale x.
        /// </value>
        double ScaleX { get; set; }
        /// <summary>
        /// Gets or sets the scale y.
        /// </summary>
        /// <value>
        /// The scale y.
        /// </value>
        double ScaleY { get; set; }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>
        /// The parent.
        /// </value>
        IDisplayObjectContainer Parent { get; set;  }

        #endregion
        
        /// <summary>
        /// Checks if the point is intersecting with the object's boundaries. Implying that the point is within the same parent
        /// </summary>
        /// <param name="rect">The rectangle to check with values in this item's local coordinate system</param>
        /// <returns>True for intersect, False for no intersection</returns>
        bool HitTestRect(Rectangle rect);

        /// <summary>
        /// Checks if the point is within the object's boundaries. Implying that the point is within the same parent
        /// </summary>
        /// <param name="x">X Position of the point in this item's local coordinate system</param>
        /// <param name="y">Y Position of the point in this item's local coordinate system</param>
        /// <returns>True for yes, False for no</returns>
        bool HitTestPoint(double x, double y);

        bool HitTestLine(Line line);
    }
}
