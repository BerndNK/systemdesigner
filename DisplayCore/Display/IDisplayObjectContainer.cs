﻿using System;

namespace DisplayCore.Display
{
    /// <summary>
    /// Describes a displayable objects, which consists of other displayable objects
    /// </summary>
    /// <seealso cref="DisplayCore.Display.IDisplayObject" />
    public interface IDisplayObjectContainer : IDisplayObject
    {

        /// <summary>
        /// Tells this container that one of it's children has had a transformation change
        /// </summary>
        void RegisterChildBoundaryChange();


        /// <summary>
        /// Occurs when one or more [children changed].
        /// </summary>
        event EventHandler ChildrenChanged;
    }
}
