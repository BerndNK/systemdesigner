﻿namespace SystemDesignerResources.Image
{
    public static class Icons
    {
        public const string Plus = "Plus-100.png";
        public const string Minus = "Minus-100.png";
        public const string Advance = "Advance-100.png";
        public const string Anchor = "Anchor-100.png";
        public const string Export = "Export-100.png";
        public const string Hammer = "Hammer-100.png";
        public const string Bug = "icons8-Bug-100.png";
        public const string Multicast = "icons8-Multicast-100.png";
        public const string Pen = "icons8-Pencil-100.png";
        public const string More = "More-100.png";
        public const string Move = "Move-100.png";
        public const string Right = "Right-100.png";
        public const string ResizeVertical = "Resize Vertical-100.png";
        public const string Rotate = "Rotate-100.png";
        public const string Cursor = "icons8-Cursor-100.png";
    }
}
