﻿using System.Globalization;
using System.Xml.Serialization;

namespace SvgAbstraction.Svg.Forms {
    public class SvgRect : SvgItem {
        #region Properties

        [XmlElement(ElementName = "X")]
        public new double X {
            get { return base.X; }
            set { base.X = value; }
        }

        [XmlElement(ElementName = "Y")]
        public new double Y {
            get { return base.Y; }
            set { base.Y = value; }
        }

        [XmlElement(ElementName = "Width")]
        public new double Width {
            get { return base.Width; }
            set { base.Width = value; }
        }

        [XmlElement(ElementName = "Height")]
        public new double Height {
            get { return base.Height; }
            set { base.Height = value; }
        }
        #endregion


        public SvgRect(double width = 0, double height = 0) {
            BaseWidth = width;
            BaseHeight = height;
            CachedWidth = width;
            CachedHeight = height;
            BaseScaleX = 1;
            BaseScaleY = 1;
        }

        public override string ToXml() {
            var toReturn = "";
            var amountOfParents = AmountOfParents;
            // add a tabstop for each parent
            for (var i = 0; i < amountOfParents; i++) {
                toReturn += '\t';
            }

            toReturn += "<rect";
            // create helper delegate which takes a value and a property name, gets the xml name via attributes, then it returns a xmlName="value" like string with proper formatting
            var addAttribute = new XmlTranslatorHelper.AddAttributeBuilder((name, value) => $" {XmlTranslatorHelper.GetXmlName(GetType(), name)}=\"{value.ToString("0.00", CultureInfo.InvariantCulture)}\"");
            toReturn += addAttribute(nameof(X), X); // add X,Y,W & H. E.g. x="10.00" 
            toReturn += addAttribute(nameof(Y), Y);
            toReturn += addAttribute(nameof(Width), BaseWidth); // use the base width (without scale) since the scaling is within the transformation string
            toReturn += addAttribute(nameof(Height), BaseHeight);

            // Add the transformation string (which is the result of the scaleX/Y, the parent X/Y,scaleX/Y AND the pivot of this item
            toReturn += $" transform=\"{XmlTranslatorHelper.GetTransformationString(this)}\"";
            toReturn += "/>";

            return toReturn;
        }
    }
}