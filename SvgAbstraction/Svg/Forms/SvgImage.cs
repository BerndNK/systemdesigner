﻿using System.Globalization;

namespace SvgAbstraction.Svg.Forms
{
    public class SvgImage : SvgItem
    {

        /// <summary>
        /// Gets or sets the source of the image.
        /// </summary>
        public string ImageSource { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SvgImage"/> class.
        /// </summary>
        /// <param name="imageSource">The image source.</param>
        /// <param name="width">The displayed width of this.</param>
        /// <param name="height">The displayed height of this.</param>
        public SvgImage(string imageSource, double width = 100, double height = 100)
        {
            ImageSource = imageSource;
            BaseWidth = width;
            BaseHeight = height;
            CachedWidth = width;
            CachedHeight = height;
            BaseScaleX = 1;
            BaseScaleY = 1;
        }

        public override string ToXml()
        {
            var toReturn = "";
            var amountOfParents = AmountOfParents;
            // add a tabstop for each parent
            for (var i = 0; i < amountOfParents; i++)
            {
                toReturn += '\t';
            }
            toReturn += "<image";

            var addAttribute = new XmlTranslatorHelper.AddAttributeBuilder((name, value) => $" {XmlTranslatorHelper.GetXmlName(GetType(), name)}=\"{value.ToString("0.00", CultureInfo.InvariantCulture)}\"");
            toReturn += addAttribute(nameof(X), X + BaseWidth); // add center X which is X + base width / 2 to get the pivot in the center PLUS the other half, because transform auto moves the shape
            toReturn += addAttribute(nameof(Y), Y + BaseHeight);
            toReturn += " transform=\"" + XmlTranslatorHelper.GetTransformationString(this) + "\"";
            toReturn += " xlink:href=\"" + ImageSource + "\"";

            toReturn += "/>";
            return toReturn;
        }
    }
}
