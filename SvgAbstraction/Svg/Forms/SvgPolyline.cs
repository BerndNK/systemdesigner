﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using DisplayCore.Render.Geometry;

namespace SvgAbstraction.Svg.Forms
{
    public class SvgPolyline : SvgItem, ICollection<Point>
    {
        #region Properties

        #region ICollection Implementation

        public int Count => _points.Count;
        public bool IsReadOnly => false;

        #endregion

        /// <summary>
        /// The points for this polyLine.
        /// </summary>
        private readonly List<Point> _points;

        private bool _hasChanged;

        /// <summary>
        /// Gets or sets the stroke thickness.
        /// </summary>
        public double StrokeThickness { get; set; } = 1;

        /// <summary>
        /// Gets the width of the dimensions. That is the polyline without any scaling.
        /// </summary>
        public double LineDimensionsWidth => BaseWidth;

        /// <summary>
        /// Gets the height of the dimensions. That is the polyline without any scaling.
        /// </summary>
        public double LineDimensionsHeight => BaseHeight;

        /// <summary>
        /// Gets or sets a value indicating whether this instance has changed. This gets set to true every time a point is modified, gets added or removed. It is recommended to use this property to optimize the rendering cycle.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has changed; otherwise, <c>false</c>.
        /// </value>
        public bool HasChanged
        {
            get { return _hasChanged; }
            set
            {
                _hasChanged = value;
                // every time the point list changed, recalculate the dimensions.
                if(value) CalculateDimensions();
            }
        }

        #endregion
        /// <summary>
        /// Initializes a new instance of the <see cref="SvgPolyline"/> class.
        /// </summary>
        public SvgPolyline(double strokeThickness = 1, uint color = 0xff000000)
        {
            StrokeThickness = strokeThickness;
            FillColor = color;
            _points = new List<Point>();
        }

        /// <summary>
        /// Property changed handler for every point that is within the list.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="propertyChangedEventArgs">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void PointPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            HasChanged = true;
        }

        private void CalculateDimensions()
        {
            if (_points.Count == 0) return;

            // calculate the width and height by taking the most left/up/right/down point
            var minX = double.MaxValue;
            var minY = double.MaxValue;
            var maxX = double.MinValue;
            var maxY = double.MinValue;

            foreach (var point in _points)
            {
                // check if the point exceeds any boundary
                if (point.X > maxX) maxX = point.X;
                if (point.Y > maxY) maxY = point.Y;
                if (point.X < minX) minX = point.X;
                if (point.Y < minY) minY = point.Y;
            }

            BaseWidth = maxX - minX;
            BaseHeight = maxY - minY;
            CachedWidth = BaseWidth;
            CachedHeight = BaseHeight;
            BaseScaleX = 1;
            BaseScaleY = 1;
        }

        #region ICollection Implementation

        public override string ToXml()
        {
            throw new NotImplementedException();
        }

        public IEnumerator<Point> GetEnumerator() => _points.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public void Add(Point item)
        {
            _points.Add(item);
            item.PropertyChanged += PointPropertyChanged;
            HasChanged = true;
        }

        public void Clear()
        {
            // before clearing, disconnect every event listener
            foreach (var point in _points)
            {
                point.PropertyChanged -= PointPropertyChanged;
            }
            _points.Clear();
        }

        public bool Contains(Point item) => _points.Contains(item);

        public void CopyTo(Point[] array, int arrayIndex) => _points.CopyTo(array, arrayIndex);

        public bool Remove(Point item)
        {
            var removed = _points.Remove(item);
            if (removed)
            {
                HasChanged = true; // if we did remove the item, set the flag and remove the event listener
                item.PropertyChanged -= PointPropertyChanged;
            }

            return removed;
        }

        #endregion

        /// <summary>
        /// Special hook for a <c>collectionChanged</c> event from a ObservableCollection of Point's (or equivalent). This handler will try to cast the type to Point and mirror each action done. So if points were added, they will be added to this Polyline as well. (Same with remove, move, replace etc.)
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">The <see cref="NotifyCollectionChangedEventArgs" /> instance containing the event data. Expecting to be instances of <c>Point</c></param>
        /// <exception cref="NotSupportedException">Move and replace operations are not supported by the collection hook right now.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown for an invalid action of the eventArgs</exception>
        /// <exception cref="InvalidCastException">May occur when the hooked up collection is not a collection of <c>Point</c></exception>
        public void PointCollectionChangedHandlerHook(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var newItem in e.NewItems)
                    {
                        // use direct cast, as this method is described to throw an InvalidCastException
                        Add((Point)newItem);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Move:
                case NotifyCollectionChangedAction.Replace:
                    throw new NotSupportedException("Move and replace operations are not supported by the collection hook right now.");
                case NotifyCollectionChangedAction.Reset:
                    Clear();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}