﻿using System.Linq;

namespace SvgAbstraction.Svg.Forms.Container {
    /// <summary>
    /// Svg Item which represents the <svg></svg> tag. Ideally used as root
    /// </summary>
    public class SvgRoot : SvgItemContainer {

        #region Properties
        public string Xmlns { get; set; } = "http://www.w3.org/2000/svg";
        #endregion
        public override string ToXml() {
            var toReturn = "";
            var amountOfParents = AmountOfParents;
            const string tagName = "svg"; 

            // add a tabstop for each parent
            for (var i = 0; i < amountOfParents; i++) {
                toReturn += '\t';
            }
            toReturn += "<" + tagName;

            toReturn += " xmlns=\"" + Xmlns + "\"";

            if (_childs == null || _childs.Count == 0) {
                toReturn += "/>";
            } else {
                toReturn += ">\n";
                toReturn = _childs.OfType<IXmlTranslatable>().Aggregate(toReturn, (current, child) => current + child.ToXml() + "\n");

                // again add a tabstop for each parent for the closing tag
                for (var i = 0; i < amountOfParents; i++) {
                    toReturn += '\t';
                }
                toReturn += "</" + tagName + ">";
            }

            return toReturn;

        }
    }
}
