﻿using System.Linq;

namespace SvgAbstraction.Svg.Forms.Container {
    public class SvgGroup : SvgItemContainer {
        public override string ToXml() {
            var toReturn = "";
            var amountOfParents = AmountOfParents;
            const string tagName = "g"; // SVG group name g

            // add a tabstop for each parent
            for (var i = 0; i < amountOfParents; i++) {
                toReturn += '\t';
            }
            toReturn += "<" + tagName;

            // X,Y and scaling Note: Translate BEFORE Transform, otherwise the X and Y translation will be affected by scaleX/Y
            toReturn += $" transform=\"translate({X}, {Y}) {XmlTranslatorHelper.GetTransformationString(this)}\""; 
            
            if (_childs == null || _childs.Count == 0) {
                toReturn += "/>";
            } else {
                toReturn += ">\n";
                toReturn = _childs.OfType<IXmlTranslatable>().Aggregate(toReturn, (current, child) => current + child.ToXml() + "\n");

                // again add a tabstop for each parent for the closing tag
                for (var i = 0; i < amountOfParents; i++) {
                    toReturn += '\t';
                }
                toReturn += "</" + tagName + ">";
            }

            return toReturn;

        }
    }
}