﻿using System;
using System.Globalization;
using System.Xml.Serialization;

namespace SvgAbstraction.Svg.Forms {
    public class SvgEllipse : SvgItem {
        #region Properties
        [XmlElement(ElementName = "CX")] // x translates to center X
        public new double X {
            get { return base.X; }
            set { base.X = value; }
        }

        [XmlElement(ElementName = "CY")] // y translates to center y
        public new double Y {
            get { return base.Y; }
            set { base.Y = value; }
        }

        [XmlElement(ElementName = "R")] // no width or height but instead a radius r
        public double Radius { get; set; }

        #endregion

        public SvgEllipse(double radius) {
            Radius = radius;
            BaseWidth = Radius * 2;
            BaseHeight = Radius * 2;
            CachedWidth = Radius * 2;
            CachedHeight = Radius * 2;
            BaseScaleX = 1;
            BaseScaleY = 1;
        }

        public override bool HitTestPoint(double x, double y)
        {
            // check if the point even hits the boundaries
            var hitsBoundaries = base.HitTestPoint(x, y);
            if (!hitsBoundaries) return false;

            // if not, adjust the point to the stretch of this ellipse
            var adjustedX = (x - X - Width/2 + PivotX) / ScaleX;
            var adjustedY = (y - Y - Height/2 + PivotY) / ScaleY;

            // then check if this adjust point, is not farther away than the radius of this circle's radius.
            var distance = Math.Sqrt(Math.Pow(adjustedX, 2) + Math.Pow(adjustedY, 2));
            return distance <= Radius;
        }

        public override string ToXml() {
            var toReturn = "";
            var amountOfParents = AmountOfParents;
            // add a tabstop for each parent
            for (var i = 0; i < amountOfParents; i++) {
                toReturn += '\t';
            }
            toReturn += "<circle";

            var addAttribute = new XmlTranslatorHelper.AddAttributeBuilder((name, value) => $" {XmlTranslatorHelper.GetXmlName(GetType(), name)}=\"{value.ToString("0.00", CultureInfo.InvariantCulture)}\"");
            toReturn += addAttribute(nameof(X), X + BaseWidth); // add center X which is X + base width / 2 to get the pivot in the center PLUS the other half, because transform auto moves the shape
            toReturn += addAttribute(nameof(Y), Y + BaseHeight);
            toReturn += addAttribute(nameof(Radius), Radius);
            toReturn += " transform=\"" + XmlTranslatorHelper.GetTransformationString(this) + "\"";
            
            toReturn += "/>";
            return toReturn;
        }
    }
}