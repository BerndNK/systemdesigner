﻿using DisplayCore.Display;

namespace SvgAbstraction.Svg {
    public abstract class SvgItemContainer : DisplayObjectContainer<ISvgItem>, ISvgItem {

        public bool ShowSelectionHint { get; set; }
        

        public abstract string ToXml();
    }
}