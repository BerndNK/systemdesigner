﻿using System;
using DisplayCore.Display;
using DisplayCore.Render;
using DisplayCore.Render.Geometry;

namespace SvgAbstraction.Svg
{
    public abstract class SvgItem : DisplayObject, ISvgItem
    {

        private const double SelectionLineThickness = 6;


        #region Properties

        public bool ShowSelectionHint { get; set; }


        /// <summary>
        /// Gets or sets the color of the fill.
        /// </summary>
        public UInt32 FillColor { get; set; } = 0xff000000;

        /// <summary>
        /// Gets or sets the color of the line.
        /// </summary>
        public UInt32 LineColor { get; set; }

        /// <summary>
        /// Gets or sets the line thickness.
        /// </summary>
        public double LineThickness { get; set; }

        /// <summary>
        /// Gets or sets the color of the selection hint.
        /// </summary>
        public UInt32 SelectionHintColor { get; set; } = 0xffffff00;

        /// <summary>
        /// The identifier for the selection hint render.
        /// </summary>
        private int _selectionId = GetNewId();
        private int _selectionId2 = GetNewId();
        private int _selectionId3 = GetNewId();

        #endregion

        public override void InvokeRender(double parentPivotX, double parentPivotY, double parentScaleX, double parentScaleY, double parentRotation)
        {
            // override render process, to invoke a second render for the selection
            RenderRotationPoint = new Point(PivotX * parentScaleX, PivotY * parentScaleY);
            RenderRotation = Rotation + parentRotation;


            GetRenderBoundingBox(parentPivotX, parentPivotY, parentScaleX, parentScaleY, parentRotation).ReplaceIfDifferent(this, x => x.RenderBoundingBox);
            // calculate the rotatedBoundingBox (RotateAroundPoint takes an absolute rotationPoint, therefore we need to convert the relative one) If the rotation is about 0 however, the rotatedBoundingBox is the same as the unrotated one
            (Math.Abs(RenderRotation) < 0.01 ? RenderBoundingBox : RenderBoundingBox.RotateAroundPoint(new Point(RenderRotationPoint.X + RenderBoundingBox.X, RenderRotationPoint.Y + RenderBoundingBox.Y), RenderRotation)).ReplaceIfDifferent(this, x => x.RotatedBoundingBox);


            RendererHandler.InvokeRender(this, RenderBoundingBox, Rotation + parentRotation, RenderRotationPoint, RotatedBoundingBox);

            // if this should render a selection hint
            if (ShowSelectionHint)
            {
                // save the original line color and thickness
                var originalLineColor = LineColor;
                var originalLineThickness = LineThickness;
                var originalFill = FillColor;
                var originalId = Id; // use a different ID for the selection hint, so the renderer knows it's a different object
                Id = _selectionId;
                LineColor = 0xccffffff;
                LineThickness = SelectionLineThickness;
                FillColor = 0x00000000;
                // render
                RendererHandler.InvokeRender(this, RenderBoundingBox, Rotation + parentRotation, RenderRotationPoint, RotatedBoundingBox);
                Id = _selectionId2;
                LineColor = 0xcc000000;
                LineThickness = SelectionLineThickness-2;
                FillColor = 0x00000000;
                // render
                RendererHandler.InvokeRender(this, RenderBoundingBox, Rotation + parentRotation, RenderRotationPoint, RotatedBoundingBox);
                Id = _selectionId3;
                LineColor = SelectionHintColor;
                LineThickness = SelectionLineThickness-4;
                FillColor = 0x00000000;
                // render
                RendererHandler.InvokeRender(this, RenderBoundingBox, Rotation + parentRotation, RenderRotationPoint, RotatedBoundingBox);
                // revert original values, and let it render normally
                LineColor = originalLineColor;
                LineThickness = originalLineThickness;
                Id = originalId;
                FillColor = originalFill;
            }
        }

        #region IXmlTranslatable Implementation
        public abstract string ToXml();
        #endregion
    }
}
