﻿using System;
using System.Globalization;
using System.Reflection;
using System.Xml.Serialization;
using DisplayCore.Display;

namespace SvgAbstraction.Svg {
    public static class XmlTranslatorHelper {
        internal delegate string AddAttributeBuilder(string name, double value); // helper delegate for XML translation. Take name of property and the value and return xmlName="value" properly formatted

        /// <summary>
        /// Creates the transformation matrix string for a .svg representation. E.g. matrix(50, 0, 0, 50, 2, 2)
        /// </summary>
        /// <param name="target">SvgItem to create the attributes of</param>
        /// <returns>String containing the xml attributes</returns>
        /// <exception cref="ArgumentNullException">Thrown when target is null</exception>
        public static string GetTransformationString(ISvgItem target) {
            if (target == null) throw new ArgumentNullException(nameof(target));

            var centerX = 0.0;
            var centerY = 0.0;
            if (target.Parent != null) {
                centerX = -target.Parent.X * target.ScaleX; // get distance to parent coordinate origin. This is to compensate the X/Y distortion caused by the scale of the parent
                centerX += target.X; // add X/Y placement of target (scale is not relevant here, because the scale will be in this parents transform attribute)
                centerX -= double.IsNaN(target.PivotX) ? 0 : target.PivotX; // if there is a valid pivot add that aswell

                // same with Y
                centerY = -target.Parent.Y * target.ScaleY;
                centerY += target.Y;
                centerY -= double.IsNaN(target.PivotY) ? 0 : target.PivotY;
            }


            return "matrix("
                + target.ScaleX.ToString("0.00", CultureInfo.InvariantCulture)
                + ", 0, 0, "
                + target.ScaleY.ToString("0.00", CultureInfo.InvariantCulture)
                + ", "
                + centerX.ToString("0.00", CultureInfo.InvariantCulture) + ", "
                + centerY.ToString("0.00", CultureInfo.InvariantCulture) + ")";
        }

        /// <summary>
        /// Tries to get a XmlElement attribute of a specified property of a specified Type
        /// </summary>
        /// <param name="type">Target type</param>
        /// <param name="property">Name of the target property</param>
        /// <returns>First XmlElement Attribute name or null</returns>
        /// <exception cref="ArgumentNullException">Thrown when type or property is null</exception>
        /// <exception cref="ArgumentException">Thrown when property is an invalid string</exception>
        public static string GetXmlName(Type type, string property) {
            if (type == null) throw new ArgumentNullException(nameof(type));
            if (property == null) throw new ArgumentNullException(nameof(property));
            if (string.IsNullOrWhiteSpace(property)) throw new ArgumentException("Invalid property string", nameof(property));

            try {
                var attribute = type.GetRuntimeProperty(property).GetCustomAttribute(typeof(XmlElementAttribute));
                return ((XmlElementAttribute)attribute).ElementName;
            } catch (Exception e) {
                throw new ArgumentException($"The given Type: \"{type.Name}\" and property: \"{property}\" combination did not resolve in a valid Attribute", e);
            }
        }
    }
}
