﻿using DisplayCore.Display;

namespace SvgAbstraction.Svg {
    public interface ISvgItem : IDisplayObject, IXmlTranslatable {
        // all properties from DisplayObject

        /// <summary>
        /// Gets or sets a value indicating whether to render a [selection hint], which is the same object again, but with a thick stroke in the color of SelectionHintColor.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show selection hint]; otherwise, <c>false</c>.
        /// </value>
        bool ShowSelectionHint { get; set; }

    }
}
