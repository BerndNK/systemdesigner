﻿namespace SvgAbstraction.Svg {

    /// <summary>
    /// Describes a class which can be translated into an xml representation
    /// </summary>
    public interface IXmlTranslatable {
        /// <summary>
        /// Translates this class into XML Representation
        /// </summary>
        string ToXml();
    }
}