using System.Xml.Serialization;

namespace ShapeDrawer.Utils
{
    [XmlType(TypeName = "Point")]
	public class Point
	{
        [XmlElement("X")]
		public double X { get; set; }
        [XmlElement("Y")]
        public double Y { get; set; }

	    public Point(double x, double y)
	    {
	        X = x;
	        Y = y;
	    }

	    public Point()
	    {
	        
	    }

    }

}

