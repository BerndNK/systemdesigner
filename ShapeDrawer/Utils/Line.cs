﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapeDrawer.Utils {
    public class Line {
        public Point PointA { get; set; }
        public Point PointB { get; set; }

        public Line(Point pointA, Point pointB)
        {
            PointA = pointA;
            PointB = pointB;
        }
    }
}
