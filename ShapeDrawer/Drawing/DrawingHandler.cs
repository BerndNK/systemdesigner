using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using ShapeDrawer.Drawing.Shapes;
using ShapeDrawer.Utils;

namespace ShapeDrawer.Drawing {
    public class DrawingHandler : INotifyPropertyChanged{
        #region Fields
        private readonly Random _random; // RNG used to select the next shape to draw
        private readonly List<IShape> _fullShapePool; // full pool of shapes that can be drawn
        private List<IShape> _shapePool; // current pool of shapes to be drawn (used so every shape gets drawn, before a shape is requested twice)
        private bool _isDrawing; // flag whether to currently store points from input events or not

        // backing fields of properties
        private Drawing _drawing;
        private IShape _nextShapeToDraw;
        #endregion

        #region Properties

        /// <summary>
        /// The current drawn Drawing.
        /// </summary>
        public Drawing Drawing {
            get { return _drawing; }
            set { _drawing = value; }
        }

        /// <summary>
        /// The next shape that shall be drawn by the user.
        /// </summary>
        public IShape NextShapeToDraw
        {
            get { return _nextShapeToDraw; }
            private set
            {
                _nextShapeToDraw = value; 
                OnPropertyChanged();
            }
        }
        

        #endregion
        public DrawingHandler() {
            // init fields and properties
            Drawing = new Drawing();
            _random = new Random(1234);
            _fullShapePool = new List<IShape> {new Circle(), new Diamond(), new Rectangle(), new Triangle(), new TShape()};
            _shapePool = new List<IShape>(_fullShapePool);

            // set the initial shape to be drawn
            GetNewShape();
        }

        private void GetNewShape()
        {
            // get a random index of the current shape pool
            var newShapeIndex = _random.Next(0, _shapePool.Count - 1);
            // set the shape at that index to be the one to be drawn
            NextShapeToDraw = _shapePool[newShapeIndex];
            // and remove it from the list
            _shapePool.RemoveAt(newShapeIndex);

            // if the pool is empty, reset it
            if (_shapePool.Count == 0) _shapePool = new List<IShape>(_fullShapePool);
        }

        /// <summary>
        /// Takes the drawing folder (containing all persisted drawings), zips it and opens a new Explorer window with the folder selected.
        /// </summary>
        public async void ZipAllDrawings() {
            // get the directory of the drawings. If it doesn't exist, abort
            var path = Directory.GetCurrentDirectory() + "\\drawings\\";
            if (!Directory.Exists(path)) return;
            // if we already previously zipped the file, delete it
            if (File.Exists("Drawings.zip"))
            {
                File.Delete("Drawings.zip");
                // wait a short period for windows to catch up to the changes
                await Task.Delay(700);
            }

            // zip the directory
            ZipFile.CreateFromDirectory(path, "Drawings.zip");

            // open the folder in explorer
            var argument = "/select, \"" + Directory.GetCurrentDirectory() + "\\Drawings.zip" + "\"";
            System.Diagnostics.Process.Start("explorer.exe", argument);
        }

        /// <summary>
        /// Clears the currently set Drawing.
        /// </summary>
        public void ClearDrawing()
        {
            Drawing.Points.Clear();
        }

        /// <summary>
        /// Tells the drawing handler to start capturing input events.
        /// </summary>
        public void StartDrawing() {
            _isDrawing = true;
        }

        /// <summary>
        /// Gives a input for the drawing process. Usually a mouse or pen move event.
        /// </summary>
        /// <param name="position">The position of the input; the point to store</param>
        /// <returns>Whether the point has been stored or not. May only be false, when the DrawingHandler is not in drawing state.</returns>
        public bool DrawInput(Point position) {
            // if we're not drawing, return false
            if (!_isDrawing) return false;
            // otherwise add the point to the collection and return true
            Drawing.Points.Add(position);
            return true;
        }

        /// <summary>
        /// Tells the DrawingHandler to stop storing input events.
        /// </summary>
        public void StopDrawing() {
            _isDrawing = false;
        }

        /// <summary>
        /// Tells the DrawingHandler to serialize the current Drawing instance and persist it as xml.
        /// </summary>
        /// <param name="getNewShape">Whether to request a new shape to be drawn or not.</param>
        public void FinishDrawing(bool getNewShape) {
            // serialize and persist
            var xml = GetSerializedXml();
            SaveFile(xml);
            // get new shape if requested and clear the current drawing
            if(getNewShape) GetNewShape();
            ClearDrawing();
        }

        /// <summary>
        /// Persists the given string under the default drawing folder.
        /// </summary>
        /// <param name="str">The string to store</param>
        private void SaveFile(string str) {
            // variables used to create the final path
            var name = NextShapeToDraw.Name;
            var path = Directory.GetCurrentDirectory() + "\\drawings\\";
            var count = 0;
            const string fileExtension = ".xml";

            // create the directory if it doesn't exist
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // check if the file already exists, if so, increase the count at the end of the fileName e.g. Circle0.xml -> Circle1.xml and so on
            while (File.Exists(path + name + count + fileExtension)) count++;

            // write the file
            File.WriteAllText(path + name + count + fileExtension, str);
        }

        /// <summary>
        /// Serializes the current Drawing instance to xml.
        /// </summary>
        /// <returns>The serialized xml.</returns>
        private string GetSerializedXml() {
            // create serializer
            var xmlSerializer = new XmlSerializer(typeof(Drawing), new XmlRootAttribute("Drawing"));
            // Use stringWriter to write to a string
            using (var textWriter = new StringWriter()) {
                // serialize and return the result
                xmlSerializer.Serialize(textWriter, Drawing);
                return textWriter.ToString();
            }
        }

        /// <summary>
        /// Deserializes the given xml to a Drawing instance.
        /// </summary>
        /// <param name="xml">The XML to deserialize.</param>
        /// <returns>Deserialized Drawing instance.</returns>
        public Drawing Deserialize(string xml) {
            // Parse the xml
            var doc = XDocument.Parse(xml);

            // create serializer
            var xmlSerializer = new XmlSerializer(typeof(Drawing), new XmlRootAttribute("Drawing"));

            // create reader from the Root, use the serializer to parse it, return the result
            using (var reader = doc.Root.CreateReader()) {
                return (Drawing)xmlSerializer.Deserialize(reader);
            }
        }

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

}

