using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using ShapeDrawer.Utils;

namespace ShapeDrawer.Drawing
{
    [XmlType(TypeName = "Drawing")]
    public class Drawing {
        [XmlArray("Points")]
        [XmlArrayItem("Point", Type = typeof(Point))]
        public ObservableCollection<Point> Points { get; }

	    public Drawing()
	    {
	        Points = new ObservableCollection<Point>();
	    }
	}

}

