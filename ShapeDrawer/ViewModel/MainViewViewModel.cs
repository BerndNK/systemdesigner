﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using ShapeDrawer.Drawing;
using ShapeDrawer.Model;
using ShapeDrawer.Utils;

namespace ShapeDrawer.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainViewViewModel : ViewModelBase
    {
        #region Fields
        // backing fields for properties
        private ICommand _mouseMoveCommand;
        private ICommand _mouseUpCommand;
        private ICommand _mouseDownCommand;
        private double _mouseY;
        private ICommand _saveButtonCommand;
        private ICommand _resetButtonCommand;
        private ICommand _zipBottonCommand;
        private bool _switchShapes;
        private double _mouseX;
        private ICommand _keyUpCommand;
        private ShapeDrawer.Drawing.Drawing _exampleDrawing;
        #endregion

        #region Properties

        /// <summary>
        /// The DrawingHandler which will create the Drawing instance and persist it.
        /// </summary>
        public DrawingHandler DrawingHandler { get; private set; }

        /// <summary>
        /// Gets or sets the mouse position in x which will be passed to DrawingHandler.
        /// </summary>
        public double MouseX {
            get { return _mouseX; }
            set { Set(ref _mouseX, value); }
        }


        /// <summary>
        /// Gets or sets the mouse position in y which will be passed to DrawingHandler.
        /// </summary>
        public double MouseY {
            get { return _mouseY; }
            set { Set(ref _mouseY, value); }
        }


        /// <summary>
        /// Example Drawing instance for the current shape to draw.
        /// </summary>
        public Drawing.Drawing ExampleDrawing {
            get { return _exampleDrawing; }
            set { Set(ref _exampleDrawing, value); }
        }

        /// <summary>
        /// Whether or not to change up the shape after a drawing has been approved.
        /// </summary>
        public bool SwitchShapes {
            get {
                return _switchShapes;
            }
            set {
                Set(ref _switchShapes, value);
            }
        }
        /// <summary>
        /// Gets or sets the shape to shall be drawn next by the user.
        /// </summary>
        public IShape ShapeToDraw => DrawingHandler.NextShapeToDraw;

        /// <summary>
        /// Command for a mouse buttonup event.
        /// </summary>
        public ICommand MouseDownCommand {
            get { return _mouseDownCommand; }
            set { Set(ref _mouseDownCommand, value); }
        }

        /// <summary>
        /// Command for a mouse button up event.
        /// </summary>
        public ICommand MouseUpCommand {
            get { return _mouseUpCommand; }
            set { Set(ref _mouseUpCommand, value); }
        }

        /// <summary>
        /// Command for a mouse move event.
        /// </summary>
        public ICommand MouseMoveCommand {
            get { return _mouseMoveCommand; }
            set { Set(ref _mouseMoveCommand, value); }
        }

        /// <summary>
        /// Command for a Key up event.
        /// </summary>
        public ICommand KeyUpCommand {
            get { return _keyUpCommand; }
            set { Set(ref _keyUpCommand, value); }
        }

        /// <summary>
        /// Command for the Save button
        /// </summary>
        public ICommand SaveButtonCommand {
            get { return _saveButtonCommand; }
            set { Set(ref _saveButtonCommand, value); }
        }

        /// <summary>
        /// Command for the Reset button
        /// </summary>
        public ICommand ResetButtonCommand {
            get { return _resetButtonCommand; }
            set { Set(ref _resetButtonCommand, value); }
        }

        /// <summary>
        /// Command for the Zip button
        /// </summary>
        public ICommand ZipBottonCommand {
            get { return _zipBottonCommand; }
            set { Set(ref _zipBottonCommand, value); }
        }


        #endregion



        /// <summary>
        /// Initializes a new instance of the MainViewViewModel class.
        /// </summary>
        public MainViewViewModel(IDataService dataService)
        {
            // init DrawingHandler and register PropertyChanged event (to tell the UI to update the next ShapeToDraw)
            DrawingHandler = new DrawingHandler();
            DrawingHandler.PropertyChanged += DrawingHandlerOnPropertyChanged;

            // load the initial example
            LoadExample();

            // create commands
            MouseDownCommand = new RelayCommand<MouseButtonEventArgs>(OnMouseDown);
            MouseUpCommand = new RelayCommand<MouseButtonEventArgs>(OnMouseUp);
            MouseMoveCommand = new RelayCommand<MouseEventArgs>(OnMouseMove);
            KeyUpCommand = new RelayCommand<KeyEventArgs>(OnKeyUp);

            SaveButtonCommand = new RelayCommand(OnFinishButtonClicked);
            ResetButtonCommand = new RelayCommand(OnResetButtonClicked);
            ZipBottonCommand = new RelayCommand(OnZipButtonClicked);
        }

        /// <summary>
        /// Called when a property of the DrawingHandler instance changes
        /// </summary>
        /// <param name="sender">The sender (in this case should be DrawingHandler)</param>
        /// <param name="propertyChangedEventArgs">Event arguments of the event.</param>
        private void DrawingHandlerOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            // we only want to pass through the NextShapeToDraw
            if (propertyChangedEventArgs.PropertyName != nameof(DrawingHandler.NextShapeToDraw)) return;
            RaisePropertyChanged(nameof(ShapeToDraw));

            // since the next shape to draw has changed, load the next example
            LoadExample();
        }

        /// <summary>
        /// Loads and deserializes a drawing example xml. Based on the current ShapeToDraw of the DrawingHandler.
        /// </summary>
        private void LoadExample()
        {
            // try to read the file
            string xml;
            try
            {
                xml = File.ReadAllText("examples\\" + ShapeToDraw.Name + "Example.xml");
            }
            catch (Exception)
            {
                // on error, we display nothing so set the example to null
                ExampleDrawing = null;
                return;
            }
            // deserialize
            ExampleDrawing = DrawingHandler.Deserialize(xml);
        }

        /// <summary>
        /// Called from when a keyboard key was released.
        /// </summary>
        /// <param name="obj">Key event arguments.</param>
        private void OnKeyUp(KeyEventArgs obj)
        {
            switch (obj.Key)
            {
                case Key.Enter:
                    // simulate a click on the finish button.
                    OnFinishButtonClicked();
                    break;
                case Key.Escape:
                    // simulate a click on the resetbutton.
                    OnResetButtonClicked();
                    break;
            }
        }

        /// <summary>
        /// Called when the reset button was clicked or the corresponding key was pressed.
        /// </summary>
        private void OnResetButtonClicked()
        {
            DrawingHandler.StopDrawing();
            DrawingHandler.ClearDrawing();
        }

        /// <summary>
        /// Called when the finish button was clicked or the corresponding key was pressed.
        /// </summary>
        private void OnFinishButtonClicked()
        {
            DrawingHandler.FinishDrawing(SwitchShapes);
            OnResetButtonClicked();
        }

        /// <summary>
        /// Called when the zip button was clicked or the corresponding key was pressed.
        /// </summary>
        public void OnZipButtonClicked()
        {
            DrawingHandler.ZipAllDrawings();
        }

        /// <summary>
        /// Called when the LMB was pressed down.
        /// </summary>
        public void OnMouseDown(MouseButtonEventArgs e)
        {
            OnResetButtonClicked();
            DrawingHandler.StartDrawing();
        }

        /// <summary>
        /// Called when the LMB was released.
        /// </summary>
        public void OnMouseUp(MouseButtonEventArgs e)
        {
            DrawingHandler.StopDrawing();
        }

        /// <summary>
        /// Called when the mouse moves of the canvas.
        /// </summary>
        public void OnMouseMove(MouseEventArgs e)
        {
            DrawingHandler.DrawInput(new Point(MouseX, MouseY));
        }
    }
}