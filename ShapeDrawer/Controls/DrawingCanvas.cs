﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using ShapeDrawer.Drawing;


namespace ShapeDrawer.Controls {

    /// <summary>
    /// Provides a canvas, which displays a Drawing class.
    /// </summary>
    /// <seealso cref="Drawing" />
    [TemplatePart(Name = "PART_Canvas", Type = typeof(Canvas))]
    public class DrawingCanvas : Control {

        #region DependencyProperties

        public static readonly DependencyProperty DrawingProperty = DependencyProperty.Register(
            "Drawing", typeof(Drawing.Drawing), typeof(DrawingCanvas), new PropertyMetadata(default(Drawing.Drawing), PropertyChangedCallback));

        public Drawing.Drawing Drawing {
            get { return (Drawing.Drawing)GetValue(DrawingProperty); }
            set { SetValue(DrawingProperty, value); }
        }

        private static void PropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs) {
            var drawingCanvas = dependencyObject as DrawingCanvas;
            if (drawingCanvas == null) return;
            switch (dependencyPropertyChangedEventArgs.Property.Name) {
                case nameof(Drawing):
                    drawingCanvas.OnDrawingChanged(dependencyPropertyChangedEventArgs.NewValue as Drawing.Drawing);
                    break;
                default:
                    Debug.Print("PropertyChangedCallback was triggered for unknown dependencyProperty: " + dependencyPropertyChangedEventArgs.Property);
                    return;
            }
        }


        #endregion

        #region Fields

        /// <summary>
        /// The canvas used to render all items.
        /// </summary>
        private Canvas _canvas;

        /// <summary>
        /// The polygon used to draw the points on the Canvas.
        /// </summary>
        private Polygon _polygon;
        #endregion

        static DrawingCanvas() {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(DrawingCanvas), new FrameworkPropertyMetadata(typeof(DrawingCanvas)));
        }

        public DrawingCanvas() {
            _polygon = new Polygon {
                StrokeThickness = 4,
                Stroke = new SolidColorBrush(Color.FromRgb(0, 0, 0))
            };
        }

        /// <summary>
        /// Called when the [drawing property changed].
        /// </summary>
        /// <param name="drawing">The new drawing value.</param>
        private void OnDrawingChanged(Drawing.Drawing drawing) {
            if (drawing == null) return;
            drawing.Points.CollectionChanged += PointsOnCollectionChanged;
            _polygon.Points.Clear();
            foreach (var drawingPoint in drawing.Points) {
                _polygon.Points.Add(new Point(drawingPoint.X, drawingPoint.Y));
            }
        }

        private void PointsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs) {
            switch (notifyCollectionChangedEventArgs.Action) {
                case NotifyCollectionChangedAction.Add:
                    foreach (var newPoint in notifyCollectionChangedEventArgs.NewItems) {
                        var asPoint = (Utils.Point)newPoint;
                        _polygon.Points.Add(new Point(asPoint.X, asPoint.Y));
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var newPoint in notifyCollectionChangedEventArgs.OldItems) {
                        var asPoint = (Utils.Point)newPoint;
                        _polygon.Points.Remove(new Point(asPoint.X, asPoint.Y));
                    }

                    break;
                case NotifyCollectionChangedAction.Replace:
                case NotifyCollectionChangedAction.Move:
                    throw new InvalidOperationException("The Drawing Canvas cannot accept replace or move operations on the Drawing.");
                case NotifyCollectionChangedAction.Reset:
                    _polygon.Points.Clear();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes call <see cref="M:System.Windows.FrameworkElement.ApplyTemplate" />.
        /// </summary>
        public override void OnApplyTemplate() {
            _canvas = GetTemplateChild("PART_Canvas") as Canvas;

            _canvas?.Children.Add(_polygon);

            base.OnApplyTemplate();
        }
    }
}
