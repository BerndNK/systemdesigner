SystemDesigner is a tool that strives to create a simple and quick way to 'doodle' a system.
In terms of features and complexity it tries to settle itself somewhere in between of pen & paper and a UML designer. The idea is, to give the same basic functionality as when drawing on a piece of paper, but also give a couple of tools to speed up the process.
The designer shall be available on all major platforms: Windows Desktop, Windows Universal, MacOS, iOS and Android. This is to ensure that the tool is “always available”.
### How do I get set up? ###
* Use Visual Studio with Xamarin
* Open project from source control and use the repo link https://BerndNK@bitbucket.org/BerndNK/systemdesigner.git

### Solution structure ###
* Core – Contains the core functionality of SystemDesigner
* Libs – Helper libs which are necessary for SystemDesigner, but do work separated
* Modeling – Projects which describe in UML the functionality of SystemDesigner and the libs. Note that class diagrams are stored within the code projects themselves and not in the modeling projects
* Tests – Contains mostly unit tests for the libs and core
* Natives – Contains the projects for each specific platform
* Doc – Contains the documenting files (Not in the solution but in the repo)

### Guideline and convention ###
See “Doc” folder. [wip. Currently only containing pptx describing the structure and render flow]