﻿using System.Collections.Generic;

namespace XmlParser.Xml {
    public interface IXmlTag : ICollection<IXmlTag> {
        #region Properties
        string Name { get; set; } /// Name of the Tag
        IXmlTag Parent { get; set; } /// Parent of the Tag. (This tag is a child of it's parent)
        string TextContent { get; set; } // string content of this tag (only valid when there are no children)
        bool HasTextContent { get; } // wether TextContent is the valid content or Children is}

        #endregion

        /// <summary>
        /// Gets whether this tag has a specific attribute or not. Not case sensitive
        /// </summary>
        /// <param name="name">Name of the attribute</param>
        /// <returns>True for yes, false for no</returns>
        bool HasAttribute(string name);

        /// <summary>
        /// Gets the value of an attribute. Not case sensitive
        /// </summary>
        /// <param name="name">Name of the attribute</param>
        /// <returns>The attribute value as object</returns>
        /// <exception cref="KeyNotFoundException">Thrown when given name is not a valid attribute</exception>
        object GetAttribute(string name);

        /// <summary>
        /// Removes a attribute from this tag. Not case sensitive
        /// </summary>
        /// <param name="name">Name of the attribute</param>
        void RemoveAttribute(string name);

        /// <summary>
        /// Adds a batch of attributes to this tag. Not case sensitive
        /// </summary>
        /// <param name="attributes">The attributes to add</param>
        void AddAttributes(Dictionary<string, object> attributes);

        /// <summary>
        /// Adds a attribute to this tag. Not case sensitive
        /// </summary>
        /// <param name="key">Key or name of the attribute</param>
        /// <param name="value">Value of the attribute</param>
        void AddAttribute(string key, object value);

    }
}
