﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace XmlParser.Xml {
    public class XmlTag : IXmlTag {
        private string _textContent;
        private List<IXmlTag> _children;

        #region Properties
        private Dictionary<string, string> Attributes { get; } = new Dictionary<string, string>(); // Attributes of this tag

        #region IXmlTag
        public List<IXmlTag> Children {
            get { return _children; }
            private set {
                if (TextContent != null && value != null) throw new InvalidOperationException("A XmlTag cannot have IXmlTag children and TextContent at the same time");
                _children = value;
            }
        } // Children of this tag (only valid if there is no text content)

        public string TextContent {
            get { return _textContent; }
            set {
                if (Children != null && value != null) throw new InvalidOperationException("A XmlTag cannot have IXmlTag children and TextContent at the same time");
                _textContent = value;
            }
        } // Text content of this tag (only valid if there are no XmlTag Children)

        public bool HasTextContent => Children == null;
        public string Name { get; set; }
        public IXmlTag Parent { get; set; }
        #endregion

        #region ICollection<T>
        public int Count => Children.Count;
        public bool IsReadOnly => false;
        #endregion

        #endregion

        /// <summary>
        /// Initializes a tag with children
        /// </summary>
        /// <param name="children">Child tags to add</param>
        /// <exception cref="ArgumentException">Thrown when one of the given children already has a set parent or a child is null</exception>
        /// <exception cref="ArgumentNullException">Thrown when children is null</exception>
        public XmlTag(IEnumerable<IXmlTag> children) {
            // guard clauses
            if (children == null) throw new ArgumentNullException(nameof(children), "Children may not be null, use standard Constructor for a SvgTag with no children");

            var asList = new List<IXmlTag>(children);
            foreach (var child in asList) {
                if (child == null) throw new ArgumentException("At least one child is null and is not allowed to be a child", nameof(children));
                if (child.Parent != null) throw new ArgumentException("At least one given child already has a parent", nameof(children));
            }

            // set the parent property for each new child
            foreach (var child in asList) {
                child.Parent = this;
            }

            Children = new List<IXmlTag>(asList);
        }

        public XmlTag(string textContent) {
            // guard clauses
            if (textContent == null) throw new ArgumentNullException(nameof(textContent));

            TextContent = textContent;
        }

        public XmlTag() {
            Children = new List<IXmlTag>();
        }

        #region IXmlTag Implementation


        public bool HasAttribute(string name) {
            return Attributes.Keys.Contains(name.ToLower());
        }

        public object GetAttribute(string name) {
            return Attributes[name.ToLower()];
        }

        public void RemoveAttribute(string name) {
            Attributes.Remove(name.ToLower());
        }

        public void AddAttributes(Dictionary<string, object> attributes) {
            // guard clauses
            if (attributes == null) throw new ArgumentNullException(nameof(attributes));

            foreach (var attribute in attributes.Where(attribute => Attributes.ContainsKey(attribute.Key.ToLower()))) {
                throw new ArgumentException("Attributes contains a key which conflicts with a existing attribute: \"" + attribute.Key + "\"", nameof(attribute));
            }

            // add the attributes
            foreach (var attribute in attributes) {
                Attributes.Add(attribute.Key.ToLower(), attribute.Value.ToString());
            }
        }

        public void AddAttribute(string key, object value) {
            Attributes.Add(key.ToLower(), value.ToString());
        }
        #endregion


        /// <summary>
        /// Returns this tag represented in XML
        /// </summary>
        /// <returns>This tag represented in XML</returns>
        public override string ToString() {
            var toReturn = "";
            var amountOfParents = GetAmountOfParents();
            // add a tabstop for each parent
            for (var i = 0; i < amountOfParents; i++) {
                toReturn += '\t';
            }
            toReturn += "<" + Name;
            toReturn = Attributes.Aggregate(toReturn, (current, attribute) => current + (" " + attribute.Key + "=\"" + attribute.Value + "\""));


            if (Children == null || Children.Count == 0) {
                toReturn += "/>";
            } else {
                toReturn += ">\n";
                toReturn = Children.Aggregate(toReturn, (current, child) => current + (child + "\n"));
                // again add a tabstop for each parent for the closing tag
                for (var i = 0; i < amountOfParents; i++) {
                    toReturn += '\t';
                }
                toReturn += "</" + Name + ">";
            }

            return toReturn;
        }

        #region ICollection<T> Implementation
        public void Add(IXmlTag child) {
            if (HasTextContent) throw new InvalidOperationException("This method is not valid, when the XmlTag has Text content");
            if (child == null) throw new ArgumentNullException(nameof(child), "Child may not be null");
            if (child.Parent != null) throw new ArgumentException("Child already has a parent", nameof(child));

            if (Children == null) Children = new List<IXmlTag>();

            Children.Add(child);
            child.Parent = this;
        }
        
        /// <exception cref="InvalidOperationException">Thrown when method is called when HasTextContent is true</exception>
        public bool Remove(IXmlTag child) {
            if (HasTextContent) throw new InvalidOperationException("This method is not valid, when the XmlTag has Text content");
            if (!Children.Contains(child)) return false;
            if (!Children.Remove(child)) return false;
            child.Parent = null;
            return true;
        }

        /// <exception cref="InvalidOperationException">Thrown when method is called when HasTextContent is true</exception>
        public void Clear() {
            if (HasTextContent) throw new InvalidOperationException("This method is not valid, when the XmlTag has Text content");
            foreach (var child in Children) {
                child.Parent = null;
            }
            Children.Clear();
        }

        /// <exception cref="InvalidOperationException">Thrown when method is called when HasTextContent is true</exception>
        public bool Contains(IXmlTag item) {
            if (HasTextContent) throw new InvalidOperationException("This method is not valid, when the XmlTag has Text content");
            return Children.Contains(item);
        }

        /// <exception cref="InvalidOperationException">Thrown when method is called when HasTextContent is true</exception>
        public void CopyTo(IXmlTag[] array, int arrayIndex) {
            if (HasTextContent) throw new InvalidOperationException("This method is not valid, when the XmlTag has Text content");
            Children.CopyTo(array, arrayIndex);
        }

        /// <exception cref="InvalidOperationException">Thrown when method is called when HasTextContent is true</exception>
        public IEnumerator<IXmlTag> GetEnumerator() {
            if (HasTextContent) throw new InvalidOperationException("This method is not valid, when the XmlTag has Text content");
            return Children.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator() {
            if (HasTextContent) throw new InvalidOperationException("This method is not valid, when the XmlTag has Text content");
            return GetEnumerator();
        }
        #endregion

        /// <summary>
        /// Gets the amount of parents this Tag has by going through it's Parent reference
        /// </summary>
        /// <returns>The amount of parents this tag has</returns>
        private int GetAmountOfParents() {
            var crnt = Parent;
            var amount = 0;
            while (crnt != null) {
                crnt = crnt.Parent;
                amount++;
            }
            return amount;
        }
    }
}