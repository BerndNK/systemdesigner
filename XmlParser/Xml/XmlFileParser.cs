using System;
using System.Collections.Generic;
using System.Linq;

namespace XmlParser.Xml {
    /// <summary>
    /// Helper class to parse a xml file into usable Classes
    /// </summary>
    public static class XmlFileParser {

        /// <summary>
        /// Parses each Tag in a xml file into xmlTag's
        /// </summary>
        /// <param name="content">The xml file to parse</param>
        /// <param name="startIndex">The start index in the string for this child branch</param>
        /// <param name="endIndex">Index where the function shall stop (including the index) Usually the first char of the closing tag of the parent</param>
        /// <returns>First Layer of the xmlTag tree as Enumerable. Each tag contains another Children List</returns>
        /// <exception cref="ArgumentNullException">Thrown when content is null</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when startIndex is negative or greater or equal than length of content</exception>
        /// <exception cref="ArgumentException">Thrown when content represents no valid XML structure</exception>
        public static IEnumerable<IXmlTag> ParseIntoTags(string content, int startIndex = 0, int endIndex = -1) {
            if (content == null) throw new ArgumentNullException(nameof(content));
            if (startIndex < 0 || startIndex >= content.Length) throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, "Index must be inside the bounds of content");
            
            if (endIndex == -1) endIndex = content.Length;

            var toReturn = new List<XmlTag>();

            // while we find a new < which is inside our bounds...
            var nextOpener = content.IndexOf('<', startIndex);
            while (nextOpener >= 0 && nextOpener < endIndex) { // while indexOf succeeded (>=0) and are still in bounds (< endIndex)

                // find next closing / and >
                var nextSlash = IndexOfNextSlash(content, nextOpener);
                var nextCloser = content.IndexOf('>', nextOpener);

                // check if chars were found and if they're outside the endIndex
                if (nextSlash == -1 || nextCloser == -1)
                    throw new ArgumentException("Given xml file has no valid XML structure", nameof(content));
                if (nextSlash >= endIndex || nextCloser >= endIndex)
                    throw new ArgumentException("Given xml file is invalid, or endIndex restricts finding the end of a tag", nameof(content));

                var tagContent = content.Substring(nextOpener, nextCloser - nextOpener);
                var attributes = ReadTagAttributes(tagContent);
                var name = ReadTagName(tagContent);
                XmlTag newTag;


                // if the / comes before the <, then we have a single tag = done. Otherwise it's a tag with children
                if (nextSlash > nextCloser) {
                    var childrenEnd = FindChildEnd(content, nextCloser);
                    if (childrenEnd == -1) throw new ArgumentException("Given xml file has no valid XML structure", nameof(content));
                    var childrenText = content.Substring(nextCloser+1, childrenEnd - (nextCloser+1));

                    // if children contains a <, we try to read it as tags. If not, then we read it as a text

                    if (childrenText.Contains("<")) {
                        var children = ParseIntoTags(content, nextCloser, childrenEnd);
                        newTag = new XmlTag(children); // create xmlTag with other tags as content
                    }else newTag = new XmlTag(childrenText); // create XmlTag with string as content

                    nextOpener = childrenEnd + 1;
                } else newTag = new XmlTag();

                // add attributes to new Tag and set the name
                newTag.AddAttributes(attributes);
                newTag.Name = name;
                toReturn.Add(newTag);

                nextOpener = content.IndexOf('<', nextOpener + 1);

            }
            return toReturn;
        }

        /// <summary>
        /// Reads the name of a tag
        /// </summary>
        /// <param name="tagContent">The tag the name shall be read</param>
        /// <returns>Name of the tag</returns>
        /// <exception cref="ArgumentNullException">Thrown when tagContent is null</exception>
        /// <exception cref="ArgumentException">Thrown when tagContent is too short</exception>
        private static string ReadTagName(string tagContent) {
            if (tagContent == null) throw new ArgumentNullException(nameof(tagContent));
            if (tagContent.Length < 2) throw new ArgumentException("Given tag was too short", nameof(tagContent));

            // get the indices of the next chars which can end a name. E.g. <asd key="value"> (' ') , <name/>, ('/'), </name> ('<')
            var nextChars = new List<int> {
                tagContent.IndexOf(' '),
                tagContent.IndexOf('>')
            };
            if (tagContent[1] != '/') nextChars.Add(tagContent.IndexOf('/')); // '/' is only valid if the tag is not a closing tag e.g. </name>

            nextChars.Sort();
            try {
                return tagContent.Substring(1, nextChars.First(x => x > 0) - 1);
            } catch (Exception) {
                // will occur if each index is negative (-1) and .First will throw an exception
                return tagContent.Substring(1); // if each index was -1, return the full tagContent (minus '<')
            }
        }

        /// <summary>
        /// Reads through the content and searches for the next /. However ignoring slashes which are in between ""
        /// </summary>
        /// <param name="content">The xml file to search trough</param>
        /// <param name="startIndex">Start position to look from</param>
        /// <returns>Index of the found slash, or -1 if none was found</returns>
        /// <exception cref="ArgumentNullException">Thrown when content is null</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when startIndex is outside of the bounds of content</exception>
        private static int IndexOfNextSlash(string content, int startIndex) {
            if (content == null) throw new ArgumentNullException(nameof(content));
            if (startIndex < 0 || startIndex >= content.Length) throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, "Start index must be within bounds of content");

            var withinApostrophe = false; // if we're currently in between ""
            for (var i = startIndex; i < content.Length; i++) {
                var crntChar = content[i];
                switch (crntChar) {
                    case '\"':
                    case '\'':
                        withinApostrophe = !withinApostrophe;
                        break;
                    case '/':
                        if (!withinApostrophe) return i;
                        break;
                    default:
                        // ignored;
                        break;
                }
            }
            return -1;
        }

        /// <summary>
        /// Takes a string and reads out attributes in the form of X="Y". With X being the key and Y being the value. "
        /// </summary>
        /// <param name="tagString">Target string containing the attributes</param>
        /// <returns>Dictionary with the attribute name being the key and attribute value being the value.</returns>
        /// <exception cref="ArgumentNullException">Thrown when tagString is null</exception>
        private static Dictionary<string, object> ReadTagAttributes(string tagString) {
            if (tagString == null) throw new ArgumentNullException(nameof(tagString));
            var toReturn = new Dictionary<string, object>();

            var crntKey = "";
            var crntValue = "";
            var inValue = false; // whether we're currently in between "" or not
            foreach (var crntChar in tagString) {
                switch (crntChar) {
                    case '\'':
                    case '"':
                        inValue = !inValue;
                        if (inValue == false) { // if it's not false, we just read a value
                            toReturn.Add(crntKey, crntValue);
                            crntKey = "";
                            crntValue = "";
                        }
                        break;
                    case ' ':
                        // if we're in a value, the space is part of it
                        if (inValue) crntValue += crntChar;
                        else crntKey = ""; // otherwise the space is a separator and therefore deletes the current key e.g. <asd key="value"/> the ' ' is a separator between "asd" and "key"
                        break;
                    case '\n':
                    case '\t':
                    case '\r':
                    case '=':
                    case '<':
                        // ignore whitespaces and control characters
                        break;
                    default:
                        if (inValue) crntValue += crntChar;
                        else crntKey += crntChar;
                        break;
                }
            }

            return toReturn;
        }

        /// <summary>
        /// Returns the end of a children container
        /// </summary>
        /// <param name="content">Target xml file</param>
        /// <param name="startIndex">Start Index of the children container</param>
        /// <returns>The Index of the first char of the closing tag -1 if the tag opener and closer do not match up</returns>
        /// <exception cref="ArgumentNullException">Thrown when content is null</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when startIndex is negative or greater than length of count-1</exception>
        private static int FindChildEnd(string content, int startIndex) {
            if (content == null) throw new ArgumentNullException(nameof(content), "Content may not be null");
            if (startIndex < 0 || startIndex >= content.Length) throw new ArgumentOutOfRangeException(nameof(startIndex), startIndex, "Index must be inside the bounds of content");

            var crntLevel = 0;
            var readText = false; // if we have read text after the last <
            var inTag = false; // if we're currently between '<''>' or not
            var lastOpener = -1; // the last '<' we've read
            var sawSlash = false; // if we saw a slash in the current tag
            for (var i = startIndex; i < content.Length; i++) {
                switch (content[i]) {
                    case '<':
                        inTag = true;
                        readText = false;
                        lastOpener = i;
                        break;
                    case '/':
                        if (!inTag) continue; // read next char if we encounter a / and are not in a tag
                        // if we read text after a <, we have a single line Tag e.g. <asd/> = no level change. If we did NOT read text after the <, we have a closing tag e.g. </asd> = level - 1
                        if (!readText) --crntLevel;
                        sawSlash = true;
                        if (crntLevel == -1) return lastOpener; // if we read more closer than opener, we reached the end of the parent container (which we're looking for)
                        break;
                    case '>':
                        if (!inTag) continue; // '>' is only a tag closer if we're in a tag (saw a '<')
                        if (!sawSlash) ++crntLevel; // if we encounter the end of a tag and did not see a slash, the tag opens = level + 1
                        sawSlash = false;
                        inTag = false;
                        break;
                    case ' ':
                    case '\n':
                    case '\r':
                    case '\t':
                        // ignore whitespaces
                        break;
                    default:
                        readText = true;
                        break;
                }
            }

            // if we reached the end of the file return -1, as the tags to not add up
            return -1;
        }
    }
}