﻿using System;
using System.Collections.Generic;
using System.Linq;
using XmlParser.Xml;

namespace XmlParser.Convert {
    public abstract class XmlConverter<TContainer, TItem> : IXmlConverter<TItem> where TContainer : class, ICollection<TItem>, TItem where TItem : class {
        #region Properties

        #region IXmlConverter
        public List<IXmlConverterModule<TContainer, TItem>> ConverterModules { get; } = new List<IXmlConverterModule<TContainer, TItem>>(); // Modules which converts tags

        public List<string> convertibleTags {
            get { return ConverterModules.Select(x => x.ConvertsFrom.ToLower()).ToList(); }
        }// List of tags which this converter can take
        #endregion
        #endregion


        #region IXmlConverter Implementation
        /// <exception cref="ArgumentNullException">Thrown when xmlFileRoot is null</exception>
        public TItem Convert(IXmlTag xmlFileRoot) {
            if (xmlFileRoot == null) throw new ArgumentNullException(nameof(xmlFileRoot));

            return ProduceRecursiveHelper(xmlFileRoot, null);
        }

        /// <exception cref="ArgumentNullException">Thrown when xmlFileRoot is null</exception>
        public List<string> GetUnknownTags(IXmlTag xmlFileRoot) {
            if (xmlFileRoot == null) throw new ArgumentNullException(nameof(xmlFileRoot));

            var toReturn = new List<string>();
            HasUnknownTagsRecursiveHelper(xmlFileRoot, ref toReturn);
            return toReturn;
        }
        #endregion

        /// <summary>
        /// Helper function for Convert. Recursively implemented.
        /// </summary>
        /// <param name="xmlTag">The source xml tag to convert from</param>
        /// <param name="parent">Parent this converted item shall have</param>
        /// <returns>Converted item or null if not convertible</returns>
        /// <exception cref="ArgumentNullException">Thrown when xmlTag is null</exception>
        /// <exception cref="ArgumentException">Thrown when a given tag is not convertible</exception>
        private TItem ProduceRecursiveHelper(IXmlTag xmlTag, TContainer parent) {
            if (xmlTag == null) throw new ArgumentNullException(nameof(xmlTag));
            if (!IsConvertible(xmlTag)) throw new ArgumentException($"Given tag \"{xmlTag.Name}\" is not convertible with any of the given modules");

            var convertModule = ConverterModules.First(x => string.Equals(x.ConvertsFrom, xmlTag.Name, StringComparison.CurrentCultureIgnoreCase));
            var result = convertModule.Convert(xmlTag, parent);
            parent?.Add(result);

            // if the converted tag is a container
            var container = result as TContainer;
            if (container != null) {
                // add the children of the target xml tag
                foreach (var xmlTagChild in xmlTag) {
                    ProduceRecursiveHelper(xmlTagChild, container);
                }
            }

            // return the produced item
            return result;
        }

        private bool IsConvertible(IXmlTag xmlTag) => convertibleTags.Contains(xmlTag?.Name?.ToLower());

        /// <summary>
        /// Helper function for GetUnknownTags. Recursively implemented
        /// </summary>
        /// <param name="xmlTag">Current xml tag to check. Root on the first call</param>
        /// <param name="output">List which shall contain the unknown tags</param>
        /// <exception cref="ArgumentNullException">Thrown when output or xmlTag is null</exception>
        private void HasUnknownTagsRecursiveHelper(IXmlTag xmlTag, ref List<string> output) {
            // guard clauses
            if (xmlTag == null) throw new ArgumentNullException(nameof(xmlTag));
            if (output == null) throw new ArgumentNullException(nameof(output));

            if (!xmlTag.HasTextContent) { // if xml tag has children
                // call this method for each of the childs
                foreach (var child in xmlTag) {
                    HasUnknownTagsRecursiveHelper(child, ref output);
                }
            }

            if (IsConvertible(xmlTag)) return; // if current xml Tag's name is unknown
            if (!output.Contains(xmlTag.Name)) // and the name is not already in the output
                output.Add(xmlTag.Name);
        }
    }
}