﻿using System.Collections.Generic;
using XmlParser.Xml;

namespace XmlParser.Convert {

    public interface IXmlConverter<out TItem> where TItem : class {

        /// <summary>
        /// Produces a Structure of xml tags and tries to parse it into TItem's
        /// </summary>
        /// <param name="xmlFileRoot">Xml structure to parse into svg</param>
        /// <returns>Converted item which is presumably a Container but not guaranteed</returns>
        TItem Convert(IXmlTag xmlFileRoot);

        /// <summary>
        /// Checks if a xml element has tags that this converter does not know
        /// </summary>
        /// <param name="xmlFileRoot">Root of the xml tree to check</param>
        /// <returns>List of unknown names</returns>
        List<string> GetUnknownTags(IXmlTag xmlFileRoot);

    }
}