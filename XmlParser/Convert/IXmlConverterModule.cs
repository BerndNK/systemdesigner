﻿using System.Collections.Generic;
using XmlParser.Xml;

namespace XmlParser.Convert {
    public interface IXmlConverterModule<in TContainer, out TItem> where TContainer : class, ICollection<TItem>, TItem where TItem : class {
        string ConvertsFrom { get; }

        /// <summary>
        /// Takes a xml tag and converts it to a designated type
        /// </summary>
        /// <param name="xmlTag">Source xml tag</param>
        /// <param name="parent">Parent of the current xml tag</param>
        /// <returns>TItem derived class as result</returns>
        TItem Convert(IXmlTag xmlTag, TContainer parent);
    }
}