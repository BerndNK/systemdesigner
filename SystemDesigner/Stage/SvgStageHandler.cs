﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Linq;
using System.Xml.Serialization;
using SystemDesignerResources.Image;
using DisplayCore.Annotations;
using DisplayCore.Render.Geometry;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms;
using SvgAbstraction.Svg.Forms.Container;

namespace SystemDesigner.Stage
{
    /// <summary>
    /// A handler for the SvgStage
    /// </summary>
    /// <seealso cref="SystemDesigner.Stage.IStageHandler{ISvgItem}"/>
    public class SvgStageHandler : IStageHandler<SvgStage>
    {
        private SvgStage _activeStage;

        public SvgStage ActiveStage {
            get { return _activeStage; }
            set {
                // set the active tool of the new Stage, to the same one as from the previously active stage
                var toolFromOldStage = _activeStage?.ActiveTool;
                if (toolFromOldStage != null)
                {
                    value?.SetActiveTool(value.Tools.FirstOrDefault(x => x.Name == toolFromOldStage.Name));
                }
                _activeStage = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<SvgStage> StageList { get; private set; }
        /// <summary>
        /// Gets or sets the current active instance.
        /// </summary>
        public static SvgStageHandler Current { get; set; }

        /// <summary>
        /// Adds a new stage deserialized from the given XML string.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <returns>
        /// The deserialized stage.
        /// </returns>
        public SvgStage AddNewStageFromXml(string xml)
        {
            // parse the XML
            var doc = XDocument.Parse(xml);
            if (doc.Root == null) throw new Exception("Failed to parse xml.");
            
            // create the serializer
            var xmlSerializer = new XmlSerializer(typeof(SvgStage));
            using (var reader = doc.Root.CreateReader())
            {
                // deserialize and add it to the stage
                var stage = (SvgStage)xmlSerializer.Deserialize(reader);
                StageList.Add(stage);
                return stage; // return the deserialized stage
            }

        }

        /// <summary>
        /// Gets the active stage as XML.
        /// </summary>
        /// <returns>
        /// The serialized stage.
        /// </returns>
        /// <exception cref="NotImplementedException"></exception>
        public string GetActiveStageAsXml()
        {
            // use a XmlSerializer and a stringWriter to create the XML
            var xmlSerializer = new XmlSerializer(typeof(SvgStage));
            using (var stringWriter = new StringWriter())
            {
                xmlSerializer.Serialize(stringWriter, ActiveStage);
                return stringWriter.ToString(); // return the result
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SvgStageHandler"/> class.
        /// </summary>
        public SvgStageHandler()
        {
            StageList = new ObservableCollection<SvgStage>();
            StageList.CollectionChanged += StageListOnCollectionChanged;
            Current = this;
            //StageList.Add(new SvgStage());
            //ActiveStage.SetActiveTool(Tools.First());
        }

        private void StageListOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (ActiveStage != null || StageList.Count == 0) return;
            ActiveStage = StageList.First();
            ActiveStage.SetActiveTool(ActiveStage.Tools.FirstOrDefault());
        }


        /// <summary>
        /// Adds a couple of shapes to the stage for testing in design time
        /// </summary>
        public void AddTestShapes()
        {
            if (ActiveStage == null)
            {
                var svgStage = new SvgStage();
                ActiveStage = svgStage;
                StageList.Add(svgStage);
            }
            var rnd = new Random();
            var toAdd = new List<ISvgItem>();
            for (var i = 0; i < 10; i++)
            {

                toAdd.Add(new SvgRect(171, 141) { X = 38, Y = 44, FillColor = 0xff0000cc });
                toAdd.Add(new SvgRect(171, 171) { X = 62, Y = 181, FillColor = 0xff00cc00 });
                toAdd.Add(new SvgRect(171, 171) { X = 162, Y = 164, FillColor = 0xff3300cc });
                toAdd.Add(new SvgRect(220, 231) { X = 144, Y = 18, FillColor = 0xffcc6600 });
                toAdd.Add(new SvgRect(184, 184) { X = 107 + 184 / 2, Y = 113 + 184 / 2, FillColor = 0xffcc0000, Rotation = Math.PI / 4, PivotX = 184.0 / 2, PivotY = 184.0 / 2 });
                if (i == 0) continue;
                foreach (var svgItem in ActiveStage.Root)
                {
                    svgItem.X += rnd.Next(-500, 500);
                    svgItem.Y += rnd.Next(-500, 500);
                }
            }




            // make grid

            // vertical lines
            for (var x = 0; x <= 50; x++)
            {
                toAdd.Add(new SvgRect((x % 10 == 0 ? 2 : 1), 25 * 50) { X = x * 25, Y = 0 });
            }

            // horizontal lines
            for (var y = 0; y <= 50; y++)
            {
                toAdd.Add(new SvgRect(25 * 50, (y % 10 == 0 ? 2 : 1)) { X = 0, Y = 25 * y });
            }



            toAdd.Add(new SvgRect(50 * 5, 50 * 5) { X = 0 + 500, Y = 0 + 500, FillColor = 0xff2A5ED2 });
            toAdd.Add(new SvgRect(100, 100) { X = 100 + 500, Y = 100 + 500, FillColor = 0xffffffff });
            toAdd.Add(new SvgRect(100, 100) { X = 50 + 500, Y = 50 + 500, FillColor = 0xff579DE3 });
            // vertical lines
            for (var x = 0; x <= 5; x++)
            {
                toAdd.Add(new SvgRect((x == 0 || x == 10 ? 2 : 1), 50 * 5) { X = x * 50 + 500, Y = 0 + 500, FillColor = 0xffffffff });
            }

            // horizontal lines
            for (var y = 0; y <= 5; y++)
            {
                toAdd.Add(new SvgRect(50 * 5, (y == 0 || y == 10 ? 2 : 1)) { X = 0 + 500, Y = 50 * y + 500, FillColor = 0xffffffff });
            }


            // add ellipse and image test
            toAdd.Add(new SvgEllipse(50) { X = 200, Y = 200, FillColor = 0xffB83939, Width = 150 });
            toAdd.Add(new SvgEllipse(50) { X = 300, Y = 200, FillColor = 0xffB83939, Height = 150 });
            toAdd.Add(new SvgEllipse(50) { X = 200, Y = 300, FillColor = 0xffB83939, ScaleX = 0.5, ScaleY = 2 });
            toAdd.Add(new SvgEllipse(50) { X = 300 + 100, Y = 300 + 100, FillColor = 0xffB83939, ScaleY = 0.25, ScaleX = 0.25 });
            toAdd.Add(new SvgEllipse(80) { X = 220, Y = 220, FillColor = 0xff7AB839 });

            toAdd.Add(new SvgEllipse(100) { X = 0, Y = 000, PivotX = 100, PivotY = 100, FillColor = 0xff0000ff });
            toAdd.Add(new SvgEllipse(80) { X = 0, Y = 000, PivotX = 80, PivotY = 80, FillColor = 0xff00ffff });
            toAdd.Add(new SvgEllipse(60) { X = 0, Y = 000, PivotX = 60, PivotY = 60, FillColor = 0xff00ff00 });
            toAdd.Add(new SvgEllipse(40) { X = 0, Y = 000, PivotX = 40, PivotY = 40, FillColor = 0xffffff00 });
            toAdd.Add(new SvgEllipse(20) { X = 0, Y = 000, PivotX = 20, PivotY = 20, FillColor = 0xffff0000 });

            toAdd.Add(new SvgImage(Icons.Plus) { X = 100, Y = 400 });

            toAdd.Add(new SvgPolyline {
                new Point(0),
                new Point(100),
                new Point(100,100),
                new Point(0,100),
                new Point(0),
                new Point(100,100),
                new Point(100),
                new Point(0,100)
            });

            var newContainer = new SvgGroup();
            newContainer.Rotation = 30;
            newContainer.Add(new SvgRect(100, 100) { FillColor = 0xff00ff00 });
            toAdd.Add(newContainer);
            ActiveStage.Root.AddMultiple(toAdd);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
