﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace SystemDesigner.Stage {
    public interface IStageHandler<T> : INotifyPropertyChanged where T  : IStage {

        /// <summary>
        /// Gets the stage.
        /// </summary>
        /// <value>
        /// The stage.
        /// </value>
        T ActiveStage { get; }

        /// <summary>
        /// Gets the list of loaded stages.
        /// </summary>
        ObservableCollection<T> StageList { get; }

        /// <summary>
        /// Adds a new stage deserialized from the given XML string.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <returns>The deserialized stage.</returns>
        T AddNewStageFromXml(string xml);

        /// <summary>
        /// Gets the active stage as XML.
        /// </summary>
        /// <returns>The serialized stage.</returns>
        string GetActiveStageAsXml();
    }
}
