﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;
using SystemDesigner.Input;
using SystemDesigner.Stage.Tools;
using SystemDesigner.Stage.Tools.Action;
using SystemDesigner.Svg.Convert;
using DisplayCore.Annotations;
using DisplayCore.Display;
using DisplayCore.Render.Geometry;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms.Container;
using XmlParser.Xml;

namespace SystemDesigner.Stage {
    /// <summary>
    /// Stage for ISvgItems
    /// </summary>
    [XmlRoot("stage")]
    public class SvgStage : IStage<ISvgItem> {
        #region Fields
        private IStageTool<IStage<ISvgItem>> _activeTool;

        /// <summary>
        /// The actions which shall be reversed on an undo invoke.
        /// </summary>
        private Stack<IAction> _undoActions;

        /// <summary>
        /// The action which shall be executed on an redo invoke.
        /// </summary>
        private Stack<IAction> _redoActions;

        private string _name = "New Stage";
        private string _source;
        private bool _hasUnsavedChanges = true;

        #endregion

        #region Properties
        
        [XmlIgnore]
        public IList<IStageTool<IStage<ISvgItem>>> Tools { get; }

        [XmlAttribute("name")]
        public string Name
        {
            get => _name;
            set
            {
                _name = value; 
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the source of this stage (a filePath for example).
        /// </summary>
        [XmlIgnore]
        public string Source
        {
            get => _source;
            set
            {
                _source = value; 
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the root as XML. This should only be used by the XML serializer.
        /// </summary>
        [XmlElement("root")]
        public string RootAsXml
        {
            get => ((SvgItemContainer) Root).ToXml();
            set
            {
                var converter = new XmlToSvgConverter();
                Root = converter.Convert(XmlFileParser.ParseIntoTags(value).First()) as SvgItemContainer;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance can redo an action.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance can redo; otherwise, <c>false</c>.
        /// </value>
        public bool CanRedo => _redoActions.Count != 0;

        /// <summary>
        /// Gets or sets a value indicating whether this instance has unsaved changes.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has unsaved changes; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool HasUnsavedChanges
        {
            get { return _hasUnsavedChanges; }
            set
            {
                _hasUnsavedChanges = value; 
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance can undo an action.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance can undo; otherwise, <c>false</c>.
        /// </value>
        public bool CanUndo => _undoActions.Count != 0;
        #region IRenderable

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [XmlIgnore]
        public int Id { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is visible.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is visible; otherwise, <c>false</c>.
        /// </value>
        public bool IsVisible { get; set; }

        #endregion
        #region IStage<T>
        /// <summary>
        /// Gets the root.
        /// </summary>
        [XmlIgnore]
        public DisplayObjectContainer<ISvgItem> Root { get; private set; } = new SvgRoot();

        /// <summary>
        /// Gets the active tool.
        /// </summary>
        [XmlIgnore]
        public IStageTool<IStage<ISvgItem>> ActiveTool {
            get { return _activeTool; }
            private set {
                _activeTool = value;
                OnPropertyChanged();
            }
        }

        #endregion
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SvgStage"/> class.
        /// </summary>
        public SvgStage()
        {
            Tools = GetTools();
            Id = DisplayObject.GetNewId();
            // select the first tool by default
            ActiveTool = Tools.FirstOrDefault();
            _redoActions = new Stack<IAction>();
            _undoActions = new Stack<IAction>();
        }

        /// <summary>
        /// Called when an [action was executed] by a tool.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ActionEventArgs"/> instance containing the event data.</param>
        private void OnActionExecuted(object sender, ActionEventArgs e)
        {
            // track the executed action, so we can undo it later.
            _undoActions.Push(e.Action);
        }

        /// <summary>
        /// Gets the tools for this stage.
        /// </summary>
        /// <returns>The tools that can be used with this stage.</returns>
        private IList<IStageTool<IStage<ISvgItem>>> GetTools()
        {
            return new List<IStageTool<IStage<ISvgItem>>> {
                new SelectTool(),
                new MultiSelectTool(),
                new FreeHandTool(),
                new NeuralNetworkTestTool()
            };
        }


        public override string ToString() => Name;

        public void Undo()
        {
            if (!CanUndo) return;
            // pop the last action from the stack
            var undoneAction = _undoActions.Pop();
            // reverse its effects
            undoneAction.Reverse();
            // and add it to the redo stack
            _redoActions.Push(undoneAction);
        }

        public void Redo()
        {
            if (!CanRedo) return;
            // pop the last action from the redo actions
            var redoneAction = _redoActions.Pop();
            // execute it again
            redoneAction.Execute();
            // and push it again to the undo stack
            _undoActions.Push(redoneAction);
        }

        public void SetActiveTool(IStageTool<IStage<ISvgItem>> tool)
        {
            if (ActiveTool != null) ActiveTool.ActionExecuted -= OnActionExecuted;
            ActiveTool = tool;
            ActiveTool.SetTarget(this);
            ActiveTool.ActionExecuted += OnActionExecuted;
        }

        public bool? OnInputMove(Point location, InputType inputType, int id) {
            // pass through the input to the active tool
            return ActiveTool?.OnInputMove(location, inputType, id);
        }

        public bool? OnInputDown(Point location, InputType inputType, int id) {
            // pass through the input to the active tool
            return ActiveTool?.OnInputDown(location, inputType, id);
        }

        public bool? OnInputUp(Point location, InputType inputType, int id) {
            // pass through the input to the active tool
            return ActiveTool?.OnInputUp(location, inputType, id);
        }

        public void InvokeRender(double parentPivotX, double parentPivotY, double parentScaleX, double parentScaleY, double parentRotation) {
            Root.InvokeRender(parentPivotX, parentPivotY, parentScaleX, parentScaleY, parentRotation);
            // render UI if there is one for the active Tool
            ActiveTool?.UiContainer?.InvokeRender(parentPivotX, parentPivotY, parentScaleX, parentScaleY, parentRotation);
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}