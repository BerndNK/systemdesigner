﻿using System.Collections.Generic;
using System.ComponentModel;
using SystemDesigner.Input;
using SystemDesigner.Stage.Tools;
using DisplayCore.Display;
using DisplayCore.Render;

namespace SystemDesigner.Stage
{
    public interface IStage : IFullInputReceiver, IRenderable, INotifyPropertyChanged
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the source of this stage (a filePath for example).
        /// </summary>
        string Source { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance can redo an action.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance can redo; otherwise, <c>false</c>.
        /// </value>
        bool CanRedo { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has unsaved changes.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has unsaved changes; otherwise, <c>false</c>.
        /// </value>
        bool HasUnsavedChanges { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance can undo an action.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance can undo; otherwise, <c>false</c>.
        /// </value>
        bool CanUndo { get; }

        /// <summary>
        /// Invoke a undo of the last made action on this stage.
        /// </summary>
        void Undo();

        /// <summary>
        /// Invoke a redo of the last reversed action on this stage.
        /// </summary>
        void Redo();
        
    }

    /// <summary>
    /// Describes a stage, which handles IDisplayObjects in it. Containing a root and input functionality
    /// </summary>
    /// <typeparam name="T">Derived type of IDisplayObject</typeparam>
    public interface IStage<T> : IStage where T : IDisplayObject
    {
        #region Properties

        DisplayObjectContainer<T> Root { get; } // Display root of the stage

        IStageTool<IStage<T>> ActiveTool { get; } // currently active tool

        #endregion


        /// <summary>
        /// Sets a new active StageTool
        /// </summary>
        /// <param name="tool">The tool to be set active</param>
        void SetActiveTool(IStageTool<IStage<T>> tool);


        /// <summary>
        /// The tools available for the stage
        /// </summary>
        /// <value>
        /// The tools.
        /// </value>
        IList<IStageTool<IStage<T>>> Tools { get; }

    }
}