﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DisplayCore.Annotations;

namespace SystemDesigner.Stage.Tools.Parts
{
    /// <summary>
    /// Basic tool modifier
    /// </summary>
    /// <seealso cref="SystemDesigner.Stage.Tools.IToolModifier" />
    public class BaseToolModifier : IToolModifier
    {
        private string _description;
        private string _name;
        public string IconPath { get; }
        public List<int> ActivatingTokens { get; }

        public bool IsActive => ActivatingTokens.Count > 0;

        public string Name {
            get => _name;
            set {
                _name = value;
                OnPropertyChanged();
            }
        }

        public string Description {
            get => _description;
            set {
                _description = value;
                OnPropertyChanged();
            }
        }

        public void AddActivationInput(int id)
        {
            if (!ActivatingTokens.Contains(id))
            {
                ActivatingTokens.Add(id);
                OnPropertyChanged(nameof(IsActive));
            }
        }

        public void RemoveActivationInput(int id)
        {
            if (ActivatingTokens.Contains(id))
            {
                ActivatingTokens.Remove(id);
                OnPropertyChanged(nameof(IsActive));
            }
        }

        public BaseToolModifier(string iconPath, string name, string description)
        {
            IconPath = iconPath;
            Name = name;
            Description = description;
            ActivatingTokens = new List<int>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
