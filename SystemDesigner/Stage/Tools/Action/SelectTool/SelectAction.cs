﻿using System.Collections.Generic;
using System.Linq;
using SvgAbstraction.Svg;

namespace SystemDesigner.Stage.Tools.Action.SelectTool
{
    /// <summary>
    /// Describes the action of selecting or deselecting one or multiple items
    /// </summary>
    /// <seealso cref="SystemDesigner.Stage.Tools.Action.IAction" />
    public class SelectAction : ITargetedAction<ISvgItem>
    {
        private readonly bool _deselect; // whether it was an deselect action or not
        private readonly Tools.SelectTool _tool; // the tool that performed the action

        public IEnumerable<ISvgItem> Targets { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SelectAction" /> class.
        /// </summary>
        /// <param name="targets">The targets of the action.</param>
        /// <param name="tool">The tool that performed the action.</param>
        /// <param name="deselect">if set to <c>true</c> it was an [deselecting] action.</param>
        public SelectAction(IEnumerable<ISvgItem> targets, Tools.SelectTool tool, bool deselect = false)
        {
            Targets = targets.ToList(); // make sure to only use a copy of the given IEnumerable, to avoid having the list change
            _tool = tool;
            _deselect = deselect;
        }

        public void Execute()
        {
            // go through each target
            foreach (var svgItem in Targets)
            {
                // set the selection hint
                svgItem.ShowSelectionHint = !_deselect;
                // add or remove the item from the tools' selection list
                if (_deselect)
                    _tool.SelectedItems.Remove(svgItem);
                else
                    _tool.SelectedItems.Add(svgItem);
            }
        }

        public void Reverse()
        {
            // go through each target
            foreach (var svgItem in Targets)
            {
                // set the selection hint
                svgItem.ShowSelectionHint = _deselect;
                // add or remove the item from the tools' selection list
                if (!_deselect)
                    _tool.SelectedItems.Remove(svgItem);
                else
                    _tool.SelectedItems.Add(svgItem);
            }
        }
    }
}
