﻿namespace SystemDesigner.Stage.Tools.Action
{
    /// <summary>
    /// Describes a action for a stage, which can be executed and reversed.
    /// </summary>
    public interface IAction
    {

        /// <summary>
        /// Executes this action.
        /// </summary>
        void Execute();

        /// <summary>
        /// Reverses the effects of this action.
        /// </summary>
        void Reverse();
    }
}
