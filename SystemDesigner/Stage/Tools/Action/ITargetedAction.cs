using System.Collections.Generic;

namespace SystemDesigner.Stage.Tools.Action
{
    /// <summary>
    /// Special action with targets of type T
    /// </summary>
    /// <typeparam name="T">Type of the targets</typeparam>
    public interface ITargetedAction<out T> : IAction
    {
        /// <summary>
        /// Gets the targets of this action.
        /// </summary>
        IEnumerable<T> Targets { get; }
    }
}