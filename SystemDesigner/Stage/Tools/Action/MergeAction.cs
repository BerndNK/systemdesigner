﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SystemDesigner.Stage.Tools.Action
{
    /// <summary>
    /// Action that contains multiple actions, so they can be treated as one.
    /// </summary>
    /// <seealso cref="SystemDesigner.Stage.Tools.Action.IAction" />
    public class MergeAction : IList<IAction>, IAction
    {
        public List<IAction> Actions { get; private set; }


        public MergeAction(IEnumerable<IAction> actions)
        {
            Actions = actions.ToList();
        }

        /// <summary>
        /// Adds the action at index = 0. In front of all other elements.
        /// </summary>
        /// <param name="action">The action to be added.</param>
        public void AddAtStart(IAction action)
        {
            var newActions = new IAction[Actions.Count + 1];
            Actions.CopyTo(0, newActions, 1, Count);
            newActions[0] = action;
            Actions = newActions.ToList();
        }

        #region IAction implementation

        public void Execute()
        {
            foreach (var action in Actions)
            {
                action.Execute();
            }
        }

        public void Reverse()
        {
            // go through the action list in reverse and trigger the reverse method
            for (var i = Actions.Count - 1; i >= 0; i--)
            {
                Actions[i].Reverse();
            }
        }

        #endregion

        #region IList implementation

        public IEnumerator<IAction> GetEnumerator() => Actions.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public void Add(IAction item) => Actions.Add(item);

        public void Clear() => Actions.Clear();

        public bool Contains(IAction item) => Actions.Contains(item);

        public void CopyTo(IAction[] array, int arrayIndex) => Actions.CopyTo(array, arrayIndex);

        public bool Remove(IAction item) => Actions.Remove(item);

        public int Count => Actions.Count;
        public bool IsReadOnly => false;
        public int IndexOf(IAction item) => Actions.IndexOf(item);

        public void Insert(int index, IAction item) => Actions.Insert(index, item);

        public void RemoveAt(int index) => Actions.RemoveAt(index);

        public IAction this[int index]
        {
            get => Actions[index];
            set => Actions[index] = value;
        }

        #endregion
    }
}