﻿using System;

namespace SystemDesigner.Stage.Tools.Action
{
    /// <summary>
    /// Event arguments for an event which is relevant to an IAction.
    /// </summary>
    /// <seealso cref="System.EventArgs" />
    public class ActionEventArgs : EventArgs
    {
        public IAction Action { get; set; }

        public ActionEventArgs(IAction action)
        {
            Action = action;
        }
    }
}