﻿using System.Collections.Generic;

namespace SystemDesigner.Stage.Tools.Action
{
    /// <summary>
    /// Static extensions for IAction management
    /// </summary>
    public static class ActionExtensions
    {
        /// <summary>
        /// Merges both this and the target into a single action.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="target">The target action that shall always be executed in succession of this action.</param>
        /// <returns>A new MergeAction instance which contains both [this] instance and the target.</returns>
        public static IAction Merge(this IAction action, IAction target)
        {
            // return both this and the target in a new MergeAction. If any of the two is already a merge action, add the other action to the list
            // if [this] action is an MergeAction
            if (action is MergeAction)
            {
                // add the target to the list
                ((MergeAction)action).Add(target);
                return action;
            }

            // if the target is an merge action
            if (target is MergeAction)
            {
                // add [this] to the list, but in front of the list, to make sure that [this] is executed first.
                ((MergeAction) target).AddAtStart(action);
                return target;
            }

            // otherwise return a new action which merged both [this] and the target
            return new MergeAction(new List<IAction> { action, target });
        }
    }
}
