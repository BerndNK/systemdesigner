﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SystemDesigner.Input;
using SystemDesigner.Input.Shortcuts;
using SystemDesigner.Stage.Tools.Action;
using SystemDesigner.Stage.Tools.Action.SelectTool;
using SystemDesigner.Stage.Tools.Helper;
using SystemDesigner.Stage.Tools.Parts;
using SystemDesigner.Stage.Tools.Ui;
using SystemDesignerResources.Image;
using SystemDesignerResources.Text;
using DisplayCore.Render.Geometry;
using SvgAbstraction.Svg;

namespace SystemDesigner.Stage.Tools
{
    public class SelectTool : BaseSvgStageTool<SelectTool>, IStageTool<SvgStage>
    {
        #region Fields

        private ISvgItem _selectedItem;

        private DragHelper _dragHelper;

        private IToolModifier _addSelectionModifier;
        private IToolModifier _removeSelectionModifier;
        private IToolModifier _scaleLockModifier;
        private ObservableCollection<ISvgItem> _selectedItems;

        private List<UiTouchButton> _transformationButtons; // list of buttons to transform the selected objects
        private UiTouchButton _leftUpperDiagonalScaleButton;
        private UiTouchButton _rightLowerDiagonalScaleButton;
        private UiTouchButton _leftLowerDiagonalScaleButton;
        private UiTouchButton _rightUpperDiagonalScaleButton;
        private UiTouchButton _moveButton;
        private UiTouchButton _rightHorizontalScaleButton;
        private UiTouchButton _leftHorizontalScaleButton;
        private UiTouchButton _upperVerticalScaleButton;
        private UiTouchButton _lowerVerticalScaleButton;
        private UiTouchButton _rotateButton;

        #endregion

        #region Properties

        public string Name => TextResources.SelectToolName;
        public string IconPath => Icons.Cursor;
        public IUiLayer UiContainer { get; private set; }
        public IList<IToolModifier> ToolModifiers { get; private set; }
        public IList<IToolOption> ToolOptions { get; private set; }
        public IList<IToolFunction> ToolFunctions { get; private set; }

        public SvgStage TargetStage { get; private set; }

        /// <summary>
        /// Gets the currently selected item.
        /// </summary>
        public ISvgItem SelectedItem {
            get { return _selectedItem; }
            private set {
                _selectedItem = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the selected items.
        /// </summary>
        public ObservableCollection<ISvgItem> SelectedItems {
            get => _selectedItems;
            private set {
                _selectedItems = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public SelectTool()
        {
            _dragHelper = new DragHelper();
            _addSelectionModifier = new BaseToolModifier(Icons.Plus, TextResources.AddToSelectionName, TextResources.ScaleLockDescription);
            _removeSelectionModifier = new BaseToolModifier(Icons.Minus, TextResources.RemoveFromSelectionName, TextResources.ScaleLockDescription);
            _scaleLockModifier = new BaseToolModifier(Icons.Advance, TextResources.ScaleLockName, TextResources.ScaleLockDescription);

            ToolModifiers = new List<IToolModifier>
            {
                _addSelectionModifier,
                _removeSelectionModifier,
                _scaleLockModifier
            };

            SelectedItems = new ObservableCollection<ISvgItem>();
            UiContainer = new SelectedToolUiLayer(this);

            _transformationButtons = GetTransformationButtons();
            // add all transformation buttons to the UiContainer
            foreach (var button in _transformationButtons)
            {
                ((SelectedToolUiLayer) UiContainer).Add(button);
            }

            ActionExecuted += OnActionExecuted;
        }

        /// <summary>
        /// Gets the list of UI buttons, which are used to displayed transformation hotspots for the selected items.
        /// </summary>
        /// <returns>List of buttons to transform the selected items.</returns>
        /// <exception cref="NotImplementedException"></exception>
        private List<UiTouchButton> GetTransformationButtons()
        {
            var list = new List<UiTouchButton>();
            // diagonal scale buttons
            _leftUpperDiagonalScaleButton = new UiTouchButton(Icons.ResizeVertical) { IconImage = { Rotation = - Math.PI / 4 } };
            _rightUpperDiagonalScaleButton = new UiTouchButton(Icons.ResizeVertical) { IconImage = { Rotation = Math.PI / 4 } };
            _leftLowerDiagonalScaleButton = new UiTouchButton(Icons.ResizeVertical) { IconImage = { Rotation = Math.PI / 4 } };
            _rightLowerDiagonalScaleButton = new UiTouchButton(Icons.ResizeVertical) { IconImage = { Rotation = -Math.PI / 4 } };
            // add to list
            list.AddRange(new[] { _leftUpperDiagonalScaleButton, _rightUpperDiagonalScaleButton, _leftLowerDiagonalScaleButton, _rightLowerDiagonalScaleButton });

            // horizontal scale buttons
            _leftHorizontalScaleButton = new UiTouchButton(Icons.ResizeVertical) {IconImage = { Rotation = Math.PI/2}};
            _rightHorizontalScaleButton = new UiTouchButton(Icons.ResizeVertical) {IconImage = { Rotation = Math.PI/2}};
            // add to list
            list.AddRange(new [] {_leftHorizontalScaleButton, _rightHorizontalScaleButton});

            // vertical scale buttons
            _upperVerticalScaleButton = new UiTouchButton(Icons.ResizeVertical);
            _lowerVerticalScaleButton = new UiTouchButton(Icons.ResizeVertical);
            // add to list
            list.AddRange(new[] { _upperVerticalScaleButton, _lowerVerticalScaleButton });

            // move and rotate button
            _rotateButton = new UiTouchButton(Icons.Rotate);
            _moveButton = new UiTouchButton(Icons.Move);
            // add to list
            list.AddRange(new[] { _rotateButton, _moveButton});
            
            // return final list
            return list;
        }

        /// <summary>
        /// Called when an [action is executed] for example an item was selected. Used to update UI elements.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="actionEventArgs">The <see cref="ActionEventArgs"/> instance containing the event data.</param>
        private void OnActionExecuted(object sender, ActionEventArgs actionEventArgs)
        {
            if (SelectedItems.Count == 0)
            {
                // nothing is selected, make all transformation buttons invisible
                foreach (var button in _transformationButtons)
                {
                    button.IsVisible = false;
                }
            }
            else
            {
                // one or multiple items selected
                var box = SelectedItems.Select(x => x.RenderBoundingBox).GetCombinedBoundingBox();
                foreach (var button in _transformationButtons)
                {
                    button.IsVisible = true;
                }
            }
        }

        /// <summary>
        /// Creates a new SelectAction on the given target.
        /// </summary>
        /// <param name="target">The item that shall be (de)selected.</param>
        /// <param name="deselect">if set to <c>true</c> [deselect].</param>
        /// <returns>The created action.</returns>
        private IAction CreateSelect(ISvgItem target, bool deselect = false) => new SelectAction(new List<ISvgItem> { target }, this, deselect);

        /// <summary>
        /// Creates a new SelectAction on the given targets.
        /// </summary>
        /// <param name="targets">The items that shall be (de)selected.</param>
        /// <param name="deselect">if set to <c>true</c> [deselect].</param>
        /// <returns>The created action.</returns>
        private IAction CreateSelect(IEnumerable<ISvgItem> targets, bool deselect = false) => new SelectAction(targets, this, deselect);


        /// <summary>
        /// Gets the shortcuts for this tool.
        /// </summary>
        /// <returns>List of shortcuts for this tool.</returns>
        public static IEnumerable<Shortcut> GetShortcuts()
        {
            var toReturn = new List<Shortcut>();
            toReturn.Add(
                new Shortcut("SelectTool", new List<KeyCombination> { new KeyCombination("s") })
                {
                    OnPressedAction = () => SelectThisTool()
                });
            toReturn.Add(new Shortcut("AddToSelection", new List<KeyCombination> { new KeyCombination("LEFTSHIFT"), new KeyCombination("LEFTSHIFT", KeyboardModifier.Shift) }) { OnPressedAction = () => SetModifierState(0, true), OnReleasedAction = () => SetModifierState(0, false) });
            toReturn.Add(new Shortcut("RemoveFromSelection", new List<KeyCombination> { new KeyCombination("LEFTCTRL"), new KeyCombination("LEFTCTRL", KeyboardModifier.Shift) }) { OnPressedAction = () => SetModifierState(1, true), OnReleasedAction = () => SetModifierState(1, false) });
            toReturn.Add(new Shortcut("ScaleLock", new List<KeyCombination> { new KeyCombination("LEFTALT"), new KeyCombination("LEFTALT", KeyboardModifier.Shift) }) { OnPressedAction = () => SetModifierState(2, true), OnReleasedAction = () => SetModifierState(2, false) });
            return toReturn;
        }


        #region StageTool implementation

        public void SetTarget(IStage to)
        {
            TargetStage = to as SvgStage;
            _dragHelper.SetTarget(to as SvgStage);
        }

        public bool? OnInputMove(Point location, InputType inputType, int id)
        {
            return _dragHelper.OnInputMove(location, inputType, id);
        }

        public bool? OnInputDown(Point location, InputType inputType, int id)
        {
            if (UiContainer.OnInputDown(location, inputType, id) == true) return true;

            return _dragHelper.OnInputDown(location, inputType, id);
        }

        public bool? OnInputUp(Point location, InputType inputType, int id)
        {

            if (UiContainer.OnInputUp(location, inputType, id) == true) return true;

            // only if the dragging was not happening
            if (_dragHelper.OnInputUp(location, inputType, id) == false)
            {

                if (inputType == InputType.Pen) return false; // prevent selection with pens

                var hittedChild = TargetStage.Root.HitTestPointFirst(location.X, location.Y);

                // if the "add clicked child" modifier is active
                if (_addSelectionModifier.IsActive)
                {
                    if (hittedChild == null) return true; // do nothing if no child was clicked, but this modifier was active

                    // add the child to the collection (if it isn't already)
                    if (!SelectedItems.Contains(hittedChild)) InvokeAction(CreateSelect(hittedChild));

                    return true;
                }

                // if the "remove clicked child" modifier is active
                if (_removeSelectionModifier.IsActive)
                {
                    if (hittedChild == null) return true; // do nothing if no child was clicked, but this modifier was active

                    // remove the child from the collection (if it's in it)
                    if (SelectedItems.Contains(hittedChild)) InvokeAction(CreateSelect(hittedChild, true)); // deselect
                    return true;
                }

                // when there are no items currently selected (so there is nothing to deselect) and nothing new was hit, do nothing
                if (SelectedItems.Count == 0 && hittedChild == null) return true;


                // otherwise the default behavior is to replace all selected items, with the new item
                var deselectAction = CreateSelect(SelectedItems, true); // deselect all action
                if (hittedChild != null)
                {
                    // merge the deselect and new select action into one and invoke it
                    InvokeAction(deselectAction.Merge(CreateSelect(hittedChild)));
                }
                else InvokeAction(deselectAction); // if nothing was hit, simply invoke the deselect action
            }
            return true;
        }


        #endregion

    }
}
