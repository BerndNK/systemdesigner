﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using SystemDesigner.Stage.Tools.Action;
using DisplayCore.Annotations;

namespace SystemDesigner.Stage.Tools
{
    /// <summary>
    /// Provides a base class for IStageTool of SvgStage implementations.
    /// </summary>
    /// <typeparam name="T">Type of the IStageTool implementation</typeparam>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public abstract class BaseSvgStageTool<T> : INotifyPropertyChanged where T : IStageTool<SvgStage>
    {
        /// <summary>
        /// The identifier used when using shortcuts to activate tool modifier.
        /// </summary>
        private const int ShortcutActivationId = -5;
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<ActionEventArgs> ActionExecuted;

        [NotifyPropertyChangedInvocator]
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Gets the active tool as T.
        /// </summary>
        /// <returns>
        /// T instance or null if the active tool of the active stage is null (or the stage itself).
        /// </returns>
        private static T GetActiveSelectTool()
        {
            if (SvgStageHandler.Current?.ActiveStage?.ActiveTool is T)
                return (T)SvgStageHandler.Current.ActiveStage.ActiveTool;
            return default(T);
        }

        /// <summary>
        /// Selects this tool as the active tool for the current active stage.
        /// </summary>
        protected static void SelectThisTool()
        {
            SvgStageHandler.Current?.ActiveStage?.SetActiveTool(SvgStageHandler.Current.ActiveStage.Tools.First(x => x is T));
        }

        /// <summary>
        /// Sets the active flag of the modifier at the given index for the activeTool of the active stage, IF this tool has the same type as T.
        /// </summary>
        /// <param name="index">The index of the modifier.</param>
        /// <param name="active">Value for the Active property of the modifier. <c>true</c> = on, <c>false</c> = off</param>
        protected static void SetModifierState(int index, bool active)
        {
            // get the tool, abort if it's not currently selected (or stage is null)
            var tool = GetActiveSelectTool();
            if (tool == null) return;

            // activate the modifier of the given index. Abort if the index is out of range
            if (index >= tool.ToolModifiers.Count) return;
            if(active) tool.ToolModifiers[index].AddActivationInput(ShortcutActivationId);
            else tool.ToolModifiers[index].RemoveActivationInput(ShortcutActivationId);
        }

        /// <summary>
        /// Invokes the ActionExecuted event with the given action instance.
        /// </summary>
        /// <param name="action">The action.</param>
        protected void InvokeAction(IAction action)
        {
            action.Execute();
            ActionExecuted?.Invoke(this, new ActionEventArgs(action));
        }
    }
}
