﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using SystemDesigner.Input;
using SystemDesigner.Stage.Tools.Action;
using SystemDesigner.Stage.Tools.Ui;

namespace SystemDesigner.Stage.Tools
{

    /// <summary>
    /// Describes a tool which can modify children of a stage. Input events a usually passed through by the stage to this tool
    /// </summary>
    public interface IStageTool<out TStage> : IMoveInputReceiver, IUpDownInputReceiver, INotifyPropertyChanged where TStage : IStage
    {
        /// <summary>
        /// Gets the name of the tool.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; }

        /// <summary>
        /// Gets the icon path.
        /// </summary>
        string IconPath { get; }

        /// <summary>
        /// Gets the UI container which shows the user interface for this tool.
        /// </summary>
        /// <value>
        /// The UI container.
        /// </value>
        IUiLayer UiContainer { get; }

        /// <summary>
        /// Gets the tool modifiers, which are temporary modifications to the behavior of the tool (usually done by holding a key on the keyboard or holding a button on touch devices).
        /// </summary>
        /// <value>
        /// The tool modifiers.
        /// </value>
        IList<IToolModifier> ToolModifiers { get; }

        /// <summary>
        /// Gets the tool options, which are preferences on the behavior of the tool.
        /// </summary>
        /// <value>
        /// The tool options.
        /// </value>
        IList<IToolOption> ToolOptions { get; }

        /// <summary>
        /// Gets the tool functions, which are additional functionality relevant for this tool.
        /// </summary>
        /// <value>
        /// The tool functions.
        /// </value>
        IList<IToolFunction> ToolFunctions { get; }

        /// <summary>
        /// Occurs when an [action was executed].
        /// </summary>
        event EventHandler<ActionEventArgs> ActionExecuted;

        /// <summary>
        /// Gets the target stage of this tool.
        /// </summary>
        /// <value>
        /// The target stage.
        /// </value>
        TStage TargetStage { get; }

        /// <summary>
        /// Sets the target of this tool.
        /// </summary>
        /// <param name="to">The new target of this tool.</param>
        void SetTarget(IStage to);

    }
}