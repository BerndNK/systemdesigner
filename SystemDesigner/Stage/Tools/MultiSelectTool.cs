﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using SystemDesigner.Input;
using SystemDesigner.Stage.Tools.Action;
using SystemDesigner.Stage.Tools.Ui;
using SystemDesignerResources.Image;
using DisplayCore.Render.Geometry;
using SvgAbstraction.Svg;

namespace SystemDesigner.Stage.Tools {
    public class MultiSelectTool : IStageTool<SvgStage> {
        public event PropertyChangedEventHandler PropertyChanged;
        

        public event EventHandler<ActionEventArgs> ActionExecuted;

        #region Properties

        public string Name => "Debug";
        public string IconPath => Icons.Bug;
        public IUiLayer UiContainer { get; } = null;
        public IList<IToolModifier> ToolModifiers { get; } = null;
        public IList<IToolOption> ToolOptions { get; } = null;
        public IList<IToolFunction> ToolFunctions { get; } = null;

        public SvgStage TargetStage { get; private set; }

        public ObservableCollection<ISvgItem> SelectedItems { get; }

        #endregion


        public MultiSelectTool() {
            SelectedItems = new ObservableCollection<ISvgItem>();
        }

        public void SetTarget(IStage to)
        {
            TargetStage = to as SvgStage;
        }

        public bool? OnInputMove(Point location, InputType inputType, int id)
        {
            return false;
        }

        public bool? OnInputDown(Point location, InputType inputType, int id) {

            if (inputType == InputType.Pen) return false; // prevent selection with pens
            
            var hittedChild = TargetStage.Root.HitTestPointFirst(location.X, location.Y);
            if (SelectedItems.Contains(hittedChild)) SelectedItems.Remove(hittedChild);
            else SelectedItems.Add(hittedChild);
            return true;
        }

        public bool? OnInputUp(Point location, InputType inputType, int id)
        {
            return false;
        }
    }
}
