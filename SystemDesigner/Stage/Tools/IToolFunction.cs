﻿namespace SystemDesigner.Stage.Tools
{
    /// <summary>
    /// Extend the functionality of a tool in context of that very tool.
    /// </summary>
    public interface IToolFunction
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }
    }
}