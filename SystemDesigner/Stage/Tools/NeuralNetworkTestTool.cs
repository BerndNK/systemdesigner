﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using SystemDesigner.Input;
using SystemDesigner.Stage.Tools.Action;
using SystemDesigner.Stage.Tools.Ui;
using SystemDesignerResources.Image;
using DisplayCore.Render.Geometry;

namespace SystemDesigner.Stage.Tools {
    public class NeuralNetworkTestTool : IStageTool<SvgStage>{
        

        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler<ActionEventArgs> ActionExecuted;
        public string Name => "Neural Network Test Tool";
        public string IconPath => Icons.Multicast;
        public IUiLayer UiContainer { get; } = null;
        public IList<IToolModifier> ToolModifiers { get; } = null;
        public IList<IToolOption> ToolOptions { get; } = null;
        public IList<IToolFunction> ToolFunctions { get; } = null;

        public FreeHandTool FreeHandTool { get; set; }
        public SvgStage TargetStage { get; } = null;

        public static NeuralNetworkTestTool Current;

        public NeuralNetworkTestTool()
        {
            Current = this;
        }


        public void SetTarget(IStage to)
        {
            // not needed
        }

        public bool? OnInputMove(Point location, InputType inputType, int id) {
            return FreeHandTool?.OnInputMove(location, inputType, id);
        }

        public bool? OnInputDown(Point location, InputType inputType, int id) {
            return FreeHandTool?.OnInputDown(location, inputType, id);
        }

        public bool? OnInputUp(Point location, InputType inputType, int id) {
            return FreeHandTool?.OnInputUp(location, inputType, id);
        }

        
    }
}
