﻿using System.Collections.Generic;
using System.ComponentModel;

namespace SystemDesigner.Stage.Tools
{
    /// <summary>
    /// Changes the behavior of a tool, designed to be temporarily.
    /// </summary>
    public interface IToolModifier : INotifyPropertyChanged
    {

        /// <summary>
        /// Gets the image path for the icon.
        /// </summary>
        string IconPath { get; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="IToolModifier" /> is active. This can be modified by adding or removing activation tokens.
        /// </summary>
        /// <value>
        ///   <c>true</c> if active; otherwise, <c>false</c>.
        /// </value>
        bool IsActive { get; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Gets the tokens which activate this modifier;
        /// </summary>
        /// <value>
        /// The activating tokens.
        /// </value>
        List<int> ActivatingTokens { get; }

        /// <summary>
        /// Adds a token which activates this modifier.
        /// </summary>
        /// <param name="id">The identifier of the token.</param>
        void AddActivationInput(int id);

        /// <summary>
        /// Removes a token which activates this modifier.
        /// </summary>
        /// <param name="id">The identifier of the token.</param>
        void RemoveActivationInput(int id);

    }
}