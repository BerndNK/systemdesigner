﻿using System;
using SystemDesigner.Input;
using DisplayCore.Render.Geometry;

namespace SystemDesigner.Stage.Tools.Helper {
    /// <summary>
    /// Provides functionality to attach dragging (of a stage) to a StageTool
    /// </summary>
    public class DragHelper : IFullInputReceiver {
        #region Fields

        /// <summary>
        /// The target for the dragging
        /// </summary>
        private SvgStage _target;

        /// <summary>
        /// The last location a down input event was received
        /// </summary>
        private Point _lastDownLocation;

        /// <summary>
        /// The last position the drag was applied to
        /// </summary>
        private Point _lastDraggedPosition;

        /// <summary>
        /// The id of the input that started the drag (and is the only valid one that will be used).
        /// </summary>
        private int _dragId;

        /// <summary>
        /// Describes a Input Receiver as used in the IMoveInputReceiver and IUpDownInputReceiver interfaces
        /// </summary>
        /// <param name="location">The location.</param>
        /// <param name="inputType">Type of the input.</param>
        public delegate void InputReceiver(Point location, InputType inputType);

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether this instance is dragging.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is dragging; otherwise, <c>false</c>.
        /// </value>
        public bool IsDragging { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the helper will simulate a down event before passing through a up event. If disabled, the helper will NEVER pass through a down event
        /// </summary>
        /// <value>
        ///   <c>true</c> [simulate down input]; otherwise, <c>false</c>.
        /// </value>
        public bool SimulateDownInput { get; set; }

        /// <summary>
        /// Gets or sets the needed width for dragging.
        /// </summary>
        /// <value>
        /// The needed width for dragging.
        /// </value>
        public double NeededWidthForDragging { get; set; } = 10;
        #endregion

        /// <summary>
        /// Sets the target of the dragging operation.
        /// </summary>
        /// <param name="to">The new target.</param>
        public void SetTarget(SvgStage to)
        {
            _target = to;
        }

        /// <summary>
        /// Event Handler for a input device moving (like a finger on the screen, stylus or mouse cursor)
        /// </summary>
        /// <param name="location">Location of the cursor relative to this stage</param>
        /// <param name="inputType">The type of the input. Mouse, touch or stylus etc.</param>
        /// <param name="id">The identifier of the input event. E.g. for multiple fingers on a touch event.</param>
        /// <exception cref="NotImplementedException"></exception>
        public bool? OnInputMove(Point location, InputType inputType, int id) {
            if (_lastDownLocation == null) {
                return false;
            }

            // only accept the input which started the drag
            if (_dragId != id) return false;

            if (!IsDragging) {
                // get distance to the last down input. If it's greater than the necessary distance to drag, start dragging
                var distance = Math.Sqrt(Math.Pow(location.Y - _lastDownLocation.Y, 2) + Math.Pow(location.X - _lastDownLocation.X, 2));
                if (distance > NeededWidthForDragging) {
                    IsDragging = true;
                    _lastDraggedPosition = _lastDownLocation;
                } else return false;
            }

            _target.Root.X += location.X - _lastDraggedPosition.X;
            _target.Root.Y += location.Y - _lastDraggedPosition.Y;

            _lastDraggedPosition = location;
            return true;
        }

        /// <summary>
        /// Event handler for when a "Down" event appears. Can be mouse down or touch down
        /// </summary>
        /// <param name="location">Location of the event appearing relative to this stage</param>
        /// <param name="inputType">The type of the input. Mouse, touch or stylus etc.</param>
        /// <param name="id">The identifier of the input event. E.g. for multiple fingers on a touch event.</param>
        /// <exception cref="NotImplementedException"></exception>
        public bool? OnInputDown(Point location, InputType inputType, int id)
        {
            if (IsDragging) return false;
            _lastDownLocation = location;
            _dragId = id;
            return true;
        }

        /// <summary>
        /// Event handler for when a "Up" event appears. Can be mouse down or touch down
        /// </summary>
        /// <param name="location">Location of the event appearing relative to this stage</param>
        /// <param name="inputType">The type of the input. Mouse, touch or stylus etc.</param>
        /// <param name="id">The identifier of the input event. E.g. for multiple fingers on a touch event.</param>
        /// <exception cref="NotImplementedException"></exception>
        public bool? OnInputUp(Point location, InputType inputType, int id)
        {
            // only accept the input which started the drag
            if (_dragId != id) return false;
            _lastDownLocation = null;
            // if we're dragging - stop. Otherwise pass through
            if (!IsDragging) return false;

            IsDragging = false;
            return true;
        }
    }
}
