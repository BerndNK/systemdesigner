﻿using System;
using System.Xml.Linq;
using System.Xml.Serialization;
using SystemDesigner.Draw;
using SystemDesigner.Input;
using DisplayCore.Render.Geometry;

namespace SystemDesigner.Stage.Tools.Helper {
    public class DrawHelper : IFullInputReceiver
    {
        #region Fields

        /// <summary>
        /// Whether we're currently capturing events and thus are drawing.
        /// </summary>
        private bool _isDrawing;

        #endregion

        #region Properties

        /// <summary>
        /// Occurs when the [drawing is completed].
        /// </summary>
        public event EventHandler<Drawing> DrawingCompleted;

        /// <summary>
        /// Gets the current drawing.
        /// </summary>
        public Drawing CurrentDrawing { get; private set; }

        #endregion

        public DrawHelper()
        {
            CurrentDrawing = new Drawing();
        }

        //private string GetSerializedXml() {
        //    // create serializer
        //    var xmlSerializer = new XmlSerializer(typeof(Drawing), new XmlRootAttribute("Drawing"));
        //    using (var textWriter = new StringWriter()) {
        //        xmlSerializer.Serialize(textWriter, CurrentDrawing);
        //        return textWriter.ToString();
        //    }
        //}

        public static Drawing Deserialize(string xml) {
            var doc = XDocument.Parse(xml);

            // create serializer
            var xmlSerializer = new XmlSerializer(typeof(Drawing), new XmlRootAttribute("Drawing"));

            // create reader from the Root, use the serializer to parse it, return the result
            using (var reader = doc.Root?.CreateReader()) {
                return (Drawing)xmlSerializer.Deserialize(reader);
            }
        }

        #region IFullInputReceiver Implementation

        public bool? OnInputMove(Point location, InputType inputType, int id)
        {
            // limit drawing to mouse and pen inputs
            if (!_isDrawing) return false;
            if (inputType != InputType.Mouse && inputType != InputType.Pen) return false;
            
            CurrentDrawing.Points.Add(location);
            return true;
        }

        public bool? OnInputDown(Point location, InputType inputType, int id)
        {
            // limit drawing to mouse and pen inputs
            if (inputType != InputType.Mouse && inputType != InputType.Pen) return false;
            _isDrawing = true;
            CurrentDrawing.Points.Clear();
            return true;
        }

        public bool? OnInputUp(Point location, InputType inputType, int id)
        {
            // limit drawing to mouse and pen inputs
            if (inputType != InputType.Mouse && inputType != InputType.Pen && !_isDrawing) return false;
            _isDrawing = false;

            DrawingCompleted?.Invoke(this, CurrentDrawing);
            return true;
        }

        #endregion
    }
}
