﻿using System;
using System.Collections.Generic;
using System.Linq;
using SystemDesigner.Draw;
using SystemDesigner.Input;
using SystemDesigner.Input.Shortcuts;
using SystemDesigner.Log;
using SystemDesigner.Neural;
using SystemDesigner.Stage.Tools.Helper;
using SystemDesigner.Stage.Tools.Ui;
using SystemDesigner.Svg;
using SystemDesignerResources.Image;
using SystemDesignerResources.Text;
using DisplayCore.Render.Geometry;
using SvgAbstraction.Svg.Forms;

namespace SystemDesigner.Stage.Tools
{
    public class FreeHandTool : BaseSvgStageTool<FreeHandTool>, IStageTool<SvgStage>
    {

        /// <summary>
        /// The neural network XML that has to be set by each platform before initializing this sage.
        /// </summary>
        public static string NeuralNetworkXml;
        
        /// <summary>
        /// The relative path that leads to the trained neural network used to recognize shapes.
        /// </summary>
        public const string NeuralNetworkPath = "./Neural/NeuralNetwork.neuralNetwork";


        /// <summary>
        /// The shapes that are recognizable by this tool.
        /// </summary>
        public static readonly List<Shape> RecognizableShapes = new List<Shape> {Shape.Diamond, Shape.Ellipse, Shape.Rectangle, Shape.Triangle};

        #region Fields

        private SvgPolyline _polyline; // Polyline in the UI Container used to give visual feedback of the drawing process

        /// <summary>
        /// The neural network manager for the network used to recognize shapes.
        /// </summary>
        private NeuralNetworkManager _neuralNetworkManager;

        #endregion

        #region Properties

        public string Name => TextResources.FreeHandToolName;
        public string IconPath => Icons.Pen;

        public IUiLayer UiContainer { get; }
        public IList<IToolModifier> ToolModifiers { get; } = null;
        public IList<IToolOption> ToolOptions { get; } = null;
        public IList<IToolFunction> ToolFunctions { get; } = null;
        public SvgStage TargetStage { get; private set; }


        /// <summary>
        /// Gets the draw helper.
        /// </summary>
        public DrawHelper DrawHelper { get; private set; }

        #endregion

        public FreeHandTool()
        {
            _polyline = new SvgPolyline(4);


            DrawHelper = new DrawHelper();
            DrawHelper.DrawingCompleted += DrawHelperOnDrawingCompleted;
            // hook up the polyline to the Points of the Drawing
            DrawHelper.CurrentDrawing.Points.CollectionChanged += _polyline.PointCollectionChangedHandlerHook;

            UiContainer = new BaseTouchLayer(this);
            ((BaseTouchLayer) UiContainer).Add(_polyline);

            if(string.IsNullOrWhiteSpace(NeuralNetworkXml)) throw new InvalidOperationException("NeuralNetworkXml has to be set before initializing the FreeHandTool!");
            _neuralNetworkManager = new NeuralNetworkManager();
            _neuralNetworkManager.FromXml(NeuralNetworkXml);

            // check if the loaded network's outputs matches the recognizable shapes. If not, this means the loaded network is out of date and cannot be used.
            if(_neuralNetworkManager.OutputSize != RecognizableShapes.Count) throw new Exception("The loaded neural network's outputs do not match the amount of recognizable shapes!");
            //_dragHelper = new DragHelper(target, OnInputMovePassThrough, OnInputDownPassThrough, OnInputUpPassThrough);
        }

        public void SetTarget(IStage to)
        {
            TargetStage = to as SvgStage;
        }

        /// <summary>
        /// Called whenever the user has drawn something.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="drawing">The drawing.</param>
        private void DrawHelperOnDrawingCompleted(object sender, Drawing drawing)
        {
            if (drawing.Points.Count == 0) return; // abort if the user just clicked, but didn't actually draw
            
            var calculatedOutput = _neuralNetworkManager.Compute(GetDrawingInBytes(drawing).Select(x => Convert.ToDouble(x) / 255).ToArray()).ToList();

            DebugLogger.GetInstance().LogInfo(string.Join(",", calculatedOutput.Select(x => x.ToString("##.000")).ToArray()));
            var detectedShape = RecognizableShapes[calculatedOutput.IndexOf(calculatedOutput.Max())];
            DebugLogger.GetInstance().LogInfo("Shape: " + Enum.GetName(typeof(Shape), detectedShape));

            DrawShape(detectedShape, drawing);
            //throw new NotImplementedException();
        }

        private void DrawShape(Shape detectedShape, Drawing drawing)
        {
            _polyline.Clear();

            // get the dimensions of all points
            var minX = drawing.Points.Min(x => x.X);
            var minY = drawing.Points.Min(x => x.Y);
            var maxX = drawing.Points.Max(x => x.X);
            var maxY = drawing.Points.Max(x => x.Y);

            var centerX = minX + (maxX - minX) / 2;
            var centerY = minY + (maxY - minY) / 2;

            switch (detectedShape)
            {
                case Shape.Rectangle:
                    _polyline.Add(new Point(minX, minY));
                    _polyline.Add(new Point(maxX, minY));
                    _polyline.Add(new Point(maxX, maxY));
                    _polyline.Add(new Point(minX, maxY));
                    _polyline.Add(new Point(minX, minY));
                    break;
                case Shape.Ellipse:
                    var rad = (maxX - minX) / 2;

                    var iterations = 1000;
                    for (var i = 0; i < iterations; i++)
                    {
                        var crntAngle = Math.PI * 2 / iterations * i;
                        _polyline.Add(new Point(centerX + Math.Cos(crntAngle)*rad, centerY + Math.Sin(crntAngle)*rad));
                    }
                    break;
                case Shape.Triangle:
                    _polyline.Add(new Point(centerX,minY));
                    _polyline.Add(new Point(maxX, maxY));
                    _polyline.Add(new Point(minX, maxY));
                    _polyline.Add(new Point(centerX, minY));
                    break;
                case Shape.Diamond:
                    _polyline.Add(new Point(centerX, minY));
                    _polyline.Add(new Point(maxX, centerY));
                    _polyline.Add(new Point(centerX, maxY));
                    _polyline.Add(new Point(minX, centerY));
                    _polyline.Add(new Point(centerX, minY));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(detectedShape), detectedShape, null);
            }
        }


        public bool? OnInputMove(Point location, InputType inputType, int id) {
            return DrawHelper.OnInputMove(location, inputType, id);
        }

        public bool? OnInputDown(Point location, InputType inputType, int id) {
            return DrawHelper.OnInputDown(location, inputType, id);
        }

        public bool? OnInputUp(Point location, InputType inputType, int id) {
            return DrawHelper.OnInputUp(location, inputType, id);
        }

        public static IEnumerable<Shortcut> GetShortcuts()
        {
            var toReturn = new List<Shortcut>
            {
                new Shortcut("FreeHandTool", new List<KeyCombination> {new KeyCombination("h")})
                {
                    OnPressedAction = () => SelectThisTool()
                }
            };
            return toReturn;
        }

        /// <summary>
        /// Converts a drawing to an array of bytes. With the resolution of the input array of the neural network. Each byte indicates how black a pixel is. 255 = full black, 0 = full white
        /// </summary>
        /// <param name="drawing">The drawing that shall be converted.</param>
        /// <returns>The drawing in bytes with the dimensions matching the input array of the loaded neural network. Each pixel is only one byte, indicating how black the pixel is. 255 = full black, 0 = full white.</returns>
        /// <exception cref="NullReferenceException">Neural network cannot be null, when converting a drawing as it is needed to retrieve the target picture resolution.</exception>
        private byte[] GetDrawingInBytes(Drawing drawing)
        {
            if(_neuralNetworkManager == null) throw new NullReferenceException("Neural network cannot be null, when converting a drawing as it is needed to retrieve the target picture resolution.");

            // get the one dimensional resolution of the resulting bitmap
            var pictureResolution = (int)Math.Sqrt(_neuralNetworkManager.InputSize);
            // create byte array based on this data
            var bytesAmount = pictureResolution * pictureResolution;
            var byteArray = new byte[bytesAmount];


            // local function to write the strength to the byte at position (xPos, yPos). Only applies the strength, if it's stronger than the already existing value.
            void ApplyPixel(int xPos, int yPos, byte strength)
            {
                var index = yPos * pictureResolution + xPos;
                if (byteArray[index] < strength)
                    byteArray[index] = strength;
            }

            // get the dimensions of all points
            var minX = drawing.Points.Min(x => x.X);
            var minY = drawing.Points.Min(x => x.Y);
            var maxX = drawing.Points.Max(x => x.X);
            var maxY = drawing.Points.Max(x => x.Y);

            // track the point of the last iteration. By setting this value to the actual last point in the Points list, the algorithm creates a line between the first and last point
            var lastPoint = drawing.Points.Last();

            // go through each point
            foreach (var drawingPoint in drawing.Points)
            {
                // get the distance between the point of the last iteration and the current point
                var diffX = lastPoint.X - drawingPoint.X;
                var diffY = lastPoint.Y - drawingPoint.Y;
                // as well as the angle and distance
                var angle = Math.Atan2(diffY, diffX) + Math.PI;
                var dist = Math.Sqrt(diffX * diffX + diffY * diffY);

                // move from the last point to the current point, while tracking the distance
                var crntDist = 0.0;
                while (crntDist < dist)
                {
                    // move in 1 "pixel" steps. This is not exactly one pixel, as we move diagonally
                    crntDist += 1.0;
                    if (crntDist > dist) crntDist = dist; // make sure not to overshoot the target

                    // retrieve the current position
                    var crntX = lastPoint.X + Math.Cos(angle) * crntDist;
                    var crntY = lastPoint.Y + Math.Sin(angle) * crntDist;

                    // normalize this position to the pictureResolution (the drawing's resolution is usually a lot bigger than the resolution the neural network works with)


                    // Note: minus one, because it starts at 0, so the max. amount we're allowed to get is the max -1. e.g. for 28 pixels, 27
                    // remove additional 2 and add 1 in the end, so the x and y value never reach the exact minimum and maximum. That's because we add pixel to the surroundings anyway
                    var x = (int)Math.Round((crntX - minX) / (maxX - minX) * (pictureResolution - 3)) + 1;
                    var y = (int)Math.Round((crntY - minY) / (maxY - minY) * (pictureResolution - 3)) + 1;

                    // make sure it does not get out of bounds
                    if (x >= pictureResolution)
                        x = pictureResolution - 1;
                    if (y >= pictureResolution)
                        y = pictureResolution - 1;
                    if (x < 0)
                        x = 0;
                    if (y < 0)
                        y = 0;

                    // apply a full strength pixel to the current position
                    ApplyPixel(x, y, 255);
                    
                    // apply half strength pixel to each pixel surrounding the current one
                    if (x > 0) ApplyPixel(x - 1, y, 255);
                    if (y > 0) ApplyPixel(x, y - 1, 255 / 2);
                    if (x > 0 && y > 0) ApplyPixel(x - 1, y - 1, 255 / 2);
                    if (x < pictureResolution - 1) ApplyPixel(x + 1, y, 255 / 2);
                    if (y < pictureResolution - 1) ApplyPixel(x, y + 1, 255 / 2);
                    if (x < pictureResolution - 1 && y < pictureResolution - 1) ApplyPixel(x + 1, y + 1, 255 / 2);
                }

                // set the lastPoint (which means, for the next iteration the last point is the one in the current iteration)
                lastPoint = drawingPoint;
            }

            // return the final array of bytes
            return byteArray;
        }
    }
}
