﻿using SystemDesigner.Utils;

namespace SystemDesigner.Stage.Tools.Ui
{
    /// <summary>
    /// Collection of runtime calculated variables, for UI specific tasks.
    /// </summary>
    public static class UiVariables
    {
        /// <summary>
        /// The default dpi which will be used as reference to the current device dpi.
        /// </summary>
        public const double DefaultDpi = 72;


        /// <summary>
        /// Gets or sets the UI scaling (which is an additional scale on top of the DeviceDpi scaling).
        /// </summary>
        /// <value>
        /// The UI scaling.
        /// </value>
        public static double UiScale { get; set; } = 1;

        /// <summary>
        /// Gets the combined UI scale, which is the scale resulting from the device dpi + the additionally set UiScale.
        /// </summary>
        /// <value>
        /// The combined UI scale.
        /// </value>
        public static double CombinedUiScale => DeviceInfo.DeviceDpi / DefaultDpi * UiScale;
    }
}
