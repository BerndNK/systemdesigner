﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms;

namespace SystemDesigner.Stage.Tools.Ui
{
    /// <summary>
    /// A general button that is intended for the use of touch devices.
    /// </summary>
    /// <seealso cref="SvgAbstraction.Svg.SvgItemContainer" />
    public class UiTouchButton : SvgItemContainer
    {
        #region Fields

        private const double DefaultButtonRadius = 50;
        private const uint DefaultButtonColor = 0xffB6B6B6;
        private const uint DefaultButtonColorActivated = 0xffdedede;
        private const int ButtonActivationInputId = -10;

        private SvgEllipse _buttonBase; // The circle which will be the base of the button.


        private IToolModifier _targetModifier;
        private bool _wasActive; // flag to store whether this instance was active the last time the UpdateActive method was called (to trigger activated and deactivated events)
        private bool _externalSourceActivation;

        #endregion

        #region Properties
        /// <summary>
        /// Gets the id's of the inputs which are causing this button to activate.
        /// </summary>
        /// <value>
        /// The activating inputs.
        /// </value>
        public List<int> ActivatingInputs { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this button is active.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this button is active; otherwise, <c>false</c>.
        /// </value>
        public bool IsActive => ExternalSourceActivation || ActivatingInputs.Count > 0;

        /// <summary>
        /// Gets or sets a value indicating whether this button is activated by an external source.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [external source activation]; otherwise, <c>false</c>.
        /// </value>
        public bool ExternalSourceActivation {
            get => _externalSourceActivation;
            set {
                _externalSourceActivation = value;
                OnPropertyChanged();
                UpdateIsActive();
            }
        }

        /// <summary>
        /// Gets or sets a target tool modifier.
        /// </summary>
        public IToolModifier TargetModifier {
            get { return _targetModifier; }
            set {
                _targetModifier = value;
                HookUpTargetModifier();
            }
        }

        /// <summary>
        /// The image of the icon.
        /// </summary>
        public SvgImage IconImage { get; private set; }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when this button is [activated].
        /// </summary>
        public event EventHandler Activated;

        /// <summary>
        /// Occurs when this button is [deactivated].
        /// </summary>
        public event EventHandler Deactivated;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UiTouchButton"/> class.
        /// </summary>
        /// <param name="iconSource">The path for the icon image.</param>
        public UiTouchButton(string iconSource)
        {
            CreateControls(iconSource);
            ActivatingInputs = new List<int>();
        }

        /// <summary>
        /// Hooks up a propertyChanged event unto the target modifier.
        /// </summary>
        private void HookUpTargetModifier()
        {
            TargetModifier.PropertyChanged += TargetModifierOnPropertyChanged;
        }

        /// <summary>
        /// PropertyChanged handler for a set TargetModifier
        /// </summary>
        /// <param name="sender">The sender of the vent.</param>
        /// <param name="propertyChangedEventArgs">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void TargetModifierOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName != nameof(IToolModifier.IsActive)) return;
            ExternalSourceActivation = (sender as IToolModifier)?.IsActive == true;
        }

        /// <summary>
        /// Creates the controls, so the button base and the icon.
        /// </summary>
        /// <param name="iconSource">Path for the icon image.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void CreateControls(string iconSource)
        {
            // create button
            _buttonBase = new SvgEllipse(DefaultButtonRadius) { FillColor = DefaultButtonColor };
            _buttonBase.PivotX = _buttonBase.Width / 2;
            _buttonBase.PivotY = _buttonBase.Height / 2;

            // create image
            IconImage = new SvgImage(iconSource, _buttonBase.Width, _buttonBase.Height);
            IconImage.PivotX = IconImage.Width / 2;
            IconImage.PivotY = IconImage.Height / 2;

            // add both to display hierarchy
            AddMultiple(new SvgItem[] { _buttonBase, IconImage });
        }


        public override string ToXml()
        {
            throw new NotSupportedException("The touch button is a non svg item, merely intended to display items on the screen for system designer");
        }

        /// <summary>
        /// Adds an input to the activation list. If this list is not empty, this button is active.
        /// </summary>
        /// <param name="id">The identifier of the input.</param>
        public void AddInput(int id)
        {
            if (!ActivatingInputs.Contains(id))
                ActivatingInputs.Add(id);
            UpdateIsActive();
        }

        /// <summary>
        /// Removes an input from the activation list. If this list is empty, this button is not active.
        /// </summary>
        /// <param name="id">The identifier of the input.</param>
        public void RemoveInput(int id)
        {
            if (ActivatingInputs.Contains(id)) ActivatingInputs.Remove(id);
            UpdateIsActive();
        }

        /// <summary>
        /// Triggers PropertyChanged for IsActive and updates the button color.
        /// </summary>
        private void UpdateIsActive()
        {
            OnPropertyChanged(nameof(IsActive));
            _buttonBase.FillColor = IsActive ? DefaultButtonColorActivated : DefaultButtonColor;

            // trigger activated or deactivated events (if the last state [_wasActive] is different from the new one [IsActive])
            if (_wasActive != IsActive)
            {
                if (_wasActive) Deactivated?.Invoke(this, EventArgs.Empty);
                else Activated?.Invoke(this, EventArgs.Empty);
            }
            // store current active status for next update
            _wasActive = IsActive;

            // when there is a tool modifier associated with this button,
            if (TargetModifier == null) return;

            // add an activation token for the tool modifier, activating it
            if (ActivatingInputs.Count > 0) TargetModifier.AddActivationInput(ButtonActivationInputId);
            else TargetModifier.RemoveActivationInput(ButtonActivationInputId); // or remove one

        }
    }
}
