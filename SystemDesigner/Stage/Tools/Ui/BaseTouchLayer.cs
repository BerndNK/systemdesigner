﻿using System;
using System.Collections.Generic;
using SystemDesigner.Input;
using SystemDesigner.Log;
using SystemDesigner.Stage.Tools.Parts;
using DisplayCore.Render.Geometry;
using SvgAbstraction.Svg;

namespace SystemDesigner.Stage.Tools.Ui
{
    /// <summary>
    /// UiLayer for touch devices, which suits the basic needs for most tools, displaying the tool modifiers.
    /// </summary>
    /// <seealso cref="SystemDesigner.Stage.Tools.Ui.IUiLayer" />
    public class BaseTouchLayer : SvgItemContainer, IUiLayer
    {

        /// <summary>
        /// The default margin which is used on various occurrences.
        /// </summary>
        private const double DefaultMargin = 5;

        public IList<UiTouchButton> ModifierButtons { get; set; }


        public BaseTouchLayer(IStageTool<SvgStage> forTool)
        {
            ModifierButtons = new List<UiTouchButton>();

            if (forTool?.ToolModifiers == null) return;

            // create a button for each tool modifier
            foreach (var modifier in forTool.ToolModifiers)
            {
                var button = new UiTouchButton(modifier.IconPath);
                button.TargetModifier = modifier;
                ModifierButtons.Add(button);
                Add(button);
            }
        }

        public void Arrange(double availableWidth, double availableHeight)
        {
            // base y to arrange the button from
            var baseY = availableHeight - DefaultMargin * UiVariables.CombinedUiScale;
            var i = 0;
            // arrange the buttons from the bottom up on the lower left portion of the screen
            foreach (var modifierButton in ModifierButtons)
            {
                modifierButton.X = modifierButton.Width / 2 + DefaultMargin * UiVariables.CombinedUiScale;
                modifierButton.Y = baseY - (modifierButton.Height + DefaultMargin * UiVariables.CombinedUiScale) * i -
                                   modifierButton.Height / 2;
                i++;
            }
        }

        public override string ToXml()
        {
            throw new NotSupportedException("BaseTouchLayer is an abstract class not meant to be converted to svg.");
        }

        public bool? OnInputDown(Point location, InputType inputType, int id)
        {
            foreach (var button in ModifierButtons)
            {
                if (button.HitTestPoint(location.X, location.Y))
                {
                    button.AddInput(id);
                    return true;
                }
            }
            return false;
        }

        public bool? OnInputUp(Point location, InputType inputType, int id)
        {
            foreach (var button in ModifierButtons)
            {
                button.RemoveInput(id);
            }
            return false;
        }
    }
}
