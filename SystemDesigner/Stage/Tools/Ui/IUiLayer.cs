﻿using SystemDesigner.Input;
using DisplayCore.Display;

namespace SystemDesigner.Stage.Tools.Ui
{
    /// <summary>
    /// Describes a layer, which contains UI elements
    /// </summary>
    public interface IUiLayer : IDisplayObjectContainer, IUpDownInputReceiver
    {


        /// <summary>
        /// Arranges the elements of this layer to the given size;
        /// </summary>
        /// <param name="availableWidth">Width of the available size.</param>
        /// <param name="availableHeight">Height of the available size.</param>
        void Arrange(double availableWidth, double availableHeight);
    }
}
