﻿namespace SystemDesigner.Stage.Tools
{
    /// <summary>
    /// Changes the behavior of a tool in a similar way as ToolModifier, however is more designed to be (semi-)permanent.
    /// </summary>
    public interface IToolOption
    {
    }
}