﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SystemDesigner.Neural
{
    /// <summary>
    /// Describes a series of inputs and their desired outputs for a NeuralNetwork.
    /// </summary>
    public struct TrainingSet
    {

        /// <summary>
        /// The input output list, with the first item being the inputs and the second item being the desired outputs for that.
        /// </summary>
        public readonly List<Tuple<double[], double[]>> InputOutputList;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrainingSet"/> struct.
        /// </summary>
        /// <param name="inputs">The inputs with the second dimension being the size of the input layer.</param>
        /// <param name="desiredOutputs">The desired outputs with the second dimension being the size of the output layer.</param>
        public TrainingSet(double[][] inputs, double[][] desiredOutputs)
        {
#if DEBUG
            if (inputs.Length != desiredOutputs.Length) throw new ArgumentException("The two given arrays are not the same size, which is invalid.");
#endif
            // zip the input outputs to tuples
            InputOutputList = inputs.Zip(desiredOutputs, (input, output) => new Tuple<double[], double[]>(input, output)).ToList();

        }
    }
}