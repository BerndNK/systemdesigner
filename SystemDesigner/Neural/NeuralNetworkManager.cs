﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using SystemDesigner.Log;

namespace SystemDesigner.Neural
{
    public class NeuralNetworkManager : INeuralNetworkManager
    {
        public double Error => _train.Error;
        public string DebugInformation => _train.GetDebugInfo();
        public string Name => "System Designer";

        private NeuralNetwork _neuralNetwork;
        private TrainingSet _trainingSet;
        private NeuralNetworkTrainer _train;

        /// <summary>
        /// Gets the size of the input layer.
        /// </summary>
        public int InputSize { get; private set; }

        /// <summary>
        /// Gets the size of the output layer.
        /// </summary>
        public int OutputSize { get; private set; }

        /// <summary>
        /// Initializes the neural network with the given input sizes. With a given amount of layers = 4. (input, hidden 1, hidden 2, output)
        /// </summary>
        /// <param name="inputSize">Size of the input layer.</param>
        /// <param name="hiddenLayer">The hidden layer.</param>
        /// <param name="outputLayerSize">Size of the output layer.</param>
        public void Init(int inputSize, List<int> hiddenLayer, int outputLayerSize)
        {
            _neuralNetwork = new NeuralNetwork();
            _neuralNetwork.AddLayer(inputSize);
            foreach (var i in hiddenLayer)
            {
                _neuralNetwork.AddLayer(i);
            }
            _neuralNetwork.AddLayer(outputLayerSize);
            InputSize = inputSize;
            OutputSize = outputLayerSize;
        }

        /// <summary>
        /// Sets the training data.
        /// </summary>
        /// <param name="input">The inputs.</param>
        /// <param name="desiredOutput">The desired output.</param>
        public void SetTrainingData(double[][] input, double[][] desiredOutput)
        {
            _trainingSet = new TrainingSet(input, desiredOutput);
            _train = new NeuralNetworkTrainer(_neuralNetwork, _trainingSet);
        }

        /// <summary>
        /// Converts the network to XML.
        /// </summary>
        /// <returns>The network as XML</returns>
        public string ToXml()
        {
            var xmlSerializer = new XmlSerializer(typeof(NeuralNetwork));
            using (var stringWriter = new StringWriter())
            {
                xmlSerializer.Serialize(stringWriter, _neuralNetwork);
               return stringWriter.ToString();
            }
        }

        /// <summary>
        /// Deserializes a network from the given XML.
        /// </summary>
        /// <param name="xml">The network serialized in XML.</param>
        public void FromXml(string xml)
        {
            // deserialize
            // parse the XML
            try
            {
                var doc = XDocument.Parse(xml);
                if (doc.Root == null) throw new Exception("Failed to parse xml.");

                // create the serializer
                var xmlSerializer = new XmlSerializer(typeof(NeuralNetwork));
                using (var reader = doc.Root.CreateReader())
                {
                    // deserialize and add it to the stage
                    _neuralNetwork = (NeuralNetwork)xmlSerializer.Deserialize(reader);
                    // set output and input sizes from the loaded network
                    InputSize = _neuralNetwork.Layers.First().Size;
                    OutputSize = _neuralNetwork.Layers.Last().Size;
                }
            }
            catch (Exception e)
            {
                DebugLogger.GetInstance().LogError(e.ToString());
            }
        }

        /// <summary>
        /// Trains the initialized neural network with the given trainingSet instance.
        /// </summary>
        public void Iteration(double learningRate)
        {
            if (_neuralNetwork == null) throw new InvalidOperationException("Network and TrainingSet has to be initialized before training.");

            _train.Iteration(learningRate);

        }

        /// <summary>
        /// Tests the specified input array.
        /// </summary>
        /// <param name="inputArray">The input array.</param>
        /// <returns>
        /// Outputs of the last layer
        /// </returns>
        public double[] Compute(double[] inputArray) => _neuralNetwork.Compute(inputArray);
    }
}
