﻿using System;
using SystemDesigner.Log;

namespace SystemDesigner.Neural
{
    public class LogisticGradientProgram
    {
        public static void Test()
        {
            DebugLogger.GetInstance().LogInfo("\nBegin Logistic Regression (binary) Classification demo");
            DebugLogger.GetInstance().LogInfo("Goal is to demonstrate training using gradient descent");

            const int numFeatures = 8; // synthetic data
            const int numRows = 10000;
            const int seed = 1;

            DebugLogger.GetInstance().LogInfo("\nGenerating " + numRows +
                              " artificial data items with " + numFeatures + " features");
            var allData = MakeAllData(numFeatures, numRows, seed);

            DebugLogger.GetInstance().LogInfo("Creating train (80%) and test (20%) matrices");
            MakeTrainTest(allData, 0, out var trainData, out var testData);
            DebugLogger.GetInstance().LogInfo("Done");


            DebugLogger.GetInstance().LogInfo("\nTraining data: \n");
            ShowData(trainData, 3, 2, true);

            DebugLogger.GetInstance().LogInfo("\nTest data: \n");
            ShowData(testData, 3, 2, true);


            DebugLogger.GetInstance().LogInfo("Creating LR binary classifier");
            var lc = new LogisticClassifier(numFeatures); //

            const int maxEpochs = 1000; // 
            DebugLogger.GetInstance().LogInfo("Setting maxEpochs = " + maxEpochs);
            const double alpha = 0.01;
            DebugLogger.GetInstance().LogInfo("Setting learning rate = " + alpha.ToString("F2"));

            DebugLogger.GetInstance().LogInfo("\nStarting training using (stochastic) gradient descent");
            var weights = lc.Train(trainData, maxEpochs, alpha);
            DebugLogger.GetInstance().LogInfo("Training complete");

            DebugLogger.GetInstance().LogInfo("\nBest weights found:");
            ShowVector(weights, 4, true);

            var trainAcc = lc.Accuracy(trainData, weights);
            DebugLogger.GetInstance().LogInfo("Prediction accuracy on training data = " +
                              trainAcc.ToString("F4"));

            var testAcc = lc.Accuracy(testData, weights);
            DebugLogger.GetInstance().LogInfo("Prediction accuracy on test data = " +
                              testAcc.ToString("F4"));

            DebugLogger.GetInstance().LogInfo("\nEnd LR binary classification demo\n");
           // DebugLogger.GetInstance().LogInfo.ReadLine();
        }

        static double[][] MakeAllData(int numFeatures, int numRows, int seed)
        {
            var rnd = new Random(seed);
            var weights = new double[numFeatures + 1]; // inc. b0
            for (var i = 0; i < weights.Length; ++i)
                weights[i] = 20.0 * rnd.NextDouble() - 10.0; // [-10.0 to +10.0]

            var result = new double[numRows][]; // allocate matrix
            for (var i = 0; i < numRows; ++i)
                result[i] = new double[numFeatures + 1]; // Y in last column

            for (var i = 0; i < numRows; ++i) // for each row
            {
                var z = weights[0]; // the b0 
                for (var j = 0; j < numFeatures; ++j) // each feature / column except last
                {
                    var x = 20.0 * rnd.NextDouble() - 10.0; // random X in [10.0, +10.0]
                    result[i][j] = x; // store x
                    var wx = x * weights[j + 1]; // weight * x 
                    z += wx; // accumulate to get Y
                }
                var y = 1.0 / (1.0 + Math.Exp(-z));
                if (y > 0.55)  // slight bias towards 0
                    result[i][numFeatures] = 1.0; // store y in last column
                else
                    result[i][numFeatures] = 0.0;
            }
            DebugLogger.GetInstance().LogInfo("Data generation weights:");
            ShowVector(weights, 4, true);

            return result;
        } // MakeAllData

        static void MakeTrainTest(double[][] allData, int seed,
            out double[][] trainData, out double[][] testData)
        {
            var rnd = new Random(seed);
            var totRows = allData.Length;
            var numTrainRows = (int)(totRows * 0.80); // 80% hard-coded
            var numTestRows = totRows - numTrainRows;
            trainData = new double[numTrainRows][];
            testData = new double[numTestRows][];

            var copy = new double[allData.Length][]; // ref copy of all data
            for (var i = 0; i < copy.Length; ++i)
                copy[i] = allData[i];

            for (var i = 0; i < copy.Length; ++i) // scramble order
            {
                var r = rnd.Next(i, copy.Length); // use Fisher-Yates
                var tmp = copy[r];
                copy[r] = copy[i];
                copy[i] = tmp;
            }
            for (var i = 0; i < numTrainRows; ++i)
                trainData[i] = copy[i];

            for (var i = 0; i < numTestRows; ++i)
                testData[i] = copy[i + numTrainRows];
        } // MakeTrainTest


        private static void ShowData(double[][] data, int numRows,
            int decimals, bool indices)
        {
            var len = data.Length.ToString().Length;
            for (var i = 0; i < numRows; ++i)
            {
                var str = "";
                if (indices)
                    str += "[" + i.ToString().PadLeft(len) + "]  ";

                for (var j = 0; j < data[i].Length; ++j)
                {
                    var v = data[i][j];
                    if (v >= 0.0)
                        str += " ";

                    str += v.ToString("F" + decimals) + "  ";
                }
                DebugLogger.GetInstance().LogInfo(str);
            }

            DebugLogger.GetInstance().LogInfo(". . .");

            var lastRow = data.Length - 1;
            var str2 = "";
            if (indices)
                str2 = "[" + lastRow.ToString().PadLeft(len) + "]  ";

            for (var j = 0; j < data[lastRow].Length; ++j)
            {
                double v = data[lastRow][j];
                if (v >= 0.0)
                    str2 += " ";
                str2 += v.ToString("F" + decimals) + "  ";
            }
            DebugLogger.GetInstance().LogInfo(str2);
        }

        static void ShowVector(double[] vector, int decimals, bool newLine)
        {
            foreach (var t in vector)
                DebugLogger.GetInstance().LogInfo(t.ToString("F" + decimals) + " ");

            DebugLogger.GetInstance().LogInfo("");
            if (newLine)
                DebugLogger.GetInstance().LogInfo("");
        }

    } // Program

    public class LogisticClassifier
    {
        private readonly double[] _weights; // b0 = constant
        private readonly Random _rnd;

        public LogisticClassifier(int numFeatures)
        {
            _weights = new double[numFeatures + 1]; // [0] = b0 constant
            _rnd = new Random(1234);
            //for (int i = 0; i < weights.Length; ++i) // not necessary
            //  weights[i] = 0.01 * rnd.NextDouble(); // [0.00, 0.01)
        }

        public double[] Train(double[][] trainData, int maxEpochs, double alpha)
        {
            // alpha is the learning rate
            var epoch = 0;
            var sequence = new int[trainData.Length]; // random order
            for (var i = 0; i < sequence.Length; ++i)
                sequence[i] = i;

            while (epoch < maxEpochs)
            {
                ++epoch;

                if (epoch % 100 == 0 && epoch != maxEpochs)
                {
                    var mse = Error(trainData, _weights);
                    DebugLogger.GetInstance().LogInfo("epoch = " + epoch);
                    DebugLogger.GetInstance().LogInfo("  error = " + mse.ToString("F4"));
                }

                Shuffle(sequence); // process data in random order

                // stochastic/online/incremental approach
                for (var ti = 0; ti < trainData.Length; ++ti)
                {
                    var i = sequence[ti];
                    var computed = ComputeOutput(trainData[i], _weights);
                    var targetIndex = trainData[i].Length - 1;
                    var target = trainData[i][targetIndex];

                    _weights[0] += alpha * (target - computed) * 1; // the b0 weight has a dummy 1 input
                    //weights[0] += alpha * (target - computed) * computed * (1 -computed) * 1; // alt. form
                    for (var j = 1; j < _weights.Length; ++j)
                        _weights[j] += alpha * (target - computed) * trainData[i][j - 1];
                    //weights[j] += alpha * (target - computed) * computed * (1 - computed) * trainData[i][j - 1]; // alt. form
                }

                // batch/offline approach
                //double[] accumulatedGradients = new double[weights.Length]; // one acc for each weight

                //for (int i = 0; i < trainData.Length; ++i)  // accumulate
                //{
                //  double computed = ComputeOutput(trainData[i], weights); // no need to shuffle order
                //  int targetIndex = trainData[i].Length - 1;
                //  double target = trainData[i][targetIndex];
                //  accumulatedGradients[0] += (target - computed) * 1; // for b0
                //  for (int j = 1; j < weights.Length; ++j)
                //    accumulatedGradients[j] += (target - computed) * trainData[i][j - 1];
                //}

                //for (int j = 0; j < weights.Length; ++j) // update
                //  weights[j] += alpha * accumulatedGradients[j];

            } // while
            return _weights; // by ref is somewhat risky
        } // Train

        private void Shuffle(int[] sequence)
        {
            for (var i = 0; i < sequence.Length; ++i)
            {
                var r = _rnd.Next(i, sequence.Length);
                var tmp = sequence[r];
                sequence[r] = sequence[i];
                sequence[i] = tmp;
            }
        }

        private double Error(double[][] trainData, double[] weights)
        {
            // mean squared error using supplied weights
            int yIndex = trainData[0].Length - 1; // y-value (0/1) is last column
            double sumSquaredError = 0.0;
            for (int i = 0; i < trainData.Length; ++i) // each data
            {
                double computed = ComputeOutput(trainData[i], weights);
                double desired = trainData[i][yIndex]; // ex: 0.0 or 1.0
                sumSquaredError += (computed - desired) * (computed - desired);
            }
            return sumSquaredError / trainData.Length;
        }

        private double ComputeOutput(double[] dataItem, double[] weights)
        {
            double z = 0.0;
            z += weights[0]; // the b0 constant
            for (int i = 0; i < weights.Length - 1; ++i) // data might include Y
                z += (weights[i + 1] * dataItem[i]); // skip first weight
            return 1.0 / (1.0 + Math.Exp(-z));
        }

        private int ComputeDependent(double[] dataItem, double[] weights)
        {
            double y = ComputeOutput(dataItem, weights); // 0.0 to 1.0
            if (y <= 0.5)
                return 0;
            else
                return 1;
        }

        public double Accuracy(double[][] trainData, double[] weights)
        {
            int numCorrect = 0;
            int numWrong = 0;
            int yIndex = trainData[0].Length - 1;
            for (int i = 0; i < trainData.Length; ++i)
            {
                int computed = ComputeDependent(trainData[i], weights);
                int target = (int)trainData[i][yIndex]; // risky?

                if (computed == target)
                    ++numCorrect;
                else
                    ++numWrong;
            }
            return (numCorrect * 1.0) / (numWrong + numCorrect);
        }
    } // LogisticClassifier
} // ns

