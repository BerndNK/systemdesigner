﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SystemDesigner.Neural.Layer;
using SystemDesigner.Utils;
using DisplayCore.Annotations;

namespace SystemDesigner.Neural
{
    public class NeuralNetworkTrainer
    {
        private readonly NeuralNetwork _network;
        private TrainingSet _trainingSet;

        public double Error => GetError();

        private double GetError()
        {
            var sumError = 0.0;
            foreach (var inputOutputTuple in _trainingSet.InputOutputList)
            {
                var givenInput = inputOutputTuple.Item1;
                var expectedOutput = inputOutputTuple.Item2;
                // calculate outputs
                lock (_network)
                {
                    var actualOutput = _network.Compute(givenInput);
                    sumError += expectedOutput.Select((expected, i) => Math.Pow(expected - actualOutput[i], 2)).Sum();

                }
            }
            return sumError / (_trainingSet.InputOutputList.Count * 2);
        }

        public string GetDebugInfo()
        {
            var correctOnes = 0;
            string example = null;
            foreach (var inputOutputTuple in _trainingSet.InputOutputList)
            {

                var givenInput = inputOutputTuple.Item1;
                var expectedOutput = inputOutputTuple.Item2.ToList();
                // calculate outputs
                lock (_network)
                {
                    var actualOutput = _network.Compute(givenInput).ToList();

                    var expectedIndex = expectedOutput.IndexOf(expectedOutput.Max());
                    var actualIndex = actualOutput.IndexOf(actualOutput.Max());

                    if (expectedIndex == actualIndex) correctOnes++;


                    if (example == null) 
                        example = $"Expected: {expectedOutput.ToArray().Print()} Actual: {actualOutput.ToArray().Print()}";
                }
            }
            return $"Correct {correctOnes} of {_trainingSet.InputOutputList.Count} sumError: {GetError()} Example {example}";
        }

        public NeuralNetworkTrainer([NotNull] NeuralNetwork network, TrainingSet trainingSet)
        {
            _network = network ?? throw new ArgumentNullException();

            _trainingSet = trainingSet;
        }

        public void Iteration(double learningRate)
        {
            var batchSize = 150;
            // shuffle training set
            _trainingSet.InputOutputList.Shuffle();
            var batches = ListExtensionAndUtils.SplitList(_trainingSet.InputOutputList, batchSize).ToList();
            var tasks = new Task[batches.Count()];

            for (var i = 0; i < tasks.Length; i++)
            {
                var batch = batches[i];
                tasks[i] = Task.Run(() =>
                {
                    foreach (var inputOutputTuple in batch)
                    {

                        var givenInput = inputOutputTuple.Item1;
                        var expectedOutput = inputOutputTuple.Item2;

                        lock (_network)
                        {
                            // calculate outputs
                            _network.Compute(givenInput);
                            // calculate error deltas
                            BackpropagateError(expectedOutput);
                            // update Weights
                            UpdateWeights(givenInput, learningRate);
                        }
                    }
                });
            }

            Task.WaitAll(tasks);


        }

        /// <summary>
        /// Updates the weights while assuming that DeltaError and LastOutput from each neuron is set.
        /// </summary>
        /// <param name="givenInput">The given input to update the weights with.</param>
        /// <param name="learningRate">The learning rate.</param>
        private void UpdateWeights(double[] givenInput, double learningRate)
        {
            // the outputs from the last layer. For the first layer, this will be the input for the network.
            var outputsFromLastLayer = givenInput;
            // go through each layer
            foreach (var networkLayer in _network.Layers)
            {
                // go through each neuron
                foreach (var crntNeuron in networkLayer.Neurons)
                {
                    // update the weight from each neuron of the last layer(outputsFromLastLayer) with the error delta and the learning rate
                    // if we're not in the first layer (which has no weights)
                    if (!Equals(networkLayer, _network.Layers.First()))
                    {
                        for (var j = 0; j < outputsFromLastLayer.Length; j++)
                        {
                            crntNeuron.Weights[j] += learningRate * crntNeuron.ErrorDelta * outputsFromLastLayer[j];
                        }
                    }

                    // update bias
                    crntNeuron.Bias += learningRate * crntNeuron.ErrorDelta;
                }

                // set the outputs from the current layer, for the next iteration
                outputsFromLastLayer = networkLayer.Neurons.Select(x => x.LastOutput).ToArray();
            }
        }

        private void BackpropagateError(IReadOnlyList<double> expectedOutput)
        {
            // go through each layer in reverse
            for (var i = _network.Layers.Count - 1; i >= 0; --i)
            {
                var crntLayer = _network.Layers[i];
                // if the currentLayer is the last layer (the output layer)
                if (Equals(crntLayer, _network.Layers.Last()))
                {
                    // calculate error for each neuron. Error for one neuron in the output layer is (expectedOutput - actualOutput)
                    // NOTE: size of the expected output MUST be the size of the outputLayer.
                    for (var crntNeuronIndex = 0; crntNeuronIndex < crntLayer.Neurons.Length; crntNeuronIndex++)
                    {
                        // Set the ErrorDelta of the neuron, which is the (expectedOutput - actualOutput) * Derivate of actualOutput
                        crntLayer.Neurons[crntNeuronIndex].SetDelta(expectedOutput[crntNeuronIndex]);
                    }
                }
                else
                {
                    var layerAfterCrnt = _network.Layers[i + 1];
                    for (var j = 0; j < crntLayer.Neurons.Length; j++)
                    {
                        var crntNeuron = crntLayer.Neurons[j];
                        // error of the current neuron is the sum of all weights to other neurons (so we take the j'th weight of each neuron on the NEXT layer) multiplied by the delta of the next neuron)
                        var error = layerAfterCrnt.Neurons.Sum(neuron => neuron.Weights[j] * neuron.ErrorDelta);
                        crntNeuron.ErrorDelta = error * NeuronExtension.Derivative(crntNeuron.LastOutput);
                    }
                }
            }

        }

    }
}
