﻿using System.Collections.Generic;

namespace SystemDesigner.Neural
{
    /// <summary>
    /// Describes a managing instance for a machine learning neural network.
    /// </summary>
    public interface INeuralNetworkManager
    {

        /// <summary>
        /// Gets the error sum of this network.
        /// </summary>
        /// <returns>Calculated error sum.</returns>
        double Error { get; }

        /// <summary>
        /// Gets debug information.
        /// </summary>
        /// <returns></returns>
        string DebugInformation { get; }

        /// <summary>
        /// Gets the name that shall be displayed for this.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Initializes the neural network with the given input sizes. With a given amount of layers = 4. (input, hidden 1, hidden 2, output)
        /// </summary>
        /// <param name="inputSize">Size of the input layer.</param>
        /// <param name="hiddenLayers">The sizes (and indirectly amount) of the hidden layers.</param>
        /// <param name="outputLayerSize">Size of the output layer.</param>
        void Init(int inputSize, List<int> hiddenLayers, int outputLayerSize);
        
        /// <summary>
        /// Sets the training data.
        /// </summary>
        /// <param name="input">The inputs.</param>
        /// <param name="desiredOutput">The desired output.</param>
        void SetTrainingData(double[][] input, double[][] desiredOutput);

        /// <summary>
        /// Tests the specified input array.
        /// </summary>
        /// <param name="inputArray">The input array.</param>
        /// <returns>Outputs of the last layer</returns>
        double[] Compute(double[] inputArray);

        /// <summary>
        /// Iterates one training.
        /// </summary>
        void Iteration(double learningRate);

    }
}
