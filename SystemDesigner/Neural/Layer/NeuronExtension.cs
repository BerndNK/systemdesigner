using System;
using System.Linq;

namespace SystemDesigner.Neural.Layer
{
    /// <summary>
    /// Extension class for Neuron struct.
    /// </summary>
    public static class NeuronExtension
    {

        /// <summary>
        /// Activates the neuron with the given inputs.
        /// </summary>
        /// <param name="neuron">The neuron to be used for activation.</param>
        /// <param name="inputs">The input values. Must be the same size as the weights array.</param>
        /// <returns>
        /// A value between 0 and 1, calculated by the sigmoid function
        /// </returns>
        /// <exception cref="System.ArgumentException">The input array was a different size as the weights array</exception>
        public static double Activate(this Neuron neuron, ref double[] inputs)
        {
#if DEBUG
            if (inputs.Length != neuron.Weights.Length) throw new ArgumentException("The input array was a different size as the weights array");
#endif
            // output = Sigmoid result of (Sum of all inputs * their weight PLUS the bias of this neuron)
            var sum = inputs.Select((t, i) => t * neuron.Weights[i]).Sum();
            neuron.LastOutput = Sigmoid(sum + neuron.Bias);
            return neuron.LastOutput;
        }

        /// <summary>
        /// Sets the error delta of the neuron with the LastOutput and the given expectedOutput.
        /// </summary>
        /// <param name="neuron">The neuron.</param>
        /// <param name="expectedOutput">The expected output.</param>
        public static void SetDelta(this Neuron neuron, double expectedOutput)
        {
            neuron.ErrorDelta = (expectedOutput - neuron.LastOutput) * Derivative(neuron.LastOutput);
        }

        /// <summary>
        /// Returns the Sigmoid function value of the specified input.
        /// </summary>
        /// <param name="input">The input for the function.</param>
        /// <returns>Value [0,1]</returns>
        private static double Sigmoid(double input) => 1.0 / (1.0 + Math.Exp(-input));

        /// <summary>
        /// Derivatives the specified output.
        /// </summary>
        /// <param name="output">The output to derivate.</param>
        /// <returns>Derivation of the given output.</returns>
        public static double Derivative(double output) => output * (1.0 - output);
    }
}