﻿using System.Xml.Serialization;
using SystemDesigner.Utils;

namespace SystemDesigner.Neural.Layer
{
    [XmlType("NeuralLayer")]
    public struct NeuralLayer
    {

        /// <summary>
        /// Gets or sets the neurons of this layer.
        /// </summary>
        [XmlArray("Neurons")]
        [XmlArrayItem("Neuron", Type = typeof(Neuron))]
        public Neuron[] Neurons { get; set; }

        /// <summary>
        /// Gets the size of this layer, which is the amount of neurons.
        /// </summary>
        [XmlIgnore]
        public int Size => Neurons.Length;

        /// <summary>
        /// Gets the size of the weights of the neurons. In other words, how many outputs the layer before this has.
        /// </summary>
        [XmlElement("WeightSize")]
        public int WeightSize { get; set; }

        public NeuralLayer(int size, int sizePreviousLayer)
        {
            // create the neuron array with the given size
            Neurons = new Neuron[size];
            WeightSize = sizePreviousLayer;

            // fill it with neuron structs with random biases and weights
            for (var i = 0; i < size; i++)
            {
                // random bias using normal distributed random values
                Neurons[i] = new Neuron(sizePreviousLayer, DistributedRandom.GetGaussianDistributedRandom());

                // fill weights also with normal distributed values
                Neurons[i].Weights.FillWithRandom(() => DistributedRandom.GetGaussianDistributedRandom());
            }
        }

        public override string ToString()
        {
            return $"Layer - Size: {Size}";
        }
    }
}
