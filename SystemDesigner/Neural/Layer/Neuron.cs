﻿using System.Xml.Serialization;

namespace SystemDesigner.Neural.Layer
{
    /// <summary>
    /// Describes a neuron in a neuralNetwork. Has weights for each output (usually of the previous layer) as input for this neuron.
    /// </summary>
    [XmlType("Neuron")]
    public class Neuron
    {

        /// <summary>
        /// The weights of each output of the previous layer, to this neuron. The sum of all weights, compared to the bias results in the output of this neuron
        /// </summary>
        /// <value>
        /// The weights of each output as an input for this neuron.
        /// </value>
        [XmlArray("Weights")]
        public double[] Weights { get; set; }

        /// <summary>
        /// Gets or sets the bias of this neuron.
        /// </summary>
        [XmlElement("Bias")]
        public double Bias { get; set; }

        /// <summary>
        /// Gets or sets the ErrorDelta. Which is a helper property for the training set.
        /// </summary>
        [XmlIgnore]
        public double ErrorDelta { get; set; }

        /// <summary>
        /// Gets or sets the last output that was calculated for this neuron.
        /// </summary>
        [XmlIgnore]
        public double LastOutput { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Neuron"/> struct.
        /// </summary>
        /// <param name="weigthCount">The amount of outputs (of usually the previous layer) which will have weights as inputs for this neuron.</param>
        /// <param name="bias">The bias of this neuron.</param>
        public Neuron(int weigthCount, double bias)
        {
            Weights = new double[weigthCount];
            Bias = bias;
            ErrorDelta = 0;
            LastOutput = 0;
        }

        public Neuron()
        {

            ErrorDelta = 0;
            LastOutput = 0;
        }

        public override string ToString()
        {
            return $"Bias: {Bias} Weights.Length {Weights.Length}";
        }
    }
}
