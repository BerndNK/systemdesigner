using System;
using System.Linq;

namespace SystemDesigner.Neural.Layer
{
    public static class NeuralLayerExtension
    {
        /// <summary>
        /// Computes the specified output for the given input.
        /// </summary>
        /// <param name="layer">The layer to compute with.</param>
        /// <param name="input">The input for the layer (which must be the same size as the weights the layer has been initialized with).</param>
        /// <returns>Vector of doubles which is the output of the layer.</returns>
        /// <exception cref="System.ArgumentException">Input vector was not the same size as the input layer</exception>
        public static double[] Compute(this NeuralLayer layer, double[] input)
        {
            // if the current layer has no weights, set the output = input
            if (layer.WeightSize == 0)
            {
#if DEBUG
                if (input.Length != layer.Size) throw new ArgumentException("Length of given input layer was not equal to the size of this layer even though it has 0 weights.");
#endif
                // set the LastOutput values of the neurons
                for (var i = 0; i < layer.Neurons.Length; i++)
                {
                    layer.Neurons[i].LastOutput = input[i];
                }
                // return copy of input array
                return input.ToArray();
            }
#if DEBUG
            if (input.Length != layer.WeightSize) throw new ArgumentException("Length of given input layer was not equal to the amount of weights in this layer.");
#endif
                // return the output of each neuron as array
                return (from neuron in layer.Neurons
                    select neuron.Activate(ref input)).ToArray();
        }
    }
}