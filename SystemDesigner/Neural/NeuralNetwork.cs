﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using SystemDesigner.Neural.Layer;

namespace SystemDesigner.Neural
{
    [XmlType("Network")]
    public class NeuralNetwork
    {

        #region Fields
        [XmlArray("Layers")]
        [XmlArrayItem("Layer", Type = typeof(NeuralLayer))]
        public readonly List<NeuralLayer> Layers;
        #endregion


        /// <summary>
        /// Initializes a new instance of the <see cref="NeuralNetwork"/> class.
        /// </summary>
        public NeuralNetwork()
        {
            Layers = new List<NeuralLayer>();
        }

        /// <summary>
        /// Adds a new layer.
        /// </summary>
        /// <param name="size">The size of the layer to add.</param>
        public void AddLayer(int size)
        {
            //add new neural layer, with a weight size of the size of the last layer (which will be the layer before the one we'll add)
            Layers.Add(new NeuralLayer(size, Layers.Count != 0 ? Layers.Last().Size : 0));
        }

        public double[] Compute(double[] input)
        {
#if DEBUG
            if (Layers.Count == 0) throw new Exception("Cannot compute with a neural network with 0 layers.");
            if (input.Length != Layers.First().Size) throw new ArgumentException("Input vector was not the same size as the input layer");
#endif

            var outputLastLayer = input;
            foreach (var neuralLayer in Layers)
            {
                outputLastLayer = neuralLayer.Compute(outputLastLayer);
            }

            return outputLastLayer;
        }

        public override string ToString()
        {
            return $"Layer count: {Layers.Count}";
        }
    }
}
