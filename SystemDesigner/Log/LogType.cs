using System;

namespace SystemDesigner.Log {
    /// <summary>
    /// Describes the type of a log message
    /// </summary>
    [Flags]
    public enum LogType {
        Info = 1,
        Error = 2,
        All = Info | Error
    }
}