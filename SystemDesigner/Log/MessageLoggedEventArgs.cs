﻿using System;

namespace SystemDesigner.Log {
    public class MessageLoggedEventArgs : EventArgs {
        /// <summary>
        /// Gets the logged message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public LogMessage Message { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageLoggedEventArgs"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public MessageLoggedEventArgs(LogMessage message) {
            Message = message;
        }
    }
}