﻿using System;

namespace SystemDesigner.Utils
{
    public static class DistributedRandom
    {
        private static readonly Random GlobalRng = new Random(1234);

        /// <summary>
        /// Gets a gaussian distributed random number.
        /// </summary>
        /// <param name="mean">The mean of the gaussian function.</param>
        /// <param name="stdDev">The standard deviation which is the square root of the variance.</param>
        /// <param name="rng">The random number generator used as a base. If null, the global instance is used.</param>
        /// <returns>A value between 0 and 1. With probabilities decided through the given parameter.</returns>
        /// <see cref="https://en.wikipedia.org/wiki/Normal_distribution"/>
        /// <seealso cref="http://stackoverflow.com/questions/218060/random-gaussian-variables"/>
        public static double GetGaussianDistributedRandom(double mean = 0, double stdDev = 1, Random rng = null)
        {
            // use the global rng if the given one is null
            if (rng == null) rng = GlobalRng;
            // get two uniform random doubles (0,1]
            var u1 = 1.0 - rng.NextDouble();
            var u2 = 1.0 - rng.NextDouble();

            var randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                                Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)

            return mean + stdDev * randStdNormal; //random normal(mean,stdDev^2)
        }
    }
}
