﻿using System;
using System.Linq;

namespace SystemDesigner.Utils
{
    public static class DoubleArrayExtension
    {
        private static readonly Random _rng = new Random(1234);

        /// <summary>
        /// Fills the given array with random numbers.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="randomGeneratorMethod">The random generator method.</param>
        public static void FillWithRandom(this double[] array, Func<double> randomGeneratorMethod)
        {
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = randomGeneratorMethod();
            }
        }

        /// <summary>
        /// Shuffles the specified array.
        /// </summary>
        /// <param name="array">The array to be shuffled.</param>
        public static void Shuffle(this double[] array)
        {
            var rng = _rng;
            var n = array.Length;
            // swap each item with a new position
            while (n > 1)
            {
                n--;
                var k = rng.Next(n + 1);
                var value = array[k];
                array[k] = array[n];
                array[n] = value;
            }

        }

        public static string Print(this double[] array)
        {
            var toReturn = array.Aggregate("[", (current, d) => current + (d + " "));
            return toReturn + "]";
        }

    }
}
