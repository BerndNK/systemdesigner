﻿using System;
using System.Collections.Generic;

namespace SystemDesigner.Utils {
    public static class ListExtensionAndUtils {
        private static Random rng = new Random(1234);

        public static void Shuffle<T>(this IList<T> list) {
            var n = list.Count;
            while (n > 1) {
                n--;
                var k = rng.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static IEnumerable<List<T>> SplitList<T>(List<T> locations, int nSize = 30) {
            for (var i = 0; i < locations.Count; i += nSize) {
                yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
            }
        }
    }
}
