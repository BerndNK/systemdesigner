﻿using System;

namespace SystemDesigner.Utils
{
    /// <summary>
    /// A collective class which contains basic information about the device. The variables have to be set prior of the usage of SystemDesigner's more advanced classes.
    /// </summary>
    public static class DeviceInfo
    {

        public static double DeviceDpi { get; set; } = 72;

        public static bool TouchCapable { get; set; } = true; // whether the device has touch capacities or not
    }
}
