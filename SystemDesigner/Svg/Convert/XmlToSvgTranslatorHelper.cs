﻿using System;
using System.Globalization;
using SvgAbstraction.Svg;
using XmlParser.Xml;

namespace SystemDesigner.Svg.Convert {
    public static class XmlToSvgTranslatorHelper {

        internal delegate T SetPropertyByAttributeDelegate<out T>(string propertyName); // delegate for setting a property value via attributes. Implement directly in a module to spare injection

        /// <summary>
        /// Takes a svg matrix transformation string and applies it to the target item. 
        /// </summary>
        /// <param name="transformationString">Transformation string. Expected structure: "matrix([values])"</param>
        /// <param name="target">The item to apply the transformation to</param>
        /// <param name="parent">Parent item of the target</param>
        /// <exception cref="ArgumentNullException">Thrown when target or transformation string is null</exception>
        public static void SetTransformationByString(string transformationString, ISvgItem target, SvgItemContainer parent) {
            if (target == null) throw new ArgumentNullException(nameof(target));
            if (transformationString == null) throw new ArgumentNullException(nameof(transformationString));

            // get pure transform string
            var matrixIndex = transformationString.IndexOf("matrix", StringComparison.Ordinal);
            if (matrixIndex == -1) throw new ArgumentException("\"matrix\" was not found in transformation string");
            var openBrace = transformationString.IndexOf('(', matrixIndex) + 1; // + 1 because we do not want the ( do be included
            var closeBrace = transformationString.IndexOf(')', openBrace);

            if (openBrace == 0) throw new ArgumentException("Open bracelet was not found in transformation string.");
            if (closeBrace == -1) throw new ArgumentException("Close bracelet was not found in transformation string.");
            var rawString = transformationString.Substring(openBrace, closeBrace - openBrace);
            var filteredString = rawString.Replace(" ", ""); // remove spaces from the string

            // split by comma
            var splitted = filteredString.Split(',');

            // assign to variables
            var scaleX = System.Convert.ToDouble(splitted[0], CultureInfo.InvariantCulture);
            var scaleY = System.Convert.ToDouble(splitted[3], CultureInfo.InvariantCulture);
            var xDistortion = System.Convert.ToDouble(splitted[4], CultureInfo.InvariantCulture);
            var yDistortion = System.Convert.ToDouble(splitted[5], CultureInfo.InvariantCulture);

            target.ScaleX = scaleX;
            target.ScaleY = scaleY;

            CalculatePivot(xDistortion, yDistortion, target, parent);
        }

        /// <summary>
        /// Takes a transformation matrix and calculates the X and Y pivot for the item
        /// </summary>
        /// <param name="xDistortion">X Distortion of the transformation matrix</param>
        /// <param name="yDistortion">Y Distortion of the transformation matrix</param>
        /// <param name="target">The target to apply the pivot to</param>
        /// <param name="parent">Parent of the target necessary to fully calculate the pivot</param>
        /// <exception cref="ArgumentNullException">Thrown when target is null</exception>
        private static void CalculatePivot(double xDistortion, double yDistortion, ISvgItem target, ISvgItem parent = null) {
            if (target == null) throw new ArgumentNullException(nameof(target));

            // if there is no parent, the distortion is 1 to 1 the pivot
            if (parent == null) {
                target.PivotX = xDistortion;
                target.PivotY = yDistortion;
                return;
            }

            // if it isnt, we reverse the formula of SvgToXmlTranslatorHelper
            //xDistortion + PivotX - target.X = -target.Parent.X * target.ScaleX
            //PivotX = -target.Parent.x * target.ScaleX + target.X - xDistortion

            target.PivotX = -parent.X*target.ScaleX + target.X - xDistortion;
            target.PivotY = -parent.Y * target.ScaleY + target.Y - yDistortion;
        }

        /// <summary>
        /// Reads an attribute of a xmlTag and returns a casted value. If the attribute does not exist the default will be returned
        /// </summary>
        /// <typeparam name="T">Type the attribute shall be casted to</typeparam>
        /// <param name="xmlTag">Target xmlTag to read the attribute from</param>
        /// <param name="attributeName">Name of the xml attribute</param>
        /// <param name="cultureInfo">Additional casting information as culture info</param>
        /// <returns>T casted value (may be null) or default(T) if the attribute did not exist</returns>
        /// <exception cref="ArgumentNullException">Thrown when xmlTag or attributeName is null</exception>
        /// <exception cref="ArgumentException">Thrown when attributeName is an invalid string</exception>
        public static T GetAttributeValue<T>(IXmlTag xmlTag, string attributeName, CultureInfo cultureInfo = null) {
            // guard clauses
            if (xmlTag == null) throw new ArgumentNullException(nameof(xmlTag));
            if (attributeName == null) throw new ArgumentNullException(nameof(attributeName));
            if (string.IsNullOrWhiteSpace(attributeName)) throw new ArgumentException("attributeName may not be only whitespace", nameof(attributeName));

            // check if the xml tag has the attribute, if not return default value
            if (!xmlTag.HasAttribute(attributeName)) return default(T);

            // if it does has the attribute, get the value and cast it to T
            var value = xmlTag.GetAttribute(attributeName); // value should not be null, since we checked if the tag has the attribute, but this is a safety measure
            return value == null ? default(T) : (T)System.Convert.ChangeType(value, typeof(T), cultureInfo);
        }
    }
}
