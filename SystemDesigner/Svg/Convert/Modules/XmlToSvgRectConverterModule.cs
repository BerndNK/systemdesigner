﻿using System.Globalization;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms;
using XmlParser.Convert;
using XmlParser.Xml;

namespace SystemDesigner.Svg.Convert.Modules {
    public class XmlToSvgRectConverterModule : IXmlConverterModule<SvgItemContainer, ISvgItem> {

        #region Properties
        #region IXmlConverterModule

        public string ConvertsFrom => "rect";

        #endregion
        #endregion
        
        #region IXmlConverterModule Implementation
        public ISvgItem Convert(IXmlTag xmlTag, SvgItemContainer parent) {
            var rect = new SvgRect();
            // create helper delegate to set a property via a attribute
            var getPropertyValue = new XmlToSvgTranslatorHelper.SetPropertyByAttributeDelegate<double>(
                // get property of the xml (GetAttributeValue). The Xml Propertyname is resolved by "GetXmlName"
                name => XmlToSvgTranslatorHelper.GetAttributeValue<double>(xmlTag, XmlTranslatorHelper.GetXmlName(typeof(SvgRect), name), CultureInfo.InvariantCulture));

            // X/Y
            rect.X = getPropertyValue(nameof(rect.X));
            rect.Y = getPropertyValue(nameof(rect.Y));
            rect.Width = getPropertyValue(nameof(rect.Width));
            rect.Height = getPropertyValue(nameof(rect.Height));

            //Attribute.GetCustomAttributes

            // get the pivotX/Y and scaleX/Y
            if(xmlTag.HasAttribute("transform"))
                XmlToSvgTranslatorHelper.SetTransformationByString(xmlTag.GetAttribute("transform").ToString(), rect, parent);

            return rect;
        }
        #endregion
    }
}
