﻿using System;
using System.Globalization;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms.Container;
using XmlParser.Convert;
using XmlParser.Xml;

namespace SystemDesigner.Svg.Convert.Modules.Container
{
    public class XmlToSvgRootConverter : IXmlConverterModule<SvgItemContainer, ISvgItem>
    {
        public string ConvertsFrom => "svg";

        #region IXmlConverterModule Implementation
        /// <exception cref="ArgumentException">Thrown when the given transform attribute has an invalid structure</exception>
        public ISvgItem Convert(IXmlTag xmlTag, SvgItemContainer parent)
        {
            var root = new SvgRoot();
            // group only has a scaleX/Y and X and Y and both are in the transform tag

            // get the pivotX/Y and scaleX/Y
            if (xmlTag.HasAttribute("transform"))
            {
                var transform = xmlTag.GetAttribute("transform").ToString();
                // get the translate part if there is one
                var translateIndex = transform.IndexOf("translate", StringComparison.Ordinal);
                if (translateIndex != -1)
                {
                    var openBrace = transform.IndexOf("(", translateIndex, StringComparison.Ordinal) + 1;
                    var closeBrace = transform.IndexOf(")", openBrace, StringComparison.Ordinal);
                    // check if any of the braces were not found (check for 0 for the opener, because we added a +1)
                    if (openBrace == 0 || closeBrace == -1) throw new ArgumentException($"Transform attribute was not in the correct format. Source: \"{transform}\"");
                    var rawString = transform.Substring(openBrace, closeBrace - openBrace);
                    var filtered = rawString.Replace(" ", ""); // remove the blanks

                    var splitted = filtered.Split(',');
                    if (splitted.Length != 2) throw new ArgumentException("Translate part of transform string had an invalid structure");

                    root.X = System.Convert.ToDouble(splitted[0], CultureInfo.InvariantCulture);
                    root.Y = System.Convert.ToDouble(splitted[1], CultureInfo.InvariantCulture);
                }

                // now do the scale X and Y
                XmlToSvgTranslatorHelper.SetTransformationByString(transform, root, parent);
            }

            return root;
        }
        #endregion
    }
}
