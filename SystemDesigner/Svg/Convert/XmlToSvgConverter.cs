﻿using SystemDesigner.Svg.Convert.Modules;
using SystemDesigner.Svg.Convert.Modules.Container;
using SvgAbstraction.Svg;
using XmlParser.Convert;

namespace SystemDesigner.Svg.Convert {
    public class XmlToSvgConverter : XmlConverter<SvgItemContainer, ISvgItem> {

        public XmlToSvgConverter() {
            // add svg convert modules
            ConverterModules.Add(new XmlToSvgRectConverterModule()); // SvgRect
            ConverterModules.Add(new XmlToSvgGroupConverterModule()); // SvgGroup
            ConverterModules.Add(new XmlToSvgRootConverter()); // SvgGroup
        }
    }
}