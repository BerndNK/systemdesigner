﻿namespace SystemDesigner.Svg
{
    /// <summary>
    /// Describes a shape
    /// </summary>
    public enum Shape
    {
        Rectangle,
        Ellipse,
        Triangle,
        Diamond
    }
}
