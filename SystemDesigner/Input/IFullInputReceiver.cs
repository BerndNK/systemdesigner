﻿namespace SystemDesigner.Input {
    /// <summary>
    /// Implements both move and up/down input receiver
    /// </summary>
    /// <seealso cref="SystemDesigner.Input.IMoveInputReceiver" />
    /// <seealso cref="SystemDesigner.Input.IUpDownInputReceiver" />
    public interface IFullInputReceiver : IMoveInputReceiver, IUpDownInputReceiver{
    }
}
