﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;
using SystemDesigner.Stage.Tools;
using SystemDesignerResources.Text;

namespace SystemDesigner.Input.Shortcuts
{
    /// <summary>
    /// Describes a shortcut for SystemDesigner actions.
    /// </summary>
    [XmlType("Shortcut")]
    public class Shortcut
    {
        /// <summary>
        /// Gets or sets the combinations for this shortcut.
        /// </summary>
        [XmlArray("Combinations")]
        [XmlArrayItem("Combinations", typeof(KeyCombination))]
        public List<KeyCombination> Combinations { get; set; }

        /// <summary>
        /// Gets or sets the key, which should be the same key which is used in TextResources.
        /// </summary>
        [XmlAttribute("Key")]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the action this shortcut does when the combination is pressed.
        /// </summary>
        [XmlIgnore]
        public Action OnPressedAction { get; set; }

        /// <summary>
        /// Gets or sets the action this shortcut does when the combination is released.
        /// </summary>
        [XmlIgnore]
        public Action OnReleasedAction { get; set; }

        /// <summary>
        /// Gets or sets the requirement necessary for this shortcut to be active.
        /// </summary>
        [XmlElement("Requirement")]
        public ShortcutRequirement Requirement { get; set; }

        /// <summary>
        /// Gets the name for the shortcut action.
        /// </summary>
        [XmlIgnore]
        public string Name => GetNameForShortcut(Key);

        /// <summary>
        /// Gets the description for this shortcut action.
        /// </summary>
        [XmlIgnore]
        public string Description => GetDescriptionForShortcut(Key);

        /// <summary>
        /// The application shortcuts, which may be set by platform implementations.
        /// </summary>
        public static List<Shortcut> AppShortcuts = new List<Shortcut>();

        /// <summary>
        /// Initializes a new instance of the <see cref="Shortcut"/> class.
        /// </summary>
        public Shortcut() : this(null)
        {
            // parameterless constructor for serialization
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Shortcut"/> class.
        /// </summary>
        /// <param name="key">The unique key of this shortcut, which should have the same name as used in TextResources.</param>
        /// <param name="combinations">The combinations needed to trigger this shortcut.</param>
        public Shortcut(string key, List<KeyCombination> combinations = null)
        {
            Key = key;
            if (combinations == null) combinations = new List<KeyCombination>();
            Combinations = combinations;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Shortcut"/> class.
        /// </summary>
        /// <param name="key">The unique key of this shortcut, which should have the same name as used in TextResources.</param>
        /// <param name="combination">The combination needed to trigger this shortcut.</param>
        public Shortcut(string key, KeyCombination combination) : this(key, new List<KeyCombination> {combination})
        {
            
        }

        /// <summary>
        /// Gets the default shortcuts, which is a list of all shortcuts, with some of the an actual key combination assigned.
        /// </summary>
        /// <returns></returns>
        public static List<Shortcut> GetDefaultShortcuts()
        {
            var toReturn = new List<Shortcut>();
            toReturn.AddRange(SelectTool.GetShortcuts());
            toReturn.AddRange(FreeHandTool.GetShortcuts());
            toReturn.AddRange(AppShortcuts);
            return toReturn;
        }

        /// <summary>
        /// Serializes the given list of shortcuts.
        /// </summary>
        /// <param name="shortcuts">The shortcuts.</param>
        /// <returns>Serialized XML string.</returns>
        public static string Serialize(List<Shortcut> shortcuts)
        {
            // create serializer
            var xmlSerializer = new XmlSerializer(typeof(List<Shortcut>), new XmlRootAttribute("Shortcuts"));
            using (var textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, shortcuts);
                return textWriter.ToString();
            }
        }

        /// <summary>
        /// Deserializes the specified XML.
        /// </summary>
        /// <param name="xml">The shortcut XML.</param>
        /// <returns>List of the deserialized shortcuts.</returns>
        public static List<Shortcut> Deserialize(string xml)
        {
            XDocument doc;
            try
            {
                doc = XDocument.Parse(xml);
            }
            catch (Exception)
            {
                // on failure (due to corrupted data maybe, return an empty list)
                return new List<Shortcut>();
            }

            if (doc.Root == null) throw new SerializationException("Failed to serialize the shortcuts.");
            // create serializer
            var xmlSerializer = new XmlSerializer(typeof(List<Shortcut>), new XmlRootAttribute("Shortcuts"));

            // create reader from the Root, use the serializer to parse it, return the result
            using (var reader = doc.Root.CreateReader())
            {
                return (List<Shortcut>)xmlSerializer.Deserialize(reader);
            }

        }

        /// <summary>
        /// Merges the specified the lists together, with listOld being the authoritative, meaning that when two shortcuts with same key appear in both lists, the one in listOld will be used.
        /// </summary>
        /// <param name="listOld">The already existing list.</param>
        /// <param name="listNew">The new list which may be a superset of listOld. Only shortcuts with keys that are not present in listOld will be added.</param>
        /// <returns>Merged list of both lists.</returns>
        public static List<Shortcut> Merge(List<Shortcut> listOld, List<Shortcut> listNew)
        {
            var mergedList = listOld.ToList();
            // add all shortcuts which have keys, that do not appear in the old list (so we leave eventual custom shortcuts intact)
            foreach (var shortcut in listNew)
            {
                var collision = listOld.FirstOrDefault(x => x.Key == shortcut.Key);
                if (collision == null)
                {
                    mergedList.Add(shortcut);
                    continue;
                }

                collision.OnPressedAction = shortcut.OnPressedAction;
                collision.OnReleasedAction = shortcut.OnReleasedAction;
            }
            return mergedList;
        }

        /// <summary>
        /// Gets the localized description for the given shortcut key.
        /// </summary>
        /// <param name="key">The key of the shortcut.</param>
        /// <returns>Localized description for the shortcut action.</returns>
        public static string GetDescriptionForShortcut(string key) => GetTextResource(key + "Description");

        /// <summary>
        /// Gets the localized name for the given shortcut key.
        /// </summary>
        /// <param name="key">The key of the shortcut.</param>
        /// <returns>Localized name for the shortcut action.</returns>
        public static string GetNameForShortcut(string key) => GetTextResource(key + "Name");

        /// <summary>
        /// Goes through each property of TextResources and tries to find the value for the given key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>Value of the resource, otherwise null.</returns>
        private static string GetTextResource(string key)
        {
            // get each property through reflection
            var properties = typeof(TextResources).GetRuntimeProperties().ToArray();
            // return the value of the property which has the same name as the given key
            return (from property in properties
                    where property.Name == key
                    select property.GetValue(typeof(TextResources), null)?.ToString()).FirstOrDefault();
        }
    }
}