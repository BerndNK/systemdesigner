﻿using System.Collections.Generic;
using System.Linq;
using SystemDesigner.Helpers;

namespace SystemDesigner.Input.Shortcuts
{
    public class ShortcutManager
    {
        #region Singleton pattern
        /// <summary>
        /// The singleton instance
        /// </summary>
        private static ShortcutManager _instance;

        /// <summary>
        /// Gets the instance of the DebugLogger.
        /// </summary>
        /// <returns>DebugLogger instance</returns>
        public static ShortcutManager GetInstance()
        {
            return _instance ?? (_instance = new ShortcutManager());
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="ShortcutManager"/> class from being created.
        /// </summary>
        private ShortcutManager()
        {}

        #endregion
        
        /// <summary>
        /// Gets the shortcuts.
        /// </summary>
        public List<Shortcut> Shortcuts { get; private set; }

        /// <summary>
        /// Loads the shortcuts from the settings.
        /// </summary>
        public void LoadShortcuts()
        {
            // load the saved shortcuts
            var savedShortcuts = Shortcut.Deserialize(Settings.Shortcuts);
            // update them with this version of SystemDesigner's default shortcuts
            var defaultShortcuts = Shortcut.GetDefaultShortcuts();

            // store the merged list
            Shortcuts = Shortcut.Merge(savedShortcuts, defaultShortcuts);
            Settings.Shortcuts = Shortcut.Serialize(Shortcuts);
        }

        /// <summary>
        /// Enters a combination and eventually triggers a shortcut.
        /// </summary>
        /// <param name="combination">The combination.</param>
        public void EnterCombination(KeyCombination combination)
        {
            if (Shortcuts == null) return; // abort if there are no shortcuts

            // match any shortcut that requirement is fulfilled (or a requirement doesn't exist and any key combination matches)
            var matches = Shortcuts.Where(x => (x.Requirement == null || x.Requirement.IsFullfilled) && x.Combinations.Any(y => y.Equals(combination)) && x.Combinations.Count > 0).ToList();
            foreach (var shortcut in matches)
            {
                shortcut?.OnPressedAction?.Invoke();
            }
        }
        
        /// <summary>
        /// Enters a combination which has been previously pressed, which will then eventually trigger a shortcut.
        /// </summary>
        /// <param name="combination">The combination.</param>
        public void ReleaseCombination(KeyCombination combination)
        {
            if (Shortcuts == null) return; // abort if there are no shortcuts

            // match any shortcut that requirement is fulfilled (or a requirement doesn't exist and any key combination matches)
            var matches = Shortcuts.Where(x => (x.Requirement == null || x.Requirement.IsFullfilled) && x.Combinations.Any(y => y.Equals(combination)) && x.Combinations.Count > 0).ToList();
            foreach (var shortcut in matches)
            {
                shortcut?.OnReleasedAction?.Invoke();
            }
        }

        /// <summary>
        /// Gets the first shortcut combination for the shortcut with the given key.
        /// </summary>
        /// <param name="key">The key of the shortcut.</param>
        /// <returns>First key combination for the matching shortcut. Null if none was found</returns>
        public KeyCombination GetCombinationFor(string key)
        {
            var shortcut = Shortcuts.FirstOrDefault(x => x.Key == key);
            return shortcut?.Combinations?.FirstOrDefault();
        }

        /// <summary>
        /// Restores the default values for shortcuts.
        /// </summary>
        public void RestoreDefaults()
        {
            var defaultShortcuts = Shortcut.GetDefaultShortcuts();

            Shortcuts = defaultShortcuts;
            Settings.Shortcuts = Shortcut.Serialize(defaultShortcuts);
        }
    }
}
