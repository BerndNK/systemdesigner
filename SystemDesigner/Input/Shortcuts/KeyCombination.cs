﻿using System.Xml.Serialization;

namespace SystemDesigner.Input.Shortcuts
{
    /// <summary>
    /// Describes a key combination on a keyboard.
    /// </summary>
    [XmlType("KeyCombination")]
    public class KeyCombination
    {
        /// <summary>
        /// Gets or sets the required key.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the keyboard modifier.
        /// </summary>
        public KeyboardModifier KeyboardModifier { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyCombination"/> class.
        /// </summary>
        public KeyCombination() : this(null)
        {
            // parameterless constructor for serialization
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KeyCombination"/> class.
        /// </summary>
        /// <param name="key">The required key, which is allowed to be null, e.g. for a shortcut which only happens when pressing shift.</param>
        /// <param name="keyboardModifier">The required keyboard modifiers.</param>
        public KeyCombination(string key, KeyboardModifier keyboardModifier = KeyboardModifier.None)
        {
            Key = key?.ToUpper();
            KeyboardModifier = keyboardModifier;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is KeyCombination comb)) 
                return false;

            // both combinations are equal, if the key is the same and the modifier flags are the same
            return comb.Key == Key && (comb.KeyboardModifier & KeyboardModifier) == KeyboardModifier;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Key.GetHashCode() * 397) ^ (int) KeyboardModifier;
            }
        }

        public override string ToString()
        {
            return KeyboardModifier.ToString().Replace("|", " + ") + (string.IsNullOrWhiteSpace(Key) ? "" : " + " + Key);
        }
    }
}