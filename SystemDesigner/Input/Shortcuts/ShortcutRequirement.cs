﻿using System.Xml.Serialization;

namespace SystemDesigner.Input.Shortcuts
{
    /// <summary>
    /// Describes the necessary requirements for a shortcut. E.g. a specific tool is selected.
    /// </summary>
    [XmlType("Requirement")]
    public class ShortcutRequirement
    {
        [XmlIgnore]
        public bool IsFullfilled => true;
    }
}