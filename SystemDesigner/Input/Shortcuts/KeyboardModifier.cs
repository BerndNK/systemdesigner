﻿using System;

namespace SystemDesigner.Input.Shortcuts
{
    /// <summary>
    /// Modifier of keyboards, as flags.
    /// </summary>
    [Flags]
    public enum KeyboardModifier
    {
        None = 0,
        Shift = 1,
        Ctrl = 2,
        Alt = 4,
        Command = 8
    }
}