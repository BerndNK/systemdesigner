﻿namespace SystemDesigner.Input {
    public enum InputType {
        Pen,
        Mouse,
        Keyboard,
        Touch
    }
}