﻿using DisplayCore.Render.Geometry;

namespace SystemDesigner.Input {
    /// <summary>
    /// Describes a object which can take input of a device with Up and Down events
    /// </summary>
    public interface IUpDownInputReceiver {
        /// <summary>
        /// Event handler for when a "Down" event appears. Can be mouse down or touch down
        /// </summary>
        /// <param name="location">Location of the event appearing relative to this stage</param>
        /// <param name="inputType">The type of the input. Mouse, touch or stylus etc.</param>
        /// <param name="id">The identifier of the input event. E.g. for multiple fingers on a touch event.</param>
        /// <returns>True if the input was used (and should not be used for anything else) otherwise false.</returns>
        bool? OnInputDown(Point location, InputType inputType, int id);

        /// <summary>
        /// Event handler for when a "Up" event appears. Can be mouse down or touch down
        /// </summary>
        /// <param name="location">Location of the event appearing relative to this stage</param>
        /// <param name="inputType">The type of the input. Mouse, touch or stylus etc.</param>
        /// <param name="id">The identifier of the input event. E.g. for multiple fingers on a touch event.</param>
        /// <returns>True if the input was used (and should not be used for anything else) otherwise false.</returns>
        bool? OnInputUp(Point location, InputType inputType, int id);
    }
}
