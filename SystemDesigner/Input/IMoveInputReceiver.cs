﻿using DisplayCore.Render.Geometry;

namespace SystemDesigner.Input {
    /// <summary>
    /// Describes a object which can take input of a device which can be moved around the screen
    /// </summary>
    public interface IMoveInputReceiver {
        /// <summary>
        /// Event Handler for a input device moving (like a finger on the screen, stylus or mouse cursor)
        /// </summary>
        /// <param name="location">Location of the cursor relative to this stage</param>
        /// <param name="inputType">The type of the input. Mouse, touch or stylus etc.</param>
        /// <param name="id">The identifier of the input event. E.g. for multiple fingers on a touch event.</param>
        /// <returns>True if the input was used (and should not be used for anything else) otherwise false.</returns>
        bool? OnInputMove(Point location, InputType inputType, int id);
    }
}
