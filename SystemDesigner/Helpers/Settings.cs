// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace SystemDesigner.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings {
            get {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants
        
        private const string ShortcutsKey = "shortcuts";
        private const string OpenDocumentsLastSessionKey = "openDocsLastSession";
        private static readonly string OpenDocumentsLastSessionDefault = string.Empty;
        private static readonly string ShortcutsDefault = string.Empty;

        #endregion

        /// <summary>
        /// Gets or sets the shortcuts usually serialized in XML.
        /// </summary>
        public static string Shortcuts {
            get {
                return AppSettings.GetValueOrDefault(ShortcutsKey, ShortcutsDefault);
            }
            set {
                AppSettings.AddOrUpdateValue(ShortcutsKey, value);
            }
        }

        /// <summary>
        /// Gets or sets the open documents from last session.
        /// </summary>
        public static string OpenDocumentsLastSession {
            get { return AppSettings.GetValueOrDefault(OpenDocumentsLastSessionKey, OpenDocumentsLastSessionDefault); }
            set { AppSettings.AddOrUpdateValue(OpenDocumentsLastSessionKey, value); }
        }


    }
}