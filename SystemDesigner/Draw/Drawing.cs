﻿using System.Collections.ObjectModel;
using System.Xml.Serialization;
using DisplayCore.Render.Geometry;

namespace SystemDesigner.Draw {
    [XmlType(TypeName = "Drawing")]
    public class Drawing {
        [XmlArray("Points")]
        [XmlArrayItem("Point", Type = typeof(Point))]
        public ObservableCollection<Point> Points { get; }

        [XmlAttribute("FileName")]
        public string FileName { get; set; }

        [XmlIgnore]
        public string ShapeName { get; set; }

        [XmlIgnore]
        public byte[] InBytes { get; set; }
        public Drawing() {
            Points = new ObservableCollection<Point>();
        }
    }
}