<?php
    /*
    * Part of the Save a MovieClip as a PNG Example
    */
    
    class Avatar
    {
        //	Where will the PNGs be saved? This directory must be writeable by the web server
		var $output_dir = "/usr/www/users/path/to/website/";
        
        //  Gets a ByteArray containing a valid PNG file
        function saveImage($bytearray, $compressed = true)
        {
            if (!file_exists($this->output_dir) || !is_writeable($this->output_dir))
            {
                error_log("Please create a 'temp' directory first with write access");
            }

            $data = $bytearray->data;
            
            if ($compressed)
            {
                if (function_exists(gzuncompress))
                {
                    $data = gzuncompress($data);
                }
                else
                {
                    error_log("gzuncompress method does not exists, please send uncompressed data");
                }
            }
			
			//	This just sets a random filename, you may want to do something more useful like tie it in with a user ID
			$filename = uniqid() . ".png";
            
            $result = file_put_contents($this->output_dir . $filename, $data);
            
			//	Optional - some server set-ups don't require this, and depending on your web server this may be a security risk.
			@chmod($this->output_dir . $filename, 0777);
            
			//	$result will be false on failure to write the file, otherwise it will contain the number of bytes written
            if ($result !== false)
            {
                return array("success" => true, "filename" => $filename);
            }
            else
            {
                return array("success" => false);
            }
        }
    }
?>