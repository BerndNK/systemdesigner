﻿package  {
	
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.utils.ByteArray;
	
	
	public class SplashScreenExport extends MovieClip {
				
		public function SplashScreenExport() {
			var exporter : saveAsPNG = new saveAsPNG();
			// https://developer.apple.com/library/content/qa/qa1686/_index.html
			// Table 1
			exporter.createFile(new IconWrapper(), "iOS\\PureIcon.png", 160);
			exporter.createFile(new IconWrapper(), "iOS\\PureIcon@2.png", 160*2);
			exporter.createFile(new IconWrapper(), "iOS\\PureIcon@3.png", 160*3);
			exporter.createFile(new IconWrapper(), "iOS\\PureIcon@4.png", 160*4);
			exporter.createFile(new IconWrapper(), "iOS\\PureIcon@5.png", 160*5)
			exporter.createFile(new IconWrapper(), "iOS\\PureIcon@6.png", 160*6)
			
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Splash.png", 320, 480);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Splash@2.png", 640, 960);
			exporter.createFile(new SystemDesignerLogoWrapper169(), "iOS\\Splash1336.png", 640, 1136);
			exporter.createFile(new SystemDesignerLogoWrapper169(), "iOS\\Splash47.png", 750, 1334);
			exporter.createFile(new SystemDesignerLogoWrapper169(), "iOS\\Splash57.png", 1242, 2208);
			exporter.createFile(new SystemDesignerLogoWrapperiPad(), "iOS\\SplashiPad.png", 768, 1024);
			exporter.createFile(new SystemDesignerLogoWrapperHorizontaliPad(), "iOS\\SplashiPadHorizontal.png", 1024, 768);
			exporter.createFile(new SystemDesignerLogoWrapperiPad(), "iOS\\SplashiPad@2.png", 1536, 2048);
			exporter.createFile(new SystemDesignerLogoWrapperHorizontaliPad(), "iOS\\SplashiPadHorizontal@2.png", 2048, 1536);
			
			exporter.createFile(new SystemDesignerLogoWrapperiPad(), "iOS\\_SplashiPad.png", 768, 1004);
			exporter.createFile(new SystemDesignerLogoWrapperHorizontaliPad(), "iOS\\_SplashiPadHorizontal.png", 1024, 748);
			exporter.createFile(new SystemDesignerLogoWrapperiPad(), "iOS\\_SplashiPad@2.png", 1536, 2008);
			exporter.createFile(new SystemDesignerLogoWrapperHorizontaliPad(), "iOS\\_SplashiPadHorizontal@2.png", 2048, 1496);
		}
		
	}
	
}
