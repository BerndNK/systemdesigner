﻿package  {
	
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.utils.ByteArray;
	
	
	public class LogoBatchExport extends MovieClip {
				
		public function LogoBatchExport() {
			var exporter : saveAsPNG = new saveAsPNG();
			// https://developer.apple.com/library/content/qa/qa1686/_index.html
			// Table 1
			exporter.createFile(new SystemDesignerLogoWrapper(), "windows\\Icon16.png", 16);
			exporter.createFile(new SystemDesignerLogoWrapper(), "windows\\Icon24.png", 24);
			exporter.createFile(new SystemDesignerLogoWrapper(), "windows\\Icon32.png", 32);
			exporter.createFile(new SystemDesignerLogoWrapper(), "windows\\Icon64.png", 64);
			exporter.createFile(new SystemDesignerLogoWrapper(), "windows\\Icon128.png", 128);
			exporter.createFile(new SystemDesignerLogoWrapper(), "windows\\Icon512.png", 512);
			exporter.createFile(new SystemDesignerLogoWrapper(), "windows\\Icon1024.png", 1024);
			exporter.createFile(new SystemDesignerLogoWrapper(), "windows\\Icon2048.png", 2048);
			exporter.createFile(new SystemDesignerLogoWrapper(), "windows\\Icon.png", 256);
			
			exporter.createFile(new SystemDesignerLogoWrapperWhite(), "windows\\WhiteIcon16.png", 16);
			exporter.createFile(new SystemDesignerLogoWrapperWhite(), "windows\\WhiteIcon24.png", 24);
			exporter.createFile(new SystemDesignerLogoWrapperWhite(), "windows\\WhiteIcon32.png", 32);
			exporter.createFile(new SystemDesignerLogoWrapperWhite(), "windows\\WhiteIcon64.png", 64);
			exporter.createFile(new SystemDesignerLogoWrapperWhite(), "windows\\WhiteIcon128.png", 128);
			exporter.createFile(new SystemDesignerLogoWrapperWhite(), "windows\\WhiteIcon.png", 256);
			
			exporter.createFile(new SystemDesignerLogoWrapperBlack(), "windows\\BlackIcon16.png", 16);
			exporter.createFile(new SystemDesignerLogoWrapperBlack(), "windows\\BlackIcon24.png", 24);
			exporter.createFile(new SystemDesignerLogoWrapperBlack(), "windows\\BlackIcon32.png", 32);
			exporter.createFile(new SystemDesignerLogoWrapperBlack(), "windows\\BlackIcon64.png", 64);
			exporter.createFile(new SystemDesignerLogoWrapperBlack(), "windows\\BlackIcon128.png", 128);
			exporter.createFile(new SystemDesignerLogoWrapperBlack(), "windows\\BlackIcon.png", 256);
		}
		
	}
	
}
