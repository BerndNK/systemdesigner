﻿package  {
	
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.utils.ByteArray;
	
	
	public class AppBatchExport extends MovieClip {
				
		public function AppBatchExport() {
			var exporter : saveAsPNG = new saveAsPNG();
			// https://developer.apple.com/library/content/qa/qa1686/_index.html
			// Table 1
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\TunesArtwork.png", 512);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\iTunesArtwork@2x.png", 1024);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Icon-60@2x.png", 120);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Icon-60@3x.png", 180);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Icon-76.png", 76);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Icon-76@2x.png", 152);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Icon-Small-40.png", 40);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Icon-Small-40@2x.png", 80);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Icon-Small-40@3x.png", 120);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Icon-Small.png", 29);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Icon-Small@2x.png", 58);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\Icon-Small@3x.png", 87);
			exporter.createFile(new SystemDesignerLogoWrapper(), "iOS\\1536.png", 1536);
		}
		
	}
	
}
