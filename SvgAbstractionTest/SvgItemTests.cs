﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms;
using SvgAbstraction.Svg.Forms.Container;


namespace SvgAbstractionTest {

    [TestClass]
    public class SvgItemTests {

        [TestMethod]
        public void SvgItem_AmountOfParents_Success() {
            // arrange
            var group1 = new SvgGroup();
            var group2 = new SvgGroup();
            var group3 = new SvgGroup();

            var rect = new SvgRect();

            // act
            group1.Add(group2);
            group2.Add(group3);

            group3.Add(rect);

            // assert
            Assert.AreEqual(3, rect.AmountOfParents);
        }

        [TestMethod]
        public void SvgItemSvgGroup_RectToXml_Success() {
            // arrange
            var group1 = new SvgGroup() {
                X = 50,
                Y = 50,
                ScaleX = 2,
                ScaleY = 2
            };
            var rect = new SvgRect(100, 100) {
                X = 50,
                Y = 50,
                ScaleX = 1.5,
                ScaleY = 2.5
            };

            group1.Add(rect);

            // act
            var xml = group1.ToXml();

            // assert
            var pure = xml.Replace("\n", "");
            pure = pure.Replace("\r", "");
            pure = pure.Replace("\t", "");
            pure = pure.ToLower();

            const string expected = "<g transform=\"translate(50, 50) matrix(2.00, 0, 0, 2.00, 0.00, 0.00)\"><rect x=\"50.00\" y=\"50.00\" width=\"100.00\" height=\"100.00\" transform=\"matrix(1.50, 0, 0, 2.50, -25.00, -75.00)\"/></g>";
            Assert.AreEqual(expected, pure);
        }

        [TestMethod]
        public void SvgItemSvgGroup_CircleToXml_Success() {
            // arrange
            var group1 = new SvgGroup {
                X = 50,
                Y = 50,
                ScaleX = 2,
                ScaleY = 2
            };
            var circle = new SvgEllipse(50) {
                X = 0,
                Y = 0,
                ScaleX = 1.25,
                ScaleY = 2.5
            };

            group1.Add(circle);

            // act
            var xml = group1.ToXml();

            // assert
            var pure = xml.Replace("\n", "");
            pure = pure.Replace("\r", "");
            pure = pure.Replace("\t", "");
            pure = pure.ToLower();

            const string expected = "<g transform=\"translate(50, 50) matrix(2.00, 0, 0, 2.00, 0.00, 0.00)\"><circle cx=\"100.00\" cy=\"100.00\" r=\"50.00\" transform=\"matrix(1.25, 0, 0, 2.50, -62.50, -125.00)\"/></g>";
            Assert.AreEqual(expected, pure);
        }

        [TestMethod]
        public void SvgRoot_ToXml_Success() {
            // arrange
            var root = new SvgRoot {Xmlns = "test"};

            // act
            var xml = root.ToXml();

            // assert
            const string expected = "<svg xmlns=\"test\"/>";
            Assert.AreEqual(expected, xml);
        }
    }
}
