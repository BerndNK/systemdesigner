﻿using System;
using System.Collections.Generic;
using DisplayCore.Render;
using DisplayCore.Render.Geometry;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms;
using SvgAbstraction.Svg.Forms.Container;

namespace SvgAbstractionTest {
    [TestClass]
    public class SvgItemContainerTests {

        [TestMethod]
        public void SvgGroup_RemoveAndAddChilds_Success() {
            // use Contains, GetChildAt, GetChildIndex and NumChildren as assertion
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();

            // act
            group.Add(child1);
            group.Add(child2);
            // after both get added at index 0, the array should be [child1, child2]

            // assert
            Assert.IsTrue(group.Contains(child1), "Contains method returned false. Child may not have been added");
            Assert.IsTrue(group.Contains(child2), "Contains method returned false. Child may not have been added");
            Assert.AreSame(child1, group.GetChildAt(0), "First added child is not correct");
            Assert.AreSame(child2, group.GetChildAt(1), "Second added child is not correct");
            Assert.AreEqual(0, group.GetChildIndex(child1), "GetChildIndex method did not function as expected");
            Assert.AreEqual(1, group.GetChildIndex(child2), "GetChildIndex method did not function as expected");
            Assert.AreEqual(2, group.NumChildren, "NumChildren is not correct");


            // act
            group.Remove(child1); // now array should be [child2]

            // assert
            Assert.IsFalse(group.Contains(child1), "Contains method returned false. Child may not have been added");
            Assert.IsTrue(group.Contains(child2), "Contains method returned false. Child may not have been added");
            Assert.AreSame(child2, group.GetChildAt(0), "First added child is not correct");
            Assert.AreEqual(-1, group.GetChildIndex(child1), "GetChildIndex method did not function as expected");
            Assert.AreEqual(0, group.GetChildIndex(child2), "GetChildIndex method did not function as expected");
            Assert.AreEqual(1, group.NumChildren, "NumChildren is not correct");

            // act
            group.Add(child1);
            group.Remove(child1);
            group.Remove(child2); // // now array should be [] (empty)

            // assert
            Assert.IsFalse(group.Contains(child1), "Contains method returned false. Child may not have been added");
            Assert.IsFalse(group.Contains(child2), "Contains method returned false. Child may not have been added");
            Assert.AreEqual(-1, group.GetChildIndex(child1), "GetChildIndex method did not function as expected");
            Assert.AreEqual(-1, group.GetChildIndex(child2), "GetChildIndex method did not function as expected");
            Assert.AreEqual(0, group.NumChildren, "NumChildren is not correct");
        }

        [TestMethod]
        public void SvgGroup_RemoveAndAddChildsViaIndex_Success() {
            // use Contains, GetChildAt, GetChildIndex and NumChildren as assertion
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();

            // act
            group.AddAt(child1, 0);
            group.AddAt(child2, 0);
            // after both get added at index 0, the array should be [child2, child1]

            // assert
            Assert.IsTrue(group.Contains(child1), "Contains method returned false. Child may not have been added");
            Assert.IsTrue(group.Contains(child2), "Contains method returned false. Child may not have been added");
            Assert.AreSame(child2, group.GetChildAt(0), "First added child is not correct");
            Assert.AreSame(child1, group.GetChildAt(1), "Second added child is not correct");
            Assert.AreEqual(0, group.GetChildIndex(child2), "GetChildIndex method did not function as expected");
            Assert.AreEqual(1, group.GetChildIndex(child1), "GetChildIndex method did not function as expected");
            Assert.AreEqual(2, group.NumChildren, "NumChildren is not correct");

            // act
            group.RemoveAt(1); // now array should be [child2]

            // assert
            Assert.IsFalse(group.Contains(child1), "Contains method returned false. Child may not have been added");
            Assert.IsTrue(group.Contains(child2), "Contains method returned false. Child may not have been added");
            Assert.AreSame(child2, group.GetChildAt(0), "First added child is not correct");
            Assert.AreEqual(-1, group.GetChildIndex(child1), "GetChildIndex method did not function as expected");
            Assert.AreEqual(0, group.GetChildIndex(child2), "GetChildIndex method did not function as expected");
            Assert.AreEqual(1, group.NumChildren, "NumChildren is not correct");

            // act
            group.AddAt(child1, 1); // now array should be [child2, child1]
            group.Remove(child1);
            group.Remove(child2); // now array should be [] (empty)

            // assert
            Assert.IsFalse(group.Contains(child1), "Contains method returned false. Child may not have been added");
            Assert.IsFalse(group.Contains(child2), "Contains method returned false. Child may not have been added");
            Assert.AreEqual(-1, group.GetChildIndex(child1), "GetChildIndex method did not function as expected");
            Assert.AreEqual(-1, group.GetChildIndex(child2), "GetChildIndex method did not function as expected");
            Assert.AreEqual(0, group.NumChildren, "NumChildren is not correct");
        }

        [TestMethod]
        public void SvgGroup_AddMultipleChildsAtOnce_Success() {
            // use Contains, GetChildAt, GetChildIndex and NumChildren as assertion
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();
            var child3 = new SvgRect();

            var list = new List<SvgRect> { child1, child2, child3 };

            // act
            group.AddMultiple(list); // array should be same as list [child1, child2, child3]

            // assert
            Assert.IsTrue(group.Contains(child1), "Contains method returned false. Child may not have been added");
            Assert.IsTrue(group.Contains(child2), "Contains method returned false. Child may not have been added");
            Assert.IsTrue(group.Contains(child3), "Contains method returned false. Child may not have been added");
            Assert.AreSame(child1, group.GetChildAt(0), "First added child is not correct");
            Assert.AreSame(child2, group.GetChildAt(1), "Second added child is not correct");
            Assert.AreSame(child3, group.GetChildAt(2), "Third added child is not correct");
            Assert.AreEqual(0, group.GetChildIndex(child1), "GetChildIndex method did not function as expected");
            Assert.AreEqual(1, group.GetChildIndex(child2), "GetChildIndex method did not function as expected");
            Assert.AreEqual(2, group.GetChildIndex(child3), "GetChildIndex method did not function as expected");
            Assert.AreEqual(3, group.NumChildren, "NumChildren is not correct");
        }

        [TestMethod]
        public void SvgGroup_SwapChildren_Success() {
            // use Contains, GetChildAt, GetChildIndex and NumChildren as assertion
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();
            var child3 = new SvgRect();

            var list = new List<SvgRect> { child1, child2, child3 };

            // act
            group.AddMultiple(list); // array should be same as list [child1, child2, child3]
            group.SwapChildren(child1, child1); // [child1, child2, child3]
            group.SwapChildren(child1, child3); // [child3, child2, child1]
            group.SwapChildren(child1, child3);  // [child1, child2, child3]
            group.SwapChildren(child2, child1);  // [child2, child1, child3]
            group.SwapChildren(child2, child3); // [child3, child1, child2]

            // assert
            Assert.AreEqual(0, group.GetChildIndex(child3));
            Assert.AreEqual(1, group.GetChildIndex(child1));
            Assert.AreEqual(2, group.GetChildIndex(child2));
        }

        [TestMethod]
        public void SvgGroup_SwapChildrenViaIndex_Success() {
            // use Contains, GetChildAt, GetChildIndex and NumChildren as assertion

            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();
            var child3 = new SvgRect();

            var list = new List<SvgRect> { child1, child2, child3 };

            // act
            group.AddMultiple(list); // array should be same as list [child1, child2, child3]
            group.SwapChildrenAt(0, 0); // [child1, child2, child3]
            group.SwapChildrenAt(0, 2); // [child3, child2, child1]
            group.SwapChildrenAt(0, 1); // [child2, child3, child1]
            group.SwapChildrenAt(1, 2); // [child2, child1, child3]

            // assert
            Assert.AreEqual(0, group.GetChildIndex(child2));
            Assert.AreEqual(1, group.GetChildIndex(child1));
            Assert.AreEqual(2, group.GetChildIndex(child3));
        }

        [TestMethod]
        public void SvgGroup_SetChildIndex_Success() {
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();
            var child3 = new SvgRect();

            var list = new List<SvgRect> { child1, child2, child3 };

            // act
            group.AddMultiple(list); // array should be same as list [child1, child2, child3]
            group.SetChildIndex(child3, 0); // [child3, child1, child2]
            group.SetChildIndex(child2, 1); // [child3, child2, child1]
            group.SetChildIndex(child3, 2); // [child1, child2, child3]

            // assert
            Assert.AreEqual(0, group.GetChildIndex(child1));
            Assert.AreEqual(1, group.GetChildIndex(child2));
            Assert.AreEqual(2, group.GetChildIndex(child3));
        }

        [TestMethod]
        public void SvgGroup_Enumerator_Success() {
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();
            var child3 = new SvgRect();

            var list = new List<SvgRect> { child1, child2, child3 };
            group.AddMultiple(list);

            var i = 0;
            foreach (var child in group) {
                Assert.AreSame(child, group.GetChildAt(i));
                ++i;
            }
        }

        [TestMethod]
        public void SvgGroup_HitTest_Success() {

            // assert middle values, boundaries, HitTestPoint/Rect etc.
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect(100, 100);
            var child2 = new SvgRect(100, 100) {
                X = 50,
                Y = 50
            };
            group.AddMultiple(new List<SvgItem> { child1, child2 });

            // assert
            Assert.IsTrue(group.HitTestRect(new Rectangle(20, 75, 5, 30)), "Hit test failed unexpected");
            Assert.IsTrue(group.HitTestRect(new Rectangle(20, 120, 50, 5)), "Hit test failed unexpected");
            Assert.IsTrue(group.HitTestPoint(25, 25), "Hit test failed unexpected");
            Assert.IsTrue(group.HitTestPoint(125, 125), "Hit test failed unexpected");

            Assert.IsFalse(group.HitTestRect(new Rectangle(-20, 75, 5, 30)), "Hit test succeeded unexpected");
            Assert.IsFalse(group.HitTestRect(new Rectangle(20, 250, 50, 5)), "Hit test succeeded unexpected");
            Assert.IsFalse(group.HitTestPoint(-25, 25), "Hit test succeeded unexpected");
            Assert.IsFalse(group.HitTestPoint(151, 125), "Hit test succeeded unexpected");
        }


        [TestMethod]
        public void SvgGroup_Boundaries_Success() {

            // assert middle values, boundaries, HitTestPoint/Rect etc.
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect(100, 100);
            var child2 = new SvgRect(100, 100) {
                X = 50,
                Y = 50
            };
            group.AddMultiple(new List<SvgItem> { child1, child2 });

            // assert
            Assert.AreEqual(0, group.LeftBoundary, 0.1, "Left Boundary is not correct");
            Assert.AreEqual(0, group.TopBoundary, 0.1, "Top Boundary is not correct");
            Assert.AreEqual(150, group.RightBoundary, 0.1, "Right Boundary is not correct");
            Assert.AreEqual(150, group.BottomBoundary, 0.1, "Bottom Boundary is not correct");
            Assert.AreEqual(75, group.MiddleX, 0.1, "Middle X is not correct");
            Assert.AreEqual(75, group.MiddleY, 0.1, "Middle Y is not correct");
        }


        [TestMethod]
        public void SvgGroup_ChangeScale_Success() {
            // assert middle values, boundaries, HitTestPoint/Rect etc.
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect(100, 100);
            var child2 = new SvgRect(100, 100) {
                X = 50,
                Y = 50
            };
            group.AddMultiple(new List<SvgItem> { child1, child2 });

            // act
            group.ScaleX = 1.5;
            group.ScaleY = 1.5;

            // assert
            Assert.AreEqual(0, group.LeftBoundary, 0.1, "Left Boundary is not correct");
            Assert.AreEqual(0, group.TopBoundary, 0.1, "Top Boundary is not correct");
            Assert.AreEqual(150 * 1.5, group.RightBoundary, 0.1, "Right Boundary is not correct");
            Assert.AreEqual(150 * 1.5, group.BottomBoundary, 0.1, "Bottom Boundary is not correct");
            Assert.AreEqual(75 * 1.5, group.MiddleX, 0.1, "Middle X is not correct");
            Assert.AreEqual(75 * 1.5, group.MiddleY, 0.1, "Middle Y is not correct");
            Assert.IsTrue(group.HitTestRect(new Rectangle(20, 75, 5, 30)), "Hit test failed unexpected");
            Assert.IsTrue(group.HitTestRect(new Rectangle(20, 120, 50, 5)), "Hit test failed unexpected");
            Assert.IsTrue(group.HitTestPoint(25, 25), "Hit test failed unexpected");
            Assert.IsTrue(group.HitTestPoint(125, 125), "Hit test failed unexpected");

            Assert.IsFalse(group.HitTestRect(new Rectangle(-20, 75, 5, 30)), "Hit test succeeded unexpected");
            Assert.IsFalse(group.HitTestRect(new Rectangle(20, 250, 50, 5)), "Hit test succeeded unexpected");
            Assert.IsFalse(group.HitTestPoint(-25, 25), "Hit test succeeded unexpected");
            Assert.IsFalse(group.HitTestPoint(226, 125), "Hit test succeeded unexpected");
        }

        [TestMethod]
        public void SvgGroup_ChangeWidthAndHeight_Success() {
            // assert middle values, boundaries, HitTestPoint/Rect etc.
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect(100, 100);
            var child2 = new SvgRect(100, 100) {
                X = 200
            };
            group.AddMultiple(new List<SvgItem> { child1, child2 });

            // act
            group.Width = 600; // same as scaleX = 2
            group.Height = 600; // same as scaleY = 6

            // assert
            Assert.AreEqual(0, group.LeftBoundary, 0.1, "Left Boundary is not correct");
            Assert.AreEqual(0, group.TopBoundary, 0.1, "Top Boundary is not correct");
            Assert.AreEqual(600, group.RightBoundary, 0.1, "Right Boundary is not correct");
            Assert.AreEqual(600, group.BottomBoundary, 0.1, "Bottom Boundary is not correct");
            Assert.AreEqual(300, group.MiddleX, 0.1, "Middle X is not correct");
            Assert.AreEqual(300, group.MiddleY, 0.1, "Middle Y is not correct");

            Assert.IsTrue(group.HitTestRect(new Rectangle(199, 0, 102, 1)), "Hit test failed unexpected");

            Assert.IsFalse(group.HitTestRect(new Rectangle(201, 0, 198, 5)), "Hit test succeeded unexpected");

            Assert.AreEqual(2, group.ScaleX, 0.1, "Resulting scale was not as expected");
            Assert.AreEqual(6, group.ScaleY, 0.1, "Resulting scale was not as expected");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SvgGroup_AddItemWithAnotherParent_ExceptionThrown() {
            // arrange
            var group1 = new SvgGroup();
            var group2 = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();

            // act
            group1.Add(child1);
            group2.Add(child2);

            group1.Add(child2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SvgGroup_SwapChildrenWithInvalidPartner_ExceptionThrown() {
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();
            var child3 = new SvgRect();

            group.AddMultiple(new List<SvgItem> { child1, child2 });

            // act
            group.SwapChildren(child1, child3);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void SvgGroup_SwapChildrenWithInvalidIndex_ExceptionThrown() {
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();

            group.AddMultiple(new List<SvgItem> { child1, child2 });

            // act
            group.SwapChildrenAt(0, 1);
            group.SwapChildrenAt(0, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SvgGroup_AddMultipleChildsAtOnceWithOneInvalid_ExceptionThrown() {
            // arrange
            var group1 = new SvgGroup();
            var group2 = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();
            var child3 = new SvgRect();

            group2.Add(child3);

            // act
            group1.AddMultiple(new List<SvgItem> { child1, child2, child3 });
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void SvgGroup_SetChildIndices_ExceptionThrown() {
            // set indices a couple of times correctly then one false
            // arrange
            var group = new SvgGroup();
            var child1 = new SvgRect();
            var child2 = new SvgRect();
            var child3 = new SvgRect();

            group.AddMultiple(new List<SvgItem> { child1, child2, child3 });

            // act
            group.SwapChildrenAt(0, 2);
            group.SwapChildrenAt(0, 1);
            group.SwapChildrenAt(2, 0);
            group.SwapChildrenAt(3, 1);

        }

        // Render


        [TestMethod]
        public void SvgRect_RenderTest_Success() {
            // arrange
            var rect = new SvgRect(100, 100) {
                X = 25,
                Y = 25
            };
            var group = new SvgGroup();
            group.Add(rect);

            rect.ScaleX = 1.5;
            rect.ScaleY = 1.5;

            group.X = 25;
            group.Y = 25;
            group.ScaleX = 2;
            group.ScaleY = 2;
            
            var renderHandler = new RendererHandler();
            var mock = new Mock<IRenderer>();

            renderHandler.Renderer = mock.Object;


            // act
            renderHandler.Render(group);

            // assert
            mock.Verify(x => x.RenderBegin(), Times.Once);
            mock.Verify(x => x.RenderEnd(), Times.Once);
            // with the scale the X/Y position of the group do NOT change. However the position of the rect does (multiply by two = 25*2 = 50)
            // If the parent scales, the scale gets multiplied for all its children. So the scale of the rect will be 1.5 * 2 = 3 => 100*3 = 300
            mock.Verify(x => x.Render(rect, It.Is<Rectangle>(y => y.ToString() == "X: 75 Y: 75 W: 300 H: 300"), 0, It.Is<Point>(p => p.X <= 0.01 && p.Y <= 0.01), It.Is<Rectangle>(y => y.ToString() == "X: 75 Y: 75 W: 300 H: 300")), Times.Once); // check parameter with string, since rectangle uses dynamic which is not allowed in a tree expression
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void SvgGroup_AddContainerAsItsOwnChild_ExceptionThrown() {
            // arrange
            var group = new SvgGroup();

            // act
            group.Add(group);
        }
    }
}
