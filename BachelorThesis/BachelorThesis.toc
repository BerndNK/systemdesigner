\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {UKenglish}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline List of Figures}{4}{chapter*.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Acronyms}{5}{chapter*.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{6}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Technology}{6}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Scope and goals}{7}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}Shape recognition and machine learning}{8}{subsection.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}Requirements Engineering}{8}{subsection.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.3}Implementation}{8}{subsection.1.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Machine Learning}{9}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Types of machine learning}{9}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.1}Supervised machine learning}{10}{subsection.2.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Practical Example}{11}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Neural Networks}{13}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Types}{14}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Perceptrons}{14}{subsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}Sigmoid function}{16}{subsection.2.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Gradient descent}{18}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Shape Recognition}{21}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Mapping the problem}{21}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Finding a good layout}{23}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Finding good bias and weights}{25}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Back propagation}{25}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Training results}{26}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Requirements Engineering}{29}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Overview}{29}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Tools}{29}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Tool options}{29}{section*.12}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Tool modifier}{30}{section*.13}
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\nonumberline Tool function}{30}{section*.14}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Implementation}{34}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Progress}{34}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Rendering}{34}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}iOS Rotation}{36}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Application Frame}{37}{section.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.5}Shortcuts}{38}{section.5.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.6}Shape drawer}{39}{section.5.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Conclusion}{41}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6.1}Outlook}{41}{section.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Bibliography}{42}{chapter*.15}
