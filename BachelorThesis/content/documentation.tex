\chapter{Requirements Engineering}
This chapter describes the features of System Designer and gives an overview to the structure, as well as giving a couple of interesting examples.
\section{Overview}
The entirety of features can be split into two categories:
\begin{itemize}
	\item Application Frame
	\item Tools
\end{itemize}
The ``Application Frame'' is the base program providing the foundation for the tools. This does include \ac{OS} specific and non specific code. Covering file and document\footnote{A document in this context, means a System Designer file, which is currently opened in the application} management, input (mouse, touch, keyboard, stylus) and rendering.\\

The ``Tools'' however implement only independent code and contain most of System Designer's functionality. Each tool renders its \ac{UI} with the intern display object structure and therefore doesn't need platform specific \ac{UI} frameworks.
\section{Tools}
The entire platform independent functionality is realized in virtual tools. These may be compared to real life tools. For specific tasks, there are specific tools. To force a nail through something, a hammer is for. To split pieces of wood, an ax is helpful. To create a hole, use a drill.\\

Similar to this, the tools in System Designer may be orientated. To draw a rectangle, use the rectangle drawing tool. For selecting and transforming items, use the select and transform tool and so on. With this approach, however, the amount of tools would rise linear to the features the application has. Therefore, to solve this problem, multiple possible actions and functions are clustered and for each of these cluster, a tool is created. This is done by grouping items/functionalities, which are relevant to each other. To realize this, tools have so called tool properties: Options, modifier and functions.\\
\paragraph{Tool options}
The drawing of shapes, as example. The idea for each shape is the same: Drag and drop on the screen to indicate the size. On release, fit the desired shape in a square given by the start and end point of the drag and drop operation. So it makes sense to create a tool, which implements this functionality - that always works the same - but with the addition of an ability to select the shape that shall appear. In System Designer this is called a tool option. A semi permanent setting that alters the behavior of a tool. The rotation speed of a drill, as another example.\\

\paragraph{Tool modifier} Suppose a selection tool. The user may click on a shape to select it. However occasionally two or more shapes shall be selected. This means two types of functionality. Click to make a single shape the whole selection and click to add or remove a shape to the selection group. With a tool option this can be solved, however options are meant to be semi-permanent and are not designed to be switched on and off in a matter of seconds. Which is something a user might do with the select tool. Therefore another category of tool property is necessary. In System Designer, this is called a tool modifier.\\

Tool modifier act in a similar way as tool options, but are designed to be more temporarily. They are practically meant to be activated by the press and release of a button. For example, only when holding the shift key on a keyboard, the select tool will add the clicked shape to the selection.\\

\paragraph{Tool function} The third and last tool property is the tool function. These are special methods that act only in the context of a tool. For example a tool function that aligns the selected items in the center of the screen. This only makes sense in the context of the select tool, since other tools don't have a selection.\\

To model these features of each tool in \ac{UML}, a use case diagram is used. However, the three mentioned tool properties are realized as actors:\\

\begin{figure}[h]
	\centering 
	\includegraphics[width=0.95\textwidth]{resources/ToolAbstract.png}
	\caption{Tool properties abstracted into actors}
	\label{fig:toolAbstract}
\end{figure}
Figure \ref{fig:toolAbstract} shows the three tool properties as use case actors. Note that below each are comments that quickly summarize the purpose again. With this approach it is possible to model the whole functionality of a tool (which again is a feature cluster) into a single use case diagram. The free hand tool for example. This tool uses the technology described in chapter two and three. The user can draw a crude shape on the screen, a neural network tries to recognize the shape and the tool converts it into a perfect form.\\

Seen in figure \ref{fig:freeHandUseCase} are five use case actors. The user, System Designer and the three tool properties. Note that this approach breaks the intended usage of the use case diagram, but was nonetheless chosen as it provides a good way to model the requirements.\\

\begin{figure}[h]
	\centering 
	\includegraphics[width=\textwidth]{resources/FreeHandTool.png}
	\caption{Use case diagram of free hand tool}
	\label{fig:freeHandUseCase}
\end{figure}

The only use case for the tool modifier actor is the toggling of line and shape detection. This decides which of the two extending use cases are performed. The user may draw a line (which can be a shape) on the screen. This is the only use case for the user, extended by two more use cases, which are performed by System Designer. Either the tool modifier is enabled, then the system merely draws a straight line from the beginning to the ending of the user's stroke, or the modified is disabled and it tries to recognize and convert the shape.\\

This behavior on the other hand, can be altered by the tool option use case. It describes, that it's possible to activate and deactivate the recognition of certain shapes. If the user may only want to draw rectangles, but is extraordinarily bad at drawing, for example. Then System Designer may sometimes falsely recognize the drawings as circles instead of rectangles. In that case the user can disable the recognition of circles entirely.\\


Another, more complex example is the select tool. Seen in figure \ref{fig:selectionToolUseCase}. The use cases of the user are again extended by use cases of tool properties. On this tool - when the modifier is enabled - the use case gets extended, otherwise it won't. Notice that this tool has a lot of tool functions, as the selection of one or multiple shapes is a necessary action for a lot of features. These include the creation of groups, alignment methods, copy and pasting and arranging the display order.\\ 

A second use case of the user ``Transform selected items'' is extended by a tool option. In this case this makes sense, as the modification ``transformation as grouped item'' is usually not as rapidly changed. This illustrates clearly, that modifiers and options are essentially the same, but already give a recommendation about the design and implementation of the feature.\\

The full modeling project can be viewed in the System Designer repository. \footcite{BitBucket}.\\

\begin{figure}[h]
	\centering 
	\includegraphics[width=0.95\textwidth]{resources/SelectionToolUseCase.png}
	\caption{Use case diagram of selection tool}
	\label{fig:selectionToolUseCase}
\end{figure}
This whole system of tool modifiers, options and functions are implemented \ac{OS} independently. Unlike the ``Application Frame'', which is special for each platform, the \ac{UI} for each of the tool properties is also implemented independently. Instead of using \ac{UI} frameworks of the specific environment, the System Designer rendering tree is used. Which means that the interface is also generated and managed by the tool itself. This is easier to explain by looking at the mock up for the touch interface:\\

\begin{figure}[h]
	\centering 
	\includegraphics[width=0.65\textwidth]{resources/TouchUiMockup.png}
	\caption{Touch user interface mock up}
	\label{fig:uiMockup}
\end{figure}
Seen in this figure \ref{fig:uiMockup} is a sketch of a possible user interface for the select tool. Suppose the black rectangle in the middle is selected. To realize the ``transform'' use case, the buttons surrounding the rectangle are rendered. Allowing the movement, rotation and stretching of the object.\\

Additionally, since not every device may have a keyboard attached, the buttons on the lower left corner of the screen are added. These represent the tool modifier. As long as they are pressed (with a second finger), the modifier is active. All of these buttons are together with the rectangle on the System Designer stage. Which means that they are rendered as part of the document. Even though they are technically not part of it. In contrast to this, the top bar represents \ac{OS} specific buttons, which may only be seen on a single platform. Note that tool options and tool functions are missing from the mock up.\\

This design has the advantage that the whole \ac{UI} for each tool must only be written once, which saves time during development and ensures the same experience of the application on each platform. So to implement each tool and most of System Designer's features, merely the rendering process and the input translation have to programmed.\\

\begin{figure}[h]
	\centering 
	\includegraphics[width=0.60\textwidth]{resources/ClassDiagramTools.png}
	\caption{Class diagram of the stage tools}
	\label{fig:classDia}
\end{figure}

Shown in figure \ref{fig:classDia} is the class diagram for the above mentioned tools. While there is a generic interface ``IStageTool'' which describes a tool for an ``IStage'', both ``FreeHandTool'' and ``SelectTool'' are specific tools for the ``SvgStage''. Note that both derive from a base class ``BaseSvgStageTool'', which is not necessary to implement above described functionality, but is useful to speed up development. \\

