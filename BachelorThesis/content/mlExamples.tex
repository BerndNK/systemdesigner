
\section{Practical Example}
To get a better understanding on how \ac{ML} works, take a look at the following example. Imagine a company let its employees take a survey on how happy they are on a scale of 0 to 100. This rating is then plotted on a graph (\ref{fig:mlEx1}) in relation to the employees monthly salary. This example is inspired by Nick McCrea. \cite{TOPTAL}
\begin{figure}[H]
	\centering 
		\begin{tikzpicture}
		\begin{axis}[
		enlargelimits=false,
		legend pos=north west,
		xlabel = Salary in 100\$/Month,
		ylabel = {Satisfaction rating in \%}
		]
		\addplot[
		only marks,
		scatter,
		xmajorgrids=true,
		ymajorgrids=true,
		mark=halfcircle*,
		mark size=2.9pt]
		table[meta=y]
		{content/mlExample01.dat};
		\end{axis}
		\end{tikzpicture}
	\caption[Salary example scatter chart\ldots]{\protect Salary example scatter chart} 
	\label{fig:mlEx1} 
\end{figure}
Looking at this figure \ref{fig:mlEx1} a clear trend can be seen. The further the points are to the right, the more they rise in height. So seemingly a relation between employee salary and satisfaction can be deduced. The more money a employee gets, the more satisfied he or she is. Before looking into more detail, note that these figures are generated. As a base value, a linear function $f_{base}$ is used: (The constants $50$ and $0.33$ are randomly chosen and have no further significance.)
	\begin{align*}
		f_{base}(x) = 50 + 0.33x \| x \in \mathbb{R}
	\end{align*}
	This results in a straight line. To make it a bit more realistic, a random number is added for each x:
	\begin{equation} \label{eq:2}
	f(x) = f_{base}(x) + \left \lfloor{R\frac{x}{7}}\right \rfloor \| R,x \in \mathbb{R}
	\end{equation}
Again the constant $7$ is arbitrarily chosen. $R$ is a random number for each $x$, evenly distributed between $[-1,1]$. Only $\{x \in [10,98] \mid x \text{ is even}\}$ are plotted.\\
The higher the value of $x$ in equation \ref{eq:2}, the more it will evenly differ from the base value of $f_{base}$, however maximal an absolute value of a seventh of $x$ for each $x$.\\
\\
From this data, we want to create a predictor (regression system), which is an algorithm that takes a monthly salary as input and outputs (predicts) a employee satisfaction rating. Suppose a general linear predictor with $n$ inputs (assuming the algorithm can be linear for this example):
\begin{align*}
p(x_{1}...x_{n}) = \theta_0 + \sum_{k=1}^{n}\theta_kx_k \| x,\theta_{0}...\theta_{k} \in \mathbb{R}
\end{align*}

With $\theta_{0}...\theta_{k}$ being constants. This will sum each input $x$ multiplied by a weight $\theta$. For this example we can simplify it down to only one input ($x$):\\
\begin{equation} \label{eq:1}
p(x) = \theta_0 + \theta_1x
\end{equation}
The goal of this \ac{ML} example is, to find the optimal value for $\theta_0 \text{ and } \theta_1$. Optimizing this function $p(x)$ can be done by using training samples (the previously collected survey data). For each $p(x_{train})$ we will receive a predicted $y_{predicted}$. However, since $x_{train}$  is from the training set, the actual, correct $y_{actual}$ is known.\\
Therefore, the difference between $y_{predicted}$ and $y_{actual}$ determines how wrong the predictor is. A total error degree of the predictor can then be formed by the sum of all differences, divided by the amount of training samples $n$ (average):
	\begin{equation} \label{eq:er}
	er = \frac{1}{n}\sum_{i = 1}^{n} |y_{i^{predicted}} - y_{i^{actual}}|
	\end{equation}
	
In mathematical terms, the goal of the \ac{ML} algorithm is to minimize $er$ (\ref{eq:er}) by modifying the constants $\theta_0$ and $\theta_1$ in $p(x)$ (\ref{eq:1}).\\\\
Multiple algorithms for this problem exist. One of the most commonly used ones is gradient descent, which is explained in more detail in the next chapter. For this example it is not necessary to understand it.\\
Intuitively, to begin the learning process the start values for $\theta_0$ and $\theta_1$ are guessed. Suppose we take $\theta_0 = 70$ and $\theta_1 = -0.4$.\\
Now since $f_{base}(x) = 50 + 0.33x$ is the base value for each point and the random number $R$ is evenly distributed, an optimal predictor would be around the two constants of the base function $\theta_0 = 50$ and $\theta_1 = 0.33$. Therefore the guessed values are obviously horribly wrong. Plotting these two functions into the graph makes this very clear:\\

\begin{figure}[H]
	\centering 
	\begin{tikzpicture}
	\begin{axis}[
	enlargelimits=false,
	legend pos=north west,
	xlabel = Salary in 100\$/Month,
	ylabel = {Satisfaction rating \%}
	]
	\addplot [
	domain=0:100, 
	samples=200, 
	color=red,
	]
	{50 + 0.33*x};
	\addlegendentry{$f_{base}(x) = 50 + 0.33x$}
	\addplot [
	domain=0:100, 
	samples=200, 
	color=blue,
	]
	{70 - 0.4*x};
	\addlegendentry{$p_0(x) = 70 - 0.4x$}
	\addplot[
	only marks,
	scatter,
	xmajorgrids=true,
	ymajorgrids=true,
	mark=halfcircle*,
	mark size=2.9pt]
	table[meta=y]
	{content/mlExample01.dat};
	\end{axis}
	\end{tikzpicture}
	\caption[Salary example with prediction epoch 0\ldots]{\protect Salary example with prediction epoch 0} 
	\label{fig:salary1} 
\end{figure}
The current predictor $p_0(x)$ would indicate that the more money an employee gets, the less happy he becomes. Obviously, this seems very unlikely. Using the error function (\ref{eq:er}) we get a value of $er_0 \approx 20.192$. This is a very high number for an error degree. In this case, it means that our prediction is on average wrong by $\approx20.192\%$ of employee satisfaction. Note that these are absolute values. So it's averagely 20.192 points of percentage off, not by 20.192\%. Using gradient descent however, these values get better after each iteration (called epoch). \\\\

\pagebreak
\begin{figure}[h]
	\centering
	\begin{minipage}{.32\textwidth}
	\begin{tikzpicture}[scale=0.67]
	\begin{axis}[
	enlargelimits=false,
	legend pos=north west]
	\addplot [
	domain=0:100, 
	samples=200, 
	color=red,
	]
	{50 + 0.33*x};
	\addlegendentry{$f_{base}(x) = 50 + 0.33x$}
	\addplot [
	domain=0:100, 
	samples=200, 
	color=blue,
	]
	{57.48 + 1.02*x};
	\addlegendentry{$p_1(x) = 57.48+ 1.02x$}
	\addplot[
	only marks,
	scatter,
	xmajorgrids=true,
	ymajorgrids=true,
	mark=halfcircle*,
	mark size=2.9pt]
	table[meta=y]
	{content/mlExample01.dat};
	\end{axis}
	\end{tikzpicture}
\end{minipage}
\begin{minipage}{.32\textwidth}
	\begin{tikzpicture}[scale=0.67]
	\begin{axis}[
	enlargelimits=false,
	legend pos=north west]
	\addplot [
	domain=0:100, 
	samples=200, 
	color=red,
	]
	{50 + 0.33*x};
	\addlegendentry{$f_{base}(x) = 50 + 0.33x$}
	\addplot [
	domain=0:100, 
	samples=200, 
	color=blue,
	]
	{52.78 + 0.54*x};
	\addlegendentry{$p_2(x) = 52.78 + 0.54x$}
	\addplot[
	only marks,
	scatter,
	xmajorgrids=true,
	ymajorgrids=true,
	mark=halfcircle*,
	mark size=2.9pt]
	table[meta=y]
	{content/mlExample01.dat};
	\end{axis}
	\end{tikzpicture}
\end{minipage}
\begin{minipage}{.32\textwidth}
	\begin{tikzpicture}[scale=0.67]
	\begin{axis}[
	enlargelimits=false,
	legend pos=north west]
	\addplot [
	domain=0:100, 
	samples=200, 
	color=red,
	]
	{50 + 0.33*x};
	\addlegendentry{$f_{base}(x) = 50 + 0.33x$}
	\addplot [
	domain=0:100, 
	samples=200, 
	color=blue,
	]
	{51.04 + 0.34*x};
	\addlegendentry{$p_3(x) = 51.04 + 0.34x$}
	\addplot[
	only marks,
	scatter,
	xmajorgrids=true,
	ymajorgrids=true,
	mark=halfcircle*,
	mark size=2.9pt]
	table[meta=y]
	{content/mlExample01.dat};
	\end{axis}
	\end{tikzpicture}
\end{minipage}

	\caption[Salary example scatter chart - gradient descent epoch 1-3\ldots]{\protect Salary example scatter chart - gradient descent epoch 1-3}
	\label{fig:salary2} 
\end{figure}
Shown in figure \ref{fig:salary2} above, are epochs one to three. Already after these three iterations the calculated $\theta_0 \text{ and } \theta_1$ are nearing $50 \text{ and } 0.33$ a lot. Rounded error degrees (equation \ref{eq:er}) are $er_1 \approx 7.297$, $er_2 \approx 2.72$, $er_3 \approx 1.04$.\\

After Epoch 100, at which point the algorithm was manually stopped, the calculated weights where $\theta_0 = 50.0000000000001$, $\theta_1 = 0.330000000000001$ with $er_{100} \approx 0.000000000000139$. So almost exactly our chosen constants. Due to lost accuracy on computers (and the way gradient descent works) these values might never exactly actually match up. However our results are relatively precise and are accurate enough to be useful. As said by mathematician and British professor of statistics George E. P. Box ``all models are wrong, but some are useful''\cite{Empirical}.\\
\\
This example demonstrates nicely, how \ac{ML} can find solutions through trial-and-error. Merely guided by the information of how wrong the current parameters are, the algorithm ``learns'' better values in each iteration. However important to note, is that these algorithms rely heavily on statistics. Therefore, sometimes they may achieve very accurate results, other times mixed and sometimes even no solutions at all. Meaning results which can't be purposefully used, through them being extraordinarily bad.\\

In reality, a tremendous amount of different factors will affect the satisfaction of an employee, compared to merely the salary. Coworkers, stress level, family, friends and many more factors could eventually be relevant. This at the same time would mean that the predictor would quickly grow in complexity. Adding the fact that also a predictor is needed, that can use more than just linear multiplication, but also more complex polynomial terms.\\
\\
These kind of problems would rise in execution time and soon become incalculable in a reasonable amount of time. A common way to approach these kind of problems, (with a large number of inputs and no clear relations) is the use of neural networks.