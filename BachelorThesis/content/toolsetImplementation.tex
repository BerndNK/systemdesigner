\chapter{Implementation}
As System Designer was already an ongoing project prior to the period of this thesis, certain parts and features were already implemented and are not part of this document. Therefore only features implemented within the duration are discussed.\\

\section{Progress}
Out of the two categories ``Tools'' and ``Application Frame'', the following items have already been implemented at the beginning of the thesis:
\begin{itemize}
	\item \textbf{Application Frame - Universal code} 
		Which includes the DisplayCore, XmlParser, SvgAbstraction frameworks + unit tests for those.
	\item \textbf{Application Frame - Windows implementation} 
		Rendering process for rectangles plus window structure in \ac{WPF}
	\item \textbf{Application Frame - iOS implementation} 
		Rendering process for rectangles only
	\item \textbf{Tools}\\
		Base management system plus debug and multi-select tool for testing and a simple select tool (plus a temporary UI which only worked on Windows)
\end{itemize}
For specifics refer to the Repository\footcite{BitBucket}. During the thesis the following items have been implemented:
\begin{itemize}
	\item \textbf{Application Frame - Universal code} 
	Minor refinements and optimizations, \ac{SVG} implementation of polyline
	\item \textbf{Application Frame - Windows implementation} 
	Rendering process for circles, images and polyline, document management and a refined \ac{UI}
	\item \textbf{Application Frame - iOS implementation} 
	Rendering process for circles and images and rotation support
	\item \textbf{Tools}\\
	Tool \ac{UI} support, tool-options, -modifier and -function support and the free-hand tool
\end{itemize}
There has also been a couple instances of refactoring, which I don't particularly describe. For this chapter I instead focus on noteworthy and interesting problems that occurred.

\section{Rendering}
In System Designer the rendering process is divided into two parts.
\begin{itemize}
	\item Calculate positions of each display object\footnote{A display object can be any shape or form. A rectangle, circle, embedded bitmap etcetera} on the screen
	\item Give these information \ac{OS} specific frameworks to render the actual objects
\end{itemize}
The goal of this concept is to minimize the amount of coding required on each platform and maximize the calculations done with \ac{OS} independent code. Better explained in an example:
\begin{figure}[H]
	\centering 
	\includegraphics[width=130mm]{resources/RenderCycle.png}
	\caption{Render cycle explanation example}
	\label{fig:renderCycle}
\end{figure}
Shown in figure \ref{fig:renderCycle} are three display objects. A rectangle A, a rectangle B and a circle C. These three items are inside an abstract container, with the origin (or pivot) of the container being in the upper left corner. A, B, and C are considered children of this container, which itself could be a child of another. Each item (container and display object) may be rotated, scaled and moved.\\

The goal of the first rendering process step is to go through each and every display object, combine their parents transformations and calculate effectively where on the screen they end up. Note that each display object's pivot may be altered as well. A's origin is in its center, while B's is in its upper left corner. (This is important for eventual rotations of these objects.)\\

Suppose the origin of the container in this example is also the upper left corner of the screen, then the call hierarchy in pseudo code would look like this:
\begin{lstlisting}[caption=Render cycle example - Heavily simplified called methods (pseudo code), label=lst:test]
// X, Y, Width, Height, Color
RenderRectangle(100, 65, 200, 200, Color.Red); // A
RenderRectangle(280, 120, 200, 200, Color.Blue); // B
RenderCircle(210, 250, 80, 80, Color.Green); // C
\end{lstlisting}
Noteworthy here is that A's X and Y are at 100 and 65, instead of it's pivot being at 200, 165. The actual implementation of this render cycle is a bit more complicated, however the essence of it is still clearly recognizable in this simplified example.\\
Step two of the rendering process is taking this information and giving it \ac{OS} specific frameworks to actually display the objects. Which concludes into an interesting problem on the iOS platform.
\section{iOS Rotation}
System Designer supports variable pivots for its display objects. A rectangle may be rotated around its upper left corner, center or somewhere else entirely. The used rendering frameworks on iOS called ``CoreGraphics'' \footcite{CoreGraphics} however, does not. Here, only rotations around the center are possible.\\
So in order to still support the whole functionality, whenever an object is rotated around somewhere else than its center, it has to be moved to appear rotated around the given pivot.
\begin{figure}[H]
	\centering 
	\includegraphics[width=\textwidth]{resources/IOSRotationTranslation.png}
	\caption{iOS Rotation Translation}
	\label{fig:iosRotation}
\end{figure}
Visualized in figure \ref{fig:iosRotation}, suppose the unrotated rectangle displayed in purple. The pivot R being in the upper left corner. Shown in beige is the rotated rectangle around its center A by $\alpha$ degrees. (Which is the rotation transformation performed by the iOS framework.) The actual, correct rectangle is displayed in red, which is the purple rectangle rotated around R.\\

So in order to achieve this red rectangle, the beige rectangle has to be moved. Exactly by the distance RB, which lies between the original pivot R and the new pivot B. The steps necessary are:
\begin{itemize}
	\item Get the angle between the center and the pivot $\alpha$
	\item Get the distance between the center and the pivot AR
	\item Get location of B through cosine and sinus of hypotenuse AR and angle $\alpha + \beta$
	\item Get correction distance RB, which is the difference of the points R and B
	\item Move beige rectangle by X \& Y of RB
\end{itemize}
The actual code can again be seen in the repository.\\
\section{Application Frame}
Mentioned in the last chapter, the ``Application Frame'' is \ac{OS} specific. Therefore both the Windows and iOS implementation are briefly summarized in this section.\\

\begin{figure}[h]
	\centering 
	\includegraphics[width=0.7\textwidth]{resources/SystemDesignerMarked.png}
	\caption{System Designer windows application marked}
	\label{fig:sysDMarked}
\end{figure}
Seen above in figure \ref{fig:sysDMarked} is the actual System Designer application on Windows. (As of 17.07.2017)\\

The green area marks the window frame, which is specific to the Windows operating system. Seen on the upper left corner, is the application menu ``File''. A commonly seen menu on most Windows applications, which is not present on other platforms. Below are all open documents. In this figure only ``New Stage''. The yellow area marks special debugging options which are again only present on this platform. The red area however, indicates the tool selection, which is realized on each platform in some way or another. Blue and purple mark the rendered stage, which should look exactly equal on every \ac{OS}. With the purple area being the tool modifier buttons.\\

A dramatic difference in design can be seen, when looking at the iOS implementation of System Designer seen in figure \ref{fig:sysDiOS}.\\

\begin{figure}[h]
	\centering 
	\includegraphics[width=0.5\textwidth]{resources/SystemDesigneriOS.png}
	\caption{System Designer iOS implementation}
	\label{fig:sysDiOS}
\end{figure}
Here the ``Application Frame'' has no visible elements. As iOS is an operating system for small hand held devices, with usually small screens, it is important not to waste screen space. Therefore the missing functionality is hidden under different menus, which can be accessed by button presses or gestures. Merely the tool selection bar is visible on the upper part of the screen.\\

The stage again looks exactly the same as in the Windows implementation. (However on Windows a selection border is additionally rendered for debugging purposes.)
\section{Shortcuts}
Another interesting implementation is the shortcuts. Which are exclusive features for devices with keyboards. Again - like almost every part of System Designer - the goal is to minimize the effort, that has to be done to implement this feature on each \ac{OS}. The only part that the platform shall do, is passing the pressed keys to the System Designer core. \\

Therefore, these shortcuts have to be implemented system independent. As for MacOS the usage of the command key is very common, unlike on most Windows devices where the control key is more commonly used. For example, the shortcut for saving a document in a lot of applications is ``Control + S'' on Windows, while for mac users it is ``Command + S''.\\\\

On top of that, the shortcuts have to be customizable by the user. However, merely storing each and every shortcut is not trivial, as updates to the applications may introduce new shortcuts, that have to be  merged with the existing, user-customized list. To summarize, the shortcut system has to:
\begin{itemize}
	\item Provide platform independent shortcuts.
	\item Provide customizable shortcuts and
	\item a way to support future iterations of System Designer.
\end{itemize}
To achieve this a shortcut is identifiable by a key and has multiple combinations.\\

\begin{figure}[h]
	\centering 
	\includegraphics[width=\textwidth]{resources/ClassDiagramShortcuts.png}
	\caption{Class diagram of the shortcut structure}
	\label{fig:classDiaShort}
\end{figure}
Seen in figure \ref{fig:classDiaShort} is the class diagram for this shortcut structure. A ``KeyCombination'' consists of a key (for example ``S'') and a modifier (for example ``Control'' or ``Command''). When a key combination is pressed, System Designer goes through each shortcut and checks if the pressed combination matches with one of the shortcuts. If so, the ``OnPressedAction'' command is executed. On release the ``OnReleaseAction'' is performed.\\

To solve the platform independent requirement, a single shortcut, as an example ``Save document'' may have multiple combinations; ``Control + S'' for Windows and ``Command + S'' for MacOS.\\

With the unique identifier for each shortcut, the customization and future update support is solved. On startup, System Designer loads the list of customized shortcuts. It then compares this list with the defaults, which are hard coded into each version. With the identifier, both lists get merged. So only new shortcuts get added to the list and stored again. The result is a list of all customized shortcuts and all new ones, which get added via an update.\\

\section{Shape drawer}
Shape drawer is a small program, allowing the user to draw shapes, which get stored in \ac{XML} files. It is a utility that is used to gather a large amount of training samples for the shape recognition neural network. As it is designed to be sent to multiple people with ease, it is not part of System Designer, which would increase the size of the file, but instead is a standalone application.\\

\begin{figure}[h]
	\centering 
	\includegraphics[width=0.6\textwidth]{resources/UseCaseShapeDrawer.png}
	\caption{Use case diagram of the Shape Drawer utility}
	\label{fig:useCaseShapeDrawer}
\end{figure}
Illustrated in figure \ref{fig:useCaseShapeDrawer} is the use case diagram for the utility. The application suggests to draw a certain shape, which the user draws. When the user is satisfied with the amount of shapes drawn, they can be zipped into a single file. During the drawing process, the application stores each finished drawing as a series of points in \ac{XML}.\\

This utility program can also be found in the System Designer repository.
