\chapter{Shape Recognition}
With the necessary basics for \ac{ML} and neural networks covered, this chapter goes into more details in which ways these techniques are used for System Designer.\\

Visualized in the figure \ref{fig:shapeConversion} below, the user shall be able to draw a crude shape (left), which gets recognized by the application and converted to a perfect form (right).
\begin{figure}[h]
	\centering 
	\includegraphics[width=60mm]{resources/ShapeConversion.png}
	\caption{Free hand drawn shape conversion example}
	\label{fig:shapeConversion}
\end{figure}
\\
The most difficult portion of this process is, to detect which shape the user has drawn. For this neural networks and machine learning are used.\\

As mentioned earlier, constructing the network can be broken down into three steps:
\begin{itemize}
\item Find a good way to map the problem to input and output layers
\item Find a good size/layout/type for the network 
\item Find good bias and weights
\end{itemize}
\section{Mapping the problem}
Probably the most creative and difficult part about working with neural networks, is finding a way to map a problem to the network. After all, the input layer only takes a range of numbers between 0 and 1. Analog to that, the output layer only gives a range of numbers between 0 and 1. So how could, for example, a picture be translated into these kinds of inputs? Intuitively it should be possible, since in the end, computers only work with zeros and ones. \\

For some cases, the answer is obvious; the previous happiness predictor example would output a single number between 0 and 1, which translates to 0\% and 100\%. For others, mappings get a bit more complicated, like a network that guesses in which adverts a user is interested in. Inputs could be a number of previously visited websites, possibly categorized. One input for the percentage of tech sites visited, another for news sites and a third one for comic book themed websites. (So a 1 could mean, that 100\% of the sites the user visited were tech sites.) Analog to that, the output could look the same but possibly with even more categories. Say the user visited a lot of comic book and news related websites? Then he should be interested in a comic book news article.\\

For System Designer a way to map a shape as input and a category (which shape it is; rectangle, circle, triangle etcetera) as output is needed. Through the fact, that the drawing and recognition process happens during runtime, very exact information about the input is known. Specifically each and every point the mouse (or any other form of input device) passes. See an example in \ac{XML} format; figure \ref{fig:frog1}.\\

\begin{wrapfigure}[21]{L}{0.31\textwidth}
	\includegraphics[width=0.2\textwidth]{resources/DrawnCircleXml.PNG}
	\caption{\label{fig:frog1}Points in \ac{XML}}
\end{wrapfigure}
Traditionally a programmer could think that analyzing this kind of data makes sense. A series of points which - when connected - form a shape.
One way of analyzing this data, could be something like:
\begin{itemize}
	\item Go trough each point.
	\item Calculate the angle between these points.
	\item Note sudden changes in angles.
	\item Map angle changes to shapes (for example three 90 degree angle changes could be a rectangle).
\end{itemize}
The idea is to detect a shape through its core recognition facets. A rectangle has 4 corners, a circle has 0, a triangle has three and so on. Imagine trying to draw these shapes with a car on a corn field. To draw the rectangle, the car would have to make three 90 degree turns all either clockwise or counterclockwise. For a triangle it has to make three turns, which sum up to 180 degrees\footnote{Which is a known fact in simple geometry.}.\\

This approach however, is highly flawed in practice. The shaking hand of the user alone could produce dramatic angle changes. As it turns out, this is a rather difficult task to master. Especially with an approach which tries to analyze only the series of points. \\

When using neural networks, a good idea is thinking about how a human would solve this task. A human would not try to count angle changes or analyze the points on the screen. Instead he would look at the drawing result and compare it to previously seen rectangles. If it looks alike, it's probably the same. So instead of looking at the raw data of points, look at the drawing directly.\\

In the world of computers, this would of course mean looking at an array of pixel. A circle for example could look like this:\\
\clearpage
\begin{figure}[h]
	\centering 
	\includegraphics[width=60mm]{resources/DrawnCircleZoom.png}
	\caption{Zoomed drawing of a circle}
	\label{fig:circle01}
\end{figure}
Seen in figure \ref{fig:circle01} is the very same \ac{XML} file as the excerpt figure \ref{fig:frog1} visualized. More details about the series of points in XML and converting them to a picture is handled in chapter ``Implementation''. For now it is enough to just look at the drawing.\\

This is a zoomed in view of a 50 by 50 pixel picture of a drawn ``circle''. Additionally there is a grid laid on top, to differentiate single pixels. These pixel are translated to a neural network using a one-to-one translation. Each pixel is one input neuron. With 0 meaning completely white and 1 being completely black. With this 50 by 50 resolution, this translation results in an input layer with the size of 2500 neurons.\\

The output layer however, has the same size as the amount of shapes it recognizes. So when the network distinguishes a triangle, circle, rectangle and a diamond, it has 4 output neurons. With the number between 0 and 1 meaning how likely the shape is. If the network outputs, for example, [0.2, 0.8, 0.2, 0.2] it's very likely a circle, since the output for circle is at 0.8, while every other is merely at 0.2.\\

The next step is finding a good size for the resolution of input pictures (since there could be more suitable sizes than 50 by 50 pixel) and sizes for hidden layers.

\section{Finding a good layout}
The entirety of variables to cover are:
\begin{itemize}
	\item How big is the resolution of the drawings (which will determine the size of the input layer)?
	\item How many hidden layers?
	\item How big are these hidden layers?
	\item How big is the learning rate (of the gradient descent algorithm)?
\end{itemize}
This in itself is again a complex context as all of these variables might have some effect on the network and correlate to each other. For example a learning rate of 0.1 could be too low with two hidden layers, however it might work with only one layer. Intuitively, one approach might be to apply the gradient descent again. This is indeed a good idea and is - as mentioned in chapter two - called meta learning. However, since training a network (of the sizes worked with here) can take up multiple minutes, iterating through possible variables via gradient descent will take a significant amount of time.\\

So for this approach to work, highly optimized, parallelizable algorithms are expedient. To shrink the time necessary for finding optimal values. Therefore, \ac{ML} was not chosen again for this problem. Instead, a semi-manual trial-and-error approach was taken. A list of selected values for each variable was chosen and permuted. These values are:\\

 \begin{itemize}
 	\item Resolutions of $16^2$, $32^2$ and $64^2$.
 	\item Learning rates of 1.0, 0.5, 0.25, 0.1, 0.05, 0.025, 0.01, 0.005, 0.0025, 0.001
 	\item Hidden layer combinations of: 
 		\begin{itemize}
 		\item small sizes: 8-4, 8-2
 		\item medium sizes: 10-5, 10, 10-5-5, 10-5-2, 12-10, 12-8, 12-6, 12-4
		\item and big sizes: 30-20, 30-20-10, 30-20-5, 10-9-8-7-6-5-4-3-2-1
 	\end{itemize}
 \end{itemize}
These numbers were chosen to cover a wide variety of variables. Combined, this results in 420 different combinations. Additionally to testing these parameters with the System Designer implementation, they are also tested with a popular framework called Encog.\footcite{Encog} This is to have a comparison between a highly optimized (Encog) and a more mundane implementation (System Designer).\\

Each test run is stopped when it reaches an error rate that falls below 0.005 or the hundredth epoch is reached. This is to ensure that every test is finished in the matter of one to two days and to have a good baseline to compare parameters against each other. There are a total of 494 training and 33 test shapes. These tests are not used to learn the network, but are used to evaluate on how it reacts to items it has never seen before (not trained with), which is exactly the case when the user draws a shape that shall be recognized.\footnote{Each training and test shape can be seen in \ac{XML} format in the System Designer repository.}\\

Resolutions for the shapes were kept relatively small ($16^2$, $32^2$ and $64^2$). This on one hand is to ensure, that the amounts of weights is not extraordinary big and on the other hand it is reasonable to believe, that these sizes are enough to correctly identify the given shapes. As mentioned earlier, with neural networks it makes sense to think of the problematics more humanly. ``Can I solve this problem?'' Looking at the hand drawn circle from above again in these resolutions, makes this clear:\\
\begin{figure}[H]
	\centering 
	\includegraphics[width=50mm]{resources/DrawnCircleComp.png}
	\caption[Hand drawn circle in resolutions $16^2$, $32^2$ and $64^2$\ldots]{\protect Hand drawn circle in resolutions $16^2$, $32^2$ and $64^2$. (Image is scaled to make individual pixels distinguishable.)} 
	\label{fig:circle03}
\end{figure}
At each resolution the circle can be identified. Even at the lowest resolution of 16 by 16 pixel, in figure \ref{fig:circle03} on the left. It is important to note, however, that the more different shapes are possible, the higher the resolution has to be. This makes sense because a higher pixel count can store more information than lower ones.\\

Looking at the left circle again, this could very well be a crude drawing of a diamond shape. While this is not entirely clear at this low resolutions, the middle and right picture have enough information to show the more smoothed out lines in the corners. Since System Designer supports only four different shapes (circle, triangle, rectangle, diamond) a resolution between these three will expectedly be enough.\\

\section{Finding good bias and weights}
As mentioned earlier, gradient descent can be used to find good weights and bias. With the huge amount of variables however, this is very slow. Taking the example from above, a picture resolution of $32^2$ with hidden layers of 30-20 will result in $32^2*30*20*4 = 2,457,600$ weights and $30*20*4 = 2,400$ bias. This means calculating the gradient (the partial derivate of each weight and bias) would mean deriving $2,460,000$ variables! Since this has to be done in each iteration, the execution time rises tremendously.\\

Instead, an optimized algorithm specifically for neural networks, called back propagation, is used. \footcite{backProp}
\subsection{Back propagation}
The idea here is to evaluate the network as a whole and only look at each neuron, instead of every weight and bias individually. This happens in three steps:\\
\begin{itemize}
	\item Compute the current output of the network.
	\item Use back propagation to compute the error rate.
	\item Use gradient descent to update the weights and bias.
\end{itemize}

The first step is just straight-forward feeding the network a training sample. The second and third step, however, are a bit more complicated.\\

The back propagation algorithm moves backwards through the network. Hence the name. For the last layer $j$, look at the actual output $y^j_{actual}$ and the optimal $y^j_{optimal}$. Note that $y$ is a vector as a layer may have multiple outputs.\\

Calculate a ${\Delta \text{error}}^{j}_k$ for each neuron $k$ (in the last layer $j$) based on $y^j_{actual}$ and the optimal $y^j_{optimal}$. This gives each neuron in the last layer a value for it's error degree (delta error). How far off the output of this neuron is, compared to the optimal.\\

Move backwards through the algorithm and repeat. However, there is a problem. Since for the next (previous) layer $j-1$ there is no $y^{j-1}_{optimal}$ known. Therefore the calculated ${\Delta \text{error}}^{j}_k$ for each neuron $k$ in layer $j$ are used, to calculate the ${\Delta \text{error}}^{j-1}_l$ for each neuron $l$ in layer $j-1$ combined with each weight $w^j_{kl}$ from that neuron $l$ to each neuron $k$ in layer $j$.\\

When the last (which is the first layer of the network) is reached, each neuron will have a $\Delta \text{error}$ value. These are used in step three to update each and every weight in the network. Go again forward through the network. For each layer, use the output from the previous layer (for the first layer the input vector will be used), the $\Delta \text{error}$ for each neuron and the learning rate to alter the weights and bias.\\

All in all the algorithm can be broken down to calculating the current output (step one), evaluate how ``wrong'' each neuron is (step two) and then update the weights and bias based upon this (step three). The last step here is again the gradient descent algorithm. \\

What these calculations are in detail, is not explained as they are not important in the scope of this thesis. It is just necessary to know, that this optimization algorithm was used for the training and without it, the execution time would be extraordinary big. Back propagation breaks gradient descent down to merely check - with the previous example - the $2,400$ neurons instead of the $2,460,000$ weights and bias individually.

\section{Training results}
The test results show clearly, that the System Designer implementation is capable better recognition rates, compared to the Encog framework. When sorting the test results by the amount of correctly guessed test shapes, the first Encog result places sixth. 
\begin{figure}[H]
	\centering 
\begin{center}
	\begin{tabular}{||c c c c c c c||} 
		\hline
		Impl. & Resolution &Final error rate & Epoch & Hidden layer & Learning rate & Tests \\ [0.5ex] 
		\hline
		Sys.D. & 16 & 0.004763767 & 18 & 30-20-10 & 0.25 & 33/33\\ 
		\hline
		Sys.D. & 16 & 0.004990209 & 33 & 12-4 & 0.5 & 33/33\\
		\hline
		Sys.D. & 16 & 0.004993775 & 86 & 12-6 & 0.5 & 33/33\\
		\hline
	\end{tabular}
\end{center}
	\caption{Top three test results, when sorting by most test shapes recognized correctly} 
	\label{fig:topThree01}
\end{figure}
Seen in figure \ref{fig:topThree01} are the top three results. Out of all combinations only six are capable of recognizing all 33 test shapes, all with a resolution of $16^2$. Although the amount of correctly recognized tests is not exactly linear to the error rate. This is due the fact, that the error rate determines how far the actual output is different to the correct result. So when the network, for example, outputs 0/0.5/0/0 and the correct result is 0/1/0/0 it has a lower error rate, than a different network which outputs 0/0.25/0/0. Even though both networks guessed the shape correctly.\\

When sorting by the lowest error rate, the top three results are (figure \ref{fig:topThree02}):
\begin{figure}[H]
	\centering 
\begin{center}
	\begin{tabular}{||c c c c c c c||} 
		\hline
		Impl. & Resolution & Final error rate & Epoch & Hidden layer & Learning rate & Tests \\ [0.5ex] 
		\hline
		Sys.D. & 32 & 0.002806902 & 17 & 30-20-10 & 1 & 32/33\\ 
		\hline
		Sys.D. & 64 & 0.004073667 & 11 & 12-8 & 0.5 & 32/33\\
		\hline
		Sys.D. & 16 & 0.004086236 & 8 & 30-20-10 & 0.5 & 31/33\\
		\hline
	\end{tabular}
\end{center}
\caption{Top three test results, when sorting by lost error rate} 
\label{fig:topThree02}
\end{figure}
The first Encog entry is at place 158.\\

The statistics show that higher resolutions can lead to better error rates, but not necessarily improved detection rates. Compare figure \ref{fig:topThree01} and \ref{fig:topThree02}. However it is important to note, that these might be results of the fact, that the training process was stopped at epoch 100. For bigger resolutions, more training iterations might be necessary in order to achieve the same amount of correctly guessed shapes.\\
\begin{figure}[H]
	\centering 
\begin{center}
	\begin{tabular}{||c c c||} 
		\hline
		Resolution & Avg. error rate & Avg. correct detected images \\ [0.5ex] 
		\hline\hline
		16 & 0.097830381 & 24 \\ 
		\hline
		32 & 0.105199492 & 23.11428571  \\
		\hline
		64 & 0.121597284 & 21.45  \\ 
		\hline
	\end{tabular}
\end{center}

\caption{Picture resolutions with average error rates and correctly recognized tests} 
\label{fig:resComp}
\end{figure}
Figure \ref{fig:resComp} supports this assumption. The higher the resolution, the worse the average error rate and correctly guessed shapes get.\\

Another important note is, that even though the Encog framework achieved - with the best combinations - worse results than the System Designer implementation, the training durations were tremendously lower. Sometimes of factors of over 400. Compare the following results:
\begin{figure}[H]
	\centering 
	\begin{center}
		\begin{tabular}{||c c c c c c c||} 
			\hline
			Impl. & Resolution & Final error rate & Epoch & Hidden layer & Learning rate & Duration [m:s] \\ [0.5ex] 
			\hline
			Sys.D. & 16 & 0.004316462 & 9 & 30-20 & 0.5 &  2:24.0160135\\
			\hline
			Encog & 16 & 0.030723173 & 100 & 30-20 & 0.5 & 0:0.7563499\\ 
			\hline
		\end{tabular}
	\end{center}
	\caption{Top three test results, when sorting by lost error rate} 
	\label{fig:comp}
\end{figure}
Seen in figure \ref{fig:comp}, System Designer needed only 9 epochs to reach the final error rate of < 0.005. However these 9 iterations took roughly two minutes 24 seconds. Encog achieved a similar result after 100 epochs in just under a second.\\

Additionally Encog achieved a better average error rate of 0.090912326 compared to System Designer's 0.125505779 (figure \ref{fig:comp2}). However recognized on average one test less than System Designer. These statistics show clearly the immense optimization ledge that the Encog framework has in terms of training speed, but with the disadvantage of worse results when capping the maximal epoch.\\

\begin{figure}[H]
	\centering 
\begin{center}
	\begin{tabular}{||c c c||} 
		\hline
		Impl. & Avg. error rate & Avg. correct detected images \\ [0.5ex] 
		\hline\hline
		Encog & 0.090912326 & 22.3 \\ 
		\hline
		Sys.D. & 0.125505779 & 23.40952381  \\ 
		\hline
	\end{tabular}
\end{center}
\caption{Encog and System Designer average error rates and correct test shapes} 
\label{fig:comp2}
\end{figure}
Comparing the achieved error rates to the used learning rate shows, that for this network a rate between 0.25 to 1.0 will achieve good results. (Figure \ref{fig:learnRate})
\begin{figure}[H]
	\centering 
	\begin{tikzpicture}
	\begin{axis}[
	enlargelimits=false,
	legend pos=north west,
	xlabel = Learning rate,
	ylabel = {Average error rate},
	 x post scale=1.2,
	 y post scale=1.2,
	 ymin=0,ymax=0.25,
	 xmin=0,xmax=1
	]
	\addplot[
	xmajorgrids=true,
	ymajorgrids=true,
	mark=halfcircle*,
	mark size=2.9pt]
	table[meta=y]
	{trainingResults/learningRateToAvgError.dat};
	\end{axis}
	\end{tikzpicture}
	\caption{Learning rate compared to average error rate}
	\label{fig:learnRate} 
\end{figure}
As a very interesting, but unfortunately not very revealing comparison is between the hidden layer constellation and achieved error rates:
\begin{figure}[h]
	\centering 
	\includegraphics[width=\textwidth]{trainingResults/HiddenLayerSizesToAvgError.PNG}
	\caption{Hidden layer constellations in relation to average error rates}
	\label{fig:hiddenLayerToAvgError}
\end{figure}
Although some results makes sense, the graph is not very conclusive. Seen in figure \ref{fig:hiddenLayerToAvgError}, a constellation of 10-9-8-7-6-5-4-3-2-1 will achieve extremely bad results. This makes sense, as when a hidden layer is smaller then the output layer (for this network the output layer has a size of 4), information has to get lost. Three neurons cannot store the same information as four. This also explains why 8-2 and 10-5-2 are similarly bad.\\

Remarkably the 10-5-5 constellation achieved worse than the 10-5 one. Which could indicate that more layers won't result in better error rates.\\

To conclude, the chosen combination for System Designer is a picture resolution of $16^2$, a learning rate of 0.25 and hidden layers of 30-20-10. This combination recognized all test shapes correctly and achieved a low error rate.