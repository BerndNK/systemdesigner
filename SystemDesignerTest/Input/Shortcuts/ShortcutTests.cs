﻿using SystemDesigner.Input.Shortcuts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SystemDesignerTest.Input.Shortcuts
{
    [TestClass]
    public class ShortcutTests
    {
        
        [TestMethod]
        public void DefaultShortcuts_Serialization_Success()
        {
            var defaultShortcuts = Shortcut.GetDefaultShortcuts();
            // serialize
            var xml = Shortcut.Serialize(defaultShortcuts);
            var deserialized = Shortcut.Deserialize(xml);

            Assert.IsTrue(defaultShortcuts.Count == deserialized.Count);
        }
    }
}
