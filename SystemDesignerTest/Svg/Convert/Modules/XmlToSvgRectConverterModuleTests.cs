﻿using System.Linq;
using SystemDesigner.Svg.Convert;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SvgAbstraction.Svg.Forms;
using SvgAbstraction.Svg.Forms.Container;
using XmlParser.Xml;

namespace SystemDesignerTest.Svg.Convert.Modules {
    [TestClass]
    public class XmlToSvgRectConverterModuleTests {
        [TestMethod]
        public void SvgRectConvert_PositionAndSize_Success() {
            // arrange
            var rect = new SvgRect {
                X = 100,
                Y = 150,
                Width = 200,
                Height = 200
            };

            var asXml = rect.ToXml();
            var converter = new XmlToSvgConverter();

            // act
            var converted = converter.Convert(XmlFileParser.ParseIntoTags(asXml).First());

            // assert
            var convertedXml = converted.ToXml();
            Assert.AreEqual(convertedXml, asXml);
        }

        [TestMethod]
        public void SvgRectConvert_Scaling_Success() {

            // arrange
            var rect = new SvgRect {
                X = 100,
                Y = 150,
                Width = 200,
                Height = 200,
                ScaleX = 2,
                ScaleY = 1.5
            };

            var asXml = rect.ToXml();
            var converter = new XmlToSvgConverter();

            // act
            var converted = converter.Convert(XmlFileParser.ParseIntoTags(asXml).First());

            // assert
            var convertedXml = converted.ToXml();
            Assert.AreEqual(convertedXml, asXml);
        }

        [TestMethod]
        public void SvgRectConvert_InGroup_Success() {
            // arrange
            var root = new SvgGroup {
                X = 50,
                Y = 50,
                ScaleX = 2,
                ScaleY = 2
            };
            var rect = new SvgRect {
                X = 100,
                Y = 150,
                Width = 200,
                Height = 200,
                ScaleX = 2,
                ScaleY = 1.5
            };

            root.Add(rect);

            var asXml = root.ToXml();
            var converter = new XmlToSvgConverter();

            // act
            var converted = converter.Convert(XmlFileParser.ParseIntoTags(asXml).First());

            // assert
            var convertedXml = converted.ToXml();
            Assert.AreEqual(convertedXml, asXml);
        }

        [TestMethod]
        public void SvgRectConvert_DoubleNestedGroup_Success() {
            // arrange
            var root = new SvgGroup {
                X = 50,
                Y = 50,
                ScaleX = 2,
                ScaleY = 2
            };

            var group = new SvgGroup {
                X = 25,
                Y = 25,
                ScaleX = 0.5,
                ScaleY = 0.25
            };
            var rect = new SvgRect {
                X = 100,
                Y = 150,
                Width = 200,
                Height = 200,
                ScaleX = 2,
                ScaleY = 1.5
            };

            root.Add(group);
            group.Add(rect);

            var asXml = root.ToXml();
            var converter = new XmlToSvgConverter();

            // act
            var converted = converter.Convert(XmlFileParser.ParseIntoTags(asXml).First());

            // assert
            var convertedXml = converted.ToXml();
            Assert.AreEqual(convertedXml, asXml);
        }


        [TestMethod]
        public void SvgRectConvert_DoubleNestedGroupWithPivot_Success() {
            // arrange
            var root = new SvgGroup {
                X = 50,
                Y = 50,
                ScaleX = 2,
                ScaleY = 2
            };

            var group = new SvgGroup {
                X = 25,
                Y = 25,
                ScaleX = 0.5,
                ScaleY = 0.25
            };
            var rect = new SvgRect {
                X = 100,
                Y = 150,
                Width = 200,
                Height = 200,
                PivotX = 100,
                PivotY = 100,
                ScaleX = 2,
                ScaleY = 1.5
            };

            root.Add(group);
            group.Add(rect);

            var asXml = root.ToXml();
            var converter = new XmlToSvgConverter();

            // act
            var converted = converter.Convert(XmlFileParser.ParseIntoTags(asXml).First());

            // assert
            var convertedXml = converted.ToXml();
            Assert.AreEqual(convertedXml, asXml);
        }


        [TestMethod]
        public void SvgRectConvert_TripleNestedGroupWithPivotAndScale_Success() {
            // arrange
            var root = new SvgGroup {
                X = 50,
                Y = 70,
                ScaleX = 2,
                ScaleY = 1.25
            };

            var group = new SvgGroup {
                X = 50,
                Y = 50,
                ScaleX = 0.15,
                ScaleY = 0.25
            };

            var group2 = new SvgGroup {
                X = 25,
                Y = 25,
                ScaleX = 0.5,
                ScaleY = 0.25
            };

            var rect = new SvgRect {
                X = 100,
                Y = 150,
                Width = 200,
                Height = 200,
                PivotX = 100,
                PivotY = 100,
                ScaleX = 2,
                ScaleY = 1.5
            };

            root.Add(group);
            group.Add(group2);
            group2.Add(rect);

            var asXml = root.ToXml();
            var converter = new XmlToSvgConverter();

            // act
            var converted = converter.Convert(XmlFileParser.ParseIntoTags(asXml).First());

            // assert
            var convertedXml = converted.ToXml();
            Assert.AreEqual(convertedXml, asXml);
        }

        [TestMethod]
        public void SvgRectConvert_Pivot_Success() {

            // arrange
            var rect = new SvgRect {
                X = 100,
                Y = 150,
                Width = 200,
                Height = 200,
                PivotX = 100,
                PivotY = 100,
                ScaleX = 2,
                ScaleY = 1.5
            };

            var asXml = rect.ToXml();
            var converter = new XmlToSvgConverter();

            // act
            var converted = converter.Convert(XmlFileParser.ParseIntoTags(asXml).First());

            // assert
            var convertedXml = converted.ToXml();
            Assert.AreEqual(convertedXml, asXml);
        }
    }
}
