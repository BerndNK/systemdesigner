using System;
using System.Collections.Generic;
using System.Globalization;
using SystemDesigner.Input;
using SystemDesigner.Stage;
using SystemDesigneriOS.Extensions;
using CoreGraphics;
using DisplayCore.Render;
using DisplayCore.Render.Geometry;
using Foundation;
using SpriteKit;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms;
using UIKit;

namespace SystemDesigneriOS {
    public class GameScene : SKScene, IRenderer {

        #region Properties

        /// <summary>
        /// Gets or sets the SVG stage.
        /// </summary>
        /// <value>
        /// The SVG stage.
        /// </value>
        public IStage<ISvgItem> SvgStage {
            get { return _svgStage; }
            set {
                var oldStage = _svgStage;
                _svgStage = value;
                OnSvgStageChanged(oldStage);
            }
        }

        #endregion


        #region Fields

        /// <summary>
        /// The renderer handler
        /// </summary>
        private readonly RendererHandler _rendererHandler;

        /// <summary>
        /// The SVG stage backing fields
        /// </summary>
        private IStage<ISvgItem> _svgStage;
        //TODO remove this from this scene class and put it into the controller
        private SvgStageHandler _stageHandler;
        private double _pinchStartScale;
        private CGPoint _root;

        /// <summary>
        /// Gets or sets the canvas children map. This is used to just apply property updates, instead of removing/adding new display objects each Render call
        /// </summary>
        private Dictionary<int, SKNode> _nodeMap;
        #endregion


        public GameScene(IntPtr handle) : base(handle) {
            _rendererHandler = new RendererHandler();
            _rendererHandler.Renderer = this;
            _nodeMap = new Dictionary<int, SKNode>();
        }

        /*   public override void TouchesBegan(NSSet touches, UIEvent evt) {
               // Called when a touch begins
               foreach (var touch in touches) {
                   var location = ((UITouch)touch).LocationInNode(this);
                   var translatedLocation = ConvertPointFromView(location);
                   _stageHandler.ActiveStage.OnInputDown(new Point(translatedLocation.X, translatedLocation.Y), InputType.Mouse);
               }

               if(touches.Count > 2) _stageHandler.AddTestShapes();
           }

           public override void TouchesEnded(NSSet touches, UIEvent evt) {
               // Called when a touch ends
               foreach (var touch in touches) {
                   var location = ((UITouch)touch).LocationInNode(this);
                   _stageHandler.ActiveStage.OnInputUp(new Point(location.X, location.Y), InputType.Mouse);
               }
           }

           public override void TouchesMoved(NSSet touches, UIEvent evt) {
               // Called when a touch moves
               foreach (var touch in touches) {
                   var location = ((UITouch)touch).LocationInNode(this);
                   var translatedLocation = ConvertPointFromView(location);
                   _stageHandler.ActiveStage.OnInputMove(new Point(translatedLocation.X, translatedLocation.Y), InputType.Mouse);
               }
           }*/

        public override void Update(double currentTime) {
            _rendererHandler.Render(SvgStage);
        }

        public override void DidMoveToView(SKView view) {
            _stageHandler = new SvgStageHandler();
           // _stageHandler.AddTestShapes();
            SvgStage = _stageHandler.ActiveStage;
            //_rendererHandler.Render(SvgStage);
            
            View.AddGestureRecognizer(new UIPanGestureRecognizer(OnPanGesture));
            View.AddGestureRecognizer(new UIPinchGestureRecognizer(OnPinchGesture));
            View.AddGestureRecognizer(new UITapGestureRecognizer(OnTripleTouchGesture) {NumberOfTouchesRequired = 3});
            View.AddGestureRecognizer(new UITapGestureRecognizer(OnDoubleTapGesture) { NumberOfTapsRequired = 2});
			View.AddGestureRecognizer(new UIRotationGestureRecognizer(OnRotateGesture));

            _stageHandler.AddTestShapes();
			_stageHandler.ActiveStage.Root.Rotation = 1.5;
        }

        private void OnRotateGesture(UIRotationGestureRecognizer obj) {
            if (_stageHandler?.ActiveStage?.Root == null) return;
            _stageHandler.ActiveStage.Root.Rotation = obj.Rotation;
        }

        private void OnDoubleTapGesture(UITapGestureRecognizer obj) {
            if (_stageHandler?.ActiveStage?.Root == null) return;
            if (Math.Abs(_stageHandler.ActiveStage.Root.ScaleX - 0.5) < 0.1) {
                _stageHandler.ActiveStage.Root.ScaleX = 1;
                _stageHandler.ActiveStage.Root.ScaleY = 1;
            }
            else {
                _stageHandler.ActiveStage.Root.ScaleX = 0.5;
                _stageHandler.ActiveStage.Root.ScaleY = 0.5;
            }
        }

        private void OnTripleTouchGesture(UITapGestureRecognizer obj) {
            _stageHandler.AddTestShapes();
        }

        private void OnPinchGesture(UIPinchGestureRecognizer obj) {
            if (obj.Scale < 0.1) return;
            switch (obj.State) {
                case UIGestureRecognizerState.Began:
                    _pinchStartScale = _stageHandler.ActiveStage.Root.ScaleX;
                    break;
                case UIGestureRecognizerState.Changed:
                    _stageHandler.ActiveStage.Root.ScaleX = _pinchStartScale * obj.Scale;
                    _stageHandler.ActiveStage.Root.ScaleY = _pinchStartScale * obj.Scale;
                    break;
            }
        }

        private void OnPanGesture(UIPanGestureRecognizer obj) {
            var location = obj.LocationInView(View);
            switch (obj.State) {
                case UIGestureRecognizerState.Began:
                    _stageHandler.ActiveStage.OnInputDown(new Point(location.X, location.Y), InputType.Mouse, -1);
                    break;
                case UIGestureRecognizerState.Changed:
                    _stageHandler.ActiveStage.OnInputMove(new Point(location.X, location.Y), InputType.Mouse, -1);
                    break;
                case UIGestureRecognizerState.Ended:
                    _stageHandler.ActiveStage.OnInputUp(new Point(location.X, location.Y), InputType.Mouse, -1);
                    break;
            }
        }


        /// <summary>
        /// Called when [SVG stage changed].
        /// </summary>
        private void OnSvgStageChanged(IStage<ISvgItem> oldStage) {
            // remove event handler from old stage
            if (oldStage?.Root != null) oldStage.Root.ChildrenChanged -= OnStageHasChanged;

            var newRoot = SvgStage?.Root;
            if (newRoot != null) newRoot.ChildrenChanged += OnStageHasChanged;

           // _rendererHandler.Render(SvgStage);
        }

        /// <summary>
        /// Called when [stage has changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnStageHasChanged(object sender, EventArgs eventArgs) {
            //_rendererHandler.Render(SvgStage);
        }


        #region VariousRenderer

        private void RenderRect(SvgRect target, Rectangle renderBoundingBox, double rotation, Point rotationPoint)
        {
            SKNode existingNode;
            SKSpriteNode shape;
            if (_nodeMap.TryGetValue(target.Id, out existingNode))
            {
                shape = existingNode as SKSpriteNode;
                if(shape == null) throw new Exception("Existing Node was not expected type. This should never happen.");
            }
            else
            {
                shape = new SKSpriteNode(target.FillColor.ToUiColor(), new CGSize(renderBoundingBox.Width, renderBoundingBox.Height));
                AddChild(shape);
                _nodeMap.Add(target.Id, shape);
            }

            shape.Color = target.FillColor.ToUiColor();
            shape.Size = new CGSize(renderBoundingBox.Width, renderBoundingBox.Height);

            // iOS only rotation around the center (width/2, height/2) However SystemDesigner provides rotation around a rotation point. To solve this issue, we translate the resulting X/Y change a different rotation point than the center would result in.
            var rotPointDistX = rotationPoint.X - renderBoundingBox.Width/2; // distance from center to rotation point in X
            var rotPointDistY = rotationPoint.Y - renderBoundingBox.Height/2;
            var rotPointDist = Math.Sqrt(rotPointDistX * rotPointDistX + rotPointDistY * rotPointDistY); // diagonal distance from center to rotation point x
            var rotPointAngle = Math.Atan2(rotPointDistY, rotPointDistX);
            var newRotPointLocation = new CGPoint(Math.Cos(rotPointAngle + rotation) * rotPointDist, Math.Sin(rotPointAngle + rotation) * rotPointDist); // "rotate this distance" around the center

            // the correction is the new difference between the original rotation point X/Y and the new ones
            // See iOSRotationTranslation in the documentation
            var rotationCorrectionX = rotPointDistX - newRotPointLocation.X;
            var rotationCorrectionY = rotPointDistY - newRotPointLocation.Y;
            shape.ZRotation = -(nfloat)rotation;


            shape.Position = new CGPoint(_root.X + renderBoundingBox.Width / 2 + renderBoundingBox.X + rotationCorrectionX, _root.Y - renderBoundingBox.Height / 2 - renderBoundingBox.Y - rotationCorrectionY);
        }

        #endregion

        #region IRenderer Implementation

        public void RenderBegin() {
            //RemoveAllChildren();
            _root = ConvertPointFromView(new CGPoint(Frame.Top, Frame.Left));
        }

        public void RenderEnd() {

        }

        public void Render(IRenderable target, Rectangle renderBoundingBox, double rotation, Point rotationPoint, Rectangle rotatedBoundingBox) {
            // include guards
            if (target == null) throw new ArgumentNullException(nameof(target));
            if (renderBoundingBox.Width < 0 || renderBoundingBox.Height < 0) throw new ArgumentException(@"RenderBoundingBox is invalid", nameof(renderBoundingBox));

            SKNode existingNode;
            _nodeMap.TryGetValue(target.Id, out existingNode);
                // don't render if outside of the part the user can actually see
            if (rotatedBoundingBox.X > Frame.Width || rotatedBoundingBox.Y > Frame.Height)
            {
                if (existingNode != null) existingNode.Hidden = true;
                return;
            }
            if (rotatedBoundingBox.X + rotatedBoundingBox.Width < 0 || rotatedBoundingBox.Y + rotatedBoundingBox.Height < 0)
            {
                if (existingNode != null) existingNode.Hidden = true;
                return;
            }
            if (existingNode != null) existingNode.Hidden = false;

            // depending on the SvgItem type, call a different Render method
            if (target.GetType() == typeof(SvgRect)) RenderRect(target as SvgRect, renderBoundingBox, rotation, rotationPoint);
            else throw new ArgumentException("SvgItem has unknown type");
        }

        #endregion

        public void InvokeRenderCycle() {
            if (SvgStage == null) return;
            _rendererHandler.Render(SvgStage);
        }
    }
}

