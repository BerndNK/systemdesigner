﻿using System;
using System.ComponentModel;
using System.Drawing;
using SystemDesigneriOS.Views;
using CoreFoundation;
using UIKit;
using Foundation;

namespace SystemDesigneriOS.ViewController
{
    [Register("DocumentRootView"), DesignTimeVisible(true)]
    public class DocumentRootView : UIView
    {
       // public SvgStageView SvgStageView { get; set; }
        //public ToolView ToolView { get; set; }

        public DocumentRootView() 
        {
            Initialize();
        }

        public DocumentRootView(RectangleF bounds) : base(bounds)
        {
            Initialize();
        }


        void Initialize()
        {
            BackgroundColor = UIColor.Red;
            var SvgStageView = new SvgStageView();
            var ToolView = new ToolView();


			var blurEffect = UIBlurEffect.FromStyle(UIBlurEffectStyle.Light);
			var blurView = new UIVisualEffectView(blurEffect);

            ToolView.TranslatesAutoresizingMaskIntoConstraints = false;
			SvgStageView.TranslatesAutoresizingMaskIntoConstraints = false;
			blurView.TranslatesAutoresizingMaskIntoConstraints = false;


			SvgStageView.BackgroundColor = UIColor.White;

			//Add(SvgStageView);
			AddSubview(SvgStageView);
            // add blur if the reduceMotion setting is disabled
            if (!UIAccessibility.IsReduceMotionEnabled)
            {

				AddSubview(blurView);

				blurView.WidthAnchor.ConstraintEqualTo(WidthAnchor).Active = true;
				blurView.HeightAnchor.ConstraintEqualTo(50).Active = true;
            }
            else // otherwise just set the background of the toolView to plain white
                ToolView.BackgroundColor = UIColor.White;

			ToolView.BackgroundColor = UIColor.Red;

			AddSubview(ToolView);




			ToolView.WidthAnchor.ConstraintEqualTo(WidthAnchor).Active = true;
			ToolView.HeightAnchor.ConstraintEqualTo(30).Active = true;
            ToolView.TopAnchor.ConstraintEqualTo(TopAnchor, 20).Active = true;

			//SvgStageView.TopAnchor.ConstraintEqualTo(TopAnchor, 0).Active = true;
			//SvgStageView.BottomAnchor.ConstraintEqualTo(BottomAnchor, 0).Active = true;
			SvgStageView.WidthAnchor.ConstraintEqualTo(WidthAnchor).Active = true;
            SvgStageView.HeightAnchor.ConstraintEqualTo(HeightAnchor).Active = true;
            SvgStageView.TopAnchor.ConstraintEqualTo(ToolView.TopAnchor).Active = true;
		}

        public override void LayoutSubviews()
        {
            //SvgStageView.LayoutSubviews();
            base.LayoutSubviews();
        }
    }

    [Register("DocumentViewController"), DesignTimeVisible(true)]
    public class DocumentViewController : UIViewController
    {
        public DocumentViewController(IntPtr handle) : base(handle)
        {
        }



        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            //View = new DocumentRootView();
            View = new DocumentRootView();
            View.BackgroundColor = UIColor.LightGray;
            base.ViewDidLoad();
            // Perform any additional setup after loading the view
        }
    }
}