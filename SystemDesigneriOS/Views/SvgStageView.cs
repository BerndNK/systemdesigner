﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using SystemDesigner.Input;
using SystemDesigner.Stage;
using SystemDesigneriOS.Extensions;
using CoreAnimation;
using CoreGraphics;
using DisplayCore.Render;
using DisplayCore.Render.Geometry;
using Foundation;
using SpriteKit;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms;
using UIKit;

namespace SystemDesigneriOS.Views
{
    [Register("SvgStageView"), DesignTimeVisible(true)]
    public class SvgStageView : UIView, IRenderer
    {

        #region Properties

        /// <summary>
        /// Gets or sets the SVG stage.
        /// </summary>
        /// <value>
        /// The SVG stage.
        /// </value>
        public IStage<ISvgItem> SvgStage {
            get { return _svgStage; }
            set {
                var oldStage = _svgStage;
                _svgStage = value;
                OnSvgStageChanged(oldStage);
            }
        }

        #endregion


        #region Fields

        /// <summary>
        /// The renderer handler
        /// </summary>
        private readonly RendererHandler _rendererHandler;

        /// <summary>
        /// The SVG stage backing fields
        /// </summary>
        private IStage<ISvgItem> _svgStage;
        //TODO remove this from this scene class and put it into the controller
        private SvgStageHandler _stageHandler;
        private double _pinchStartScale;
        private CGPoint _root;

        /// <summary>
        /// Gets or sets the canvas children map. This is used to just apply property updates, instead of removing/adding new display objects each Render call
        /// </summary>
        private Dictionary<int, CALayer> _layerMap;

        private CGRect _dimensions; // dimensions of this view;


        /// <summary>
        /// Index used to track whether the order of rendering has changed. (So an item moved in front or back another items, or an item has been deleted)
        /// </summary>
        private int _currentExpectedChildIndex;

        /// <summary>
        /// The display objects that were removed in order to fix the rendering order.
        /// </summary>
        private List<int> _removedChildren;

        /// <summary>
        /// The expected rendering order of the various item id's. (This represents in which order display objects was last rendered)
        /// </summary>
        private List<int> _expectedRenderingOrder;

        /// <summary>
        /// Whether the rendering order has changed.
        /// </summary>
        private bool _renderingOrderHasChanged;

        #endregion

        public SvgStageView(IntPtr handle) : base(handle)
        {
            _rendererHandler = new RendererHandler();
            _rendererHandler.Renderer = this;
            _layerMap = new Dictionary<int, CALayer>();
            _removedChildren = new List<int>();
            _expectedRenderingOrder = new List<int>();
        }

        public SvgStageView(){

			_rendererHandler = new RendererHandler();
			_rendererHandler.Renderer = this;
			_layerMap = new Dictionary<int, CALayer>();
            _removedChildren = new List<int>();
            _expectedRenderingOrder = new List<int>();
            Initialize();

		}

        public override void AwakeFromNib()
        {
            Initialize();
            base.AwakeFromNib();
        }

        private void Initialize()
        {
            BackgroundColor = UIColor.White;

            _stageHandler = new SvgStageHandler();
            _stageHandler.StageList.Add(new SystemDesigner.Stage.SvgStage());
            //_stageHandler.AddTestShapes();
            // _stageHandler.AddTestShapes();
            SvgStage = _stageHandler.ActiveStage;
            MultipleTouchEnabled = true;
            //_rendererHandler.Render(SvgStage);

            //AddGestureRecognizer(new UIPanGestureRecognizer(OnPanGesture));
            //AddGestureRecognizer(new UIPinchGestureRecognizer(OnPinchGesture));
            AddGestureRecognizer(new UITapGestureRecognizer(OnTripleTouchGesture) { NumberOfTouchesRequired = 3 });
            AddGestureRecognizer(new UITapGestureRecognizer(OnDoubleTapGesture) { NumberOfTapsRequired = 2 });
            //AddGestureRecognizer(new UIRotationGestureRecognizer(OnRotateGesture));

            _stageHandler.AddTestShapes();
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            // Called when a touch begins
            foreach (var touch in touches)
            {
                var location = ((UITouch) touch).LocationInView(this);
                
               // var translatedLocation = ConvertPointFromView(location);
                _stageHandler.ActiveStage.OnInputDown(new Point(location.X, location.Y), ((UITouch)touch).Type == UITouchType.Stylus? InputType.Pen :  InputType.Touch, touch.GetHashCode());
            }
            SetNeedsLayout();
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            // Called when a touch ends
            foreach (var touch in touches)
            {
                var location = ((UITouch)touch).LocationInView(this);
                _stageHandler.ActiveStage.OnInputUp(new Point(location.X, location.Y), ((UITouch)touch).Type == UITouchType.Stylus ? InputType.Pen : InputType.Touch, touch.GetHashCode());
            }
            SetNeedsLayout();
        }

        public override void TouchesMoved(NSSet touches, UIEvent evt)
        {
            // Called when a touch moves
            foreach (var touch in touches)
            {
                var location = ((UITouch)touch).LocationInView(this);
               // var translatedLocation = ConvertPointFromView(location);
                _stageHandler.ActiveStage.OnInputMove(new Point(location.X, location.Y), ((UITouch)touch).Type == UITouchType.Stylus ? InputType.Pen : InputType.Touch, touch.GetHashCode());
            }
            SetNeedsLayout();
        }

        public override void Draw(CGRect rect)
        {
            _dimensions = rect;
            _stageHandler?.ActiveStage?.ActiveTool?.UiContainer?.Arrange(Frame.Width, Frame.Height);
            if(SvgStage != null)
                _rendererHandler.Render(SvgStage);
            base.Draw(rect);
        }

        private void OnRotateGesture(UIRotationGestureRecognizer obj)
        {
            if (_stageHandler?.ActiveStage?.Root == null) return;
            _stageHandler.ActiveStage.Root.Rotation = obj.Rotation;
            SetNeedsLayout();
        }

        private void OnDoubleTapGesture(UITapGestureRecognizer obj)
        {
            if (_stageHandler?.ActiveStage?.Root == null) return;
            if (Math.Abs(_stageHandler.ActiveStage.Root.ScaleX - 0.5) < 0.1)
            {
                _stageHandler.ActiveStage.Root.ScaleX = 1;
                _stageHandler.ActiveStage.Root.ScaleY = 1;
            }
            else
            {
                _stageHandler.ActiveStage.Root.ScaleX = 0.5;
                _stageHandler.ActiveStage.Root.ScaleY = 0.5;
            }
        }

        private void OnTripleTouchGesture(UITapGestureRecognizer obj)
        {
            _stageHandler.AddTestShapes();
        }

        private void OnPinchGesture(UIPinchGestureRecognizer obj)
        {
            if (obj.Scale < 0.1) return;
            switch (obj.State)
            {
                case UIGestureRecognizerState.Began:
                    _pinchStartScale = _stageHandler.ActiveStage.Root.ScaleX;
                    break;
                case UIGestureRecognizerState.Changed:
                    _stageHandler.ActiveStage.Root.ScaleX = _pinchStartScale * obj.Scale;
                    _stageHandler.ActiveStage.Root.ScaleY = _pinchStartScale * obj.Scale;
                    SetNeedsLayout();
                    break;
            }
        }

        private void OnPanGesture(UIPanGestureRecognizer obj)
        {
            var location = obj.LocationInView(this);
            switch (obj.State)
            {
                case UIGestureRecognizerState.Began:
                    _stageHandler.ActiveStage.OnInputDown(new Point(location.X, location.Y), InputType.Mouse, -1);
                    break;
                case UIGestureRecognizerState.Changed:
                    _stageHandler.ActiveStage.OnInputMove(new Point(location.X, location.Y), InputType.Mouse, -1);
                    SetNeedsLayout();
                    break;
                case UIGestureRecognizerState.Ended:
                    _stageHandler.ActiveStage.OnInputUp(new Point(location.X, location.Y), InputType.Mouse, -1);
                    SetNeedsLayout();
                    break;
            }
        }


        /// <summary>
        /// Called when [SVG stage changed].
        /// </summary>
        private void OnSvgStageChanged(IStage<ISvgItem> oldStage)
        {
            // remove event handler from old stage
            if (oldStage?.Root != null) oldStage.Root.ChildrenChanged -= OnStageHasChanged;

            var newRoot = SvgStage?.Root;
            if (newRoot != null) newRoot.ChildrenChanged += OnStageHasChanged;
            // _rendererHandler.Render(SvgStage);
        }

        /// <summary>
        /// Called when [stage has changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnStageHasChanged(object sender, EventArgs eventArgs)
        {
            //_rendererHandler.Render(SvgStage);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            InvokeRenderCycle();
        }


        #region VariousRenderer

        private void RenderRect(SvgRect target, Rectangle renderBoundingBox, double rotation, Point rotationPoint)
        {
            CALayer existingLayer;
            CAShapeLayer layer;
            if (_layerMap.TryGetValue(target.Id, out existingLayer))
            {
                layer = existingLayer as CAShapeLayer;
                if (layer == null) throw new Exception("Existing Node was not expected type. This should never happen.");
            }
            else
            {
                layer = new CAShapeLayer();
                Layer.AddSublayer(layer);
                layer.BackgroundColor = target.FillColor.ToCgColor();
                _layerMap.Add(target.Id, layer);

            }

            // iOS only rotation around the center (width/2, height/2) However SystemDesigner provides rotation around a rotation point. To solve this issue, we translate the resulting X/Y change a different rotation point than the center would result in.
            var rotPointDistX = rotationPoint.X - renderBoundingBox.Width / 2; // distance from center to rotation point in X
            var rotPointDistY = rotationPoint.Y - renderBoundingBox.Height / 2;
            var rotPointDist = Math.Sqrt(rotPointDistX * rotPointDistX + rotPointDistY * rotPointDistY); // diagonal distance from center to rotation point x
            var rotPointAngle = Math.Atan2(rotPointDistY, rotPointDistX);
            var newRotPointLocation = new CGPoint(Math.Cos(rotPointAngle + rotation) * rotPointDist, Math.Sin(rotPointAngle + rotation) * rotPointDist); // "rotate this distance" around the center

            // the correction is the new difference between the original rotation point X/Y and the new ones
            // See iOSRotationTranslation in the documentation
            var rotationCorrectionX = rotPointDistX - newRotPointLocation.X;
            var rotationCorrectionY = rotPointDistY - newRotPointLocation.Y;
            //layer.ZRotation = -(nfloat)rotation;
            CATransaction.Begin();
            CATransaction.DisableActions = true;
            //layer.Bounds = new CGRect(0,0,renderBoundingBox.Width, renderBoundingBox.Height);
            //layer.Position = new CGPoint((int)(renderBoundingBox.X + renderBoundingBox.Width / 2), (int)(renderBoundingBox.Y + renderBoundingBox.Height / 2));

            layer.Position = new CGPoint((int)(renderBoundingBox.X + renderBoundingBox.Width / 2 + rotationCorrectionX), (int)(renderBoundingBox.Height / 2 + renderBoundingBox.Y + rotationCorrectionY));
            layer.StrokeColor = target.LineColor.ToCgColor();
            layer.LineWidth = (nfloat)target.LineThickness;
            layer.Bounds = new CGRect(0, 0, (int)renderBoundingBox.Width, (int)renderBoundingBox.Height);

            layer.Transform = CATransform3D.MakeRotation((nfloat)rotation, 0, 0, (nfloat)1.0);
            //layer.Position = new CGPoint(renderBoundingBox.X, renderBoundingBox.Y);

            CATransaction.Commit();

            //layer.Position = new CGPoint(_root.X + renderBoundingBox.Width / 2 + renderBoundingBox.X + rotationCorrectionX, _root.Y - renderBoundingBox.Height / 2 - renderBoundingBox.Y - rotationCorrectionY);


        }

        private void RenderEllipse(SvgEllipse target, Rectangle renderBoundingBox, double rotation, Point rotationPoint)
        {
            CALayer existingLayer;
            CAShapeLayer layer;
            if (_layerMap.TryGetValue(target.Id, out existingLayer))
            {
                layer = existingLayer as CAShapeLayer;
                if (layer == null) throw new Exception("Existing Node was not expected type. This should never happen.");
            }
            else
            {
                layer = new CAShapeLayer();
                Layer.AddSublayer(layer);
                // use a path from a rectangle with rounded corners, to create the circle
                layer.Path = UIBezierPath.FromRoundedRect(new CGRect(0, 0, target.Radius * 2, target.Radius * 2), (nfloat)target.Radius).CGPath;
                layer.Bounds = new CGRect(0, 0, target.Radius * 2, target.Radius * 2);
                _layerMap.Add(target.Id, layer);

            }

            // iOS only rotation around the center (width/2, height/2) However SystemDesigner provides rotation around a rotation point. To solve this issue, we translate the resulting X/Y change a different rotation point than the center would result in.
            var rotPointDistX = rotationPoint.X - renderBoundingBox.Width / 2; // distance from center to rotation point in X
            var rotPointDistY = rotationPoint.Y - renderBoundingBox.Height / 2;
            var rotPointDist = Math.Sqrt(rotPointDistX * rotPointDistX + rotPointDistY * rotPointDistY); // diagonal distance from center to rotation point x
            var rotPointAngle = Math.Atan2(rotPointDistY, rotPointDistX);
            var newRotPointLocation = new CGPoint(Math.Cos(rotPointAngle + rotation) * rotPointDist, Math.Sin(rotPointAngle + rotation) * rotPointDist); // "rotate this distance" around the center

            // the correction is the new difference between the original rotation point X/Y and the new ones
            // See iOSRotationTranslation in the documentation
            var rotationCorrectionX = rotPointDistX - newRotPointLocation.X;
            var rotationCorrectionY = rotPointDistY - newRotPointLocation.Y;
            //layer.ZRotation = -(nfloat)rotation;
            CATransaction.Begin();
            CATransaction.DisableActions = true;
            layer.Position = new CGPoint((int)(renderBoundingBox.X + renderBoundingBox.Width/2 + rotationCorrectionX), (int)(renderBoundingBox.Height/2 + renderBoundingBox.Y + rotationCorrectionY));

            var rotTrans = CATransform3D.MakeRotation((nfloat)rotation, 0, 0, (nfloat)1.0);
            layer.Transform = rotTrans.Scale((nfloat)renderBoundingBox.Width/ layer.Bounds.Width, (nfloat)renderBoundingBox.Height/ layer.Bounds.Height, 1);
            layer.LineWidth = (nfloat)target.LineThickness;
            layer.StrokeColor = target.LineColor.ToCgColor();

            layer.FillColor = target.FillColor.ToCgColor();

            CATransaction.Commit();

        }
        private void RenderImage(SvgImage target, Rectangle renderBoundingBox, double rotation, Point rotationPoint)
        {
            CALayer existingLayer;
            CALayer layer;
            if (_layerMap.TryGetValue(target.Id, out existingLayer))
            {
                layer = existingLayer as CALayer;
                if (layer == null) throw new Exception("Existing Node was not expected type. This should never happen.");
            }
            else
            {
                layer = new CALayer();
                Layer.AddSublayer(layer);
                if (System.IO.File.Exists(target.ImageSource)) {
                    
					var img = new UIImage(target.ImageSource)?.CGImage;
					layer.Contents = img;
                }else{
                    // on error loading the image, use a background color instead
                    layer.BackgroundColor = new CGColor(1, 0, 0, 0.5f);
                }

                _layerMap.Add(target.Id, layer);

            }

            // iOS only rotation around the center (width/2, height/2) However SystemDesigner provides rotation around a rotation point. To solve this issue, we translate the resulting X/Y change a different rotation point than the center would result in.
            var rotPointDistX = rotationPoint.X - renderBoundingBox.Width / 2; // distance from center to rotation point in X
            var rotPointDistY = rotationPoint.Y - renderBoundingBox.Height / 2;
            var rotPointDist = Math.Sqrt(rotPointDistX * rotPointDistX + rotPointDistY * rotPointDistY); // diagonal distance from center to rotation point x
            var rotPointAngle = Math.Atan2(rotPointDistY, rotPointDistX);
            var newRotPointLocation = new CGPoint(Math.Cos(rotPointAngle + rotation) * rotPointDist, Math.Sin(rotPointAngle + rotation) * rotPointDist); // "rotate this distance" around the center

            // the correction is the new difference between the original rotation point X/Y and the new ones
            // See iOSRotationTranslation in the documentation
            var rotationCorrectionX = rotPointDistX - newRotPointLocation.X;
            var rotationCorrectionY = rotPointDistY - newRotPointLocation.Y;

            CATransaction.Begin();
            CATransaction.DisableActions = true;
            layer.Position = new CGPoint((int)(renderBoundingBox.X + renderBoundingBox.Width / 2 + rotationCorrectionX), (int)(renderBoundingBox.Height / 2 + renderBoundingBox.Y + rotationCorrectionY));

            layer.Bounds = new CGRect(0, 0, (int)renderBoundingBox.Width, (int)renderBoundingBox.Height);

            layer.Transform = CATransform3D.MakeRotation((nfloat)rotation, 0, 0, (nfloat)1.0);
            CATransaction.Commit();
        }

        #endregion

        #region IRenderer Implementation

        public void RenderBegin()
        {
            //RemoveAllChildren();
            _root = ConvertPointFromView(new CGPoint(Frame.Top, Frame.Left), this);
            // reset the expected rendering index, removed children and flags
            _currentExpectedChildIndex = 0;
            _renderingOrderHasChanged = false;
            _removedChildren.Clear();
        }


        public void RenderEnd()
        {
            // remove all cached items, that did not get rendered this cycle (which means they have been deleted from the rendering tree)
            if (_renderingOrderHasChanged)
            {
                // get all ids that have been cached
                var allCachedIds = _layerMap.Keys.ToArray();
                // now only select the id's which have not been rendered this cycle
                var idsThatHaveNotBeenRendered = allCachedIds.Where(x => !_expectedRenderingOrder.Contains(x));

                // expectedRenderingOrder contains every ID that has been rendered
                foreach (var id in idsThatHaveNotBeenRendered)
                {
                    // only remove the Id's from the dictionary. The canvas doesn't contain them anymore, since they have already been removed in the Render method.
                    if (_layerMap.ContainsKey(id)) _layerMap.Remove(id);
                }
            }
        }

        public void Render(IRenderable target, Rectangle renderBoundingBox, double rotation, Point rotationPoint, Rectangle rotatedBoundingBox)
        {
            // include guards
            if (target == null) throw new ArgumentNullException(nameof(target));
            if (renderBoundingBox.Width < 0 || renderBoundingBox.Height < 0) throw new ArgumentException(@"RenderBoundingBox is invalid", nameof(renderBoundingBox));

            CALayer existingLayer;

			CATransaction.Begin();
			CATransaction.DisableActions = true;
            if (_currentExpectedChildIndex >= _expectedRenderingOrder.Count)
                _renderingOrderHasChanged = true;

            // check if the expected next id matches
            if (!_renderingOrderHasChanged && _expectedRenderingOrder[_currentExpectedChildIndex] != target.Id)
            {
                // if not, this means that the rendering order has changed. Remove all items from this point on from the rendering tree.
                for (var i = _currentExpectedChildIndex; i < _expectedRenderingOrder.Count; i++)
                {
                    if (_layerMap.TryGetValue(_expectedRenderingOrder[i], out existingLayer))
                    {
                        existingLayer.RemoveFromSuperLayer(); // remove the child from the rendering tree (of the canvas)
                        _removedChildren.Add(_expectedRenderingOrder[i]); // add the removed id (so we can restore it later)
                    }

                }

                _expectedRenderingOrder.RemoveRange(_currentExpectedChildIndex, _expectedRenderingOrder.Count - _currentExpectedChildIndex);
                _renderingOrderHasChanged = true;
            }

            // check if a element already is rendered for the id
            _layerMap.TryGetValue(target.Id, out existingLayer);

            // when the rendering order has changed, add the id to the expected list (so we correct the list)
            if (_renderingOrderHasChanged)
            {
                _expectedRenderingOrder.Add(target.Id);
                if (_removedChildren.Contains(target.Id) && existingLayer != null)
                    Layer.AddSublayer(existingLayer);
            }
            else ++_currentExpectedChildIndex; // otherwise increment the expected index for next call

            if (existingLayer != null)
            {
                // don't render if outside of the part the user can actually see. However if there is not layer present at this time, render it for once
                if (!_renderingOrderHasChanged && rotatedBoundingBox.X > Frame.Width || rotatedBoundingBox.Y > Frame.Height)
                {
                    existingLayer.Hidden = true;
                    return;
                }
                if (!_renderingOrderHasChanged && rotatedBoundingBox.X + rotatedBoundingBox.Width < 0 || rotatedBoundingBox.Y + rotatedBoundingBox.Height < 0)
                {
                    existingLayer.Hidden = true;
                    return;
                }
                existingLayer.Hidden = false;
			}
			CATransaction.Commit();

            // depending on the SvgItem type, call a different Render method
            if (target.GetType() == typeof(SvgRect)) RenderRect(target as SvgRect, renderBoundingBox, rotation, rotationPoint);
            else if (target.GetType() == typeof(SvgEllipse)) RenderEllipse(target as SvgEllipse, renderBoundingBox, rotation, rotationPoint);
            else if (target.GetType() == typeof(SvgImage)) RenderImage(target as SvgImage, renderBoundingBox, rotation, rotationPoint);
            //else throw new ArgumentException("SvgItem has unknown type");
        }

        #endregion

        public void InvokeRenderCycle()
        {
            if (SvgStage == null) return;
            _rendererHandler.Render(SvgStage);
        }
    }
}
