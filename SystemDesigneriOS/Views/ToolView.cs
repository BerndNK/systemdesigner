﻿using System;
using System.ComponentModel;
using CoreGraphics;
using Foundation;
using UIKit;

namespace SystemDesigneriOS.Views
{
	[Register("ToolView"), DesignTimeVisible(true)]
	public class ToolView : UIStackView
	{

		#region Properti

		#endregion


		#region Fields


		#endregion

		public ToolView(IntPtr handle) : base(handle)
		{
		}

        public ToolView(){
        }

		public override void AwakeFromNib()
		{
			Initialize();
			base.AwakeFromNib();
		}

		private void Initialize()
		{
            Axis = UILayoutConstraintAxis.Horizontal;
            //Distribution = UIStackViewDistribution.EqualSpacing;
            Alignment = UIStackViewAlignment.Center;
			var btn1 = new UIButton();
			btn1.SetImage(new UIImage("icons8-Cursor-100"), UIControlState.Normal);
			btn1.TranslatesAutoresizingMaskIntoConstraints = false;
			btn1.WidthAnchor.ConstraintEqualTo(30).Active = true;
			btn1.HeightAnchor.ConstraintEqualTo(30).Active = true;
            

            var btn2 = new UIButton();
            btn2.SetImage(new UIImage("icons8-Pencil-100"), UIControlState.Normal);
            btn2.TranslatesAutoresizingMaskIntoConstraints = false;
            btn2.WidthAnchor.ConstraintEqualTo(30).Active = true;
            btn2.HeightAnchor.ConstraintEqualTo(30).Active = true;

            var fillView = new UIView();

            AddArrangedSubview(btn1);
			AddArrangedSubview(btn2);
			AddArrangedSubview(fillView);
		}



		public override void Draw(CGRect rect)
		{
            Initialize();
			base.Draw(rect);
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();
		}


	}
}
