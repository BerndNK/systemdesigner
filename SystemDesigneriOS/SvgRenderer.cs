﻿using System;
using System.Globalization;
using SystemDesigner.Stage;
using SystemDesigneriOS.Extensions;
using CoreGraphics;
using DisplayCore.Render;
using DisplayCore.Render.Geometry;
using Foundation;
using SpriteKit;
using SvgAbstraction.Svg;
using SvgAbstraction.Svg.Forms;
using UIKit;

namespace SystemDesigneriOS {
    public class SvgRenderer : SKScene, IRenderer {

        #region Properties

        /// <summary>
        /// Gets or sets the SVG stage.
        /// </summary>
        /// <value>
        /// The SVG stage.
        /// </value>
        public IStage<ISvgItem> SvgStage {
            get { return _svgStage; }
            set {
                var oldStage = _svgStage;
                _svgStage = value;
                OnSvgStageChanged(oldStage);
            }
        }

        #endregion


        #region Fields

        /// <summary>
        /// The renderer handler
        /// </summary>
        private readonly RendererHandler _rendererHandler;

        /// <summary>
        /// The SVG stage backing fields
        /// </summary>
        private IStage<ISvgItem> _svgStage;

        #endregion


        public SvgRenderer(IntPtr handle) : base(handle) {
            
        }
        public override void DidMoveToView(SKView view) {
            // Setup your scene here
            var myLabel = new SKLabelNode("SanFranciscoDisplay-Bold ") {
                Text = "Test",
                FontSize = 65,
                Position = new CGPoint(Frame.Width / 2, Frame.Height / 2)
            };

            var sprite = new SKSpriteNode(UIColor.Blue, new CGSize(100, 100));
            sprite.Position = new CGPoint(myLabel.Position.X - 100, myLabel.Position.Y);
            var shape = SKShapeNode.FromRect(new CGRect(0, 0, 100, 100));
            shape.Name = "Test2";
            shape.Position = myLabel.Position;
            shape.FillColor = UIColor.Brown;
            AddChild(shape);
            AddChild(sprite);
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt) {
            // Called when a touch begins
            foreach (var touch in touches) {
                var location = ((UITouch)touch).LocationInNode(this);

                var sprite = new SKSpriteNode("Spaceship") {
                    Position = location,
                    XScale = 0.5f,
                    YScale = 0.5f
                };

                var action = SKAction.RotateByAngle(NMath.PI, 1.0);

                sprite.RunAction(SKAction.RepeatActionForever(action));

                AddChild(sprite);
            }
        }

        public override void Update(double currentTime) {
            // Called before each frame is rendered
        }
        /*
        public override void DidMoveToView(SKView view) {
            _rendererHandler.Render(SvgStage);
        }*/

        /// <summary>
        /// Called when [SVG stage changed].
        /// </summary>
        private void OnSvgStageChanged(IStage<ISvgItem> oldStage) {
            // remove event handler from old stage
            if (oldStage?.Root != null) oldStage.Root.ChildrenChanged -= OnStageHasChanged;

            var newRoot = SvgStage?.Root;
            if (newRoot != null) newRoot.ChildrenChanged += OnStageHasChanged;

            _rendererHandler.Render(SvgStage);
        }

        /// <summary>
        /// Called when [stage has changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnStageHasChanged(object sender, EventArgs eventArgs) {
            _rendererHandler.Render(SvgStage);
        }


        #region VariousRenderer

        private void RenderRect(SvgRect target, Rectangle renderBoundingBox, double rotation, Point rotationPoint) {
            var shape = SKShapeNode.FromRect(renderBoundingBox.ToCgRectIgnoreXy());
            shape.FillColor = target.FillColor.ToUiColor();
            shape.FillColor = shape.FillColor.ColorWithAlpha(nfloat.Parse(target.Alpha.ToString(CultureInfo.InvariantCulture)));
            shape.Position = new CGPoint(renderBoundingBox.X, renderBoundingBox.Y);
            AddChild(shape);
        }

        #endregion

        #region IRenderer Implementation

        public void RenderBegin() {
            RemoveAllChildren();
        }

        public void RenderEnd() {
            
        }

        public void Render(IRenderable target, Rectangle renderBoundingBox, double rotation, Point rotationPoint, Rectangle rotatedBoundingBox) {
            // include guards
            if (target == null) throw new ArgumentNullException(nameof(target));
            if (renderBoundingBox.Width < 0 || renderBoundingBox.Height < 0) throw new ArgumentException(@"RenderBoundingBox is invalid", nameof(renderBoundingBox));
            // don't render if outside of the part the user can actually see
            if (renderBoundingBox.X > Frame.Width || renderBoundingBox.Y > Frame.Height) return;
            if (renderBoundingBox.X + renderBoundingBox.Width < 0 || renderBoundingBox.Y + renderBoundingBox.Height < 0) return;

            // depending on the SvgItem type, call a different Render method
            if (target.GetType() == typeof(SvgRect)) RenderRect(target as SvgRect, renderBoundingBox, rotation, rotationPoint);
             else throw new ArgumentException("SvgItem has unknown type");
        }

        #endregion
    }
}
