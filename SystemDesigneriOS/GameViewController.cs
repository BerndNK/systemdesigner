using System;
using CoreGraphics;
using SpriteKit;
using UIKit;

namespace SystemDesigneriOS {
    public partial class GameViewController : UIViewController {
        private GameScene _scene;
        public GameViewController(IntPtr handle) : base(handle) {
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            // Configure the view.
            var skView = (SKView)View;
            skView.ShowsFPS = true;
            skView.ShowsNodeCount = true;
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.IgnoresSiblingOrder = true;

            // Create and configure the scene.
            _scene = SKNode.FromFile<GameScene>("GameScene");
            _scene.BackgroundColor = UIColor.White;
            _scene.ScaleMode = SKSceneScaleMode.ResizeFill;

            // Present the scene.
            skView.PresentScene(_scene);
        }
        
        public override void ViewDidLayoutSubviews() {
            //_scene?.InvokeRenderCycle();
        }

        public override bool ShouldAutorotate() {
            return true;
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations() {
            return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone ? UIInterfaceOrientationMask.AllButUpsideDown : UIInterfaceOrientationMask.All;
        }

        public override void DidReceiveMemoryWarning() {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public override bool PrefersStatusBarHidden() {
            return true;
        }
    }
}

