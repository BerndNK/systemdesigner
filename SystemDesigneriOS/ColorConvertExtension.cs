﻿using System;
using UIKit;

namespace SystemDesigneriOS.Extensions {
    public static class ColorConvertExtension {
        public static UIColor ToUiColor(this UInt32 color) => UIColor.FromRGB((byte)(color >> 16 & 0xff), (byte)(color >> 8 & 0xff), (byte)(color & 0xff));
    }
}
