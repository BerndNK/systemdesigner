﻿using System;
using CoreGraphics;
using UIKit;

namespace SystemDesigneriOS.Extensions {
    public static class ColorConvertExtension {
        public static UIColor ToUiColor(this UInt32 color) => UIColor.FromRGB((byte)(color >> 16 & 0xff), (byte)(color >> 8 & 0xff), (byte)(color & 0xff));
        public static CGColor ToCgColor(this UInt32 color)
        {
            return new CGColor((byte)(color >> 16 & 0xff) / (nfloat)255, (byte)(color >> 8 & 0xff) / (nfloat)255, (byte)(color & 0xff) / (nfloat)255, (byte)(color >> 24 & 0xff) / (nfloat)255);
        }

        
    }
}
