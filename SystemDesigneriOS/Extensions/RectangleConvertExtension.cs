﻿using CoreGraphics;
using DisplayCore.Render.Geometry;

namespace SystemDesigneriOS.Extensions {
    public static class RectangleConvertExtension {
        public static CGRect ToCgRect(this Rectangle rect) => new CGRect(rect.X, rect.Y, rect.Width, rect.Height);

        public static CGRect ToCgRectIgnoreXy(this Rectangle rect) => new CGRect(0, 0, rect.Width, rect.Height);
    }
}
