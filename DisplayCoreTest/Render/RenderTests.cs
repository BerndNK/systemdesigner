﻿using System;
using System.Collections.Generic;
using DisplayCore.Display;
using DisplayCore.Render;
using DisplayCore.Render.Geometry;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DisplayCoreTest.Render {
    [TestClass]
    public class RenderTests {
        [TestMethod]
        public void RendererHandler_RenderCallTest_Success() {
            // arrange
            
            var rootMock = new Mock<DisplayObjectContainer<DisplayObject>>() {CallBase = true};
            var root = rootMock.Object;
            var rectMock = new Mock<DisplayObject>() {CallBase = true};
            rectMock.Setup(x => x.Width).Returns(100);
            rectMock.Setup(x => x.Height).Returns(100);
            var rect = rectMock.Object;
            var rect2Mock = new Mock<DisplayObject>() { CallBase = true };
            rect2Mock.Setup(x => x.Width).Returns(100);
            rect2Mock.Setup(x => x.Height).Returns(100);
            var rect2 = rect2Mock.Object;
            root.AddMultiple(new List<DisplayObject> { rect, rect2 });

            var renderHandler = new RendererHandler();
            var mock = new Mock<IRenderer>();
            
            renderHandler.Renderer = mock.Object;
            // act
            renderHandler.Render(root);

            // assert
            mock.Verify(x => x.RenderBegin(), Times.Once);
            mock.Verify(x => x.RenderEnd(), Times.Once);
            mock.Verify(x => x.Render(rect, It.Is<Rectangle>(y => y.ToString() == "X: 0 Y: 0 W: 100 H: 100"), 0, It.Is<Point>(p => p.X <= 0.01 && p.Y <= 0.01), It.Is<Rectangle>(y => y.ToString() == "X: 0 Y: 0 W: 100 H: 100"),1), Times.Once); // check parameter with string, since rectangle uses dynamic which is not allowed in a tree expression
            mock.Verify(x => x.Render(rect2, It.Is<Rectangle>(y => y.ToString() == "X: 0 Y: 0 W: 100 H: 100"), 0, It.Is<Point>(p => p.X <= 0.01 && p.Y <= 0.01), It.Is<Rectangle>(y => y.ToString() == "X: 0 Y: 0 W: 100 H: 100"),1), Times.Once);
        }
        
        [TestMethod]
        public void RendererHandler_RenderCallGroupTest_Success() {
            // arrange
            var rootMock = new Mock<DisplayObjectContainer<DisplayObject>>() { CallBase = true };
            var root = rootMock.Object;
            root.X = 50;
            root.Y = 50;
            root.ScaleX = 2;
            root.ScaleY = 2;

            var rectMock = new Mock<DisplayObject>() {CallBase = true};
            var rect = rectMock.Object;
            rect.X = 50;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;
            rect.ScaleX = 1.5;
            rect.ScaleY = 2.5;

            root.Add(rect);

            var renderHandler = new RendererHandler();
            var renderer = new Mock<IRenderer>();

            renderHandler.Renderer = renderer.Object;
            // act
            renderHandler.Render(root);
            
            // assert
            renderer.Verify(x => x.RenderBegin(), Times.Once);
            renderer.Verify(x => x.RenderEnd(), Times.Once);
            
            renderer.Verify(x => x.Render(rect, It.Is<Rectangle>(y => y.ToString() == "X: 150 Y: 150 W: 300 H: 500"), 0, It.Is<Point>(p => p.X <= 0.01 && p.Y <= 0.01), It.Is<Rectangle>(y => y.ToString() == "X: 150 Y: 150 W: 300 H: 500"),1), Times.Once);
            //renderer.Verify(x => x.Render(rect, new Rectangle(150, 150, 300, 500), 0), Times.Once);
        }
        
        [TestMethod]
        public void RendererHandler_RenderNestedGroupes_Success() {
            // arrange
            var group1Mock = new Mock<DisplayObjectContainer<DisplayObject>>() {CallBase = true};
            var group1 = group1Mock.Object;
            group1.X = 50;
            group1.Y = 50;
            group1.ScaleX = 2;
            group1.ScaleY = 1;

            var group2Mock = new Mock<DisplayObjectContainer<DisplayObject>>() { CallBase = true };
            var group2 = group2Mock.Object;
            group2.X = 100;
            group2.Y = 100;
            group2.ScaleX = 1;
            group2.ScaleY = 2;

            var group3Mock = new Mock<DisplayObjectContainer<DisplayObject>>() { CallBase = true };
            var group3 = group3Mock.Object;
            group3.X = 200;
            group3.Y = 200;
            group3.ScaleX = 2;
            group3.ScaleY = 2;

            var rectMock = new Mock<DisplayObject>() {CallBase = true};
            var rect = rectMock.Object;
            rect.X = 50;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;

            group3.Add(rect);
            group2.Add(group3);
            group1.Add(group2);

            var renderHandler = new RendererHandler();
            var renderer = new Mock<IRenderer>();
            renderHandler.Renderer = renderer.Object;

            // act
            renderHandler.Render(group1);

            // assert
            renderer.Verify(x => x.RenderBegin(), Times.Once);
            renderer.Verify(x => x.RenderEnd(), Times.Once);
            // X = 50 + 100 * 2 + 200 * 2 * 1 + 50 * 2 * 1 * 2 = 850
            // Y = 50 + 100 * 1 + 200 * 1 * 2 + 50 * 1 * 2 * 2 = 750
            // W = 100 * 2 * 1 * 2 = 400
            // H = 100 * 1 * 2 * 2 = 400
            
            renderer.Verify(x => x.Render(rect, It.Is<Rectangle>(y => y.ToString() == "X: 850 Y: 750 W: 400 H: 400"), 0, It.Is<Point>(p => p.X <= 0.01 && p.Y <= 0.01), It.Is<Rectangle>(y => y.ToString() == "X: 850 Y: 750 W: 400 H: 400"),1), Times.Once);
            //renderer.Verify(x => x.Render(rect, new Rectangle(850, 750, 400, 400), 0), Times.Once);

        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RendererHandler_RenderWithNoRenderer_ExceptionThrown() {
            var renderHandler = new RendererHandler();
            var groupMock = new Mock<DisplayObjectContainer<DisplayObject>>() {CallBase = true};
            renderHandler.Render(groupMock.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RendererHandler_RenderWithNullRoot_ExceptionThrown() {
            var renderHandler = new RendererHandler();
            renderHandler.Render(null);
        }
    }
}
