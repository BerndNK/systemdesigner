﻿using System;
using DisplayCore.Render.Geometry;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DisplayCoreTest.Render.Geometry
{
    [TestClass]
    public class LineTests
    {
        [TestMethod]
        public void StraightLines_Intersects_Succeeds()
        {
            var lineA = new Line(new Point(10, 10), new Point(10, 30));
            var lineB = new Line(new Point(5, 20), new Point(20, 20));

            Assert.IsTrue(lineA.IntersectsWith(lineB));
            Assert.IsTrue(lineB.IntersectsWith(lineA));
        }

        [TestMethod]
        public void StraightLines_Intersects_Fails()
        {
            var lineA = new Line(new Point(10, 10), new Point(10, 30));
            var lineB = new Line(new Point(5, 20), new Point(5, 40));

            Assert.IsFalse(lineA.IntersectsWith(lineB));
            Assert.IsFalse(lineB.IntersectsWith(lineA));
        }
        
        [TestMethod]
        public void OneDiagonalOneStraightLine_Intersects_Succeeds()
        {
            var lineA = new Line(new Point(10, 10), new Point(10, 30));
            var lineB = new Line(new Point(5, 20), new Point(20, 40));

            Assert.IsTrue(lineA.IntersectsWith(lineB));
            Assert.IsTrue(lineB.IntersectsWith(lineA));
        }
        
        [TestMethod]
        public void DiagonalLines_Intersects_Succeeds()
        {
            var lineA = new Line(new Point(10, 10), new Point(12, 70));
            var lineB = new Line(new Point(5, 20), new Point(20, 80));

            Assert.IsTrue(lineA.IntersectsWith(lineB));
            Assert.IsTrue(lineB.IntersectsWith(lineA));
        }

        [TestMethod]
        public void SmallerLength25Percent_Succeeds()
        {
            var line = new Line(new Point(0, 0), new Point(100,100));
            var smallerLine = line.WithSmallerLength(0.25);

            Assert.AreEqual(smallerLine.LineOrigin, line.LineOrigin);
            Assert.AreEqual(Math.Round(smallerLine.LineEnd.X), 25);
            Assert.AreEqual(Math.Round(smallerLine.LineEnd.Y), 25);
        }

        [TestMethod]
        public void SmallerLength100Percent_Succeeds()
        {
            var line = new Line(new Point(0, 0), new Point(100,100));
            var smallerLine = line.WithSmallerLength(1.00);

            Assert.AreEqual(smallerLine.LineOrigin, line.LineOrigin);
            Assert.AreEqual(Math.Round(smallerLine.LineEnd.X), 100);
            Assert.AreEqual(Math.Round(smallerLine.LineEnd.Y), 100);
        }

        [TestMethod]
        public void SmallerLength125Percent_Succeeds()
        {
            var line = new Line(new Point(0, 0), new Point(100,100));
            var smallerLine = line.WithSmallerLength(1.25);

            Assert.AreEqual(smallerLine.LineOrigin, line.LineOrigin);
            Assert.AreEqual(Math.Round(smallerLine.LineEnd.X), 125);
            Assert.AreEqual(Math.Round(smallerLine.LineEnd.Y), 125);
        }

        [TestMethod]
        public void SmallerLength50PercentWithStraightLine_Succeeds()
        {
            var line = new Line(new Point(50, 66), new Point(150,66));
            var smallerLine = line.WithSmallerLength(0.5);

            Assert.AreEqual(smallerLine.LineOrigin, line.LineOrigin);
            Assert.AreEqual(Math.Round(smallerLine.LineEnd.X), 100);
            Assert.AreEqual(Math.Round(smallerLine.LineEnd.Y), 66);
        }
    }
}
