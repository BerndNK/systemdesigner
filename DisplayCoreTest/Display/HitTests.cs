﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DisplayCore.Display;
using DisplayCore.Render.Geometry;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DisplayCoreTest.Display {
    [TestClass]
    public class HitTests {
        [TestMethod]
        public void DisplayObject_HitTestPoint_Success() {
            var rectMock = new Mock<DisplayObject>() { CallBase = true };
            var rect = rectMock.Object;
            rect.X = 50;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;
            var rect2Mock = new Mock<DisplayObject>() { CallBase = true };
            var rect2 = rect2Mock.Object;
            rect2.Width = 100;
            rect2.Height = 100;

            // test points which are just not inside the rectangle
            Assert.IsFalse(rect.HitTestPoint(49, 49));
            Assert.IsFalse(rect.HitTestPoint(151, 100));
            Assert.IsFalse(rect.HitTestPoint(49, 100));
            Assert.IsFalse(rect.HitTestPoint(100, 151));
            // test points which are inside the rectangle
            Assert.IsTrue(rect.HitTestPoint(75, 75));
            Assert.IsTrue(rect.HitTestPoint(50, 50));
            Assert.IsTrue(rect.HitTestPoint(150, 150));

            // do the same with rect2 which has X = 0 and Y = 0
            Assert.IsFalse(rect2.HitTestPoint(-1, -1));
            Assert.IsFalse(rect2.HitTestPoint(101, 50));
            Assert.IsFalse(rect2.HitTestPoint(-1, 50));
            Assert.IsFalse(rect2.HitTestPoint(50, 101));

            Assert.IsTrue(rect2.HitTestPoint(0, 0));
            Assert.IsTrue(rect2.HitTestPoint(50, 50));
            Assert.IsTrue(rect2.HitTestPoint(100, 100));
        }

        [TestMethod]
        public void DisplayObject_HitTestLine_Success() {
            var rectMock = new Mock<DisplayObject>() { CallBase = true };
            var rect = rectMock.Object;
            rect.X = 50;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;

            var line = new Line(new Point(25,25), new Point(150,150));

            Assert.IsTrue(rect.HitTestLine(line));
        }

        [TestMethod]
        public void DisplayObject_HitTestLine_Fails() {
            var rectMock = new Mock<DisplayObject>() { CallBase = true };
            var rect = rectMock.Object;
            rect.X = 154;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;

            var line = new Line(new Point(25,25), new Point(150,150));

            Assert.IsFalse(rect.HitTestLine(line));
        }

        [TestMethod]
        public void DisplayObject_HitTestRectangle_Success() {
            var rectMock = new Mock<DisplayObject>() { CallBase = true };
            var rect = rectMock.Object;
            rect.X = 50;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;

            // test hitBoxes which just strive the rectangle
            Assert.IsFalse(rect.HitTestRect(new Rectangle(0, 0, 49, 100)));
            Assert.IsFalse(rect.HitTestRect(new Rectangle(25, 0, 24, 100)));
            Assert.IsFalse(rect.HitTestRect(new Rectangle(0, 0, 50, 49)));
            Assert.IsFalse(rect.HitTestRect(new Rectangle(151, 0, 50, 100)));
            Assert.IsFalse(rect.HitTestRect(new Rectangle(0, 151, 50, 100)));

            Assert.IsTrue(rect.HitTestRect(new Rectangle(0, 0, 50, 100)));
            Assert.IsTrue(rect.HitTestRect(new Rectangle(0, 0, 50, 50)));
            Assert.IsTrue(rect.HitTestRect(new Rectangle(50, 0, 50, 50)));
            Assert.IsTrue(rect.HitTestRect(new Rectangle(0, 50, 50, 50)));
            Assert.IsTrue(rect.HitTestRect(new Rectangle(0, 149, 50, 50)));
            Assert.IsTrue(rect.HitTestRect(new Rectangle(149, 50, 50, 50)));
        }

        [TestMethod]
        public void DisplayObjectContainer_HitTestPoint_Success() {
            var rootMock = new Mock<DisplayObjectContainer<DisplayObject>>() { CallBase = true };
            var root = rootMock.Object;
            var rectMock = new Mock<DisplayObject> { CallBase = true };
            var rect = rectMock.Object;
            rect.X = 50;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;
            var rect2Mock = new Mock<DisplayObject> { CallBase = true };
            var rect2 = rect2Mock.Object;
            rect2.Width = 100;
            rect2.Height = 100;
            root.AddMultiple(new List<DisplayObject> { rect, rect2 });

            Assert.IsTrue(root.HitTestPoint(49, 49));
            Assert.IsFalse(root.HitTestPoint(151, 100));
            Assert.IsTrue(root.HitTestPoint(49, 100));
            Assert.IsFalse(root.HitTestPoint(100, 151));

            root.ScaleX = 0.5;
            root.ScaleY = 1.5;
            root.Remove(rect2);

            // rect.X should now translate to 25 and rect.Y to 75
            Assert.IsFalse(root.HitTestPoint(24, 75));
            Assert.IsTrue(root.HitTestPoint(25, 75));

            Assert.IsFalse(root.HitTestPoint(25, 74));
            Assert.IsTrue(root.HitTestPoint(25, 75));

            // right boundary should be 25 + 100 * 0.5 = 75
            Assert.IsFalse(root.HitTestPoint(76, 100));
            Assert.IsTrue(root.HitTestPoint(75, 100));
        }

        [TestMethod]
        public void DisplayObjectContainer_HitTestRectangle_Success() {
            var rootMock = new Mock<DisplayObjectContainer<DisplayObject>>() { CallBase = true };
            var root = rootMock.Object;
            var rectMock = new Mock<DisplayObject> { CallBase = true };
            var rect = rectMock.Object;
            rect.X = 50;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;
            var rect2Mock = new Mock<DisplayObject> { CallBase = true };
            var rect2 = rect2Mock.Object;
            rect2.Width = 100;
            rect2.Height = 100;
            root.AddMultiple(new List<DisplayObject> { rect, rect2 });

            Assert.IsTrue(root.HitTestRect(new Rectangle(0, 0, 49, 49)));
            Assert.IsTrue(root.HitTestRect(new Rectangle(0, 0, 149, 149)));
            Assert.IsFalse(root.HitTestRect(new Rectangle(150, 0, 49, 49)));
            Assert.IsFalse(root.HitTestRect(new Rectangle(0, 151, 49, 49)));

            root.ScaleX = 0.5;
            root.ScaleY = 1.5;

            Assert.IsTrue(root.HitTestRect(new Rectangle(0, 0, 49, 49)));
            Assert.IsTrue(root.HitTestRect(new Rectangle(0, 0, 149, 149)));
            Assert.IsFalse(root.HitTestRect(new Rectangle(150, 0, 49, 49)));
            Assert.IsTrue(root.HitTestRect(new Rectangle(0, 151, 49, 49)));
        }


        [TestMethod]
        public void DisplayObjectContainer_HitTestPointFirst_Success() {
            var rootMock = new Mock<DisplayObjectContainer<DisplayObject>>() { CallBase = true };
            var root = rootMock.Object;
            var rectMock = new Mock<DisplayObject> { CallBase = true };
            var rect = rectMock.Object;
            rect.X = 50;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;
            var rect2Mock = new Mock<DisplayObject> { CallBase = true };
            var rect2 = rect2Mock.Object;
            rect2.Width = 100;
            rect2.Height = 100;
            root.AddMultiple(new List<DisplayObject> { rect2, rect });

            Assert.IsTrue(root.HitTestPointFirst(49, 49) == rect2);
            Assert.IsNull(root.HitTestPointFirst(151, 100));
            Assert.IsTrue(root.HitTestPointFirst(49, 100) == rect2);
            Assert.IsNull(root.HitTestPointFirst(100, 151));
            Assert.IsTrue(root.HitTestPointFirst(50, 100) == rect);

            root.ScaleX = 0.5;
            root.ScaleY = 1.5;
            root.Remove(rect2);

            // rect.X should now translate to 25 and rect.Y to 75
            Assert.IsNull(root.HitTestPointFirst(24, 75));
            Assert.IsTrue(root.HitTestPointFirst(25, 75) == rect);

            Assert.IsNull(root.HitTestPointFirst(25, 74));
            Assert.IsTrue(root.HitTestPointFirst(25, 75) == rect);

            // right boundary should be 25 + 100 * 0.5 = 75
            Assert.IsNull(root.HitTestPointFirst(76, 100));
            Assert.IsTrue(root.HitTestPointFirst(75, 100) == rect);
        }

        [TestMethod]
        public void DisplayObjectContainer_HitTestRectangleFirst_Success() {
            var rootMock = new Mock<DisplayObjectContainer<DisplayObject>>() { CallBase = true };
            var root = rootMock.Object;
            var rectMock = new Mock<DisplayObject> { CallBase = true };
            var rect = rectMock.Object;
            rect.X = 50;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;
            var rect2Mock = new Mock<DisplayObject> { CallBase = true };
            var rect2 = rect2Mock.Object;
            rect2.Width = 100;
            rect2.Height = 100;
            root.AddMultiple(new List<DisplayObject> { rect2, rect });

            Assert.IsTrue(root.HitTestRectFirst(new Rectangle(0, 0, 49, 49)) == rect2);
            Assert.IsTrue(root.HitTestRectFirst(new Rectangle(0, 0, 149, 149)) == rect);
            Assert.IsNull(root.HitTestRectFirst(new Rectangle(150, 0, 49, 49)));
            Assert.IsNull(root.HitTestRectFirst(new Rectangle(0, 151, 49, 49)));

            root.ScaleX = 0.5;
            root.ScaleY = 1.5;
            
            Assert.IsTrue(root.HitTestRectFirst(new Rectangle(0, 0, 49, 49)) == rect2);
            Assert.IsTrue(root.HitTestRectFirst(new Rectangle(0, 0, 149, 149)) == rect);
            Assert.IsNull(root.HitTestRectFirst(new Rectangle(150, 0, 49, 49)));
            Assert.IsTrue(root.HitTestRectFirst(new Rectangle(0, 151, 49, 49)) == rect);
        }
        [TestMethod]
        public void DisplayObjectContainer_HitTestPointAll_Success() {
            var rootMock = new Mock<DisplayObjectContainer<DisplayObject>>() { CallBase = true };
            var root = rootMock.Object;
            var rectMock = new Mock<DisplayObject> { CallBase = true };
            var rect = rectMock.Object;
            rect.X = 50;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;
            var rect2Mock = new Mock<DisplayObject> { CallBase = true };
            var rect2 = rect2Mock.Object;
            rect2.Width = 100;
            rect2.Height = 100;
            root.AddMultiple(new List<DisplayObject> { rect2, rect });

            var res = root.HitTestPointAll(49, 49);
            Assert.IsTrue(res.Contains(rect2));
            Assert.IsFalse(res.Contains(rect));

            res = root.HitTestPointAll(50, 50);
            Assert.IsTrue(res.Contains(rect2));
            Assert.IsTrue(res.Contains(rect));

            root.ScaleX = 0.5;
            root.ScaleY = 2;

            res = root.HitTestPointAll(24, 99);
            Assert.IsTrue(res.Contains(rect2));
            Assert.IsFalse(res.Contains(rect));

            res = root.HitTestPointAll(25, 100);
            Assert.IsTrue(res.Contains(rect2));
            Assert.IsTrue(res.Contains(rect));
        }

        [TestMethod]
        public void DisplayObjectContainer_HitTestRectangleAll_Success() {
            var rootMock = new Mock<DisplayObjectContainer<DisplayObject>>() { CallBase = true };
            var root = rootMock.Object;
            var rectMock = new Mock<DisplayObject> { CallBase = true };
            var rect = rectMock.Object;
            rect.X = 50;
            rect.Y = 50;
            rect.Width = 100;
            rect.Height = 100;
            var rect2Mock = new Mock<DisplayObject> { CallBase = true };
            var rect2 = rect2Mock.Object;
            rect2.Width = 100;
            rect2.Height = 100;
            root.AddMultiple(new List<DisplayObject> { rect2, rect });


            var res = root.HitTestRectAll(new Rectangle(0,0,49,49));
            Assert.IsTrue(res.Contains(rect2));
            Assert.IsFalse(res.Contains(rect));

            res = root.HitTestRectAll(new Rectangle(50, 50, 1, 1));
            Assert.IsTrue(res.Contains(rect2));
            Assert.IsTrue(res.Contains(rect));

            root.ScaleX = 0.5;
            root.ScaleY = 2;

            res = root.HitTestRectAll(new Rectangle(0, 0, 24, 99));
            Assert.IsTrue(res.Contains(rect2));
            Assert.IsFalse(res.Contains(rect));

            res = root.HitTestRectAll(new Rectangle(49, 99, 1, 1));
            Assert.IsTrue(res.Contains(rect2));
            Assert.IsTrue(res.Contains(rect));
        }
    }
}
