﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XmlParser.Xml;

namespace XmlParserTest.Xml {
    [TestClass]
    public class TagTests {

        [TestMethod]
        public void TagTest_OneChild_Success() {
            // arrange
            var root = new XmlTag {Name = "root"};
            root.Add(new XmlTag {Name = "child"});

            var rootConstructed = new XmlTag(new List<IXmlTag> {new XmlTag {Name = "child"}}) {Name = "root"};

            var childrenAsList = new List<IXmlTag>(root.Children);
            var constructedChildrenAsList = new List<IXmlTag>(rootConstructed.Children);

            // assert 
            // children count
            Assert.AreEqual(1, childrenAsList.Count, "Manually setting child did not change children count accordingly");
            Assert.AreEqual(1, constructedChildrenAsList.Count, "Setting children via constructor did not set children count accordingly");

            // names
            Assert.AreEqual("root", root.Name);
            Assert.AreEqual("root", rootConstructed.Name);
            Assert.AreEqual("child", childrenAsList.First().Name);
            Assert.AreEqual("child", constructedChildrenAsList.First().Name);
        }

        [TestMethod]
        public void TagTest_AddAndRemoveChild_Success() {
            // arrange
            var root = new XmlTag { Name = "root" };
            var child = new XmlTag {Name = "child"};
            root.Add(child);

            // assert 
            // children count
            Assert.AreEqual(1, root.Children.Count, "Manually setting child did not change children count accordingly");
            root.Remove(child);

            Assert.AreEqual(0, root.Children.Count);
            Assert.IsNull(child.Parent, "Child was not removed properly");

        }

        [TestMethod]
        public void TagTest_AddAndRemoveAttribute_Success() {
            // arrange
            var tag = new XmlTag();
            // act & assert
            tag.AddAttribute("name", "value");

            Assert.IsTrue(tag.HasAttribute("name"));
            Assert.AreEqual("value", tag.GetAttribute("name").ToString());

            tag.RemoveAttribute("name");
            Assert.IsFalse(tag.HasAttribute("name"));
            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TagTest_AddAttributeWithSameName_ExceptionThrown() {
            // arrange
            var tag = new XmlTag();
            // act
            tag.AddAttribute("name", "value");
            tag.AddAttribute("name", "value");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TagTest_AddNullAsChild_ExceptionThrown() {
            // arrange
            var tag = new XmlTag();
            // act
            tag.Add(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TagTest_AddChildTwice_ExceptionThrown() {
            // arrange
            var root = new XmlTag();
            var child = new XmlTag();
            // act
            root.Add(child);
            root.Add(child);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TagTest_AddChildToTwoParents_ExceptionThrown() {
            // arrange
            var parent1 = new XmlTag();
            var parent2 = new XmlTag();

            var child = new XmlTag();

            // act
            parent1.Add(child);
            parent2.Add(child);
        }
    }
}
