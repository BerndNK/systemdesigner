﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XmlParser.Xml;

namespace XmlParserTest.Xml {
    [TestClass]
    public class ParseTests {
        [TestMethod]
        public void ParseXml_ValidXmlWithOneChild_Success() {
            // arrange
            const string xml = "<root rootKey=\"rootValue\"><child childKey=\"childValue\"/></root>";

            // act
            var tags = XmlFileParser.ParseIntoTags(xml);

            // assert
            // check tags is not null
            Assert.IsNotNull(tags, "Expected tags. Got null");
            var asList = new List<IXmlTag>(tags); // convert to list to evade multiple enumerations

            // check root
            Assert.AreEqual(1, asList.Count, "Tag list does not match count of expected tags");
            var root = asList.First();
            Assert.AreEqual("root", root.Name, "Root child has invalid name");
            // check attributes
            Assert.IsTrue(root.HasAttribute("rootKey"));
            Assert.AreEqual("rootValue", root.GetAttribute("rootKey"));

            // check child
            Assert.AreEqual(1, root.Count, "Root has no child");
            var child = root.First();
            Assert.AreEqual("child", child.Name);
            // check child attributes
            Assert.IsTrue(child.HasAttribute("childKey"));
            Assert.AreEqual("childValue", child.GetAttribute("childKey"));
        }

        [TestMethod]
        public void ParseXml_ValidXmlWithTextAsChild_Success() {
            // arrange
            const string xml = "<tag>text</tag>";

            // act
            var tags = XmlFileParser.ParseIntoTags(xml);
            var asList = new List<IXmlTag>(tags);

            // assert
            Assert.IsNotNull(tags);
            Assert.IsTrue(asList.First().HasTextContent);
            Assert.AreEqual("text", asList.First().TextContent);
        }

        [TestMethod]
        public void ParseXml_ValidXmlWithDeepNestedChilds_Success() {
            // arrange
            const string xml = "<tag1><tag2><tag3><tag4></tag4></tag3></tag2></tag1>";

            // act
            var tags = XmlFileParser.ParseIntoTags(xml);

            // assert
            // check tags is not null
            Assert.IsNotNull(tags, "Expected tags. Got null");
            var asList = new List<IXmlTag>(tags); // convert to list to evade multiple enumerations
            Assert.AreEqual(1, asList.Count, "Tag list does not match count of expected tags");
            Assert.IsNotNull(asList.FirstOrDefault(), "Tag list is empty");

            // check depth
            var crnt = asList.First();
            var depth = 0;
            while (!crnt.HasTextContent) {
                ++depth;
                Assert.AreEqual("tag"+depth, crnt.Name, "Tag at depth: "+depth + " did not have expected name: \"tag"+depth+"\" but instead: \""+crnt.Name+"\"");
                crnt = crnt.First();
            }

            Assert.AreEqual(3, depth, "Expected nested tags with depth 4. Got: "+depth);
        }

        [TestMethod]
        public void ParseXml_ValidXmlWithFourRootChilds_Success() {
            // arrange
            const string xml = "<root1/><root2/><root3/><root4/>";
            // act
            var tags = XmlFileParser.ParseIntoTags(xml);

            // assert
            // check tags is not null
            Assert.IsNotNull(tags, "Expected tags. Got null");
            var asList = new List<IXmlTag>(tags); // convert to list to evade multiple enumerations
            Assert.AreEqual(4, asList.Count, "Tag list does not match count of expected tags");

            var i = 1;
            foreach (var tag in asList) {
                Assert.AreEqual("root"+i, tag.Name, "Tag \""+tag.Name+"\" did not have expected name: \"root"+i+"\". Order may have been mixed up or parsing failed.");
                ++i;
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ParseXml_InvalidXml_ExceptionThrown() {
            // arrange
            const string xml = "<root><root>";

            // act
            XmlFileParser.ParseIntoTags(xml);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException), "Negative start index was inappropriately allowed.")]
        public void ParseXml_InvalidParameterIndex_ExceptionThrown() {
            XmlFileParser.ParseIntoTags("<root></root>", -1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException), "Null for xml file was inappropriately allowed.")]
        public void ParseXml_InvalidParameterXml_ExceptionThrown() {
            XmlFileParser.ParseIntoTags(null);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ParseXml_InvalidXmlChildWithNoName_ExceptionThrown() {
            // arrange
            const string xml = "<root><key=\"value\"></root>";

            // act
            XmlFileParser.ParseIntoTags(xml);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ParseXml_InvalidXmlNoClosingTag_ExceptionThrown() {
            // arrange
            const string xml = "<root><child1/>";

            // act
            XmlFileParser.ParseIntoTags(xml);
        }
    }
}
