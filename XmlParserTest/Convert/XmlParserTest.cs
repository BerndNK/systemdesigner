﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using XmlParser.Convert;
using XmlParser.Xml;

namespace XmlParserTest.Convert {
    [TestClass]
    public class XmlParserTest {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Produce_NullArgument_ExceptionThrown() {
            // arrange
            var mock = new Mock<XmlConverter<IXmlTag, IXmlTag>>();

            // act
            mock.Object.Convert(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetUnknownTags_NullArgument_ExceptionThrown() {
            // arrange
            var mock = new Mock<XmlConverter<IXmlTag, IXmlTag>>();

            // act
            mock.Object.GetUnknownTags(null);
        }

        [TestMethod]
        public void GetUnknownTags_ThreeModules_Success() {
            // arrange
            var converterMock = new Mock<XmlConverter<IXmlTag, IXmlTag>>();
            var appleConverterMock = new Mock<IXmlConverterModule<IXmlTag, IXmlTag>>();
            appleConverterMock.Setup(x => x.ConvertsFrom).Returns("apple");

            var bananaConverterMock = new Mock<IXmlConverterModule<IXmlTag, IXmlTag>>();
            bananaConverterMock.Setup(x => x.ConvertsFrom).Returns("banana");

            var pearConverterMock = new Mock<IXmlConverterModule<IXmlTag, IXmlTag>>();
            pearConverterMock.Setup(x => x.ConvertsFrom).Returns("pear");

            var fruitsConverterMock = new Mock<IXmlConverterModule<IXmlTag, IXmlTag>>();
            fruitsConverterMock.Setup(x => x.ConvertsFrom).Returns("fruits");

            converterMock.Object.ConverterModules.Add(appleConverterMock.Object);
            converterMock.Object.ConverterModules.Add(bananaConverterMock.Object);
            converterMock.Object.ConverterModules.Add(pearConverterMock.Object);
            converterMock.Object.ConverterModules.Add(fruitsConverterMock.Object);

            const string xml = "<fruits><apple/><pear/><banana/><strawberry/></fruits>";
            var xmlElements = XmlFileParser.ParseIntoTags(xml);
            var root = xmlElements.First();

            // act
            var unknownTags = converterMock.Object.GetUnknownTags(root);
            Assert.AreEqual("strawberry", unknownTags.First().ToLower());
        }
    }
}
